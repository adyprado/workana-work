<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	
	include('server/database_slave.php');
    $database = new database_slave();
	
	$hash_code = substr($_GET['q'],0,-1);
	$language_id = substr($_GET['q'], -1);
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];    	
    	
    $info = $database->get_questionnaire_info($hash_code);
    
    $unit = " secs";
    $ftime = $info["Fill_Time"];
    if($ftime>60){
    	$ftime = round($ftime/60,1);
    	$unit = "mins";
    }
    if($ftime==0){$ftime="N/A";$unit ="";}
    
    if(!$info){
    	echo "Invalid questionnaire.";
    }
    else{
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? echo $info["Surname"]." ".$info["Name"]?></title>
	
	<!--<link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="print">-->
    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">

	
	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
  	<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
  	<script type="text/javascript" src="graphs.js"></script>
  	<script type="text/javascript" src="html2canvas.js"></script>
  	<script src="canvas2image.js"></script>
	<script src="base64.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	
    <script type="text/javascript">
      //google.load("visualization", "1", {packages:["corechart"]});
      //google.setOnLoadCallback(drawChart);
      
      var chart_list=new Array;
      
      function drawChart() {
        
        var data_url =  "server/questionnaire_data.php?q="+<? echo "'".$hash_code."'"; ?>+"" 
        //alert(data_url)
        $.ajax({
    		type: 'GET',
    		url: data_url,
    		dataType: 'json',
    		success:CallbackQuestionnaireData , 
    		error: CallbackQuestionnaireDataFailed,
    		cache: false
		});        
       
      	drawCircleScatterChart("scatter_1","High Psychopathology","Low Psychopathology","High Addiction","Low Addiction")
        drawCircleScatterChart("scatter_2","High Psychopathology","Low Psychopathology","High Risk","Low Risk")
        drawCircleScatterChart("scatter_3","High Empathy","Low Empathy","High Assertiveness","Low Assertiveness")
      	 
      }
      function CallbackQuestionnaireData(data, textStatus, jqXHR){
      	
      	var scale_asrt;
      	var scale_empt;
      	var scale_ansc;
      	var scale_mean_clinical;
      	var scale_addiction;
      	
      	for (item in data) {       		
      		if(item!="Means"){
      			var barChart1 = new dhtmlXChart({
    							view: "line",
   								container: document.getElementById(item),//"chartDiv",
    							value: "#A_Score#",
    							label: "#A_Score#",
    							//color: "#color#",
    							width: 10,
    							gradient: "rising",
    							radius: 0,
   								tooltip: {
        							template: "#Scale_Code#<br>#A_Score#"
    							},
   								xAxis: {
        							title: "",
        							template: "#Scale_Code#"         							
   								 },
    							yAxis: {        							 
        							title: "A-Score"         							 
    							},   								
    							origin: 0
		 				});
				barChart1.parse(data[item], "json");
			
				for(var i=0;i<data[item].length;i++){
					//alert(data[item][i].Scale_Code);
					if(data[item][i].Scale_Code=="ASRT"){scale_asrt=data[item][i].A_Score}
					if(data[item][i].Scale_Code=="EMPT"){scale_empt=data[item][i].A_Score}
					if(data[item][i].Scale_Code=="ANSC"){scale_ansc=data[item][i].A_Score}
				}
			   
			   
			   chart_list.push(barChart1);
			   
			}
			else{
				for(var i=0;i<data[item].length;i++){
					if(data[item][i].Code=="MCS"){scale_mean_clinical=data[item][i].Score}
					if(data[item][i].Code=="ASME"){scale_addiction=data[item][i].Score}
				}
			}
      	}

      	drawPointOnScatterChart("scatter_1",scale_mean_clinical,scale_addiction,"./images/red-icon-8.png","Point",0)
      	drawPointOnScatterChart("scatter_2",scale_mean_clinical,scale_ansc,"./images/red-icon-8.png","Point",0)
      	drawPointOnScatterChart("scatter_3",scale_empt,scale_asrt,"./images/red-icon-8.png","Point",0)
      }
      function CallbackQuestionnaireDataFailed(jqXHR, textStatus, errorThrown){
      	alert("error loading graph data")
      }
      
      function printDiv(divId){
      	//var chart = chart_list[0];
      	//var canvas = document.getElementById("1")//chart.getCanvas();//canvas;
      	//var img = canvas.toDataURL("image/png");
      		html2canvas(document.getElementById(divId), {
      			onrendered: function(canvas) {
      				Canvas2Image.saveAsPNG(canvas);
      				//canvas.toDataURL("image/png");
      				//document.body.appendChild(canvas);
      				 //link.href = canvas.toDataURL();
    				 //link.download = "test.png";
      			}
      		});
      }
      
    </script>
  
  </head>
  <body onLoad="drawChart()">
    <?php echo "<img src='./images/LOGO_FILISTOS_".$language_code.".png' height='66' width='150' class=\"logo_image\" style=\"margin-left:50px;\">";?>  
    <div class="container">    
    <div class="header">    	 
        <h3 class="text-muted"><? echo $info["Surname"]." ".$info["Name"]?></h3>
        
        <small class="print_only">
        <table>
    	 	<tr><td>Birthdate</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Birthdate"]?></td></tr>
    	 	<tr><td>Gender</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Gender"]?></td></tr>
    	 	<tr><td>Marital Status</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Marital_Status"]?></td></tr> 
    	 	<tr><td>Profession</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Profession"]?></td></tr> 
    	 	<tr><td>Fill out date</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Last_Save"]?></td></tr>
    	 	<tr><td>Fill time</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$ftime." ".$unit?></td></tr> 
  		</table>
  		</small>
        <br>
    </div>
    
    <!--<div class="jumbotron" id="bar_chart_div">aa</div>-->
    
    <ul class="nav nav-tabs">
  		<li class="active"><a href="#report" data-toggle="tab"><b>Report</b><? echo "&nbsp&nbsp(fill time: ".$ftime." ".$unit.")"; ?></a></li>
  		<!--<li><a href="#circle_graphs" data-toggle="tab">Scatter Charts</a></li>
  		<li><a href="#critical_items" data-toggle="tab">Critical Items</a></li>-->
  		<li><a href="#risk_assessment" data-toggle="tab"><b>Risk Assessment</b></a></li>
  		<li><a href="#analytics" data-toggle="tab"><b>Results</b></a></li>
  		<li><a href="#diagnostic_criteria" data-toggle="tab"><b>Diagnostic Criteria</b></a></li>
   		<li><a href="#questionnaire" data-toggle="tab"><b>Questionnaire</b></a></li>
	</ul>
    
    <div class="tab-content">
    	<div class="tab-pane active" id="report">
    		<div class="row marketing">
    	<? 
    		$report_rows = $database->get_report("IPA",$hash_code);
    		
    		$title_col = "Title_".$language_code;
    		$descr_col = "Descr_".$language_code;
    		$heading_col = "Heading_".$language_code;
    		$info_title_col = "Info_Title_".$language_code;
    		$info_descr_col = "Info_Descr_".$language_code;
    		
    		while($row = $report_rows->fetch_array(MYSQLI_ASSOC)) {
    			
    			if($row["Rule_Type"]=="H1"){
    				//if($row[$title_col]!=""){echo "<h4><img src='images/PBI.png' width='64' height='64'/>".$row[$title_col]."</h4><br>";}
    				if($row[$title_col]!=""){
    					
    					if($row["Image_File"]!=""){
    						echo "<table><tr><td style=\"padding:0px 10px 0px 10px;\"><img src='".$row["Image_File"]."' width='".$row["Image_Width"]."' height='".$row["Image_Height"]."'/></td><td><h4>".$row[$title_col]."</h4></td></tr></table><br>";
    					}
    					else{
    						echo "<h4>".$row[$title_col]."</h4><br>";	
    					}
    					
    					//echo "<div><img src='images/PBI.png' width='64' height='64'/></div><div><h4>".$row[$title_col]."</h4></div><br>";
    					//echo "<table><tr><td><img src='images/PBI.png' width='64' height='64'/></td><td><h4>".$row[$title_col]."</h4></td></tr></table><br>";
    				}
    			}
    			if($row["Rule_Type"]=="TXT"){
    				if($row[$descr_col]!=""){echo "<div align=\"justify\">".$row[$descr_col]."</div><br><br>";}
    			
    			}
    			if($row["Rule_Type"]=="I"){
    				 
    			}
    			if($row["Rule_Type"]=="E"){
    				
    				$rule_expr = $row["Expression"];
    				
    				$rule_expr = str_replace("sc1",$row["Sc_1_Score"],$rule_expr);
					$rule_expr = str_replace("sc2",$row["Sc_2_Score"],$rule_expr);
					$rule_expr = str_replace("sc3",$row["Sc_3_Score"],$rule_expr);
    				$rule_expr = str_replace("min_val",$row["Min_Value"],$rule_expr);
    				$rule_expr = str_replace("max_val",$row["Max_Value"],$rule_expr);
    				
    				$condition = eval("if($rule_expr){return 1;}else {return 0;}"); 
    				
    				if($condition==1){
    					echo "<div align=\"justify\">";
    						if($row[$info_descr_col]!=""){echo $row[$info_descr_col]."<br>";}
    						if($row[$descr_col]!=""){echo $row[$descr_col]."<br><br>";}
    					echo "</div>";
    				}
    				
    				/*$band = "";
    				$scale_code_expr ="";
    				
    				if($row["Scale_Code_2"]!=""){$scale_code_expr="[".$row["Scale_Code_1"].$row["Operator_1"].$row["Scale_Code_2"]."]";}
    				else {$scale_code_expr = $row["Scale_Code_1"];}
    				
    				if($row["Min_Value"]==-999){$band="&le; ".$row["Max_Value"]." : ";}
    				else if($row["Max_Value"]==999){$band="&ge; ".$row["Min_Value"]." : ";}
    				else{$band="[".$row["Min_Value"].",".$row["Max_Value"]."] : ";}
    				
    				echo "<b>".$scale_code_expr." ".$band."</b>".$row[$descr_col]."<br><br>";*/
    			}
    			
    			if($row["Rule_Type"]=="GDIV"){
    				$scores = $database->get_group_rep_scores($hash_code,$row["Data"]);
    				if($scores){
    				$print_style = "no-break";
    				
    				echo "<br><div class=\"panel panel-default ".$print_style." \">
    						<div class=\"panel-heading no_print\">".$row[$heading_col]."<button class='no_print' style='float: right;' onClick=\"printDiv('".$row["Data"]."')\">Download</button></div>
    						<div class=\"panel-body\" id=\"".$row["Data"]."\">
    							 
    						</div>
    					  </div></br>";
    				
    				}
    			}
				if($row["Rule_Type"]=="CDIV"){					
					
					$print_style = "no-break";
					
					echo"<div class=\"panel panel-default ".$print_style." \">
						<div class=\"panel-heading\"></div>
						<div class=\"panel-body\">
							<canvas id=\"".$row["Data"]."\" width=\"600\" height=\"400\" style=\"background-color:#ffffff\"></canvas>
						</div>
					</div>";
					
				}   
				
				if($row["Rule_Type"]=="CRIT_ITEM"){	
					
					$records = $database->get_critical_questions_records($hash_code,$language_code,$info["Questionnaire_Type_ID"]);
					$grp_id = -1;
					$cat_id = -1;
					//error_log($records->num_rows);
					while($rec = $records->fetch_array(MYSQLI_ASSOC)) {						
						
						if($grp_id != $rec["Group_ID"]){
							
							if($grp_id != -1){echo "</tbody></table></div>";}
													
							echo "<div class=\"panel panel-default\">
								     <div class=\"panel-heading\">".$rec["Group_Descr"]."</div>
								     <table class=\"table table-condensed\">
								     	<!--<thead>
                							<tr>
                  								<th width='20'>Question Code</th>
                  								<th width='180'>Question</th>
                  								<th width='100'>Points</th>
                							</tr>
              							</thead>-->
              							<tbody>";						
							
							$grp_id = $rec["Group_ID"];
						}
						if($cat_id != $rec["Category_ID"]){
							
							echo "<tr><td colspan='3'><b>".$rec["Category_Descr"]."</b></td></tr>";
							
							$cat_id = $rec["Category_ID"];	
						}
						
						echo "
								<tr>
									<td>".$rec["Code"]."</td>
									<td>".$rec["Question"]."</td>
									<td>".$rec["Points"]."</td>
								</tr>
							";
						
					}
					echo "</tbody></table></div>";
					
				}
				
    		}
    	    		
    	?>		
    		</div>
		</div>		
		
		 
		
		<div class="tab-pane" id="risk_assessment">
			<div class="row marketing">				
				<?
					echo "<div class=\"panel panel-default\">
						     <div class=\"panel-heading\"></div>
							     <table class=\"table table-condensed\">
							     	<thead>
							     		<tr>";
					
					$columns = $database->get_report_columns("RISK_ASSESSMENT",$language_code);
					while($column = $columns->fetch_array(MYSQLI_ASSOC)) {	
											echo "<th width='".$column["Width"]."'>".$column["Column_Name"]."</th>";
								
					
					}
					echo "				</tr>
						  			</thead>
						  			<tbody>";
					
					$results = $database->get_rep_risk_assessment($hash_code,$language_code); 
					
					while($row = $results->fetch_array(MYSQLI_ASSOC)) {
						
							echo	"
									<tr>
									<td>".$row["Label"]."</td>
									 			 
									 
									<td>".$row["Raw_Score"]."</td>
									 
									<td>".$row["Percentage"]."</td>
									</tr>"; 				
					}					
					
					echo "			</tbody></table></div>";
					
				?>
			</div>
		</div>
		
		
		<div class="tab-pane" id="analytics">
			<div class="row marketing page-break-bef">
    	<? 
    		$groups = $database->get_recordset("Scale_Groups_Rep");
    		while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
    			$scores = $database->get_group_rep_scores($hash_code,$g["ID"]);    			
    			if($scores){
    				echo "<div class=\"panel panel-default no-break\">
    						<div class=\"panel-heading\">".$g["Descr_en"]."</div>
    						<table class=\"table table-condensed\">
    							<thead>
                					<tr>
                  						<th width='100'>Scale Code</th>
                  						<th width='100'>A-Score</th>
                  						<th width='100'>Raw-Score</th>
                					</tr>
              					</thead>
              					<tbody>
    						";
    				while($sc = $scores->fetch_array(MYSQLI_ASSOC)) { 		
    					echo "<tr>
    							<td>".$sc["Scale_Code"]."</td>
    							<td>".$sc["A_Score"]."</td>
    							<td>".$sc["Raw_Score"]."</td>
    						  </tr>";	
    				}
    				echo"</tbody></table></div>";    				
    			}
    			
    		}
    	?>
		
    	</div>
		</div>
		
		<div class="tab-pane" id="diagnostic_criteria">
			<div class="row marketing no_print">
				<?
					$records = $database->get_rep_diagnostic_criteria($hash_code);
					$in_header = -1;
					while($rec = $records->fetch_array(MYSQLI_ASSOC)) {
						
						if($rec["Row_Type"]=="header"){
							if($in_header!=-1){
								echo "</tbody></table></div>";
							}
							echo "<div class=\"panel panel-default\">
								     <div class=\"panel-heading\">".$rec["Header_en"]."</div>
								     <table class=\"table table-condensed\">
								     	<thead>
                							<tr>
                  								<th width='160'></th>
                  								<th width='100'>Information</th>
                  								<th width='20'>Result</th>
                  								<th width='20'>Points</th>
                							</tr>
              							</thead>
              							<tbody>";
              				$in_header = 1;
						}
						else if($rec["Row_Type"]=="scale_code_thrd" || $rec["Row_Type"]=="group_mean_thrd"){
							$math = $rec["Expr"];
							$math_score = $rec["Score"];
							$descr = $rec["Expr_Descr"];
							$label = $rec["Label_en"];
							$pts =  eval("return ($math);");
							$score = eval("return ($math_score);");
							echo "
								<tr>
									<td>".$label."</td>
									<td>".$descr."</td>
									<td>".$score."</td>
									<td>".$pts."</td>
								</tr>
							";
						}
						else if($rec["Row_Type"]=="scale_code_score"){
							$label = $rec["Label_en"];
							$descr = $rec["Expr_Descr"];
							$score = $rec["Score"];
							echo "
								<tr>
									<td>".$label."</td>
									<td>".$descr."</td>
									<td>".$score."</td>
									<td></td>
								</tr>
							";							
						}
						else if($rec["Row_Type"]=="group_end"){
							echo "<tr style='height:1px; min-line-height: 1px;'><td colspan='4' bgcolor='#dfdfdf' style='height:1px'></td></tr>";
						}
						
						/*if($rec["Row_Type"]=="scale_code_thrd"){
							$math = $rec["Expr"];
							$pts =  eval("return ($math);");
						}
						else{
							$pts = "0";
						}
						error_log($rec["Expr"]);
						error_log($rec["Expr_Descr"]." ".$pts);
						*/
					}
					echo "</tbody></table></div>";
				?>
			</div>
		</div>
				
		<div class="tab-pane" id="questionnaire">
			<div class="row marketing no_print">
				<? 
					$questionnaire = $database->get_questionnaire_answers($hash_code,$language_code,"Hash_Code");
					echo "<div class=\"panel panel-default\">
    						<div class=\"panel-heading\"></div>
    						<table class=\"table table-condensed\">
    							<thead>
                					<tr>
                  						<th width='50'>Code</th>
                  						<th width='150'>Question</th>
                  						<th width='50'>Answer</th>
                					</tr>
              					</thead>
              					<tbody>
    						";
    				while($question = $questionnaire->fetch_array(MYSQLI_ASSOC)) { 		
    					echo "<tr>
    							<td>".$question["Code"]."</td>
    							<td>".$question["Question"]."</td>
    							<td>".$question["Answer"]."</td>
    						  </tr>";	
    				}
    				echo"</tbody></table></div>";
					
				?>
			</div>			
		</div>
		
	</div>
	</div>
    
  </body>
</html>
<?php }?>