<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	
	
	include('server/database_slave.php');
    $database = new database_slave();
    
    $params = explode(',', $_GET['q']);
	
	$language_id = $params[sizeof($params)-1];
	$api_key = $params[sizeof($params)-2];
	$uid = $params[sizeof($params)-3];
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">

	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
	<script src="base64.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	 
  	<script type="text/javascript">
  		function drawCanvas(total_items,max_value){
  			
  			//alert(total_items+' '+max_value)
  			
  			for(var i=1;i<total_items;i++){
  				//var i = 1;
  				var canvas_id = "canvas"+i;
  				var cell_id = "td"+i;
  			  			
  				var c = document.getElementById(canvas_id);
				var t = document.getElementById(cell_id);
			
				var total_width = c.width;
				var value = parseInt(t.innerHTML);
				var bar_length = ((value/max_value).toPrecision(2)) * total_width
			//alert(value+' '+max_value+' '+(value/max_value).toPrecision(2))
			//alert(parseInt(t.innerHTML)+1)
			//alert(total_width)
				
				
				
				var ctx = c.getContext("2d");
				
				var grd=ctx.createLinearGradient(0,0,total_width,0);
				grd.addColorStop(0,"#d3d3d3");
				grd.addColorStop(0.3,"orange");
				grd.addColorStop(1,"red");
				//grd.addColorStop(1,"white");
				
				ctx.fillStyle = grd //"#FF0000";
				ctx.fillRect(0,0,bar_length,10);
			}
			
  		}
  	</script>
  </head>
	
  <body>
  	<?php
  	
  	if($database->app_user_valid($uid,$api_key)){
		$qids = ""; 
		for($i=0;$i<sizeof($params)-3;$i++){		
			$qids = $qids.$params[$i].",";	//error_log($params[$i]);		
		}	
		$qids = substr($qids, 0, -1);	
		
		echo "<img src='./images/LOGO_FILISTOS_".$language_code.".png' height='66' width='150' class=\"logo_image\" style=\"margin-left:50px;\">";
	?>
		
	<div class="container">	
		
		<?php 
 
			$prev_qtype = "";
		 	$canvas_id = 1;
		 	$max_val = -1;
		 	
			$sql = "
					select
						qt.Descr_en 	as 	Questionnaire_Type,
						qt.ID 			as	Questionnaire_Type_ID,
						repg.Descr_en 	as  Scale_Group,
						pa.Code 		as	Scale_Code,
						sum(c.A_Score)/sum(1) as A_Score_Mean,
						sum(c.B_Score_1)/sum(1) as B_Score_1_Mean,
						sum(c.B_Score_2)/sum(1) as B_Score_2_Mean,
						sum(c.B_Score_3)/sum(1) as B_Score_3_Mean
					from CALC_A_Scores c
					join Questionnaires q on c.Questionnaire_ID = q.ID					 
					join PARAM_Scales pa on c.Scale_ID = pa.ID
					join PARAM_Questionnaire_Types qt on q.Questionnaire_Type_ID = qt.ID
					join MAP_Scales_Groups_Rep rep on rep.Scale_ID = c.Scale_ID and rep.Questionnaire_Type_ID = q.Questionnaire_Type_ID
            		join Scale_Groups_Rep repg on rep.Group_Rep_ID = repg.ID
					where q.ID in (".$qids.")
					group by 
						qt.Descr_en ,
						qt.ID,
						repg.Descr_en,
						pa.Code
					order by 1,3,4" ;
					 
			 
			$recordset = $database->get_sql_results($sql);
			
			if($recordset->num_rows>0){				
											
				while($record = $recordset->fetch_array(MYSQLI_ASSOC)){
					
					if($record["Questionnaire_Type"]!=$prev_qtype){
						if($prev_qtype!=""){
							echo "</table></div>";	
						}
					
						echo "<div class=\"panel panel-default\">
							<div class=\"panel-heading\">".$record["Questionnaire_Type"]."</div>
							<table class=\"table table-condensed\">
								<thead>
                					<tr>
                						<th class=\"col-md-2\">Scale Code</th>                  						 
                  						<th class=\"col-md-2\">Mean A-Score</th>
                  						<th class=\"col-md-8\">Graph</th>
                					</tr>
              					</thead>";						
					}
					
					echo"<tr>
  								<td class=\"active\"><b>".$record["Scale_Code"]."</b></td>  								 
  								<td id=\"td".$canvas_id."\">".round($record["A_Score_Mean"])."</td>
  								<td class=\"active\"><canvas id=\"canvas".$canvas_id."\" height='10' width='530'></canvas></td>
  							</tr>";
					
					$prev_qtype = $record["Questionnaire_Type"];
				 	$canvas_id = $canvas_id+1;
				 	if($max_val<round($record["A_Score_Mean"])){
				 		$max_val=round($record["A_Score_Mean"]);
				 	}
				 	
				}
				echo "</table></div>";
			}				
			else{			
				
			}
			
		?>		
		 
	</div>
		
		
	<?php			 
	}
	else{
		echo "out";
	}
  	
  	echo "<script type=\"text/javascript\">drawCanvas(".$canvas_id.",".$max_val.");</script>";
  	?>
  
  
  </body>	
	
</html>