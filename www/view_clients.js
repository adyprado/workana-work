function initViewClients(){
					
	gdata.layout_main.cells("b").setText("Clients");	
	
	var clients_toolbar = gdata.layout_main.cells("b").attachToolbar();	
	clients_toolbar.setIconSize(32);
	clients_toolbar.addButton('add', 0, 'Add Client','./images/client-add-icon-32.png','./images/client-add-icon-32.png');
	clients_toolbar.addSeparator('sep_1', 1);	
	clients_toolbar.addButton('delete', 2, 'Delete Client','./images/client-delete-icon-32.png','./images/client-delete-icon-32.png');
	clients_toolbar.addSeparator('sep_2', 3);
	clients_toolbar.addButton('refresh', 4, 'Refresh List','./images/refresh-icon-32.png','./images/refresh-icon-32.png');
	clients_toolbar.addSeparator('sep_3', 5);
	
	var offset = 6
	
	if(gdata.is_admin==1){
		clients_toolbar.addButton('export', 6, 'Export','./images/export-icon-32.png','./images/export-icon-32.png');	
		clients_toolbar.addSeparator('sep_4', 7);
		clients_toolbar.addButton('excel_upload', 8, 'Excel Upload','./images/excel-icon-32.png','./images/excel-icon-32.png');
		clients_toolbar.addSeparator('sep_5', 9);
		offset = offset+4
	}	
	clients_toolbar.addButton('compare', offset, 'Compare Reports','./images/balance-icon-32.png','./images/balance-icon-32.png');
	clients_toolbar.addSeparator('sep_final', offset+1);
	
	
	//clients_toolbar.hideItem('compare');
	//clients_toolbar.hideItem('excel_upload');
	
	
	var owner_col = "true";
	var owner_col_width = "0";
	var opt_col_width = "22"
	
	if(gdata.is_admin==1){
		owner_col = "false";
		owner_col_width = "10"
		opt_col_width = "12"
	}
	
	var clients_grid = gdata.layout_main.cells("b").attachGrid();	
	clients_grid.setHeader(",Surname,Name,Birthdate,Gender,Status,Children,Profession,Questionnaires,Options,Owner,Priviledges");
	clients_grid.attachHeader("&nbsp;,#text_filter,#text_filter,&nbsp;,#cspan,#cspan,#cspan,#text_filter,&nbsp,#cspan,#cspan,#cspan");
	clients_grid.setColumnIds(",Surname,Name,Birthdate,Gender_ID,Marital_Status_ID,Children,Profession,Questionnaires,Opts,Owner_Username,Priviledges");
	clients_grid.setColTypes("ch,ro,ro,ro,co,co,ro,ro,ro,ro,ro,ro,ro");
	clients_grid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str");
	clients_grid.setInitWidthsP("3,11,11,9,8,8,8,10,10,"+opt_col_width+","+owner_col_width,0);
	clients_grid.setColumnsVisibility("false,false,false,false,false,false,false,false,false,false,"+owner_col+",true");
	clients_grid.setSkin("dhx_skyblue");
	clients_grid.setImagePath(gdata.img_path);	
	clients_grid.init();
	
	var gender_combo = clients_grid.getCombo(4);
	var status_combo = clients_grid.getCombo(5);
	
	gender_combo.put(gdata.genders[0].value,gdata.genders[0].text);
	gender_combo.put(gdata.genders[1].value,gdata.genders[1].text);
	//gender_combo.readonly(true);
	
	for(var i=0;i<gdata.marital_statuses.length;i++){
		status_combo.put(gdata.marital_statuses[i].value,gdata.marital_statuses[i].text);	
	}
	
	gdata.clients_grid = clients_grid;
	gdata.clients_toolbar = clients_toolbar;
	
	clients_toolbar.attachEvent('onClick', clientsToolbarClicked);
	
	//clients_grid.attachEvent("onRowDblClicked", function(rId,cInd){alert("in")});
}

function initClientWin(win,data){
	
	win.setText("New Client");
	win.setIcon("../../../images/client-icon-16.png", "../../../images/client-icon-16.png");
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save Client','./images/save-icon-32.png','./images/save-icon-32.png');
	
	var formStructure = [
		{type:"settings",position:"label-top" },
		    
		    {type:"fieldset", label: "Information",name: "app_user_info", inputWidth:160, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"input", name:"Surname", label:"Surname",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Name", label:"Name",inputWidth:120, validate: "NotEmpty"},
		    	{type:"calendar", name:"Birthdate", label:"Birthdate",dateFormat: "%Y-%m-%d", inputWidth:120, validate: "NotEmpty", readonly:true},
		    	{type:"combo", name:"Gender_ID", label:"Gender",inputWidth:120, validate: "NotEmpty"},
		    	{type:"combo", name:"Marital_Status_ID", label:"Marital Status",inputWidth:120, validate: "NotEmpty"},
		    	{type:"combo", name:"Language_ID", label:"Language",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Children", label:"Children",inputWidth:60}
		    	   	     	
		    ]},
		    {type:"newcolumn"},
		    {type:"fieldset", label: "Working Information",name: "app_user_work_info", inputWidth:160, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"input", name:"Profession", label:"Profession",inputWidth:120},
		    	{type:"input", name:"Work_Position", label:"Work Position",inputWidth:120},
		    	{type:"input", name:"Work_Proposed", label:"Suggested Position",inputWidth:120},
		    	{type:"input", name:"Work_Years", label:"Years",inputWidth:120, validate: "ValidNumeric"},
		    	{type:"input", name:"Studies", label:"Studies",inputWidth:120},
		    ]},
		    
		    {type:"input", name:"Comments", label:"Comments",inputWidth:160,rows:3, offsetLeft:"7"},
		    
		    {type: "hidden", name:"ID",value:-1000},		    
		    {type: "hidden", name:"json",value:""}
		    //{type: "button", value: "Save",offsetLeft: 100,offsetTop:"7"}
  
	]
	 	
	//var myPop = new dhtmlXPopup({toolbar: toolbar,id: "add"});
    var form = win.attachForm(formStructure)
    
    var gender_combo = form.getCombo("Gender_ID");
	gender_combo.addOption(gdata.genders);
	gender_combo.readonly(true);
    
    var status_combo = form.getCombo("Marital_Status_ID");
	status_combo.addOption(gdata.marital_statuses);
	status_combo.readonly(true);
	
	var language_combo = form.getCombo("Language_ID");
	language_combo.addOption(gdata.languages);
	language_combo.readonly(true);
    
    
    toolbar.attachEvent('onClick',function() {
            if(form.validate()){
            		var json = formToJSON(form) 
            		saveClient(json)
            		//myPop.hide();            	            	
            }
        });
    
    win.attachEvent("onClose", cleanNewClientWin);
    gdata.new_client_form = form;
    
    if(data!=null){
    	initFormWithData(form,data);
    	win.setText(data.Surname+" "+data.Name);
    }
    
}



function cleanNewClientWin(){
	gdata.new_client_form=null; 
	return true;
}

function clientsToolbarClicked(id){
	
	if(id=="add"){
		//document.body.clientWidth*0.1
		var win = gdata.main_layout.dhxWins.createWindow("win_new_client",0,0,400,420);
		initClientWin(win,null)
		win.centerOnScreen();
		win.setModal(true);
		win.show();
	}
	else if(id=="delete"){
		var cid = gdata.clients_grid.getCheckedRows(0); //gdata.clients_grid.getSelectedRowId();
		
		if(cid==null || cid=="" || typeof cid =="undefined"){
			dhtmlx.alert({title:"No Client Selected",type:"alert",text:"Please select a client from the list"});				
		}
		else {
			
			var res = cid.split(",");
			var colIndex=gdata.clients_grid.getColIndexById("Priviledges");
			var surnameIndex = gdata.clients_grid.getColIndexById("Surname");
			var nameIndex = gdata.clients_grid.getColIndexById("Name");
			
			for (var i = 0; i < res.length; i++) {
				
				var privs =  gdata.clients_grid.cells(res[i],colIndex).getValue();
				if(privs!="rw"){
					var surname = gdata.clients_grid.cells(res[i],surnameIndex).getValue();
					var name = gdata.clients_grid.cells(res[i],nameIndex).getValue();
					
					dhtmlx.alert({title:"Insufficient Priviledges",type:"alert",text:"Insufficient priviledges for :"+surname+" "+name+"<br>Delete calcelled."});
					return;
				}
			}
			dhtmlx.confirm({type:"confirm",text: "Are you sure you want to delete the selected clients and all of their data?",callback: function(result){if(result){deleteClient(cid)}}});		
		}
	}
	else if(id=="refresh"){
		getClientList();
	}
	else if(id=="export" || id =="compare"){
		var cid = gdata.clients_grid.getCheckedRows(0);
		
		if(cid==null || cid=="" || typeof cid =="undefined"){
			dhtmlx.alert({title:"No Clients Selected",type:"alert",text:"Please select clients from the list"});				
		}
		else {
			getQuestionnairesList(cid,id);	
		}
	}
	else if(id=="excel_upload"){
		upload_excel_form();	
	}
	/*else if(id=="compare"){
		var cid = gdata.clients_grid.getCheckedRows(0);
		if(cid==null || cid=="" || typeof cid =="undefined"){
			dhtmlx.alert({title:"No Clients Selected",type:"alert",text:"Please select clients from the list"});				
		}
		else {
			getQuestionnairesList(cid,id);
		}
		
	}*/

}
function upload_excel_form(){
	
	var path = "server/dhtmlxform_excel_upload.php?User_ID="+gdata.uid+"&Api_Key="+gdata.api_key
	
	var formData=[
		{type:"settings",position:"label-top" },		
		{type:"fieldset", label: "Excel File",name: "excel_upload_box", inputWidth:330,inputHeight:180, offsetLeft:"7",offsetTop:"15", list:[
			{type:"upload", name:"excel_upload",label:"",inputWidth:310,inputHeight:180,mode:"html5",swfPath: "dhtmlxSuite_v36_pro_131108_eval/dhtmlxForm/codebase/ext/uploader.swf",swfUrl: path,offsetLeft:"7",offsetTop:"10",url: path}
			, {type:"hidden", name:"ext", value:""}
		]}      
	]
	
	var win = gdata.main_layout.dhxWins.createWindow("win_excel_upload",0,0,400,380);
	win.setIcon("../../../images/excel-icon-16.png", "../../../images/excel-icon-16.png");
	win.setText("Upload excel");
	win.centerOnScreen();
	win.setModal(true);
	
	var tbar = win.attachToolbar();
	tbar.setIconSize(32);
	//tbar.addButton('download_template', 0, 'Download template','./images/template-download-icon-32.png','./images/template-download-icon-32.png');
	tbar.addText('download_lnk',0,'<a href=\"import_template.xlsx\" download>Download Excel Template</a>')
	
	 
	var frm = win.attachForm(formData);
	
	var myUploader = frm.getUploader("excel_upload"); 
	myUploader.setAutoStart(true);
	
	frm.attachEvent("onUploadComplete",function(count){
	
		//win.close();
	});
	frm.attachEvent("onUploadFile",function(realName,serverName){
		var json_obj = jQuery.parseJSON(serverName); 
		
		if(json_obj.Result_Code=="0"){
			dhtmlx.alert({title:"Finish",type:"alert",text:"Clients imported successfully."});
			getClientList();
			win.close();
		}		
		else{
			dhtmlx.alert({title:"Error",type:"alert-error",text:json_obj.Error_Message});
		}

	});
	
	 
}


function getQuestionnairesList(cid,purpose){
	gdata.layout_main.cells('b').progressOn();
	
	$.ajax({
    			type: 'POST',
    			url: questionnaire_list_get,
    			dataType: 'json',
    			data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Client_ID':cid,'Purpose':purpose},
    			success:CallbackGetQuestionnairesList , 
    			error: CallbackGetQuestionnairesListFailed,
    			cache: false
		});
}

function CallbackGetQuestionnairesList(data, textStatus, jqXHR){
	gdata.layout_main.cells('b').progressOff();
	
	if(data.Result_Code==0){
		var win = gdata.main_layout.dhxWins.createWindow("win_questionnaire_list",0,0,500,450);
		initQuestionnaireListWin(win,data.Questionnaires,data.Purpose)
		win.centerOnScreen();
		win.setModal(true);
		win.show();			
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Unable to retrieve questionnaire list."});	
	}
}

function CallbackGetQuestionnairesListFailed(jqXHR, textStatus, errorThrown){
	gdata.layout_main.cells('b').progressOff();
}

function initQuestionnaireListWin(win,data,purpose){
	
	win.setText("Questionnaire List");
	var qlist_toolbar = win.attachToolbar();	
	qlist_toolbar.setIconSize(32);
	
	if(purpose=="export"){
		win.setIcon("../../../images/report-icon-16.png", "../../../images/report-icon-16.png");
		qlist_toolbar.addButton('report', 0, 'Data Report','./images/reports-icon-32.png','./images/reports-icon-32.png');
		qlist_toolbar.addSeparator('sep_1', 1);
		qlist_toolbar.addButton('urls', 2, 'URL List','./images/url-icon-32.png','./images/url-icon-32.png');
		qlist_toolbar.addSeparator('sep_2', 3);
		qlist_toolbar.addButton('check_all', 4, 'Check all','./images/check-all-icon-32.png','./images/check-all-icon-32.png');
	}
	else{
		win.setIcon("../../../images/report-icon-16.png", "../../../images/report-icon-16.png");
		qlist_toolbar.addButton('compare', 0, 'Compare','./images/report-icon-32.png','./images/report-icon-32.png');
		qlist_toolbar.addSeparator('sep_1', 1);
		qlist_toolbar.addButton('check_all', 2, 'Check all','./images/check-all-icon-32.png','./images/check-all-icon-32.png');
	}
		
	var questionnaire_list_grid = win.attachGrid();	
	questionnaire_list_grid.setHeader(",Type,Surname,Name,Comments,");
	questionnaire_list_grid.setColumnIds(",Questionnaire_Type,Surname,Name,Comments,Questionnaire_File");
	 
	questionnaire_list_grid.setColTypes("ch,ro,ro,ro,ro,ro");
	questionnaire_list_grid.setColSorting("str,str,str,str,str,str");
	questionnaire_list_grid.setInitWidthsP("10,20,20,20,30,0");	
	questionnaire_list_grid.setColumnsVisibility("false,false,false,false,false,true");
	questionnaire_list_grid.setSkin("dhx_skyblue");
	questionnaire_list_grid.setImagePath(gdata.img_path);		
	questionnaire_list_grid.init();	
	
	for(var i=0;i<data.length;i++){
		questionnaire_list_grid.addRow(data[i].ID,[0,data[i].Questionnaire_Type,data[i].Surname,data[i].Name,data[i].Comments,data[i].Questionnaire_File]);
	}
	
	//questionnaire_list_grid.checkAll(true);
	
	
	qlist_toolbar.attachEvent('onClick', function(id){
		
		if(id=="check_all"){
				questionnaire_list_grid.checkAll(true);
		}
		else{
			var cid = questionnaire_list_grid.getCheckedRows(0);
			if(cid==null || cid=="" || typeof cid =="undefined"){
				dhtmlx.alert({title:"No Questionnaires Selected",type:"alert",text:"Please select questionnaires from the list"});				
			}
			else {
				if(id=="report"){
					window.open("export_rep.php?q="+cid+","+gdata.uid+","+gdata.api_key+","+gdata.language_id,"_blank")	 	
				}	
				else if(id=="urls"){
					window.open("url_list.php?q="+cid+","+gdata.uid+","+gdata.api_key+","+gdata.language_id+","+gdata.is_admin,"_blank")	
				}
				else if(id=="compare"){
					window.open("compare_rep.php?q="+cid+","+gdata.uid+","+gdata.api_key+","+gdata.language_id+","+gdata.is_admin,"_blank")	
				}			 
			}
		}
	});
	
	
}

function deleteClient(cid){
	gdata.layout_main.cells('b').progressOn();
	
	$.ajax({
    			type: 'POST',
    			url: client_delete,
    			dataType: 'json',
    			data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Client_ID':cid},
    			success:CallbackDeleteClient , 
    			error: CallbackDeleteClientFailed,
    			cache: false
		});
}

function CallbackDeleteClient(data, textStatus, jqXHR){
	gdata.layout_main.cells('b').progressOff();
	
	if(data.Result_Code==0){
		
		var client_ids = data.Client_ID
		var arr = client_ids.split(",");
		
		for(var i=0;i<arr.length;i++)
		{gdata.clients_grid.deleteRow(arr[i]);}
		
		dhtmlx.alert({title:"Success",type:"alert",text:"Client deleted successfully."});
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Client deletion failed."});	
	}
}
function CallbackDeleteClientFailed(jqXHR, textStatus, errorThrown){
	gdata.layout_main.cells('b').progressOff();	
}

function saveClient(json){
	var win = gdata.main_layout.dhxWins.window("win_new_client");
	win.progressOn();
	
	$.ajax({
    			type: 'POST',
    			url: client_set,
    			dataType: 'json',
    			data: json,
    			success:CallbackSaveClient , 
    			error: CallbackSaveClientFailed,
    			cache: false
		});
}

function CallbackSaveClient(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_new_client");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Client saved successfully."});
		addClientToGrid(data)
		win.close();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Client save failed."});
	}
	
}
function CallbackSaveClientFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_new_client");
	win.progressOff();
}

function getClientList(){
	
	gdata.layout_main.cells('b').progressOn();
	 
	$.ajax({
    		type: 'POST',
    		url: client_list,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin"},
    		success:CallbackGetClientList , 
    		error: CallbackGetClientListFailed,
    		cache: false
	});	

}

function CallbackGetClientListFailed(jqXHR, textStatus, errorThrown){
 	gdata.layout_main.cells('b').progressOff();
}

function CallbackGetClientList(data, textStatus, jqXHR){
 	gdata.layout_main.cells('b').progressOff();
 	
 	if(data.Result_Code == 0){
 		
 		gdata.clients_grid.clearAll();
 		
 		if(typeof data.Clients != 'undefined'){
 			
 			for(var i=0;i<data.Clients.length;i++){
 				var client = data.Clients[i]
 				addClientToGrid(client)
 			} 			
 		} 		 		
 	}
 	else{
 		dhtmlx.alert({title:"Login Failed",type:"alert-error",text:"Invalid Credentials."});
 	}
}

function addClientToGrid(client){
	
	var questionnaires = 0;
	
	if(typeof client.Questionnaires !='undefined'){questionnaires=client.Questionnaires}
	
	var info = "<input type='image' src='./images/client-info-icon-16.png' onClick='getClientInfo(\""+client.ID+"\")'>"
	
	var charts="";
	var tests="";
	
	charts= "<input type='image' src='./images/list-icon-16.png' onClick='getClientQuestionnaires(\""+client.ID+"\")'>"
	tests = "<input type='image' src='./images/test-icon-16.png' onClick='getClientTests(\""+client.ID+"\")'>"
	
	/*for(var i=0;i<gdata.registration_types.length;i++){
		if(gdata.registration_types[i].Category=="Questionnaires"){
			charts= "<input type='image' src='./images/list-icon-16.png' onClick='getClientQuestionnaires(\""+client.ID+"\")'>"
		}
		else if(gdata.registration_types[i].Category=="Tests"){
			tests = "<input type='image' src='./images/test-icon-16.png' onClick='getClientTests(\""+client.ID+"\")'>"
		}
	}*/	 
	var exist=gdata.clients_grid.doesRowExist(client.ID);
	
	var client_privs = ""
	
	if(gdata.is_admin==1){
		client_privs = "<input type='image' src='./images/client-access-icon-16.png' onClick='getClientAssignments(\""+client.ID+"\")'>"
		charts= "<input type='image' src='./images/list-icon-16.png' onClick='getClientQuestionnaires(\""+client.ID+"\")'>"
		tests = "<input type='image' src='./images/test-icon-16.png' onClick='getClientTests(\""+client.ID+"\")'>"
	}
	
	var options = info+"&nbsp&nbsp"+charts+"&nbsp&nbsp"+tests+"&nbsp&nbsp"+client_privs
	
	var owner_username = gdata.username;
	var priviledges = "rw";
	if(typeof client.Owner_Username !='undefined'){owner_username=client.Owner_Username}
	if(typeof client.Priviledges !='undefined'){priviledges=client.Priviledges}
	
	if(!exist){
		gdata.clients_grid.addRow(client.ID,[0,client.Surname,client.Name,client.Birthdate,client.Gender_ID,client.Marital_Status_ID,client.Children,client.Profession,questionnaires,options,owner_username,priviledges])
	}
	else{
		var idx = gdata.clients_grid.getRowIndex(client.ID);
		gdata.clients_grid.deleteRow(client.ID);
		gdata.clients_grid.addRow(client.ID,[0,client.Surname,client.Name,client.Birthdate,client.Gender_ID,client.Marital_Status_ID,client.Children,client.Profession,questionnaires,options,owner_username,priviledges],idx)
	}
		
}

function getClientAssignments(cid){
	gdata.layout_main.cells('b').progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: client_assignments_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Client_ID':cid},
    		success:CallbackGetClientAssignments, 
    		error: CallbackGetClientAssignmentsFailed,
    		cache: false
	});	
}

function CallbackGetClientAssignments(data, textStatus, jqXHR){
	gdata.layout_main.cells('b').progressOff();
	 
	if(data.Result_Code==0){
		var win = gdata.main_layout.dhxWins.createWindow("win_client_assign",0,0,500,450);
		initClientAssignmentWin(win,data)
		win.centerOnScreen();
		win.setModal(true);
		win.show();		
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving client assignments."});
	}
}

function CallbackGetClientAssignmentsFailed(jqXHR, textStatus, errorThrown){
	dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving client assignments."}); 
	gdata.layout_main.cells('b').progressOff();	
}

function initClientAssignmentWin(win,data){
	
	var surname = gdata.clients_grid.cells(data.Client_ID,0).getValue();
	var name	= gdata.clients_grid.cells(data.Client_ID,1).getValue();
	win.setIcon("../../../images/client-access-icon-16.png", "../../../images/client-access-icon-16.png");
	win.setText(surname+" "+name+" Assignments");
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save','./images/save-icon-32.png','./images/save-icon-32.png');
	
	var assi_grid = win.attachGrid();	
	assi_grid.setHeader("Surname,Name,Username,Priviledges,Options");
	assi_grid.setColumnIds("Surname,Name,Username,Priviledges,Options");
	assi_grid.setColTypes("ro,ro,ro,ro,ro");
	assi_grid.setColSorting("str,str,str,str,str");
	assi_grid.setInitWidthsP("25,25,15,0,35");	
	assi_grid.setColumnsVisibility("false,false,false,true,false");
	assi_grid.setSkin("dhx_skyblue");
	assi_grid.setImagePath(gdata.img_path);	
	assi_grid.init();
	
	var priv_combo = assi_grid.getCombo(3);
	
	priv_combo.put("rw","read-write");
	priv_combo.put("r","read only");
	priv_combo.put("na","no access");
	
	for(var i = 0;i<data.Assignments.length;i++){
		//addRowToGrid(assi_grid,data.Assignments[i])
		var checked_rw="";
		var checked_r =""
		var checked_na=""
		if(data.Assignments[i].Priviledges=="rw"){checked_rw= "checked"}
		if(data.Assignments[i].Priviledges=="r") {checked_r = "checked"}
		if(data.Assignments[i].Priviledges=="na"){checked_na= "checked"}
		
		var radio = "<input type='radio' onchange=\"clientAssignmentChanged("+data.Assignments[i].ID+",'na')\" name='priv_user_"+data.Assignments[i].ID+"' id='priv_user_"+data.Assignments[i].ID+"_na' value='na' "+checked_na+"> Deny"+
					"<input type='radio' onchange=\"clientAssignmentChanged("+data.Assignments[i].ID+",'r')\" name='priv_user_"+data.Assignments[i].ID+"' id='priv_user_"+data.Assignments[i].ID+"_r' value='r' "+checked_r+"> Read"+
					"<input type='radio' onchange=\"clientAssignmentChanged("+data.Assignments[i].ID+",'rw')\" name='priv_user_"+data.Assignments[i].ID+"' id='priv_user_"+data.Assignments[i].ID+"_rw' value='rw' "+checked_rw+"> Write"
		
		assi_grid.addRow(data.Assignments[i].ID,[data.Assignments[i].Surname,data.Assignments[i].Name,data.Assignments[i].Username,data.Assignments[i].Priviledges,radio])
				
	}
	
	assi_grid.clearChangedState();
	
	toolbar.attachEvent('onClick', function(id){
		if(id=="save"){
			var rids= assi_grid.getChangedRows(false);		
			if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
			else{
				var json_obj =	getClientAssignmentChangesJSON(rids,data.Client_ID,assi_grid);
				saveClientAssignment(json_obj)
			}
		}		
	})
	
	gdata.client_assign_grid = assi_grid;
	
	win.attachEvent("onClose", cleanClientAssignmentWin);
}

function clientAssignmentChanged(user_id,priv){
	//alert(user_id+"-"+priv);
	gdata.client_assign_grid.cells(user_id,3).setValue(priv)
	gdata.client_assign_grid.cells(user_id,3).cell.wasChanged=true;
}

function cleanClientAssignmentWin(){
	gdata.client_assign_grid = null;
	return true;
}

function getClientAssignmentChangesJSON(rids,cid,win){
	
	var rowids = rids.split(",");
	var json = ",\"Assignments\":[";
	for(var i=0;i<rowids.length;i++){
			
		json = json+"{\"ID\":"+rowids[i];
		for (var j=0; j<win.getColumnCount()-1; j++){			
			var txt = win.cells(rowids[i],j).getValue()
			json = json + ",\""+win.getColumnId(j)+"\":\""+txt+"\""
		}
		json = json+"}";
		
		if(i<rowids.length-1){json=json+","}
	}
	json = json+"]"
	json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\",\"Client_ID\":"+cid+""+json+"}";
	
	//alert(json)
	var json_obj = jQuery.parseJSON( json );
	return json_obj;
}

function saveClientAssignment(json){
	
	gdata.layout_main.cells('b').progressOn();
	 
	$.ajax({
    		type: 'POST',
    		url: client_assignments_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveClientAssignment, 
    		error: CallbackSaveClientAssignmentFailed,
    		cache: false
	});

}

function CallbackSaveClientAssignment(data, textStatus, jqXHR){
	
	gdata.layout_main.cells('b').progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Client assignment saved successfully."});
		gdata.client_assign_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving client info."});
	}
	

}

function CallbackSaveClientAssignmentFailed(jqXHR, textStatus, errorThrown){
	gdata.layout_main.cells('b').progressOff();
}


function getClientInfo(cid){
		
	gdata.layout_main.cells('b').progressOn();
	 
	$.ajax({
    		type: 'POST',
    		url: client_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Client_ID':cid},
    		success:CallbackGetClientInfo, 
    		error: CallbackGetClientInfoFailed,
    		cache: false
	});
}

function CallbackGetClientInfo(data, textStatus, jqXHR){
	gdata.layout_main.cells('b').progressOff();
	
	if(data.Result_Code==0){
		var win = gdata.main_layout.dhxWins.createWindow("win_new_client",0,0,400,420);
		initClientWin(win,data)
		win.centerOnScreen();
		win.setModal(true);
		win.show();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving client info."});
	}
}

function CallbackGetClientInfoFailed(jqXHR, textStatus, errorThrown){
	gdata.layout_main.cells('b').progressOff();
}

function newClientQuestionnaire(cid){
	initQuestionnaireWin(cid,-1000)		
}

function getClientTests(cid){
	gdata.layout_main.cells('b').progressOn();
	$.ajax({
    		type: 'POST',
    		url: client_tests_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Client_ID':cid},
    		success:CallbackGetClientTests, 
    		error: CallbackGetClientTestsFailed,
    		cache: false
	});	
}

function CallbackGetClientTests(data, textStatus, jqXHR){

	gdata.layout_main.cells('b').progressOff();
	
	if(data.Result_Code == 0){		
		var colIndex=gdata.clients_grid.getColIndexById("Priviledges");
		var privs =  gdata.clients_grid.cells(data.Client_ID,colIndex).getValue();
		initTestsListWin(data.Client_ID,data.Tests,privs)		
	}
	else{
		dhtmlx.alert({title:"No Data",type:"alert",text:"No questionnaires found."});	
	}
	
}
function CallbackGetClientTestsFailed(jqXHR, textStatus, errorThrown){
	gdata.layout_main.cells('b').progressOff();	
}

function getClientQuestionnaires(cid){
	
	gdata.layout_main.cells('b').progressOn();
	 
	$.ajax({
    		type: 'POST',
    		url: client_questionnaires_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Client_ID':cid},
    		success:CallbackGetClientQuestionnaires, 
    		error: CallbackGetClientQuestionnairesFailed,
    		cache: false
	});		
}

function CallbackGetClientQuestionnaires(data, textStatus, jqXHR){

	gdata.layout_main.cells('b').progressOff();
	
	if(data.Result_Code == 0){		
		var colIndex=gdata.clients_grid.getColIndexById("Priviledges");
		var privs =  gdata.clients_grid.cells(data.Client_ID,colIndex).getValue();
		initQuestionnairesListWin(data.Client_ID,data.Questionnaires,privs)		
	}
	else{
		dhtmlx.alert({title:"No Data",type:"alert",text:"No questionnaires found."});	
	}
	
}

function CallbackGetClientQuestionnairesFailed(jqXHR, textStatus, errorThrown){
	gdata.layout_main.cells('b').progressOff();	
}

function adjustQuestionnaireCount(rowid,adjustment){
	
	var qcount = Number(gdata.clients_grid.cells(rowid,7).getValue())+adjustment;
	gdata.clients_grid.cells(rowid,7).setValue(qcount);
	
}