function initViewUsers(win){

	win.setText("User List");
	win.setIcon("../../../images/users-icon-16.png", "../../../images/users-icon-16.png");
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('new', 0, 'New User','./images/user-add-icon-32.png','./images/user-add-icon-32.png');
	toolbar.addSeparator('sep_1', 1);	
	toolbar.addButton('delete', 2, 'Delete User','./images/user-delete-icon-32.png','./images/user-delete-icon-32.png');
	toolbar.addSeparator('sep_1', 3);
	toolbar.addButton('card', 4, 'User Card','./images/user-info-icon-32.png','./images/user-info-icon-32.png');
	
	
	toolbar.disableItem('delete')
	toolbar.disableItem('card')
	
	toolbar.attachEvent('onClick', userToolbarClicked);
	
	var myPop = new dhtmlXPopup({
            toolbar: toolbar,
            id: "new"
        });
	
	var formStructure = [
		{type:"settings",position:"label-top" },
		    
		    {type:"fieldset", label: "Information",name: "app_user_info", inputWidth:160, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"input", name:"Surname", label:"Surname",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Name", label:"Name",inputWidth:120, validate: "NotEmpty"},
				{type:"input", name:"Email", label:"E-Mail",inputWidth:120, validate: "NotEmpty"},
				{type:"input", name:"Tax_Code", label:"Tax Code",inputWidth:120},
				{type:"combo", name:"Language_ID", label:"Language",inputWidth:120, validate: "NotEmpty"},
				{type:"checkbox", name:"Is_Admin", label:"Administrator",checked:false}	
		    	
		    ]},
		    
			{type:"fieldset", label: "Security",name: "app_user_sec", inputWidth:160, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"input", name:"Username", label:"Username",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Password", label:"Password",inputWidth:120, validate: "NotEmpty"},
				{type:"input", name:"Ver_Password", label:"Verify Passowrd",inputWidth:120, validate: "NotEmpty"}		    	
		    ]}, 
		    {type: "button", value:"Save"},
		    {type: "hidden", name:"ID",value:-1000},		    
		    {type: "hidden", name:"json",value:""}
 	] 
	
	var app_user_form = myPop.attachForm(formStructure);
	
	var language_combo = app_user_form.getCombo("Language_ID");
	language_combo.addOption(gdata.languages);
	language_combo.readonly(true);
	
	myPop.attachEvent("onShow", function() {
            app_user_form.clear();
            app_user_form.setItemValue('ID',-1000);
            app_user_form.setFocusOnFirstActive();
            
        });
	
	app_user_form.attachEvent("onButtonClick", function() {
            
			if(app_user_form.validate()){
				
				var pass = app_user_form.getItemValue('Password');
				var ver_pass = app_user_form.getItemValue('Ver_Password');
				
				if(pass != ver_pass){
					dhtmlx.alert({title:"Error",type:"alert",text:"Password verification failed"});
				}
				else{
					var json = formToJSON(app_user_form) 
					saveUser(json)	
					myPop.hide();
				}
						
			}
        });
	
	var users_grid = win.attachGrid();	
	users_grid.setHeader("Surname,Name,Email,Tax Code,Username,Admin");
	users_grid.setColumnIds("Surname,Name,Email,Tax_Code,Username,Is_Admin");
	users_grid.setColTypes("ro,ro,ro,ro,ro,ch");
	users_grid.setColSorting("str,str,str,str,str,str");
	users_grid.setInitWidthsP("20,20,20,15,15,10");	
	users_grid.setSkin("dhx_skyblue");
	users_grid.setImagePath(gdata.img_path);	
	users_grid.setEditable(false);
	users_grid.init();
	
	users_grid.attachEvent("onRowDblClicked", function(rId,cInd){
		getUserInfo(rId);			
	});  
	
	users_grid.attachEvent("onRowSelect", function(rId,cInd){
		toolbar.enableItem('delete')	
		toolbar.enableItem('card')
	}); 
	
	gdata.users_grid=users_grid;
	
	win.attachEvent("onClose", cleanUserListWin);
	
	getUserList();
}



function cleanUserListWin(){
	gdata.users_grid=null; 
	return true;
}

function userToolbarClicked(id){
	if(id=="delete"){
		var uid = gdata.users_grid.getSelectedRowId();
		dhtmlx.confirm({type:"confirm",text: "Are you sure you want to delete the user and all of his data?",callback: function(result){if(result){deleteUser(uid)}}});
	}
	else if(id=="card"){
		var uid = gdata.users_grid.getSelectedRowId();
		if(uid==null){ }
		else {getUserInfo(uid);}
		 
	}
}

function getUserList(){
	
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: user_list,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin"},
    		success:CallbackGetUserList , 
    		error: CallbackGetUserListFailed,
    		cache: false
	});
	
}

function CallbackGetUserList(data, textStatus, jqXHR){

	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOff();
	
	if(data.Result_Code == 0){
 		
 		if(typeof data.Users != 'undefined'){ 			
 			for(var i=0;i<data.Users.length;i++){
 				var user = data.Users[i]
 				addUserToGrid(user)
 			} 			
 		} 		 		
 	}
 	else{
 		dhtmlx.alert({title:"Login Failed",type:"alert-error",text:"Invalid Credentials."});
 	}
	

}

function CallbackGetUserListFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOff();

}


function addUserToGrid(user){
	
	var arr = [];
	
	for (var i=0; i<gdata.users_grid.getColumnCount(); i++){
		
		 var colId=gdata.users_grid.getColumnId(i);
		 if(user.hasOwnProperty(colId)){
			var val = user[colId] 
			arr.push(val);
		 }
		 else{
			arr.push(null);
		 }
    }
	
	/*
	for(var x in user){
  		if(x!="ID"){
  			arr.push(user[x]);
  		}
	}
	*/
	var exist=gdata.users_grid.doesRowExist(user.ID);
	
	if(!exist){		
		gdata.users_grid.addRow(user.ID,arr)
	}
	else{
		var idx = gdata.users_grid.getRowIndex(user.ID);
		gdata.users_grid.deleteRow(user.ID);
		gdata.users_grid.addRow(user.ID,arr,idx)
	}
		
}


function deleteUser(uid){
	
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOn();
	
	$.ajax({
    			type: 'POST',
    			url: user_delete,
    			dataType: 'json',
    			data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'App_User_ID':uid},
    			success:CallbackDeleteUser , 
    			error: CallbackDeleteUserFailed,
    			cache: false
		});
		
}

function CallbackDeleteUser(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOff();
	
	if(data.Result_Code==0){
		gdata.users_grid.deleteRow(data.App_User_ID);
		dhtmlx.alert({title:"Success",type:"alert",text:"User deleted successfully."});
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});
	}	
}

function CallbackDeleteUserFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOff();
	
	
}

function saveUser(json){
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: user_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveUser , 
    		error: CallbackSaveUserFailed,
    		cache: false
	});
}

function CallbackSaveUser(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"User saved successfully."});
		addUserToGrid(data)
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});
	}
	
}

function CallbackSaveUserFailed(jqXHR, textStatus, errorThrown){
	
	var win = gdata.main_layout.dhxWins.window("win_user_list");
	win.progressOff();
	
}

function getUserInfo(rId){
	
	if (gdata.main_layout.dhxWins.window("win_user_list")) {	
		var win = gdata.main_layout.dhxWins.window("win_user_list");
		win.progressOn();
	}
	
	$.ajax({
    		type: 'POST',
    		url: user_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'App_User_ID':rId},
    		success:CallbackGetUserInfo , 
    		error: CallbackGetUserInfoFailed,
    		cache: false
	});
}

function CallbackGetUserInfo(data, textStatus, jqXHR){

	if (gdata.main_layout.dhxWins.window("win_user_list")) {	
		var win = gdata.main_layout.dhxWins.window("win_user_list");
		win.progressOff();
	}
	
	if(data.Result_Code==0){		 
		var win_card = gdata.main_layout.dhxWins.createWindow("win_user_card",0,0,550,600);
        initViewUserCard(win_card,data)
        win_card.centerOnScreen();
		win_card.setModal(true);
		win_card.show();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert-error",text:data.Result_Message});
	}
	
	
}

function CallbackGetUserInfoFailed(jqXHR, textStatus, errorThrown){
	
	if (gdata.main_layout.dhxWins.window("win_user_list")) {	
		var win = gdata.main_layout.dhxWins.window("win_user_list");
		win.progressOff();
	}
	
	
}
