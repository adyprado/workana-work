function initQuestionnaireScalesWin(questionnaire_id,questionnaire_comments){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_que_scales",0,0,800,600);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText(questionnaire_comments);
	win.setIcon("../../../images/measure-icon-16.png", "../../../images/measure-icon-16.png");
	win.show();	
	
	var layout = win.attachLayout("2E");
	var idx_scales_cell = layout.cells('a');
	//idx_factors_cell.setWidth(300);
	idx_scales_cell.setText("Scales");
	
	var idx_scale_questions_cell = layout.cells('b');
	idx_scale_questions_cell.setText("Scale Questions");
	
	var scales_grid = idx_scales_cell.attachGrid();	
	scales_grid.setHeader("Code,Description,Result,Questionnaire_ID");
	scales_grid.setColumnIds("Code,Descr_el,A_Score,Questionnaire_ID");
	scales_grid.setColTypes("ro,ro,ro,ro");
	scales_grid.setColumnsVisibility("false,false,false,true");
	scales_grid.setColSorting("str,str,str,str");
	scales_grid.setInitWidthsP("20,58,20,0");	
	scales_grid.setSkin("dhx_skyblue");
	scales_grid.setImagePath(gdata.img_path);	
	scales_grid.init();
	
	var scales_questions_grid = idx_scale_questions_cell.attachGrid();	
	scales_questions_grid.setHeader("Code,Question,Answer,Points");
	scales_questions_grid.setColumnIds("Code,Question,Answer,Points");
	scales_questions_grid.setColTypes("ro,ro,ro,ro");
	scales_questions_grid.setColSorting("str");
	scales_questions_grid.setInitWidthsP("20,58,10,10");	
	scales_questions_grid.setSkin("dhx_skyblue");
	scales_questions_grid.setImagePath(gdata.img_path);	
	scales_questions_grid.init();
		
	scales_grid.attachEvent("onRowSelect", function(id,ind){
		getQuestionnaireQuestionsForScale(id,questionnaire_id)		
	});
	
	win.attachEvent("onClose", cleanQuestionnaireSimpleScalesWin);
	
	gdata.questionnaire_scales_grid = scales_grid;
	gdata.questionnaire_scale_questions_grid = scales_questions_grid;
	 
	getQuestionnaireParamSimpleScales(questionnaire_id);
}

function cleanQuestionnaireSimpleScalesWin(){
	gdata.questionnaire_scales_grid = null
	gdata.questionnaire_scale_questions_grid = null

	return true;
}

function getQuestionnaireParamSimpleScales(questionnaire_id){
	
	var win = gdata.main_layout.dhxWins.window("win_que_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questionnaire_scales_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_ID':questionnaire_id},
    		success:CallbackGetQuestionnaireSimpleScales , 
    		error: CallbackGetQuestionnaireSimpleScalesFailed,
    		cache: false
	});
}

function CallbackGetQuestionnaireSimpleScales(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_que_scales");
	win.progressOff();
	
	if(data.Result_Code==0){
		//gdata.questionnaire_scales_grid.clearAll();
		
		for(var i=0;i<data.Scales.length;i++){
			addRowToGrid(gdata.questionnaire_scales_grid,data.Scales[i])
		}
		//gdata.questionnaire_scales_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	}
}

function CallbackGetQuestionnaireSimpleScalesFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_que_scales");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	
}
function getQuestionnaireQuestionsForScale(id,questionnaire_id){
	
	var win = gdata.main_layout.dhxWins.window("win_que_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questionnaire_scales_questions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_ID':questionnaire_id,'Scale_ID':id,"Language_ID":gdata.language_id},
    		success:CallbackGetQuestionnaireScaleQuestions , 
    		error: CallbackGetQuestionnaireScaleQuestionsFailed,
    		cache: false
	});
}

function CallbackGetQuestionnaireScaleQuestions(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_que_scales");
	win.progressOff();
	
	if(data.Result_Code==0){
		gdata.questionnaire_scale_questions_grid.clearAll();		
		for(var i=0;i<data.Questions.length;i++){
			addRowToGrid(gdata.questionnaire_scale_questions_grid,data.Questions[i])
		} 
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	}	
}

function CallbackGetQuestionnaireScaleQuestionsFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_que_scales");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	
}

/* 
function questionForScaleSelected(question_id){
	var scale_id = gdata.scales_grid.getSelectedRowId()
	var scale_code = gdata.scales_grid.cells(scale_id,0).getValue();
	var action = 'add'
	mapQuestionToScale(question_id,scale_code,action)
}

function mapQuestionToScale(question_id,scale_code,action){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: scale_questions_set,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Scale_Code':scale_code,'Question_ID':question_id,'Action':action},
    		success:CallbackMapQuestionToScale , 
    		error: CallbackMapQuestionToScaleFailed,
    		cache: false
	});

}

function CallbackMapQuestionToScale(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	if(data.Result_Code ==0){
		
		if(typeof data.Action != 'undefined'){
			if(data.Action == 'delete'){
				var question_id = data.Question_ID;
				gdata.scale_questions_grid.deleteRow(question_id)
				dhtmlx.alert({title:"Success",type:"alert",text:"Question unlinked successfully."});
			}
		}
		else{
			dhtmlx.alert({title:"Success",type:"alert",text:"Question linked successfully."});
			addRowToGrid(gdata.scale_questions_grid,data)	
		}
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error performing action."});
	}
}

function CallbackMapQuestionToScaleFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Error performing action."});
}

function getQuestionsForScale(scale_id){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: scale_questions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Scale_ID':scale_id},
    		success:CallbackGetScaleQuestions , 
    		error: CallbackGetScaleQuestionsFailed,
    		cache: false
	});
}

function CallbackGetScaleQuestions(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.scale_questions_grid.clearAll();
		
		for(var i=0;i<data.Questions.length;i++){
			addRowToGrid(gdata.scale_questions_grid,data.Questions[i])
		}
		gdata.scale_questions_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	}
}

function CallbackGetScaleQuestionsFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
}

function getParamSimpleScales(){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: scales_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin"},
    		success:CallbackGetParamSimpleScales , 
    		error: CallbackGetParamSimpleScalesFailed,
    		cache: false
	});
}

function CallbackGetParamSimpleScales(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.scales_grid.clearAll();
		
		for(var i=0;i<data.Scales.length;i++){
			addRowToGrid(gdata.scales_grid,data.Scales[i])
		}
		gdata.scales_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	}
}

function CallbackGetParamSimpleScalesFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	
}
*/