function initViewUserCard(win,data){
	
	win.setText(data.Surname+" "+data.Name);
	win.setIcon("../../../images/user-card-icon-16.png", "../../../images/user-card-icon-16.png");
		
	var layout = win.attachLayout("2E");
	
	var user_info_cell = layout.cells('a');
	user_info_cell.setHeight(290);
	user_info_cell.setText("Information");
	
	var toolbar = user_info_cell.attachToolbar();
	toolbar.setIconSize(32);
	
	if(gdata.is_admin == 1){
		toolbar.addButton('save', 0, 'Save Information','./images/save-icon-32.png','./images/save-icon-32.png');
		toolbar.addSeparator('sep_1', 1);	
		toolbar.addButton('credentials', 2, 'Change Credentials','./images/lock-icon-32.png','./images/lock-icon-32.png');
		toolbar.attachEvent('onClick', UserCardToolbarClicked);
	}
	else{
		toolbar.addButton('credentials', 0, 'Change Credentials','./images/lock-icon-32.png','./images/lock-icon-32.png');
	}
	
	
	
	var formStructure = [
		{type:"settings",position:"label-top" },
		    
		    {type:"fieldset", label: "Information",name: "app_user_info", inputWidth:400, offsetLeft:"10",offsetTop:"15", list:[
		    	{type:"input", name:"Surname", label:"Surname",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Name", label:"Name",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Email", label:"E-Mail",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Tax_Code", label:"Tax Code",inputWidth:120, validate: "NotEmpty"},
		    	
		    	{type: "newcolumn", offset:50},
		    	{type:"combo", name:"Language_ID", label:"Language",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Username", label:"Username",inputWidth:120, validate: "NotEmpty"},
		    	{type:"checkbox", name:"Is_Admin", label:"Administrator",checked:false},
		    	{type:"checkbox", name:"Is_Blocked", label:"Blocked",checked:false}
		    	
		    ]},
		     		    
		    {type: "hidden", name:"ID",value:-1000},		    
		    {type: "hidden", name:"json",value:""}
		     
  
	]

    var form = user_info_cell.attachForm(formStructure)
	
	var language_combo = form.getCombo("Language_ID");
	language_combo.addOption(gdata.languages);
	language_combo.readonly(true);
	
	if(gdata.is_admin ==0){form.disableItem("app_user_info")}
	
	var user_reg_cell = layout.cells('b');
	
	if(gdata.is_admin ==1){
		
		user_reg_cell.hideHeader();
		//user_reg_cell.setText("Registrations");
		var reg_toolbar = user_reg_cell.attachToolbar();
		reg_toolbar.setIconSize(32);
		reg_toolbar.addButton('new_reg', 0, 'Add Registration','./images/add-icon-32.png','./images/add-icon-32.png');
		reg_toolbar.addSeparator('sep_1', 1);	
		reg_toolbar.addButton('del_reg', 2, 'Delete Registration','./images/delete-reg-icon-32.png','./images/delete-reg-icon-32.png');	
		reg_toolbar.attachEvent('onClick', UserCardToolbarClicked);	
		reg_toolbar.disableItem('del_reg')
	
		var regPop = new dhtmlXPopup({
            toolbar: reg_toolbar,
            id: "new_reg"
        });
    
    	var regFormStructure = [
			{type:"settings",position:"label-top" },
		    
		    {type:"fieldset", label: "Information",name: "app_user_info", inputWidth:160, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"combo", name:"Category", label:"Category",inputWidth:120, validate: "NotEmpty"},
		    	{type:"combo", name:"Type_ID", label:"Type",inputWidth:120, validate: "NotEmpty"},
		    	{type:"input", name:"Pieces", label:"Quantity",inputWidth:60, validate: "ValidNumeric"},
		    	{type:"input", name:"Amount", label:"Amount Paid",inputWidth:120, validate: "ValidNumeric"}
		    	
		    ]},
		   
		    {type: "hidden", name:"ID",value:-1000},	
		    {type: "hidden", name:"App_User_ID",value:-1000},
		    {type: "hidden", name:"Purchase_Date",value:""},
		    {type: "hidden", name:"json",value:""},		    
		    
		    {type: "button", value: "Save",offsetLeft: 100,offsetTop:"7"}  
		]
	
		var new_reg_form = regPop.attachForm(regFormStructure);
		
		var pcombo = new_reg_form.getCombo("Category");
		pcombo.addOption([
		   ["Questionnaires","Questionnaires"], 
           ["Tests","Tests"]                     
		]); 
		pcombo.readonly(true);
						
		var combo = new_reg_form.getCombo("Type_ID");
		//combo.addOption(gdata.questionnaire_types);
		combo.readonly(true);
	
		pcombo.attachEvent("onChange", function(){
			
			combo.clearAll();
			var sel_val = pcombo.getSelectedValue();
			if(sel_val == "Tests"){combo.addOption(gdata.test_types);}
			else{combo.addOption(gdata.questionnaire_types);}
			
			combo.selectOption(0);			 
		});
	
		regPop.attachEvent("onShow", function() {
            new_reg_form.clear();
            new_reg_form.setItemValue('ID',-1000);
            new_reg_form.setItemValue('App_User_ID',gdata.user_card_form.getItemValue('ID'));
            new_reg_form.setFocusOnFirstActive();
            
    	});
	
		new_reg_form.attachEvent("onButtonClick", function() {            
			if(new_reg_form.validate()){			 
					var today = getDayAsString()
					new_reg_form.setItemValue('Purchase_Date',today)
					var json = formToJSON(new_reg_form) 
					saveRegistration(json)	
					regPop.hide();						
			}
        });
	
	}
	else{
		user_reg_cell.setText("Registrations");
	}
	
	var reg_grid = user_reg_cell.attachGrid();	
	reg_grid.setHeader("Category,Type,Number,Amount Paid, Date");
	reg_grid.setColumnIds("Category,Type_ID,Pieces,Amount,Purchase_Date");
	reg_grid.setColTypes("ro,coro,ro,ro,ro");
	reg_grid.setColSorting("str,str,str,str,str");
	reg_grid.setInitWidthsP("20,20,15,20,25");	
	reg_grid.setSkin("dhx_skyblue");
	reg_grid.setImagePath(gdata.img_path);	
	reg_grid.init();
	
	reg_grid.attachEvent("onRowSelect", function(rId,cInd){
		if(gdata.is_admin ==1){reg_toolbar.enableItem('del_reg')}			
	}); 
	
	var qtype_combo = reg_grid.getCombo(1);
	
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		qtype_combo.put('Q'+gdata.questionnaire_types[i].value,gdata.questionnaire_types[i].text);		
	} 
	for(var i=0;i<gdata.test_types.length;i++){
		qtype_combo.put('T'+gdata.test_types[i].value,gdata.test_types[i].text);		
	}
	
	gdata.user_card_form = form;
	gdata.user_reg_grid = reg_grid;
	
	win.attachEvent("onClose", cleanUserCardWin);
	
	
	initUserCardWithData(data)
	
	
	
	var crePop = new dhtmlXPopup({
            toolbar: toolbar,
            id: "credentials"
        });
    
    var creFormStructure = [
		{type:"settings",position:"label-top" },
		    
		    {type:"fieldset", label: "Credentials",name: "app_user_info", inputWidth:160, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"password", name:"Password", label:"New Password",inputWidth:120, validate: "NotEmpty"},
		    	{type:"password", name:"Ver_Password", label:"Verify Password",inputWidth:120, validate: "NotEmpty"}
		    	
		    ]},
		   		     	
		    {type: "hidden", name:"App_User_ID",value:-1000},		     
		    {type: "hidden", name:"json",value:""},		    		    
		    {type: "button", value: "Save",offsetLeft: 100,offsetTop:"7"}  
	]
	
	var ch_cre_form = crePop.attachForm(creFormStructure);
	
	crePop.attachEvent("onShow", function() {
            ch_cre_form.clear();
            ch_cre_form.setItemValue('ID',-1000);
            ch_cre_form.setItemValue('App_User_ID',gdata.user_card_form.getItemValue('ID'));
            ch_cre_form.setFocusOnFirstActive();
            
    });
	
	ch_cre_form.attachEvent("onButtonClick", function() {
            
			if(ch_cre_form.validate()){			 
				
				var pass = ch_cre_form.getItemValue('Password');
				var ver_pass = ch_cre_form.getItemValue('Ver_Password');
				
				if(pass != ver_pass){
					dhtmlx.alert({title:"Error",type:"alert",text:"Password verification failed"});
				}
				else{
					var json = formToJSON(ch_cre_form) 
					saveUserCredentials(json)	
					crePop.hide();
				}							
			}
        });
}

function initUserCardWithData(data){
	initFormWithData(gdata.user_card_form,data)
	
	if(typeof data.Registrations != "undefined"){
		for(var i=0;i<data.Registrations.length;i++){
			data.Registrations[i].Type_ID = data.Registrations[i].Category.substring(0,1)+data.Registrations[i].Type_ID
			addRowToGrid(gdata.user_reg_grid,data.Registrations[i])
		}		
	}
}

function cleanUserCardWin(){
	gdata.user_card_form = null;
	gdata.user_reg_grid = null;
	return true;
}


function UserCardToolbarClicked(id){
	
	if(id=="save"){
		if(gdata.user_card_form.validate()){
			var json = formToJSON(gdata.user_card_form) 
			saveUser(json)	
		}		
	}
	else if(id=="del_reg"){
		var reg_id = gdata.user_reg_grid.getSelectedRowId();
		dhtmlx.confirm({type:"confirm",text: "Are you sure you want to delete the selected registration?",callback: function(result){if(result){deleteRegistration(reg_id)}}});
	}
}

function deleteRegistration(reg_id){
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: registration_delete,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Registration_ID':reg_id},
    		success:CallbackDeleteRegistration , 
    		error: CallbackDeleteRegistrationFailed,
    		cache: false
	});
}

function CallbackDeleteRegistration(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOff();
	
	if(data.Result_Code == 0){
		gdata.user_reg_grid.deleteRow(data.Registration_ID);
		dhtmlx.alert({title:"Success",type:"alert",text:"Registration deleted successfully."});
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});	
	}
}

function CallbackDeleteRegistrationFailed(jqXHR, textStatus, errorThrown){
	
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOff();
	
	
}

function saveUserCredentials(json){
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: user_credentials_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveCredentials , 
    		error: CallbackSaveCredentialsFailed,
    		cache: false
	});

}

function CallbackSaveCredentials(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOff();
	
	if(data.Result_Code==0){
		 
		dhtmlx.alert({title:"Success",type:"alert",text:"Credentials saved successfully."});
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});
	}
}


function CallbackSaveCredentialsFailed(jqXHR, textStatus, errorThrown){
	
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOff();

	
}


function saveRegistration(json){
	
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: registration_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveRegistration , 
    		error: CallbackSaveRegistrationFailed,
    		cache: false
	});
	
}

function CallbackSaveRegistration(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOff();
	
	if(data.Result_Code==0){
		data.Type_ID = data.Category.substring(0,1)+data.Type_ID
		addRowToGrid(gdata.user_reg_grid,data)
		dhtmlx.alert({title:"Success",type:"alert",text:"Registration saved successfully."});
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});
	}
	
}

function CallbackSaveRegistrationFailed(jqXHR, textStatus, errorThrown){
	
	var win = gdata.main_layout.dhxWins.window("win_user_card");
	win.progressOff();
	
	
}