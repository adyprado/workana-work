<?php
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	include('server/database_slave.php');
	require_once('server/functions.php');
    $database = new database_slave();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	
    	<link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    	<link href="results.css" rel="stylesheet">
    	
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    	
    	<title>Finish</title>
</head>
<body>
    	<div class="container">
 		<?php 
 		 	$result = $database->save_test_answers($_POST);
 		 	$language_code = "en";
 		 	if($result){
 		 		 
 		 		$lb_finish_rec = $database->get_db_record("META_UI_Labels","FINISH","Code");
	 		 	$lb_success_rec = $database->get_db_record("META_UI_Labels","ANSWERS_STORED","Code");
	 		 	
	 		 	
	 		 	echo " <div class=\"header\">
        						<h3 class=\"text-muted\">".$lb_finish_rec["Descr_".$language_code]."</h3>
    					</div>	 		 		
	 		 			<div class=\"alert alert-success\">
    							".$lb_success_rec["Descr_".$language_code]."	 
    					</div>";
 		 	}
 		 	else{
 		 		$lb_error_rec = $database->get_db_record("META_UI_Labels","PROCESS_ERROR","Code");
	 		 	$lb_error_msg_rec = $database->get_db_record("META_UI_Labels","UNEXPECTED_ERROR","Code");
			 	 	
			 	echo " <div class=\"header\">
        					<h3 class=\"text-muted\">".$lb_error_rec["Descr_".$language_code]."</h3>
    					</div>	 		 		
	 		 			<div class=\"alert alert-danger\">
    						".$lb_error_msg_rec["Descr_".$language_code]."	 
    					</div>";
 		 	}
 		 	
 		 	$hash_code = $_POST["hash_code"]; 		 	
 		 	$test_rec = $database->get_db_record("Tests",$hash_code,"Hash_Code"); 	
 		 	
 		 	notifyForCompletion($database,$test_rec["ID"],"Tests","",$language_code);
 		 	 
 		?>
 		</div>
</body>