function LoginToServer(id){
	
	 
	
	if(gdata.login_form.validate())
	{		
		gdata.main_layout.cells('a').progressOn();
		 
		$.ajax({
    			type: 'POST',
    			url: service_login,
    			dataType: 'json',
    			data: {'Username':gdata.login_form.getItemValue("name"),'Password':gdata.login_form.getItemValue("pass"),'Channel':"WebAdmin"},
    			success:CallbackLogin , 
    			error: CallbackLoginFailed,
    			cache: false
		});	
	}	 
}

function CallbackLoginFailed(jqXHR, textStatus, errorThrown){
 	gdata.main_layout.cells('a').progressOff();
}

function CallbackLogin(data, textStatus, jqXHR){
 	gdata.main_layout.cells('a').progressOff(); 
 	
 	if(data.Result_Code == 0){
 		gdata.uid = data.User_ID;	
		gdata.username = data.Username;
		gdata.api_key = data.Api_Key;
		gdata.user_desc = data.Surname+' '+data.Name;
		gdata.is_admin = data.Is_Admin 	
		gdata.genders = data.Genders;
		gdata.marital_statuses = data.Marital_Statuses;
		gdata.questionnaire_types = data.Questionnaire_Types;
		gdata.test_types = data.Test_Types;
		gdata.languages = data.Languages;
		gdata.language_id = data.Language_ID;
		gdata.registration_types = data.Registration_Types;
		initMainUI();	
		gdata.main_layout.cells('a').view("main_view").setActive();
		
		initViewClients()
		getClientList()
		//initUserData();
 	}
 	else{
 		dhtmlx.alert({title:"Login Failed",type:"alert-error",text:"Invalid Credentials."});
 	}
}