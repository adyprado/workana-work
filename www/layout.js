function initAppUI(){	
          	          	    	
            
            dhtmlx.image_path = gdata.img_path ;  			 
			 
			gdata.main_layout = new dhtmlXLayoutObject(document.body, '1C');			 
			gdata.main_layout.cells("a").hideHeader();				
					 
			initLoginUI();			
			initMainUI();
			//gdata.main_layout.cells('a').view("main_view").setActive();
}

function initLoginUI(){
		 
	var login_view = gdata.main_layout.cells('a');
	gdata.login_form = login_view.attachForm();
			
	var formData = [
            
            {type: "container", name: "logo", label: "", inputWidth: 255, inputHeight: 109,offsetLeft:20,offsetTop:20},     
            
            //{type: "container", name: "info", width:300,height:200,position:"absolute",offsetLeft:38,offsetTop:150},
            {type: "container", name: "info", width:330,height:200,position:"absolute",offsetLeft:(document.body.clientWidth-230)/2,offsetTop:((document.body.clientHeight-200)/2)+180},
            {type: "fieldset", name: "data", label: "Login",width:280,height:220, inputWidth: "auto",offsetLeft:(document.body.clientWidth-280)/2,offsetTop:(document.body.clientHeight-220)/2,list:[
				 
				{type: "container",name: "name_image",inputHeight:36,offsetTop:10},
				{type: "container",name: "pass_image"},
				{type:"newcolumn"},
				{type:"input", name: "name", label:"", validate: "NotEmpty",position:"label-left",note:{text:"Username"},offsetLeft:5,offsetTop:8},
				{type:"password", name:"pass", label:"", validate: "NotEmpty",position:"label-left",note:{text:"Password"},offsetLeft:5},				
				{type:"button", name:"save", value:"Login",position:"label-top"}] 
		    }
		    
		    //{type: "container", name: "info", width:300,height:200, inputWidth: "auto",offsetLeft:(document.body.clientWidth-230)/2}
		    
           ]
             
    gdata.login_form.loadStruct(formData, "json", function(){});            
   
   	var container = gdata.login_form.getContainer("logo")
   	container.innerHTML = "<img src='./images/logo_eurika.png' height='106' width='255'>"
   	
   	var i_container = gdata.login_form.getContainer("info")
   	i_container.innerHTML = "<table class='info'><tr><td>E-Mail</td><td><a href='mailto:info@philanthropy.gr'>info@philanthropy.gr</a></td></tr>"+
   							"<tr><td>Support-Greece</td><td>+30 2106658272</td></tr>"+
   							"</table>"
   
    name_cont = gdata.login_form.getContainer("name_image");
    name_cont.innerHTML = '<img src="./images/user-icon-16.png">';
    pass_cont = gdata.login_form.getContainer("pass_image");
    pass_cont.innerHTML = '<img src="./images/locker-icon-16.png">';
    
    gdata.login_form.attachEvent("onButtonClick", LoginToServer); 
	gdata.login_form.attachEvent("onEnter", LoginToServer);

}

function initMainUI(){
	
	var a = gdata.main_layout.cells('a').view("main_view");
	
	var layout_main = a.attachLayout('2U');

	gdata.status_bar = layout_main.attachStatusBar();
	gdata.status_bar.setText("PBI Administration");
	
	gdata.layout_main = layout_main;
	
	initOptionsMenu()
	//initViewClients()
	
}





