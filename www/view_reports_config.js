function initReportConfigWin(){

	var win = gdata.main_layout.dhxWins.createWindow("win_rep_conf",0,0,900,500);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Reports");
	win.setIcon("../../../images/report-icon-16.png", "../../../images/report-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save','./images/save-icon-32.png','./images/save-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	toolbar.addButtonSelect('questionnaire', 2, 'Questionnaire', [], './images/test-icon-32.png','./images/test-icon-32.png');
	
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		//if(gdata.questionnaire_types[i].text=="PBI" || gdata.questionnaire_types[i].text=="IPA" || gdata.questionnaire_types[i].text=="WBEI" || ){
			toolbar.addListOption('questionnaire', 'q-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/question-mark-icon-32.png' )
		//}
	}
	
	toolbar.attachEvent('onClick', function(id){
		
		if(id=="save"){
			var rids=gdata.rep_conf_grid.getChangedRows(false);		
			if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
			else{
				var json_obj =	getRepConfigChangesJSON(rids);
				saveRepConfig(json_obj)
			}
		}
		else if(id.indexOf("q-")==0){			 
			var qtype_id = id.split("-");	
			getParamRepConfig(qtype_id[1]);
			 
		}		
	});
	
	var rep_grid = win.attachGrid();	
	rep_grid.setHeader("Scale Code 1,Scale Code 2,Title EL,Title EN,Descr EL,Descr EN");
	rep_grid.setColumnIds("Scale_Code_1,Scale_Code_2,Title_el,Title_en,Descr_el,Descr_en");
	rep_grid.setColTypes("ro,ro,txt,txt,txt,txt");
	rep_grid.setColSorting("str,str,str,str,str,str");
	rep_grid.setInitWidthsP("10,10,15,15,24,24");	
	rep_grid.setColumnsVisibility("false,false,false,false,false,false");
	rep_grid.setSkin("dhx_skyblue");
	rep_grid.setImagePath(gdata.img_path);	
	rep_grid.init();
	
	gdata.rep_conf_grid = rep_grid;
	
	win.attachEvent("onClose", cleanRepConfWin);
	
}

function cleanRepConfWin(){
	gdata.rep_conf_grid = null;
	return true;
}

function getRepConfigChangesJSON(rids){
	
	var rowids = rids.split(",");
	var json = ",\"Config\":[";
	for(var i=0;i<rowids.length;i++){
		
		json = json+"{\"ID\":"+rowids[i];
		for (var j=0; j<gdata.rep_conf_grid.getColumnCount(); j++){
			json = json + ",\""+gdata.rep_conf_grid.getColumnId(j)+"\":\""+gdata.rep_conf_grid.cells(rowids[i],j).getValue()+"\""
		}
		json = json+"}";
		
		if(i<rowids.length-1){json=json+","}
	}
	json = json+"]"
	json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\""+json+"}";
	
	//alert(json)
	var json_obj = jQuery.parseJSON( json );
	return json_obj;

}

function saveRepConfig(json){
	var win = gdata.main_layout.dhxWins.window("win_rep_conf");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: rep_conf_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveRepConfig , 
    		error: CallbackSaveRepConfigFailed,
    		cache: false
	});
}

function CallbackSaveRepConfig(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_rep_conf");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Report configuration saved successfully."});
		gdata.rep_conf_grid.clearChangedState();
	}	
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Report configuration save failed."});
	}
	
}

function CallbackSaveRepConfigFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_rep_conf");
	win.progressOff();
	
}


function getParamRepConfig(qtype_id){
	
	var win = gdata.main_layout.dhxWins.window("win_rep_conf");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: rep_conf_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_Type_ID':qtype_id},
    		success:CallbackGetRepConfig , 
    		error: CallbackGetRepConfigFailed,
    		cache: false
	});
	
}

function CallbackGetRepConfig(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_rep_conf");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.rep_conf_grid.clearAll();
		
		for(var i=0;i<data.Rep_Config.length;i++){
			addRowToGrid(gdata.rep_conf_grid,data.Rep_Config[i])
		}
		
		gdata.rep_conf_grid.clearChangedState();
	
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving report parameterization."});
	}
	

}

function CallbackGetRepConfigFailed(jqXHR, textStatus, errorThrown){

	var win = gdata.main_layout.dhxWins.window("win_rep_conf");
	win.progressOff();

}




