function getDayAsString(){
	var d = new Date()
    var year =d.getFullYear().toString()
    var month  
     
    if((d.getMonth()+1)<10){month="0"+(d.getMonth()+1).toString()}else{month=(d.getMonth()+1).toString()}
    var day
    if(d.getDate()<10){day="0"+d.getDate().toString()}else{day=d.getDate().toString()}
    
    var today = year+"-"+month+"-"+day
	return today;
}

function formToJSON(form){
		 
	form.setItemValue("json","");
	form.forEachItem(function(id, radioValue){
   		if(form.getItemType(id)!="fieldset" && id!="json" && form.getItemType(id)!="upload" && form.getItemType(id)!="container" && form.getItemType(id)!="button")
   		 { 
   		 	if(form.getItemType(id)!="calendar"){
   		 		form.setItemValue("json",form.getItemValue("json")+",\""+id+"\":\""+form.getItemValue(id)+"\"")
   		 	}
   		 	else
   		 	{
   		 		var cal = form.getCalendar(id);
   		 		form.setItemValue("json",form.getItemValue("json")+",\""+id+"\":\""+cal.getDate(true)+"\"")
   		 	}
   		 }
	});
	var json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\",\"Is_Admin\":"+gdata.is_admin+form.getItemValue("json")+"}";
	//alert(json);
	var json_obj = jQuery.parseJSON( json );
	return json_obj;
}

function initFormWithData(form,data){
	
	for(var key in data){
			if(form.isItem(key))
			{form.setItemValue(key,data[key]);}
	}
}

function addRowToGrid(grid,data){
	
	var arr = [];
	
	for (var i=0; i<grid.getColumnCount(); i++){
		
		 var colId=grid.getColumnId(i);
		 if(data.hasOwnProperty(colId)){
			var val = data[colId] 
			arr.push(val);
		 }
		 else{
			arr.push(null);
		 }
    }

	var exist=grid.doesRowExist(data.ID);
	
	if(!exist){		
		grid.addRow(data.ID,arr)
	}
	else{
		var idx = grid.getRowIndex(data.ID);
		grid.deleteRow(data.ID);
		grid.addRow(data.ID,arr,idx)
	}
		
}
