function initSupplIDXWin(){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_suppl",0,0,800,600);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Supplementary Index");
	win.setIcon("../../../images/board-icon-16.png", "../../../images/board-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save','./images/save-icon-32.png','./images/save-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	toolbar.addButtonSelect('questionnaire', 2, 'Questionnaire', [], './images/test-icon-32.png','./images/test-icon-32.png');
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		toolbar.addListOption('questionnaire', 'q-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/question-mark-icon-32.png' )
	}
	
	
	toolbar.attachEvent('onClick', function(id){
		if(id=="save"){
			var tids=gdata.thres_grid.getChangedRows(false);
			var fids=gdata.factor_grid.getChangedRows(false);

			if(tids=="" && fids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
			else{
				var json_obj =	getSupplIDXChangesJSON(tids,fids);
				saveSupplIDX(json_obj)
			}
		}
		else if(id.indexOf("q-")==0){			 
			var qtype_id = id.split("-");		
			getParamSupplIDX(qtype_id[1]);
		}
	});
	
	var layout = win.attachLayout("2E");
	var idx_factors_cell = layout.cells('a');
	//idx_factors_cell.setWidth(300);
	idx_factors_cell.setText("Factors");
	
	var idx_thres_cell = layout.cells('b');
	idx_thres_cell.setText("Thresholds");
	
	var factor_grid = idx_factors_cell.attachGrid();	
	factor_grid.setHeader("Suppl. Scale Code,Scale Code,Factor");
	factor_grid.setColumnIds("Scale_ID_SUPL,Scale_ID,Factor");
	factor_grid.setColTypes("coro,coro,ed");
	factor_grid.setColSorting("str,str,str");
	factor_grid.setInitWidthsP("40,40,19");	
	factor_grid.setSkin("dhx_skyblue");
	factor_grid.setImagePath(gdata.img_path);	
	factor_grid.init();
	
	var thres_grid = idx_thres_cell.attachGrid();	
	thres_grid.setHeader("Suppl. Scale Code,Function,Scale Code 1,Op.1,Scale Code 2,Op.2,Scale Code 3,Op.CMP,Threshold,True,False");
	thres_grid.setColumnIds("Scale_ID_SUPL,Function,Scale_ID_1,Operand_1,Scale_ID_2,Operand_2,Scale_ID_3,Operand_CMP,Threshold,Result_True,Result_False");
	thres_grid.setColTypes("coro,ro,coro,ro,coro,ro,coro,ro,ed,ed,ed");
	thres_grid.setColSorting("str,str,str");
	thres_grid.setInitWidthsP("20,8,10,5,10,5,10,8,10,7,6");	
	thres_grid.setSkin("dhx_skyblue");
	thres_grid.setImagePath(gdata.img_path);	
	thres_grid.init();
	
	win.attachEvent("onClose", cleanSupplIDXWin);
	
	gdata.thres_grid = thres_grid;
	gdata.factor_grid = factor_grid;
	
	
}

function cleanSupplIDXWin(){
	gdata.thres_grid = null;
	gdata.factor_grid = null;
	return true;
}

function SupplIDXToolbarClicked(id){
	
	if(id=="save"){
		var tids=gdata.thres_grid.getChangedRows(false);
		var fids=gdata.factor_grid.getChangedRows(false);

		if(tids=="" && fids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
		else{
			var json_obj =	getSupplIDXChangesJSON(tids,fids);
			saveSupplIDX(json_obj)
		}

	}

}

function getSupplIDXChangesJSON(tids,fids){
	
	var json = "";
	
	if(tids!=""){
		
		json = ",\"Thresholds\":[";
		var rowids = tids.split(",");
		
		for(var i=0;i<rowids.length;i++){
		
			json = json+"{\"ID\":"+rowids[i];
			for (var j=0; j<gdata.thres_grid.getColumnCount(); j++){
				json = json + ",\""+gdata.thres_grid.getColumnId(j)+"\":\""+gdata.thres_grid.cells(rowids[i],j).getValue()+"\""
			}
			json = json+"}";
		
			if(i<rowids.length-1){json=json+","}
		}
		json = json+"]"
		
	}
	
	if(fids!=""){
		
		json = json+",\"Factors\":[";
		var rowids = fids.split(",");
		
		for(var i=0;i<rowids.length;i++){
		
			json = json+"{\"ID\":"+rowids[i];
			for (var j=0; j<gdata.factor_grid.getColumnCount(); j++){
				json = json + ",\""+gdata.factor_grid.getColumnId(j)+"\":\""+gdata.factor_grid.cells(rowids[i],j).getValue()+"\""
			}
			json = json+"}";
		
			if(i<rowids.length-1){json=json+","}
		}
		json = json+"]"
		
	}
	
	json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\""+json+"}";
	
	//alert(json)
	var json_obj = jQuery.parseJSON( json );
	return json_obj;
}

function saveSupplIDX(json_obj){
	
	var win = gdata.main_layout.dhxWins.window("win_suppl");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: suppl_idx_set,
    		dataType: 'json',
    		data: json_obj,
    		success:CallbackSaveSupplIDX , 
    		error: CallbackSaveSupplIDXFailed,
    		cache: false
	});

}

function CallbackSaveSupplIDX(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_suppl");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Supplementary index parameterization saved successfully."});
		gdata.factor_grid.clearChangedState();
		gdata.thres_grid.clearChangedState();
	}	
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Supplementary index  parameterization save failed."});	
	}
}

function CallbackSaveSupplIDXFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_suppl");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Supplementary index  parameterization save failed."});
}

function getParamSupplIDX(qtype_id){
	
	var win = gdata.main_layout.dhxWins.window("win_suppl");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: suppl_idx_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_Type_ID':qtype_id},
    		success:CallbackGetSupplIDX , 
    		error: CallbackGetSupplIDXFailed,
    		cache: false
	});
}

function CallbackGetSupplIDX(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_suppl");
	win.progressOff();
	
	if(data.Result_Code==0){
		//populateQuestionsGrid(data.Questions)	
		//gdata.icn_grid.parse(data.Rules,"json");
		
		var combo_1 = gdata.thres_grid.getCombo(0);
		var combo_2 = gdata.thres_grid.getCombo(2);
		var combo_3 = gdata.thres_grid.getCombo(4);
		var combo_4 = gdata.thres_grid.getCombo(6);
		var combo_5 = gdata.factor_grid.getCombo(0);
		var combo_6 = gdata.factor_grid.getCombo(1);
		
		combo_1.put("-1","");
		combo_2.put("-1","");
		combo_3.put("-1","");
		combo_4.put("-1","");
		
		for(var i=0;i<data.Scales.length;i++){
			combo_1.put(data.Scales[i].value,data.Scales[i].text);
			combo_2.put(data.Scales[i].value,data.Scales[i].text);
			combo_3.put(data.Scales[i].value,data.Scales[i].text);
			combo_4.put(data.Scales[i].value,data.Scales[i].text);
			combo_5.put(data.Scales[i].value,data.Scales[i].text);
			combo_6.put(data.Scales[i].value,data.Scales[i].text);
		}
		
		gdata.factor_grid.clearAll();
		
		for(var i=0;i<data.Factors.length;i++){
			var arr = [];
			var rule = data.Factors[i]
			for(var x in rule){
  				if(x!="ID"){
  					arr.push(rule[x]);
  				}
			}			
			gdata.factor_grid.addRow(data.Factors[i].ID,arr)
		}
		
		gdata.thres_grid.clearAll();
		
		for(var i=0;i<data.Thresholds.length;i++){
			var arr = [];
			var rule = data.Thresholds[i]
			for(var x in rule){
  				if(x!="ID"){
  					var val = new String();
  					if(rule[x]==null && x.indexOf("Scale")>-1){val = "-1";}
  					else {val=rule[x]}
  					 
  					arr.push(val);
  				}
			}			
			gdata.thres_grid.addRow(data.Thresholds[i].ID,arr)
		}
		gdata.factor_grid.clearChangedState();
		gdata.thres_grid.clearChangedState();
	
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving supplementary index parameterization."});
	}
}

function CallbackGetSupplIDXFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_suppl");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error."});
}




