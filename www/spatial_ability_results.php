<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	include('server/database_slave.php');
    $database = new database_slave();
    			
    //$hash_code = "123";
    //$client_id = "1";
    
    $hash_code = substr($_GET['q'],0,-1);
	$language_id = substr($_GET['q'], -1);
    
    /*if(isset($_GET['q']) && strlen($_GET['q'])>1){
    	$parts = explode("_",$_GET['q']);
    	$hash_code = $parts[0];
    	$client_id = $parts[1];
    }*/
    $record = $database->get_db_record("Tests",$hash_code,"Hash_Code");
     
    $crecord = $database->get_db_record("Clients",$record["Client_ID"]);
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title><?php if($crecord==false){echo "Error";}else{echo $crecord["Surname"]." ".$crecord["Name"];} ?></title>

    	<link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
  		<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">
		<link href="results.css" rel="stylesheet">
		
		<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
  		
  		
  		
  	</head>
	<body>
    	<div class="container">
    		
    		<div class="page-header">
    			<h3 class="text-muted"><? echo $crecord["Surname"]." ".$crecord["Name"].":&nbspSpatial Ability Test"; ?></h3>
    		</div>
    		
    		
    		<?php 
  		  
    			$test_answers = $database->get_test_answers($record["ID"],$record["Test_Type_ID"]);
    			
    			if($test_answers->num_rows > 0){
  					
  					$prev_section_id = -1;				
    				$prev_exercise_id = -1; 
  					$max_score = 0 ;
  					$score = 0 ;
  					
  					echo "<div class=\"panel panel-default\">
  							<div class=\"panel-heading\"><b>Summary Results</b></div>
  							<table class=\"table\">";
  					while($row = $test_answers->fetch_array(MYSQLI_ASSOC)) { 
  						
  						if($row["Section_ID"]!=$prev_section_id){
  							if($prev_section_id!=-1){
  								//echo $score."/".$max_score."<br>";
  								echo "<td>".$score."/".$max_score."</td></tr>";
  							}
  							echo "<tr><td>".$row["Section_Descr"]."</td>";
  							
  							//echo $row["Section_Descr"].":";
  							$max_score = 0 ;
  							$score = 0 ;
  						}
  						if($row["Exercise_ID"]!=$prev_exercise_id){
  							$max_score = $max_score + $row["Exercise_Pass_Score"];
  						}
  						if($row["Test_Answer"]=="T"){
  							$score = $score + $row["Option_Score"];
  						}
  						$prev_section_id = $row["Section_ID"];
  						$prev_exercise_id = $row["Exercise_ID"];
  					}
  					echo "<td>".$score."/".$max_score."</td></tr>";
  					echo "</table></div>";
  					
  					$test_answers->data_seek(0);
  					
    				$prev_section_id = -1;				
    				$prev_exercise_id = -1; 
    				
    				while($row = $test_answers->fetch_array(MYSQLI_ASSOC)) {    					
    					    					
    					if($row["Section_ID"]!=$prev_section_id){   			 
    						   						
    						if($prev_section_id!=-1){    							
    							echo "</table></div>";
    						}  
   							echo "<div class=\"panel panel-default\">";
    						echo "<table class=\"table\">";

     					}
    					if($row["Exercise_ID"]!=$prev_exercise_id){   			 
    						
    						if($prev_exercise_id!=-1){echo "</tr>";} 
    					    						
    						$suffix = "";
    						if($row["Is_Answered"]=="F"){$suffix = "<font color=\"red\">(not answered)</font>";}
    						
    						echo "<tr><td><img src='".$row["Exercise_Descr"]."'> ".$suffix."</td>";
    						
    						//echo "<div class=\"well\"><label>".$row["Exercise_Code"]." ".$suffix."</label></div>";  						 
     					}     					
     					
     					$color = "black";
     					$suffix = "";
     					
     					if($row["Test_Answer"]=="T" && $row["Option_Score"] !="1"){
     						$color="red";
     						$suffix = "&nbsp&nbsp(answered)";
     					}     					
     					else if($row["Option_Score"]=="1")
     					{
     						$color="green";
     					}
     					
     					 
     					
     					echo "<td><div class=\"radio\">";
    					echo 	"<label><font color=\"".$color."\"><input type=\"radio\" disabled name=\"exercise_".$row["Exercise_ID"]."\" value=\"".$row["Option_ID"]."\">".$row["Option_Descr"].$suffix."</font></label>";
    					echo "</div></td>";
     					 
    					$prev_section_id = $row["Section_ID"];
    					$prev_exercise_id = $row["Exercise_ID"];
    				}
    				
    				//echo "</div></div>";
    				 
    			}
    			else{
    				$lb_error_rec = $database->get_db_record("META_UI_Labels","ANSWERS_NOT_EXIST","Code");
    				echo "  		
	 		 			<div class=\"alert alert-danger\">
    						".$lb_error_rec["Descr_en"]."	 
    					</div>";
    				
    			}
    			
    		?>
    		 
    	</div>
	</body>
</html>








