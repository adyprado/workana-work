<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	
  
    	include('server/database_slave.php');
    	$database = new database_slave();
    	
    	$info = $database->get_questionnaire_info($_GET["q1"]);
    	$language_code = "en";
    	if(!$info){echo "Invalid questionnaire";}
    	else{
    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? echo $info["Surname"]." ".$info["Name"]?></title>

    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">

	
	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
  	<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
  	<script type="text/javascript" src="graphs.js"></script>
  	<script type="text/javascript" src="html2canvas.js"></script>
  	<script src="canvas2image.js"></script>
	<script src="base64.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
		
		var chart_list=new Array;
		
		function drawChart() {
			
			var data_url =  "server/questionnaire_data_cmp.php?q1="+<? echo "'".$_GET['q1']."&q2=".$_GET['q2']."'"; ?>+"" 
         	//alert(data_url)
        	$.ajax({
    			type: 'GET',
    			url: data_url,
    			dataType: 'json',
    			success:CallbackQuestionnaireData , 
    			error: CallbackQuestionnaireDataFailed,
    			cache: false
			}); 
			
			drawCircleScatterChart("scatter_1","High Psychopathology","Low Psychopathology","High Addiction","Low Addiction")
        	drawCircleScatterChart("scatter_2","High Psychopathology","Low Psychopathology","High Risk","Low Risk")
        	drawCircleScatterChart("scatter_3","High Empathy","Low Empathy","High Assertiveness","Low Assertiveness")
			
		}
		
		function CallbackQuestionnaireData(data, textStatus, jqXHR){
			
			var scale_asrt_1;
      		var scale_empt_1;
      		var scale_ansc_1;
      		var scale_mean_clinical_1;
      		var scale_addiction_1;
      		var scale_asrt_2;
      		var scale_empt_2;
      		var scale_ansc_2;
      		var scale_mean_clinical_2;
      		var scale_addiction_2;
      		var comments_1;
      		var comments_2;
      		
      		for (item in data) {       		
      			if(item!="Means"){
      				
      				comments_1 = data[item][0].Comments_1
      				comments_2 = data[item][0].Comments_2
      				
      				var barChart1 = new dhtmlXChart({
    							view: "line",
   								container: document.getElementById(item),//"chartDiv",
    							value: "#A_Score_1#",
    							label: "#A_Score_1#",
    							color: "#58dccd",
    							width: 7,
    							gradient: "rising",
    							radius: 0,
   								tooltip: {
        							template: "#Scale_Code#<br>#Comments_1#<br>#A_Score_1#"
    							},
   								xAxis: {
        							title: "",
        							template: "#Scale_Code#"
        							
   								 },
    							yAxis: {
        							title: "AScore"
    							},
    							legend:{
									values:[{text:comments_1,color:"#58dccd"},{text:comments_2,color:"#a7ee70"}],
									valign:"top",
									align:"center",
									width:60,
									layout:"x"
								},
								line:{
                     				color:"#58dccd",
                     				width:3
                				},
    							origin: 0
		 				});
					
						barChart1.addSeries({
	    					value:"#A_Score_2#",
							color:"#a7ee70",
							label:"#A_Score_2#",
							tooltip: {
        							template: "#Scale_Code#<br>#Comments_2#<br>#A_Score_2#"
    							},
    						line:{
                     				color:"#a7ee70",
                     				width:3
                				}
						});
					
					barChart1.parse(data[item], "json");
					
					for(var i=0;i<data[item].length;i++){
					//alert(data[item][i].Scale_Code);
						if(data[item][i].Scale_Code=="ASRT"){
							scale_asrt_1=data[item][i].A_Score_1
							scale_asrt_2=data[item][i].A_Score_2
						}
						if(data[item][i].Scale_Code=="EMPT"){
							scale_empt_1=data[item][i].A_Score_1
							scale_empt_2=data[item][i].A_Score_2
						}
						if(data[item][i].Scale_Code=="ANSC"){
							scale_ansc_1=data[item][i].A_Score_1
							scale_ansc_2=data[item][i].A_Score_2
						}
					}
			   
			   
			   		chart_list.push(barChart1);
			   
				}
				else{
					for(var i=0;i<data[item].length;i++){
						if(data[item][i].Code=="MCS"){
							scale_mean_clinical_1=data[item][i].Score_1
							scale_mean_clinical_2=data[item][i].Score_2
						}
						if(data[item][i].Code=="ASME"){
							scale_addiction_1=data[item][i].Score_1
							scale_addiction_2=data[item][i].Score_2
						}
					}
				}
      		}
      		
      		drawPointOnScatterChart("scatter_1",scale_mean_clinical_1,scale_addiction_1,"./images/red-icon-8.png",comments_1,0)
      		drawPointOnScatterChart("scatter_2",scale_mean_clinical_1,scale_ansc_1,"./images/red-icon-8.png",comments_1,0)
      		drawPointOnScatterChart("scatter_3",scale_empt_1,scale_asrt_1,"./images/red-icon-8.png",comments_1,0)
      		drawPointOnScatterChart("scatter_1",scale_mean_clinical_2,scale_addiction_2,"./images/green-icon-8.png",comments_2,1)
      		drawPointOnScatterChart("scatter_2",scale_mean_clinical_2,scale_ansc_2,"./images/green-icon-8.png",comments_2,1)
      		drawPointOnScatterChart("scatter_3",scale_empt_2,scale_asrt_2,"./images/green-icon-8.png",comments_2,1)
			
		}
		
		function CallbackQuestionnaireDataFailed(jqXHR, textStatus, errorThrown){
      		alert("error loading graph data")
      	}
      	
      	function printDiv(divId){
      		html2canvas(document.getElementById(divId), {
      			onrendered: function(canvas) {
      				Canvas2Image.saveAsPNG(canvas);
      			}
      		});
      }
		
	</script>
	
  </head>
  
  <body onLoad="drawChart()">
  	<?php echo "<img src='./images/LOGO_FILISTOS_".$language_code.".png' height='66' width='150' class=\"logo_image\" style=\"margin-left:50px;\">";?>  
  	<div class="container">    
        
    <div class="header">
        <h3 class="text-muted"><? echo $info["Surname"]." ".$info["Name"]?></h3>
        
        <small class="print_only">
        <table>
    	 	<tr><td>Birthdate</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Birthdate"]?></td></tr>
    	 	<tr><td>Gender</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Gender"]?></td></tr>
    	 	<tr><td>Marital Status</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Marital_Status"]?></td></tr> 
    	 	<tr><td>Profession</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Profession"]?></td></tr>     	 	 
  		</table>
  		</small>
        <br>
        
    </div>
    
    <!--<div class="jumbotron" id="bar_chart_div">aa</div>-->
    
    <ul class="nav nav-tabs">
  		<li class="active"><a href="#graphs" data-toggle="tab">Graphs</a></li>
  		<li><a href="#circle_graphs" data-toggle="tab">Scatter Charts</a></li>
  		
	</ul>
    
    <div class="tab-content">
    	<div class="tab-pane active" id="graphs">
    		<div class="row marketing">
    	<? 
    		//$groups = $database->get_recordset("Scale_Groups_Rep");//,"MOD(ID,2)",$i);
    		$groups = $database->get_scale_groups_rep_for_questionnaire_type($info["Questionnaire_Type_ID"]);
    		$i=0;
    		while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
    			    
    			    $print_style = "";
    				//if($i>1){$print_style="panel-print-2";}else{$print_style="panel-print-1";}
    			 	if($i%2==0){$print_style="panel-print-1";}else{$print_style="panel-print-2";}
    			 	
    				echo "<div class=\"panel panel-default ".$print_style."\">
    						<div class=\"panel-heading\">".$g["Descr_en"]." <button style='float: right;' onClick=\"printDiv('".$g["ID"]."')\">Download</button></div>
    						<div class=\"panel-body\" id=\"".$g["ID"]."\"></div>
    					  </div>"; 				
    			 
    				$i=$i+1;
    		}
    	?>		
    		</div>
		</div>		
		
		<div class="tab-pane" id="circle_graphs">
			<div class="row marketing">
				<div class="panel panel-default panel-print-2">
					<div class="panel-heading">Addiction / Psychopathology</div>
					<div class="panel-body">
						<canvas id="scatter_1" width="800" height="400" style="background-color:#ffffff"></canvas>
					</div>
				</div>
				<div class="panel panel-default panel-print-2">
					<div class="panel-heading">Risk / Psychopathology</div>
					<div class="panel-body">
						<canvas id="scatter_2" width="800" height="400" style="background-color:#ffffff"></canvas>
					</div>
				</div>
				<div class="panel panel-default panel-print-2">
					<div class="panel-heading">Assertiveness / Empathy</div>
					<div class="panel-body">
						<canvas id="scatter_3" width="800" height="400" style="background-color:#ffffff"></canvas>
					</div>
				</div>
			
			
			</div>		
		</div>		
			
	</div>
	</div>
  	
  </body>
  
  </html>
  <?php }?>