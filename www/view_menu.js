function initOptionsMenu(){
	
	var config = {
	css: "borderless",
	type:{
		template:"<br><div align='center'>#Image#<br/><b>#Text#</b></div><br/>",
		padding:0,
		height:80,
		width:80
	}
    };
	
	gdata.layout_main.cells("a").setWidth(100);//document.body.clientWidth*0.1);
	gdata.layout_main.cells("a").setText("Options");
	
	var menu = gdata.layout_main.cells("a").attachDataView(config);  
	//menu.add({id:"clients",Image:"<img src='./images/clients-icon-48.png'>",Text:"Clients"});
	
	if(gdata.is_admin==1){	
		menu.add({id:"data_access",Image:"<img src='./images/access-icon-48.png'>",Text:"Access"});
		menu.add({id:"logout",Image:"<img src='./images/logout-icon-48.png'>",Text:"Logout"}) ;
		menu.add({id:"sep_1",Image:"",Text:""});
		menu.add({id:"questionnaire",Image:"<img src='./images/questionnaire-icon-48.png'>",Text:"Questionnaire"})
		menu.add({id:"simple_scales",Image:"<img src='./images/ruler-icon-48.png'>",Text:"Simple Scales"})
		menu.add({id:"icn_scales",Image:"<img src='./images/icn-icon-48.png'>",Text:"ICN"})
		menu.add({id:"supl_idx_scales",Image:"<img src='./images/board-icon-48.png'>",Text:"Indexes"})
		menu.add({id:"a_score_functs",Image:"<img src='./images/function-icon-48.png'>",Text:"Functions"}) 
		menu.add({id:"report_txt",Image:"<img src='./images/report-icon-48.png'>",Text:"Report"})
		 
		
	}
	else{
		menu.add({id:"profile",Image:"<img src='./images/profile-icon-48.png'>",Text:"Profile"});
		menu.add({id:"logout",Image:"<img src='./images/logout-icon-48.png'>",Text:"Logout"}) ;
		 
		
	}
	//board-icon-48.png
	menu.attachEvent("onItemClick", function(itemId) {
        //alert("click. Item :: " + itemId);
              
        if(itemId=="icn_scales"){
        	initICNWin()
        }
        else if(itemId=="supl_idx_scales"){
        	initSupplIDXWin();
        }
        else if(itemId=="questionnaire"){
        	initQuestionsWin();
        }
        else if(itemId=="simple_scales"){
        	initSimpleScalesWin();
        }
        else if(itemId=="a_score_functs"){
        	initAScoreFunctionsWin();
        }
        else if(itemId=="data_access"){
        	var win = gdata.main_layout.dhxWins.createWindow("win_user_list",0,0,600,400);
        	initViewUsers(win)
        	win.centerOnScreen();
			win.setModal(true);
			win.show();
        }
        else if(itemId=="profile"){
        	getUserInfo(gdata.uid)
        }
        else if(itemId=="report_txt"){
        	initReportConfigWin();
        }
        else if(itemId=="logout"){
        	dhtmlx.confirm({type:"confirm",text: "Are you sure you want to logout?",callback: function(result){if(result){location.reload();}}});
        		
        }
        
        
    });
	
	 menu.attachEvent("onBeforeSelect", function (id, state){
       //any custom logic here
       if(id=="sep_1"){return false;}
       
       return true;
  	});
}