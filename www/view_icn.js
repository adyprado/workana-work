//var vehicle_layout = gdata.form_cell.view('v_vehicle').attachLayout('1C');
function initICNWin(){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_icn",0,0,800,400);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("ICN");
	win.setIcon("../../../images/icn-icon-16.png", "../../../images/icn-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save','./images/save-icon-32.png','./images/save-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	toolbar.addButtonSelect('questionnaire', 2, 'Questionnaire', [], './images/test-icon-32.png','./images/test-icon-32.png');
	
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		toolbar.addListOption('questionnaire', 'q-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/question-mark-icon-32.png' )
	}
	
	toolbar.attachEvent('onClick', function(id){
		 
		if(id=="save"){
			var rids=gdata.icn_grid.getChangedRows(false);		
			if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
		}
		else if(id.indexOf("q-")==0){
			 
			var qtype_id = id.split("-");		
			getParamICN(qtype_id[1]);
		}
		else{
			var json_obj =	getICNChangesJSON(rids);
			saveParamICN(json_obj)
		}

	 	
	});
	
	var icn_grid = win.attachGrid();	
	icn_grid.setHeader("Scale Code,Factor 1,Op.1,Question 1,Factor 2,Op.2,Question 2,Factor 3,Op.3,Question 3,Factor 4,Op.4,Question 4");
	icn_grid.setColumnIds("Scale_ID,Factor_1,Operand_1,Question_1,Factor_2,Operand_2,Question_2,Factor_3,Operand_3,Question_3,Factor_4,Operand_4,Question_4");
	icn_grid.setColTypes("coro,ed,coro,ed,ed,coro,ed,ed,coro,ed,ed,coro,ed");
	icn_grid.setColSorting("str,str,str");
	icn_grid.setInitWidthsP("16,7,5,9,7,5,9,7,5,9,7,5,9");	
	//icn_grid.setColumnsVisibility("false,false,true");
	icn_grid.setSkin("dhx_skyblue");
	icn_grid.setImagePath(gdata.img_path);	
	icn_grid.init();
	
	op1_cb = icn_grid.getCombo(2);
	op2_cb = icn_grid.getCombo(5);
	op3_cb = icn_grid.getCombo(8);
	op4_cb = icn_grid.getCombo(11);
	
	op1_cb.put("-","-");op1_cb.put("+","+");
	op2_cb.put("-","-");op2_cb.put("+","+");
	op3_cb.put("-","-");op3_cb.put("+","+");
	op4_cb.put("-","-");op4_cb.put("+","+");
	
	gdata.icn_grid = icn_grid;
	
	win.attachEvent("onClose", cleanICNWin);
	
	
}

function cleanICNWin(){
	gdata.icn_grid = null;

	return true;
}

function ICNToolbarClicked(id){
	
	if(id=="save"){
		var rids=gdata.icn_grid.getChangedRows(false);		
		if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
		else{
			var json_obj =	getICNChangesJSON(rids);
			saveParamICN(json_obj)
		}

	}	
}

function getICNChangesJSON(rids){
	
	var rowids = rids.split(",");
	var json = ",\"Rules\":[";
	for(var i=0;i<rowids.length;i++){
		
		json = json+"{\"ID\":"+rowids[i];
		for (var j=0; j<gdata.icn_grid.getColumnCount(); j++){
			json = json + ",\""+gdata.icn_grid.getColumnId(j)+"\":\""+gdata.icn_grid.cells(rowids[i],j).getValue()+"\""
		}
		json = json+"}";
		
		if(i<rowids.length-1){json=json+","}
	}
	json = json+"]"
	json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\""+json+"}";
	
	//alert(json)
	var json_obj = jQuery.parseJSON( json );
	return json_obj;
}

function saveParamICN(json){
	var win = gdata.main_layout.dhxWins.window("win_icn");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: icn_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveParamICN , 
    		error: CallbackSaveParamICNFailed,
    		cache: false
	});
}

function CallbackSaveParamICN(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_icn");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"ICN parameterization saved successfully."});
		gdata.icn_grid.clearChangedState();
	}	
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"ICN parameterization save failed."});
	}
	
}

function CallbackSaveParamICNFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_icn");
	win.progressOff();
	
}

function getParamICN(qtype_id){
	
	var win = gdata.main_layout.dhxWins.window("win_icn");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: icn_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_Type_ID':qtype_id},
    		success:CallbackGetICN , 
    		error: CallbackGetICNFailed,
    		cache: false
	});
}

function CallbackGetICN(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_icn");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		var combo = gdata.icn_grid.getCombo(0);
		
		for(var i=0;i<data.ICN_Scales.length;i++){
			combo.put(data.ICN_Scales[i].value,data.ICN_Scales[i].text);		 	
		}
		
		gdata.icn_grid.clearAll();
		
		for(var i=0;i<data.Rules.length;i++){
			var arr = [];
			var rule = data.Rules[i]
			for(var x in rule){
  				if(x!="ID"){
  					arr.push(rule[x]);
  				}
			}			
			gdata.icn_grid.addRow(data.Rules[i].ID,arr)
		}
		gdata.icn_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving ICN parameterization."});
	}
}

function CallbackGetICNFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_icn");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error."});
}


