<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
?>

<head>
	
	<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">
	<link rel="stylesheet" type="text/css" href="custom_style.css">
	
	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
	 
	<!--<link rel="stylesheet" type="text/css" href="dhtmlxSuite/dhtmlx_std_full/dhtmlx.css">
	<script src="dhtmlxSuite/dhtmlx_std_full/dhtmlx.js"></script>-->	
	<script src="jquery-2.0.3.js" type="text/javascript"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	
	
	<script src="constants.js"></script>
	<script src="utilities.js"></script>
	<script src="authentication.js"></script>
	<script src="view_reports_config.js"></script>
	<script src="view_clients.js"></script>
	<script src="view_menu.js"></script>
	<script src="view_questionnaire.js"></script>
	<script src="view_questionnaires_list.js"></script>
	<script src="view_tests_list.js"></script>
	<script src="view_icn.js"></script>
	<script src="view_suppl_idx.js"></script>
	<script src="view_users.js"></script>
	<script src="view_user_card.js"></script>
	<script src="view_questions.js"></script>
	<script src="view_simple_scales.js"></script>
	<script src="view_questionnaire_simple_scales.js"></script>
	<script src="view_question_list_popup.js"></script>
	<script src="view_ascore_functions.js"></script>
	<script src="layout.js"></script>
	
</head>

<body onload="initAppUI();">
</body>
