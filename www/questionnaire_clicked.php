<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	include('server/database_slave.php');
    $database = new database_slave();
    			
    $hash_code = "-1";
    $client_id = "-1";
    		
    if(isset($_GET['q']) && strlen($_GET['q'])>1){
    	$parts = explode("_",$_GET['q']);
    	$hash_code = $parts[0];
    	$client_id = $parts[1];
    }
    $record = $database->get_db_record("Questionnaires",$hash_code,"Hash_Code");
    $crecord = $database->get_db_record("Clients",$client_id);
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title><?php if($crecord==false){echo "Error";}else{echo $crecord["Surname"]." ".$crecord["Name"];} ?></title>

    	<link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
  		<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">
		<link href="results.css" rel="stylesheet">
		
		<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
  		
  		<script type="text/javascript">
  			
  			//var time_in;
  			
  			function focusElement(){
 				
  				setInterval(function () {
  					var secs = document.getElementById('time_secs').value;
  					secs = Number(secs) + 1;
  					document.getElementById('time_secs').value = secs;
  				}, 1000);
  				
  				var first_question = document.getElementById("first_question");
  				var radio_button = document.getElementsByName(first_question.value);
  				if(radio_button != null){
  					radio_button[0].focus();
  				}
  			
  			}  
  			function radioButtonClick(radio){
  				var active_radio = document.getElementsByName(document.activeElement.name);  				 			
  				var radio_group = radio.getAttribute("name"); 
  				clearPreviousRadioGroupSelection(radio_group,radio);  				 
  				radio.focus();
  			}
  			
  			function focusCheckedRadioButton(radio_group){
  				var box_selected = false;
  				for (var i = 0; i < radio_group.length; i++) {
        			var button = radio_group[i];
        			if (button.checked) {
            			button.focus();
            			box_selected = true;
        			}
    			}
    			if(!box_selected){
    				var button = radio_group[0];
    				button.focus();
    			}
  			}
  			function clearPreviousRadioGroupSelection(radio_group,checkbox){
  				//alert(radio_group)  				
  				group = document.getElementsByName(radio_group)
  				for (var i = 0; i < group.length; i++) {  					 
  					var button = group[i];
  					if (button.checked && button != checkbox) {
  						button.checked = false;
  					}
  				}
  			}
  			
  			function radioButtonKeyDown(e){
  				
  				var code = e.keyCode;  				
  				var active_radio = document.getElementsByName(document.activeElement.name);
  				 
  				var radio_group = document.activeElement.name
  				var parts = radio_group.split("_");
  				var radio_group_num = parseInt(parts[1])
  				
  				var next_radio_group_num = (radio_group_num+1) //next radio group num
  				var prev_radio_group_num = (radio_group_num-1)
  				
  				var next_radio_group_name = "answer_"+next_radio_group_num;
  				var prev_radio_group_name = "answer_"+prev_radio_group_num;
  				
  				var next_radio_group = document.getElementsByName(next_radio_group_name);
  				var prev_radio_group = document.getElementsByName(prev_radio_group_name);
  				//alert(next_radio_group)
  				  				
  				if(code=="38"){
					if(prev_radio_group!=null){focusCheckedRadioButton(prev_radio_group);e.preventDefault();} 
  				}
  				else if(code=="40"){  				
  					if(next_radio_group!=null){focusCheckedRadioButton(next_radio_group);e.preventDefault();} 
  				}
  				else{
  					var answer = String.fromCharCode(code)
  					var selected_radio_button = "answer_"+radio_group_num+"_"+answer
  					var checkbox = document.getElementById(selected_radio_button)
					if(typeof checkbox != 'undefined' && checkbox != null){
						clearPreviousRadioGroupSelection(radio_group,checkbox)
						checkbox.checked = true;
						checkbox.focus();
					}
  				}
  				
  				/*else if(code=="49"){ // || code=="50" || code=="51" || code == "52"){
  					active_radio[0].checked= true;
  					active_radio[0].focus();
  				}
  				else if(code=="50"){
  					active_radio[1].checked= true;
  					active_radio[1].focus();
  				}
  				else if(code=="51"){
  					active_radio[2].checked = true;
  					active_radio[2].focus();
  				}
  				else if(code=="52"){
  					active_radio[3].checked= true;
  					active_radio[3].focus();
  				}*/
  				return true;
  			}
  			
  			function CheckAndSubmit(){
  				
  				var prev_box = "";
  				var answered = false;
  				var result = true;
  				var is_first = true; 
  				var first_empty = ""; 
  				var first_empty_found = false;
  				
  				for(i=0; i<document.frm_questionnaire.elements.length; i++){
  					  					
  					if(document.frm_questionnaire.elements[i].type=="checkbox"){  						
  						 
  						if(is_first){
  							prev_box=document.frm_questionnaire.elements[i].name;
  							is_first=false;
  						} 
  						var div_name = "div_"+document.frm_questionnaire.elements[i].name
  						
  						if(document.frm_questionnaire.elements[i].name != prev_box){  						 							
  							var prev_div_name = "div_"+prev_box
  							if(!answered){  								
  								document.getElementById(prev_div_name).style.color = '#ff0000';
  								result = false;
  								if(!first_empty_found){
  									first_empty = prev_div_name
  									first_empty_found = true;
  								}
  							}
  							prev_box = document.frm_questionnaire.elements[i].name;
  							answered = false;
  						}
  						
  						if(document.frm_questionnaire.elements[i].checked){
  							answered = true;
  							document.getElementById(div_name).style.color = '#000000';
  						}  					
  					}
  				}
  				
  				if(!answered){
  					result = false;
  					var div_name = "div_"+prev_box
  					document.getElementById(div_name).style.color = '#ff0000';
  					if(!first_empty_found){
  						first_empty = div_name
  						first_empty_found = true;  					
  					}
  				}
  				
  				if(!result){
					var scroll_div = "#"+first_empty
					$('html, body').animate({
        				scrollTop: $(scroll_div).offset().top
    				}, 1000);
  				}  				
  				return result;
  			}
  			
  		</script>
  		
  	</head>
	<body onLoad="focusElement()">
    	<div class="container">
			
			<?php 
    			
    			
    			if($record == false || $crecord==false){
    				echo "  <div class=\"header\">
        						<h3 class=\"text-muted\">Something went wrong!</h3>
    						</div>
	 		 		
	 		 				<div class=\"alert alert-danger\">
    							Questionnaire not found.	 
    						</div>";
    			} 
    			else{
    				$client_id = $crecord["ID"];
    				//$crecord = $database->get_db_record("Clients",$record["Client_ID"]);    			    			
    				$lrecord = $database->get_db_record("Languages",$crecord["Language_ID"]);
					$language_code = $lrecord["Code"];   	
    				
    				if($record["Online_Access"]==0){
    					$lb_questionnaire_rec = $database->get_db_record("META_UI_Labels","PROCESS_DISABLED","Code");
    					echo "<div class=\"alert alert-danger\">".$lb_questionnaire_rec["Descr_".$language_code]."</div>";
    				}
    				else{
    					$lb_questionnaire_rec = $database->get_db_record("META_UI_Labels","QUESTIONNAIRE","Code");
    					$lb_save_rec = $database->get_db_record("META_UI_Labels","SAVE","Code");
    				
    					$info = $database->get_questionnaire_info($hash_code);    			 
    					if(!$info){
    						echo "error";
    					}
   			 ?>
			 
			 			<div class="header">
        					<h3 class="text-muted"><? echo $info["Surname"]." ".$info["Name"]?></h3>
    					</div>
			
						<ul class="nav nav-tabs">
  							<li class="active"><a href="#questionnaire" data-toggle="tab">
  								<table><tr> 
  								<? echo "<td><img src='images/".$info["Questionnaire_Type_Descr"].".png' width='48' height='48'/></td>"; 
  								   echo "<td>".$lb_questionnaire_rec["Descr_".$language_code]."</td>" ?></tr></table></a>  							
  							</li>
  		 				</ul> 
			
						<div class="tab-content">
							<div class="tab-pane active" id="questionnaire">
								<?
								$filter_cols = array("Report_Name"=>"REP_INSTRUCTIONS","Questionnaire_Type_ID"=>$record["Questionnaire_Type_ID"]);
								
								$instructions = $database->get_db_recordset("PARAM_Rep_Full",$filter_cols,"ID");
								$descr_col = "Descr_".$language_code;
								
								if($instructions){
									echo "<br><br><div align=\"justify\">";
									while($row = $instructions->fetch_array(MYSQLI_ASSOC)) {
    										if($row["Rule_Type"]=="I"){$prefix="";$suffix="<br><br>";}
    										if($row["Rule_Type"]=="TXT"){$prefix="";$suffix="<br>";}
    										if($row["Rule_Type"]=="IF"){$prefix="<br><i>";$suffix="</i><br>";}
    										
    										if($row[$descr_col]!=""){echo $prefix.$row[$descr_col].$suffix; }   							
    								}
    								echo "</div>";
    							}
								
								?>
								
								
								<div class="row marketing">
									<form role="form" action="questionnaire_submit.php" name="frm_questionnaire" method="post" onsubmit="return CheckAndSubmit()">
								<? 
									echo "<input type='hidden' name='hash_code' value='".$hash_code."'>";
									echo "<input type='hidden' name='client_id' value='".$client_id."'>";
									echo "<input type='hidden' name='time_secs' id='time_secs' value='0'>";
									
									$questionnaire = $database->get_questions_for_questionnaire_type($record["Questionnaire_Type_ID"],$language_code);
									$answer_group_set = $database->get_answer_group_for_questionnaire_type($record["Questionnaire_Type_ID"],$language_code);
									
									echo "<div class=\"panel panel-default\">
    									<div class=\"panel-heading\"></div>
    										<table class=\"table table-condensed\">
    										<thead>
                							<tr>
                  								<th width='20'>Code</th>
                  								<th width='140'>Question</th>
                  								<th width='60'>Answer</th>
                							</tr>
              								</thead>
              								<tbody>
    								";
    								
    								$i = 0;
    								
    								$answer_group = array();
									while($r = $answer_group_set->fetch_array(MYSQLI_ASSOC)) {
    									$answer_group[] = $r;
									}
    								 
    								while($question = $questionnaire->fetch_array(MYSQLI_ASSOC)) { 		
    								  	
    								  	if($i==0){echo "<input type='hidden' id='first_question' value='answer_".$question["ID"]."'>";}
    								  	$i = $i+1;
    								  	
    								  	$radio = "";
    								  	$checked = "";
    								  	$checked = "checked"; /*to comment out*/
    								  	foreach ($answer_group as $answer) {
    								  		$radio = $radio."<input type='checkbox' name='answer_".$question["ID"]."' id='answer_".$question["ID"]."_".$answer["Answer"]."' value='".$answer["Answer"]."' onclick=\"radioButtonClick(this);\"  onkeydown=\"radioButtonKeyDown(event)\" ".$checked.">&nbsp;".$answer["Answer"]."&nbsp;";
    								  		$checked = "";
    								  	}
    								  	
    									//$radio = "<input type='radio' name='answer_".$question["ID"]."' id='answer_".$question["ID"]."_1' value='1' onclick=\"radioButtonClick(this);\"  onkeydown=\"radioButtonKeyDown(event)\" checked>&nbsp;1&nbsp;<input type='radio' name='answer_".$question["ID"]."' id='answer_".$question["ID"]."_2' value='2' onclick=\"radioButtonClick(this);\"  onkeydown=\"radioButtonKeyDown(event)\">&nbsp;2 &nbsp;<input type='radio' name='answer_".$question["ID"]."' id='answer_".$question["ID"]."_3' value='3' onclick=\"radioButtonClick(this);\"  onkeydown=\"radioButtonKeyDown(event)\">&nbsp;3&nbsp;<input type='radio' name='answer_".$question["ID"]."' id='answer_".$question["ID"]."_4' value='4' onclick=\"radioButtonClick(this);\"  onkeydown=\"radioButtonKeyDown(event)\">&nbsp;4";
    									   						
    									echo "<tr>
    											<td>".$question["Code"]."</td>
    											<td><div id='div_answer_".$question["ID"]."'>".$question["Question"]."</div></td>
    											<td>".$radio."</td>
    						  				</tr>";	
    						  			
    						  			
    								}
    									echo"</tbody></table></div>";
					
							?>
									<div class="panel-footer"><button type="submit" class="btn btn-default"><? echo $lb_save_rec["Descr_".$language_code] ?></button></div>
								</form>
							</div>
						</div>
					</div>
						
			<?php 
					}
				}
			?>
		</div>
	</body>
</html>








