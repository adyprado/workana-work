function initSimpleScalesWin(){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_scales",0,0,800,600);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Simple Scales");
	win.setIcon("../../../images/measure-icon-16.png", "../../../images/measure-icon-16.png");
	win.show();	
	
	var layout = win.attachLayout("2E");
	var idx_scales_cell = layout.cells('a');
	//idx_factors_cell.setWidth(300);
	idx_scales_cell.setText("Scales");
	
	var stoolbar = idx_scales_cell.attachToolbar();
	stoolbar.setIconSize(32);	 
	stoolbar.addButtonSelect('questionnaire', 0, 'Questionnaire', [], './images/test-icon-32.png','./images/test-icon-32.png');
	stoolbar.addInput('qtype', 1,"",1);
	stoolbar.hideItem('qtype');
	
	stoolbar.attachEvent('onClick', function(id){
			var qtype_id = id.split("-");		
			getParamSimpleScales(qtype_id[1])
			stoolbar.setValue("qtype",qtype_id[1])
	});
		
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		stoolbar.addListOption('questionnaire', 'q-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/question-mark-icon-32.png' )
	}
		
	var idx_scale_questions_cell = layout.cells('b');
	idx_scale_questions_cell.setText("Scale Questions");
	
	var scales_grid = idx_scales_cell.attachGrid();	
	scales_grid.setHeader("Code,Description");
	scales_grid.setColumnIds("Code,Descr_en");
	scales_grid.setColTypes("ro,ro");
	scales_grid.setColSorting("str,str");
	scales_grid.setInitWidthsP("20,80");	
	scales_grid.setSkin("dhx_skyblue");
	scales_grid.setImagePath(gdata.img_path);	
	scales_grid.init();
	
	
	
	
	var toolbar = idx_scale_questions_cell.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('add', 0, 'Link Question','./images/link-icon-32.png','./images/link-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	toolbar.addButton('delete', 2, 'Unlink Question','./images/unlink-icon-32.png','./images/unlink-icon-32.png');
		
	toolbar.attachEvent('onClick', scaleQuestionsToolbarClicked);
	toolbar.disableItem('delete')
	toolbar.disableItem('add')
	
	var scales_questions_grid = idx_scale_questions_cell.attachGrid();	
	scales_questions_grid.setHeader("Code,Question");
	scales_questions_grid.setColumnIds("Code,Question_el");
	scales_questions_grid.setColTypes("ro,ro");
	scales_questions_grid.setColSorting("str");
	scales_questions_grid.setInitWidthsP("20,80");	
	scales_questions_grid.setSkin("dhx_skyblue");
	scales_questions_grid.setImagePath(gdata.img_path);	
	scales_questions_grid.init();
	
	scales_questions_grid.attachEvent("onRowSelect", function(id,ind){
		toolbar.enableItem('delete')
	});
	
	scales_grid.attachEvent("onRowSelect", function(id,ind){
		getQuestionsForScale(id,stoolbar.getValue("qtype"))
		toolbar.enableItem('add')
	});
	
	win.attachEvent("onClose", cleanSimpleScalesWin);
	
	gdata.scales_grid = scales_grid;
	gdata.scales_grid_toolbar = stoolbar;
	gdata.scale_questions_grid = scales_questions_grid;
	gdata.scale_questions_toolbar = toolbar;

	//getParamSimpleScales();
}

function cleanSimpleScalesWin(){
	gdata.scales_grid = null
	gdata.scale_questions_grid = null
	gdata.scale_questions_toolbar = null
	gdata.scales_grid_toolbar = null
	return true;
}

 

function scaleQuestionsToolbarClicked(id){
	
	if(id=="delete"){
		var question_id = gdata.scale_questions_grid.getSelectedRowId()
		var scale_id = gdata.scales_grid.getSelectedRowId()
		//var scale_code = gdata.scales_grid.cells(scale_id,0).getValue();
		var action = 'delete'
		mapQuestionToScale(question_id,scale_id,action)
		
	}
	else if(id=="add"){		
		var qtype = gdata.scales_grid_toolbar.getValue("qtype");
		initQuestionListPopUpWin(qtype)		
	}

}

function questionForScaleSelected(question_id){
	var scale_id = gdata.scales_grid.getSelectedRowId()
	//var scale_code = gdata.scales_grid.cells(scale_id,0).getValue();
	var action = 'add'
	mapQuestionToScale(question_id,scale_id,action)
}

function mapQuestionToScale(question_id,scale_id,action){
	
	var qtype =  gdata.scales_grid_toolbar.getValue("qtype");
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: scale_questions_set,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Scale_ID':scale_id,'Question_ID':question_id,'Action':action,'Questionnaire_Type_ID':qtype},
    		success:CallbackMapQuestionToScale , 
    		error: CallbackMapQuestionToScaleFailed,
    		cache: false
	});

}

function CallbackMapQuestionToScale(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	if(data.Result_Code ==0){
		
		if(typeof data.Action != 'undefined'){
			if(data.Action == 'delete'){
				var question_id = data.Question_ID;
				gdata.scale_questions_grid.deleteRow(question_id)
				dhtmlx.alert({title:"Success",type:"alert",text:"Question unlinked successfully."});
			}
		}
		else{
			dhtmlx.alert({title:"Success",type:"alert",text:"Question linked successfully."});
			addRowToGrid(gdata.scale_questions_grid,data)	
		}
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error performing action."});
	}
}

function CallbackMapQuestionToScaleFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Error performing action."});
}

function getQuestionsForScale(scale_id,questionnaire_type_id){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: scale_questions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Scale_ID':scale_id,'Questionnaire_Type_ID':questionnaire_type_id},
    		success:CallbackGetScaleQuestions , 
    		error: CallbackGetScaleQuestionsFailed,
    		cache: false
	});
}

function CallbackGetScaleQuestions(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.scale_questions_grid.clearAll();
		
		for(var i=0;i<data.Questions.length;i++){
			addRowToGrid(gdata.scale_questions_grid,data.Questions[i])
		}
		gdata.scale_questions_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	}
}

function CallbackGetScaleQuestionsFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
}

function getParamSimpleScales(qtype_id){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: scales_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_Type_ID':qtype_id},
    		success:CallbackGetParamSimpleScales , 
    		error: CallbackGetParamSimpleScalesFailed,
    		cache: false
	});
}

function CallbackGetParamSimpleScales(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.scales_grid.clearAll();
		
		for(var i=0;i<data.Scales.length;i++){
			addRowToGrid(gdata.scales_grid,data.Scales[i])
		}
		gdata.scales_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	}
}

function CallbackGetParamSimpleScalesFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_scales");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving scales."});
	
}