function drawPointOnScatterChart(divCanvas,x,y,img,label,ordinal){
	
	
	var c=document.getElementById(divCanvas);
	var pixelX = 140+(x-10)*4;
	var pixelY = 360-(y-10)*4;
	
	if(pixelY < 0 ){pixelY=0;}
	if(pixelX < 0 ){pixelX=0;}
	
	var context = c.getContext("2d");
	var drawing = new Image() 
	drawing.src = img 
	drawing.onload = function() {
   		context.drawImage(drawing,pixelX,pixelY);
	};
	
	context.font="italic 11px Georgia";
	//context.fillText("test",480,300)
	rounded_x = Math.round(x*100)/100
	rounded_y = Math.round(y*100)/100
	//context.fillText("["+rounded_x+","+rounded_y+"]",pixelX+10,pixelY+10)
	var offset = ordinal * 25 ;
	context.fillText("["+rounded_x+" , "+rounded_y+"] :"+label,480,300+offset)
	//context.drawImage(drawing,x,y);
}

function drawCircleScatterChart(divCanvas,xAxisLbTop,xAxisLbBottom,yAxisLbTop,yAxisLbBottom) {		
		
		var c=document.getElementById(divCanvas);
		var dwidth 	= 600 //	c.offsetWidth;
		var dheight = 400 //	c.offsetHeight;
		  
		var ctx=c.getContext("2d");
		ctx.beginPath();
		ctx.arc(300,200,160,0,2*Math.PI);
		ctx.stroke();

    	var context = c.getContext("2d");
   		
    	for (var x = 140; x <= 460; x += 40) {
        	context.beginPath();        		        		
        	if(x==300){
        		context.setLineDash([30,10])
        		context.moveTo(x,45);
        		context.lineTo(x,365);
        	}
        	else{
        		context.setLineDash([1,2])
        		context.moveTo(x,40);
        		context.lineTo(x,360);
        	}
        	context.strokeStyle = "black";
        	context.stroke();
    	}
    	for (var y = 40; y <= 360; y += 40) {
        	context.beginPath();
        	if(y==200){
        		context.setLineDash([30,10])
        		context.moveTo(145,y);
        		context.lineTo(465,y);
        	}
        	else{        			
        		context.setLineDash([1,2])
        		context.moveTo(140,y);
        		context.lineTo(460,y);        			
        	}       		
        	context.strokeStyle = "black";
        	context.stroke();
   		}
   			
   		context.font="8px Georgia";
			  			
   		for(var i=1;i<10;i++){
   			var offset
   			if(i==1 || i==9){offset =0} else{offset=3}
   			context.fillText((10-i)*10,300-offset,(i*40)+offset); 
   			context.fillText(i*10,100+(i*40)-offset,200+offset);
   		} 
   		
   		context.font = "10px Georgia";
   		context.fillText(xAxisLbTop,480,200)
   		context.fillText(xAxisLbBottom,20,200)
 		context.fillText(yAxisLbTop,270,20)
 		context.fillText(yAxisLbBottom,270,380)
}