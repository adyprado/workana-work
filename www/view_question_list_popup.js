function initQuestionListPopUpWin(questionnaire_type_id){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_que_popup",0,0,500,300);
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Questions");
	win.setIcon("../../../images/question-mark-icon-16.png", "../../../images/question-mark-icon-16.png");
	win.show();
	
	var q_grid = win.attachGrid();	
	q_grid.setHeader("Code,Question");
	q_grid.setColumnIds("Code,Question_el");
	q_grid.setColTypes("ro,ro");
	q_grid.setColSorting("str,str");
	q_grid.setInitWidthsP("10,85");	
	//icn_grid.setColumnsVisibility("false,false,true");
	q_grid.setSkin("dhx_skyblue");
	q_grid.setImagePath(gdata.img_path);	
	q_grid.init();
	
	q_grid.attachEvent("onRowDblClicked", function(rId,cInd){
		questionForScaleSelected(rId)
		win.close();
	}); 
	
	gdata.questions_grid_popup = q_grid;
	
	win.attachEvent("onClose", cleanQuestionsPopUpWin);
	
	getQuestionListForType(questionnaire_type_id)
}

function cleanQuestionsPopUpWin(){
	gdata.questions_grid_popup = null ;
	return true;
}

function getQuestionListForType(questionnaire_type_id){
	var win = gdata.main_layout.dhxWins.window("win_que_popup");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin","Questionnaire_Type_ID":questionnaire_type_id},
    		success:CallbackGetQuestionListPopup , 
    		error: CallbackGetQuestionListPopupFailed,
    		cache: false
	});
}

function CallbackGetQuestionListPopup(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_que_popup");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.questions_grid_popup.clearAll();
		
		for(var i=0;i<data.Questions.length;i++){
			addRowToGrid(gdata.questions_grid_popup,data.Questions[i])
		}
		gdata.questions_grid_popup.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving questions."});
	}
}

function CallbackGetQuestionListPopupFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_que_popup");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error."});
}