<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	
	
	include('server/database_slave.php');
    $database = new database_slave();
    
    $params = explode(',', $_GET['q']);
	
	$is_admin = $params[sizeof($params)-1];
	$language_id = $params[sizeof($params)-2];
	$api_key = $params[sizeof($params)-3];
	$uid = $params[sizeof($params)-4];
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">

	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
	<script src="base64.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	 
  </head>
	
  <body>
  	<?php
  	
  	if($database->app_user_valid($uid,$api_key)){
		$qids = ""; 
		for($i=0;$i<sizeof($params)-4;$i++){		
			$qids = $qids.$params[$i].",";	 	
		}	
		$qids = substr($qids, 0, -1);	
		
		if($is_admin!="1"){
			$sql = "select count(1) as AccessTo from Questionnaires q 
					join ADMIN_Data_Access a on q.Client_ID = a.Client_ID
					where q.ID in (".$qids.") and App_User_ID = '".$uid."'";
			
			error_log($sql);
			$recordset = $database->get_sql_results($sql);
			$record = $recordset->fetch_array(MYSQLI_ASSOC);
			
			if($record["AccessTo"]!=(sizeof($params)-4)){
				error_log("unauthorized");
				exit;
			}		
		}
		
		echo "<img src='./images/LOGO_FILISTOS_".$language_code.".png' height='66' width='150' class=\"logo_image\" style=\"margin-left:50px;\">";
	?>
		
	<div class="container">	
		
		<?php 
		 	
			$sql = "Select v.Questionnaire_Type,v.Client_ID,c.Surname,c.Name,v.Questionnaire_File from v_Questionnaires v
					join Clients c on v.Client_ID = c.ID 
					where v.ID in (".$qids.") order by 2,1" ;
			 
			$recordset = $database->get_sql_results($sql);
			$prev_id = -1;
			
			if($recordset->num_rows>0){				
				
				while($record = $recordset->fetch_array(MYSQLI_ASSOC)){	
					 	
					 	if($prev_id != $record["Client_ID"]){
					 		if($prev_id !=-1){
					 			echo "</table></div>";
					 		}
					 		echo "<div class=\"panel panel-default\">
							<div class=\"panel-heading\">".$record["Surname"]." ".$record["Name"]."</div>
							<table class=\"table table-condensed\">
								<thead>
                					<tr>
                						<th class=\"col-md-2\">Type</th>  
                						<th class=\"col-md-2\">Surname</th>
                  						<th class=\"col-md-2\">Name</th>
                  						<th class=\"col-md-8\">URL</th>
                					</tr>
              					</thead>";
					 		
					 	}
					 	
					 	echo"<tr>
  								<td>".$record["Questionnaire_Type"]."</td>  								 
  								<td>".$record["Surname"]."</td>
  								<td>".$record["Name"]."</td>
  								<td>".$_SERVER['HTTP_HOST']."/".$record["Questionnaire_File"]."</td>
  							</tr>";		
				
					$prev_id = 	$record["Client_ID"];
				}
				echo "</table></div>";
			}				
			else{			
				
			}
			
		?>		
		 
	</div>
		
		
	<?php			 
	}
	else{
		echo "out";
	}
  	  	 
  	?>
  
  
  </body>	
	
</html>