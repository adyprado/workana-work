function initAScoreFunctionsWin(){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_functs",0,0,900,400);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Functions");
	win.setIcon("../../../images/function-icon-16.png", "../../../images/function-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save','./images/save-icon-32.png','./images/save-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	toolbar.addButtonSelect('questionnaire', 2, 'Questionnaire', [], './images/test-icon-32.png','./images/test-icon-32.png');
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		toolbar.addListOption('questionnaire', 'q-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/question-mark-icon-32.png' )
	}
	
	
	toolbar.attachEvent('onClick', function(id){
		
		if(id=="save"){
			var rids=gdata.afuncts_grid.getChangedRows(false);		
			if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
			else{
				var json_obj =	getAFunctionChangesJSON(rids);
				saveParamAFunctions(json_obj)
			}
		}
		else if(id.indexOf("q-")==0){			 
			var qtype_id = id.split("-");	
			getParamAFunctions(qtype_id[1]);
			 
		}
		
		
	});
	
	var functs_grid = win.attachGrid();	
	functs_grid.setHeader("Scale Code,Expression,Scale Code 1,Scale Code 2,Scale Code 3,Scale Code 4,Scale Code 5,Scale Code 6,Factor 1,Factor 2");
	functs_grid.setColumnIds("Scale_ID,Expression,Scale_ID_1,Scale_ID_2,Scale_ID_3,Scale_ID_4,Scale_ID_5,Scale_ID_6,Factor_1,Factor_2");
	functs_grid.setColTypes("coro,ed,coro,coro,coro,coro,coro,coro,ed,ed");
	functs_grid.setColSorting("str,str,str,str,str,str,str,str,str,str");
	functs_grid.setInitWidthsP("8,20,9,9,9,9,9,9,8,8");	
	//icn_grid.setColumnsVisibility("false,false,true");
	functs_grid.setSkin("dhx_skyblue");
	functs_grid.setImagePath(gdata.img_path);	
	functs_grid.init();
	
	gdata.afuncts_grid = functs_grid;
	
	win.attachEvent("onClose", cleanAFunctsWin);
	
	
	
}

function cleanAFunctsWin(){
	gdata.afuncts_grid = null;
	return true;
}

function AFunctionsToolbarClicked(id){
	
	if(id=="save"){
		var rids=gdata.afuncts_grid.getChangedRows(false);		
		if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
		else{
			var json_obj =	getAFunctionChangesJSON(rids);
			saveParamAFunctions(json_obj)
		}

	}	
}

function getAFunctionChangesJSON(rids){
	
	var rowids = rids.split(",");
	var json = ",\"Functions\":[";
	for(var i=0;i<rowids.length;i++){
		
		json = json+"{\"ID\":"+rowids[i];
		for (var j=0; j<gdata.afuncts_grid.getColumnCount(); j++){
			json = json + ",\""+gdata.afuncts_grid.getColumnId(j)+"\":\""+gdata.afuncts_grid.cells(rowids[i],j).getValue()+"\""
		}
		json = json+"}";
		
		if(i<rowids.length-1){json=json+","}
	}
	json = json+"]"
	json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\""+json+"}";
	
	//alert(json)
	var json_obj = jQuery.parseJSON( json );
	return json_obj;
}


function saveParamAFunctions(json){
	var win = gdata.main_layout.dhxWins.window("win_functs");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: ascore_functions_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveParamAFunctions , 
    		error: CallbackSaveParamAFunctionsFailed,
    		cache: false
	});
}

function CallbackSaveParamAFunctions(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_functs");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"A-Score parameterization saved successfully."});
		gdata.afuncts_grid.clearChangedState();
	}	
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"A-Score parameterization save failed."});
	}
	
}

function CallbackSaveParamAFunctionsFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_functs");
	win.progressOff();
	
}



function getParamAFunctions(qtype_id){
	
	var win = gdata.main_layout.dhxWins.window("win_functs");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: ascore_functions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_Type_ID':qtype_id},
    		success:CallbackGetAFunctions , 
    		error: CallbackGetAFunctionsFailed,
    		cache: false
	});
	
}

function CallbackGetAFunctions(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_functs");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		var combo_1 = gdata.afuncts_grid.getCombo(0);
		var combo_2 = gdata.afuncts_grid.getCombo(2);
		var combo_3 = gdata.afuncts_grid.getCombo(3);
		var combo_4 = gdata.afuncts_grid.getCombo(4);
		var combo_5 = gdata.afuncts_grid.getCombo(5);
		var combo_6 = gdata.afuncts_grid.getCombo(6);
		var combo_7 = gdata.afuncts_grid.getCombo(7);
		
		combo_1.put("-1","");
		combo_2.put("-1","");
		combo_3.put("-1","");
		combo_4.put("-1","");
		combo_5.put("-1","");
		combo_6.put("-1","");
		combo_7.put("-1","");
		
		for(var i=0;i<data.Scales.length;i++){
			combo_1.put(data.Scales[i].value,data.Scales[i].text);
			combo_2.put(data.Scales[i].value,data.Scales[i].text);
			combo_3.put(data.Scales[i].value,data.Scales[i].text);
			combo_4.put(data.Scales[i].value,data.Scales[i].text);
			combo_5.put(data.Scales[i].value,data.Scales[i].text);
			combo_6.put(data.Scales[i].value,data.Scales[i].text);
			combo_7.put(data.Scales[i].value,data.Scales[i].text);
		}
		
		gdata.afuncts_grid.clearAll();
			
		for(var i=0;i<data.Functions.length;i++){
			addRowToGrid(gdata.afuncts_grid,data.Functions[i])
		}
		
		gdata.afuncts_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving ICN parameterization."});
	}

}

function CallbackGetAFunctionsFailed(jqXHR, textStatus, errorThrown){

	var win = gdata.main_layout.dhxWins.window("win_functs");
	win.progressOff();
	
	

}







