function initQuestionsWin(){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_questions",0,0,850,400);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Questions");
	win.setIcon("../../../images/question-mark-icon-16.png", "../../../images/question-mark-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save','./images/save-icon-32.png','./images/save-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	toolbar.addButtonSelect('questionnaire', 2, 'Questionnaire', [], './images/test-icon-32.png','./images/test-icon-32.png');
	
	toolbar.attachEvent('onClick', QuestionsToolbarClicked);
	
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		//alert(gdata.questionnaire_types[i].value)
		toolbar.addListOption('questionnaire', 'q-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/question-mark-icon-32.png' )
	}
	
	var q_grid = win.attachGrid();	
	q_grid.setHeader("Code,Question GR,Question EN");
	q_grid.setColumnIds("Code,Question_el,Question_en");
	q_grid.setColTypes("ro,ed,ed");
	q_grid.setColSorting("str,str,str");
	q_grid.setInitWidthsP("6,46,46");	
	//icn_grid.setColumnsVisibility("false,false,true");
	q_grid.setSkin("dhx_skyblue");
	q_grid.setImagePath(gdata.img_path);	
	q_grid.init();
	
	gdata.questions_grid = q_grid;
	
	win.attachEvent("onClose", cleanQuestionsWin);
	
	//getParamICN();
	
}

function cleanQuestionsWin(){
	gdata.questions_grid = null;
	return true;
}

function QuestionsToolbarClicked(id){
	
	if(id=="save"){
		var rids=gdata.questions_grid.getChangedRows(false);		
		if(typeof rids == 'undefined' || rids==null || rids==""){dhtmlx.alert({title:"No Changes",type:"alert-warning",text:"No changes are made."});}
		else{
			var json_obj =	getQuestionsChangesJSON(rids);
			saveQuestionsList(json_obj)
		}	
	}
	else if(id.indexOf("q-")!=-1){
		var qtype_id = id.split("-");		
		getQuestionList(qtype_id[1])		 
	}
	
}

function getQuestionsChangesJSON(rids){
	
	var rowids = rids.split(",");
	var json = ",\"Questions\":[";
	for(var i=0;i<rowids.length;i++){
		
		json = json+"{\"ID\":"+rowids[i];
		for (var j=0; j<gdata.questions_grid.getColumnCount(); j++){
			json = json + ",\""+gdata.questions_grid.getColumnId(j)+"\":\""+gdata.questions_grid.cells(rowids[i],j).getValue()+"\""
		}
		json = json+"}";
		
		if(i<rowids.length-1){json=json+","}
	}
	json = json+"]"
	json = "{\"Api_Key\":\""+gdata.api_key+"\",\"Is_Admin\":"+gdata.is_admin+",\"User_ID\":\""+gdata.uid+"\""+json+"}";
	
	//alert(json)
	var json_obj = jQuery.parseJSON( json );
	return json_obj;

}

function saveQuestionsList(json){
	
	var win = gdata.main_layout.dhxWins.window("win_questions");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questions_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveQuestions , 
    		error: CallbackSaveQuestionsFailed,
    		cache: false
	});
}

function CallbackSaveQuestions(data, textStatus, jqXHR){
	
	var win = gdata.main_layout.dhxWins.window("win_questions");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Questions saved successfully."});
	}	
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Questions save failed."});
	}
}

function CallbackSaveQuestionsFailed(jqXHR, textStatus, errorThrown){
	
	var win = gdata.main_layout.dhxWins.window("win_questions");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Questions save failed."});
	
}

function getQuestionList(questionnaire_type_id){
	var win = gdata.main_layout.dhxWins.window("win_questions");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin","Questionnaire_Type_ID":questionnaire_type_id},
    		success:CallbackGetQuestionList , 
    		error: CallbackGetQuestionListFailed,
    		cache: false
	});
}

function CallbackGetQuestionList(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_questions");
	win.progressOff();
	
	if(data.Result_Code==0){
		
		gdata.questions_grid.clearAll();
		
		for(var i=0;i<data.Questions.length;i++){
			addRowToGrid(gdata.questions_grid,data.Questions[i])
		}
		gdata.questions_grid.clearChangedState();
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving questions."});
	}
}

function CallbackGetQuestionListFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_questions");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error."});
}







