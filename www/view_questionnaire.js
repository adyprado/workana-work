function initQuestionnaireWin(client_id,questionnaire_id,questionnaire_type_id){
	
	var surnameIndex = gdata.clients_grid.getColIndexById("Surname");
	var nameIndex = gdata.clients_grid.getColIndexById("Name");
	
	var surname = gdata.clients_grid.cells(client_id,surnameIndex).getValue();
	var name	= gdata.clients_grid.cells(client_id,nameIndex).getValue();
	
	var win = gdata.main_layout.dhxWins.createWindow("win_questionnaire",0,0,680,500);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText(surname+" "+name);
	win.setIcon("../../../images/question-icon-16.png", "../../../images/question-icon-16.png");
	win.show();
 
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	toolbar.addButton('save', 0, 'Save & Process','./images/process-icon-32.png','./images/process-icon-32.png');
	toolbar.addSeparator('sep_1', 1);
	//toolbar.addButton('process', 2, 'Process','./images/process-icon-32.png','./images/process-icon-32.png');
	//toolbar.addSeparator('sep_2', 3);
	
	toolbar.addInput('cid', 2, client_id,1);
	toolbar.addInput('qid', 3, questionnaire_id,1);
	toolbar.addInput('type_id', 4, questionnaire_type_id,1);
	
	toolbar.addText("label", 5, 'Comments: ');
	toolbar.addInput('comments', 6,'',300);
	toolbar.addButtonTwoState('online_access', 7, 'Online Access', './images/link-edit-icon-32.png', './images/link-edit-icon-32.png');
	toolbar.setItemState('online_access', true);
	
	toolbar.hideItem('cid');
	toolbar.hideItem('qid');
	toolbar.hideItem('type_id');
	toolbar.hideItem('online_access');
		
	
	var questionnaire_grid = win.attachGrid();	
	questionnaire_grid.setHeader("Code,Question,Answer");
	questionnaire_grid.setColumnIds("Code,Question,Answer");
	questionnaire_grid.setColTypes("ro,ro,ro");
	questionnaire_grid.setColSorting("str,str,str");
	questionnaire_grid.setInitWidthsP("7,65,26");	
	questionnaire_grid.setSkin("dhx_skyblue");
	questionnaire_grid.setImagePath(gdata.img_path);	
	questionnaire_grid.init();
	
	questionnaire_grid.attachEvent("onKeyPress", function(code,cFlag,sFlag){
		//alert(code);
		//if(code=="49" || code=="50" || code=="51" || code == "52"){
			var selectedId = questionnaire_grid.getSelectedRowId();
			if(selectedId!=null){
				
				var answer = String.fromCharCode(code)				
				/*var num;
				if(code=="49"){num=1}
				if(code=="50"){num=2}
				if(code=="51"){num=3}
				if(code=="52"){num=4}*/
				var cb = "answer_"+selectedId+"_"+answer;				 
				var checkbox = document.getElementById(cb)
				if(typeof checkbox != 'undefined' && checkbox != null){
					checkbox.checked = true;
				}
			}
		//}
		
		return true
	});
	
	
	
	win.attachEvent("onClose", cleanQuestionnaireWin);
	toolbar.attachEvent('onClick', questionnaireToolbarClicked);
	
	toolbar.attachEvent("onStateChange", function(id, state){
		if(id=="online_access"){
			if(state){
				toolbar.setItemImage("online_access", './images/link-edit-icon-32.png');
			}
			else{
				toolbar.setItemImage("online_access", './images/link-edit-disabled-icon-32.png');
			}
		}
	});
	
	gdata.questionnaire_toolbar = toolbar;
	gdata.questionnaire_grid  = questionnaire_grid;
		
	if(questionnaire_id<0){getQuestions(questionnaire_type_id);}
	else{getQuestionnaire(questionnaire_id)}
	
	
	
}

function questionnaireToolbarClicked(id){
	
	if(id=="save"){
		var json = questionnaireToJSON();
		saveQuestionnaire(json)
	}
	else{
		//getQuestionnaire(1)
	}
}

function cleanQuestionnaireWin(){
	gdata.questionnaire_grid = null;
	gdata.questionnaire_toolbar = null;
	
	return true;
}

function getQuestions(questionnaire_type_id){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questions_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin","Questionnaire_Type_ID":questionnaire_type_id,"Language_ID":gdata.language_id},
    		success:CallbackGetQuestions , 
    		error: CallbackGetQuestionsFailed,
    		cache: false
	});
}

function CallbackGetQuestions(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOff();
	
	if(data.Result_Code==0){
		populateQuestionsGrid(data.Questions,data.Answer_Group)	
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving questions."});
	}
}

function CallbackGetQuestionsFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error."});
}


function getQuestionnaire(questionnaire_id){
	
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questionnaire_get,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_ID':questionnaire_id,"Language_ID":gdata.language_id},
    		success:CallbackGetQuestionnaire , 
    		error: CallbackGetQuestionnaireFailed,
    		cache: false
	});	
}
function CallbackGetQuestionnaire(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOff();
	
	if(data.Result_Code==0){
		gdata.questionnaire_grid.clearAll();
		populateQuestionsGrid(data.Questionnaire,data.Answer_Group)	
		
		gdata.questionnaire_toolbar.setValue("comments",data.Comments);
		gdata.questionnaire_toolbar.setItemState("online_access", data.Online_Access);
		
		if(data.Online_Access == 1) {gdata.questionnaire_toolbar.setItemImage("online_access", './images/link-edit-icon-32.png');}
		else{gdata.questionnaire_toolbar.setItemImage("online_access", './images/link-edit-disabled-icon-32.png');}
		
		//toolbar.setItemImage("online_access", './images/link-edit-icon-32.png');
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error retrieving questionnaire."});
	}
}

function CallbackGetQuestionnaireFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOff();
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error"});
}



function populateQuestionsGrid(data,answer_group){
	
	for(var i=0;i<data.length;i++){
		
		var answer = answer_group[0].Answer;
		
		if(typeof data[i].Answer != 'undefined'){answer = data[i].Answer}
		
		/*var checked_1="";
		var checked_2="";
		var checked_3="";
		var checked_4="";
		
		if(answer==1)checked_1 = "checked";
		if(answer==2)checked_2 = "checked";
		if(answer==3)checked_3 = "checked";
		if(answer==4)checked_4 = "checked";
		
		var radio = "<input type='radio' name='answer_"+data[i].ID+"' id='answer_"+data[i].ID+"_1' value='1' "+checked_1+">1<input type='radio' name='answer_"+data[i].ID+"' id='answer_"+data[i].ID+"_2' value='2' "+checked_2+">2<input type='radio' name='answer_"+data[i].ID+"' id='answer_"+data[i].ID+"_3' value='3' "+checked_3+">3<input type='radio' name='answer_"+data[i].ID+"' id='answer_"+data[i].ID+"_4' value='4' "+checked_4+">4";
		*/
		var radio = "";
		var checked;
		
		for(var j=0;j<answer_group.length;j++){
			checked = ""
			if(answer_group[j].Answer == answer){checked = "checked"}			
			radio = radio+"<input type='radio' name='answer_"+data[i].ID+"' id='answer_"+data[i].ID+"_"+answer_group[j].Answer+"' value='"+answer_group[j].Answer+"' "+checked+">"+answer_group[j].Answer
		}
		
		gdata.questionnaire_grid.addRow(data[i].ID,[data[i].Code,data[i].Question,radio])
	}
	
}

function questionnaireToJSON(){
	
	var answers = "";
	var qid = gdata.questionnaire_toolbar.getValue("qid")
	var cid = gdata.questionnaire_toolbar.getValue("cid")
	var type_id = gdata.questionnaire_toolbar.getValue("type_id")
	
	var comments = gdata.questionnaire_toolbar.getValue("comments")
	var online_access = 0 ;
	if(gdata.questionnaire_toolbar.getItemState('online_access')){
		online_access = 1;
	}
	
	for (var i=0; i<gdata.questionnaire_grid.getRowsNum(); i++){
        
        var rid =  gdata.questionnaire_grid.getRowId(i) 
        var radio = "answer_"+rid;
        var value = 1;
        
        var radios = document.getElementsByName(radio);
		for (var j = 0;j < radios.length; j++) {
    		if (radios[j].checked) {         
      			value = radios[j].value
      			break;
      		}
		}
		
		answers = answers + "{\"QID\":"+rid+",\"Ans\":"+value+"}"
		if(i<gdata.questionnaire_grid.getRowsNum()-1){answers = answers +","}
    }
    
    answers = ",\"Answers\":["+answers+"]";    
    
    var last_save = getDayAsString();
    
    var json = "{\"Api_Key\":\""+gdata.api_key+"\",\"User_ID\":\""+gdata.uid+"\",\"Is_Admin\":"+gdata.is_admin+",\"Questionnaire_Type_ID\":"+type_id+",\"Online_Access\":"+online_access+",\"Client_ID\":"+cid+",\"ID\":"+qid+""+answers+",\"Last_Save\":\""+last_save+"\",\"Comments\":\""+comments+"\"}";
    //alert(json);
    
    var json_obj = jQuery.parseJSON( json );
	return json_obj;
}

function saveQuestionnaire(json){
	
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questionnaire_process,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveQuestionnaire , 
    		error: CallbackSaveQuestionnaireFailed,
    		cache: false
	});	
}

function CallbackSaveQuestionnaire(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOff();
	
	if(typeof data.ID != 'undefined'){
		
		var qid = gdata.questionnaire_toolbar.getValue("qid");
		var new_qid = data.ID;
		
		if(qid != new_qid){
			gdata.questionnaire_toolbar.setValue("qid",new_qid);
			adjustQuestionnaireCount(data.Client_ID,1)
			
			var type;
			for(var j=0;j<gdata.questionnaire_types.length;j++)
			{if(gdata.questionnaire_types[j].value==data.Questionnaire_Type_ID){type = gdata.questionnaire_types[j].text}}
			
			gdata.questionnaires_list_grid.addRow(data.ID,[0,data.Last_Save,data.Comments,data.Questionnaire_File,data.Hash_Code,data.Questionnaire_Type_ID,data.Result_File,data.Result_Cmp_File,type]);				
		}
		else{
			gdata.questionnaires_list_grid.cells(data.ID,1).setValue(data.Last_Save);
			gdata.questionnaires_list_grid.cells(data.ID,2).setValue(data.Comments);
		}	
	}
		
	if(data.Result_Code==0){
		dhtmlx.confirm({type:"Success",text: "Questionnaire saved successfully!<br><br>Would you like to check the results?",callback: function(result){closeQuestionnaireWindow(new_qid,result)}});
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});
	}
	
	
	
}
function CallbackSaveQuestionnaireFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.progressOff();	
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error."});
}

function closeQuestionnaireWindow(qid,showResults){
	
	if(showResults){
		var hash_code = gdata.questionnaires_list_grid.cells(qid,4).getValue();
		var url_code = hash_code + gdata.language_id;
		var result_file = gdata.questionnaires_list_grid.cells(qid,6).getValue();
		window.open(result_file+"?q="+url_code,"_blank");		 	
	}
	var win = gdata.main_layout.dhxWins.window("win_questionnaire");
	win.close();
}


