<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8');
	
	require_once('server/functions.php');
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>.</title>

    	<link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
  		<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">
		<link href="results.css" rel="stylesheet">
		
		<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
  
  	</head>
	<body>
    	<div class="container">


<?php
	include('server/calc_questionnaire.php');
	
	$database = new database_slave();
	
	if(!isset($_POST["hash_code"]) || !isset($_POST["client_id"])){
		echo " <div class=\"header\">
        			<h3 class=\"text-muted\">Something went wrong!</h3>
    			</div>
	 		 		
	 		 	<div class=\"alert alert-danger\">
    				Insufficient parameters.	 
    			</div>";	
	}
	else{
	
		$hash_code = $_POST["hash_code"];
		$client_id = $_POST["client_id"];
		$fill_time = $_POST["time_secs"];
		
		$questionnaire = $database->get_db_record("Questionnaires",$hash_code,"Hash_Code");
		
		if($client_id!=$questionnaire["Client_ID"]){
			echo " <div class=\"header\">
        			<h3 class=\"text-muted\">Something went wrong!</h3>
    			</div>	 		 		
	 		 	<div class=\"alert alert-danger\">
    				Invalid parameters.	 
    			</div>";		
		}
		else{
			
			$crecord = $database->get_db_record("Clients",$questionnaire["Client_ID"]);    			    			
    		$lrecord = $database->get_db_record("Languages",$crecord["Language_ID"]);
			$language_code = $lrecord["Code"];
			
			$questionnaire_id = $questionnaire["ID"];
	
			$mysqli = $database->get_connection();
			
			if($questionnaire["Online_Access"]==0){
    			$lb_questionnaire_rec = $database->get_db_record("META_UI_Labels","PROCESS_DISABLED","Code");
    			echo "<div class=\"alert alert-danger\">".$lb_questionnaire_rec["Descr_".$language_code]."</div>";
    		}
    		else{
			
				try {
					$mysqli->autocommit(FALSE);
	
					foreach ($_POST as $key => $value){
        	        	
        	        	//error_log($questionnaire_id.":".$key."=".$value);
        	        	
        				if(strpos($key,"answer_")!==false){
        					$question_id =str_replace("answer_","",$key);        		
        					$sql = "Delete from Questionnaires_Details where Questionnaire_ID='".$questionnaire_id."' and Question_ID='".$question_id."'";
    						$res = $mysqli->query($sql);
							if($res===false)
								{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
        		
        					$sql = "insert into Questionnaires_Details(Questionnaire_ID,Question_ID,Answer) values ('".$questionnaire_id."','".$question_id."','".$value."')";
    						$res = $mysqli->query($sql);
							if($res===false)
								{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
        		
        				}         
    				} 
					$mysqli->commit();
					$mysqli->close();
					
					$status_code = "ANSWERS_STORED";
					
					$result = calc_questionnaire($questionnaire_id);
					if(!$result){
			 	 	
			 	 		$lb_error_rec = $database->get_db_record("META_UI_Labels","PROCESS_ERROR","Code");
	 		 			$lb_error_msg_rec = $database->get_db_record("META_UI_Labels","UNEXPECTED_ERROR","Code");
			 	 	
			 	 		echo " <div class=\"header\">
        						<h3 class=\"text-muted\">".$lb_error_rec["Descr_".$language_code]."</h3>
    						</div>	 		 		
	 		 				<div class=\"alert alert-danger\">
    							".$lb_error_msg_rec["Descr_".$language_code]."	 
    						</div>";
    					
    					$status_code = "UNEXPECTED_ERROR";
					}
					else{
	 		 		
	 		 			$lb_finish_rec = $database->get_db_record("META_UI_Labels","FINISH","Code");
	 		 			$lb_success_rec = $database->get_db_record("META_UI_Labels","ANSWERS_STORED","Code");
	 		 			
	 		 			$upd_res = $database->adjust_questionnaire_process_times($questionnaire_id,1);
	 		 			$upd_res = $database->adjust_questionnaire_online_access($questionnaire_id,0);
	 		 			$upd_res = $database->adjust_questionnaire_fill_time($questionnaire_id,$fill_time);
	 		 			
	 		 			echo " <div class=\"header\">
        						<h3 class=\"text-muted\">".$lb_finish_rec["Descr_".$language_code]."</h3>
    						</div>	 		 		
	 		 				<div class=\"alert alert-success\">
    							".$lb_success_rec["Descr_".$language_code]."	 
    						</div>";
					}
				
					notifyForCompletion($database,$questionnaire_id,"Questionnaires",$status_code,$language_code);
				}
				catch (Exception $e) { 
						$mysqli->rollback();
  						$mysqli->close();
  						echo " <div class=\"header\">
        						<h3 class=\"text-muted\">Something went wrong!</h3>
    						</div>	 		 		
	 		 				<div class=\"alert alert-danger\">
    							Unexpexted error occured. Please try again later.	 
    						</div>";
				}	
			}
		}
	}
	?>
	</div>
	</body>
</html>
