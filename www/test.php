<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
?>
<?
error_log(hash('sha256',"admin"));
?>

<html>
<head>
	<script src="graphs.js"></script>	
</head>

<body>
	<canvas id="myCanvas" width="600" height="400" style="background-color:#ffffff"></canvas>
	
	<script type="text/javascript">
		drawCircleScatterChart("myCanvas","Σοβαρή Ψυχοπαθολογία","Ήπια Ψυχοπαθολογία","Υψηλή Χρήση","Ηπια Χρήση")
		drawPointOnScatterChart("myCanvas",200,200,"./images/red-empty-8.png")
	</script>
	
</body>
</html>