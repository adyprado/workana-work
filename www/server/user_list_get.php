<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$is_admin = $_POST["Is_Admin"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$user_list = $database->get_user_list();
	
 	if($user_list->num_rows >0){
 		$cjson = "[";
 		
 		while($user = $user_list->fetch_array(MYSQLI_ASSOC)) {
    	 	$cjson = $cjson.json_encode($user).",";
		}
 		
 		$cjson = substr($cjson,0,-1)."]";
 		$json = "{\"Result_Code\":0,\"Users\":".$cjson."}";
 	}
 	else{
 		$json = "{\"Result_Code\":0}";		
 	}

}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>