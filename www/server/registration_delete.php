<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$cid = $_POST["Registration_ID"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$result = $database->delete_registration($cid);
	if($result){
		$json = "{\"Result_Code\":0,\"Registration_ID\":".$cid."}";
	}
	else{
		$json = "{\"Result_Code\":-2,\"Result_Message\":\"Registration deletion failed\"}";	
	}

}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>