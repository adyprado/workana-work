<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];


$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$scale_type_id = 1;
	$questionnaire_id = -1;
	
	if(isset($_POST["Scale_Type_ID"])){
		$scale_type_id = $_POST["Scale_Type_ID"];
	}
	if(isset($_POST["Questionnaire_ID"])){
		$questionnaire_id = $_POST["Questionnaire_ID"];
	}
	
	$scales = $database->get_questionnaire_scales_score($questionnaire_id,$scale_type_id);//get_recordset("PARAM_Scales","Type_ID",$scale_type_id);
		
	$rows = array();
	
	while($r = $scales->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
	
	$json_q = json_encode($rows);
	$json = "{\"Result_Code\":0,\"Scales\":".$json_q."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>