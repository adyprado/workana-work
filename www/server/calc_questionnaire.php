<?php
//header('Content-Type: application/json; charset=utf-8');

include(__DIR__.'/model_classes.php');

//$uid = $_POST["User_ID"];
//$akey = $_POST["Api_Key"];

function calc_questionnaire($questionnaire_id){

	$database = new database_slave();

	$json = "{\"Result_Code\":0}";	

	$questionnaire = new Questionnaire();
	$questionnaire->initFromDatabase($questionnaire_id);
	//$questionnaire->Questionnaire_Type_ID
	
	$result = $database->calc_questions_points($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
	if($result){
			
		$result = $database->calc_simple_scale_raw_score($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
		
		if($result){
						
			$result = $database->calc_icn_scale_raw_score($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
			
			if($result){
								
				$result = $database->calc_master_scale_raw_score($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
				
				if($result){
										
					$result  = $database->calc_a_score($questionnaire_id,"1,2,5","SIMPLE_MASTER_ICN",$questionnaire->Questionnaire_Type_ID);
					$result2 = $database->calc_a_score_questions($questionnaire_id,"1,2,5","SIMPLE_MASTER_ICN",$questionnaire->Questionnaire_Type_ID);
					
					
					if($result && $result2){
						
						$result = $database->calc_suppl_idx_factor_scale_raw_score($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
						
						if($result){
							
							$result = $database->calc_suppl_idx_threshold_scale_raw_score($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
							
							if($result){
								
								$result = $database->calc_a_score($questionnaire_id,"3","SUPL_INDEX",$questionnaire->Questionnaire_Type_ID);
								
								if($result){
								
									$result = $database->calc_mean_scores($questionnaire_id,$questionnaire->Questionnaire_Type_ID);
									
									if($result){
									}
									else{
										$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating mean scores.\"}";
										error_log($json);
										return false;
									}																		
									//echo "Suppl idx scales a scores calculated..";
								}
								else{
									$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating supplementary index A-scores.\"}";
									error_log($json);
									return false;
								}
								
							}
							else{
								//echo " Error calculating suppl idx threshold scales";
								$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating supplementary index raw scores.\"}";
								error_log($json);
								return false;
							}
							
						}
						else{
							$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating supplementary index raw scores.\"}";
							error_log($json);
							return false;
						}
						
					}
					else{
						$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating A-scores.\"}";
						error_log($json);
						return false;
					}
				}
				else{
					$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating master scales raw scores.\"}";
					error_log($json);
					return false;				
				}
					
				
			}
			else{
				$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating ICN raw scores.\"}";
				error_log($json);
				return false;
			}
			
			
		}
		else{
			//echo "error calculating simple scales";
			$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating simple scales raw scores.\"}";
			error_log($json);
			return false;
		}
		
	}
	else{
		$json = "{\"Result_Code\":-1,\"Result_Message\":\"Error calculating question points.\"}";
		error_log($json);
		return false;
	}
	
/*}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}*/
	
	//$result = $database->adjust_process_times($questionnaire_id,1);
	
	return true;
	//return $json; 
}
?>