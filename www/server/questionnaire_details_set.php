<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$answers = $_POST["Answers"];
	$questionnaire_id = $_POST["Questionnaire_ID"];
	
	$questions_failed = "";
	foreach ($answers as $asw) {
    	
    	$result = $database->set_question_answer($questionnaire_id,$asw["QID"],$asw["Ans"]);
    	if(!$result){
    		$questions_failed = $questions_failed.$asw["QID"].",";
    	}
	}
	
	if($questions_failed!=""){
		$questions_failed = substr($questions_failed,0,-1);
		$json = "{\"Result_Code\":-1,\"Result_Message\":\"Questions [".$questions_failed."] failed.\"}";
	}
	else{
		$json = "{\"Result_Code\":0}";	
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 



?>