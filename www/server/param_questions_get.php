<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];


$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$questionnaire_type_id = 1 ;
	$language_id = 1; 
	
	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	if(isset($_POST["Language_ID"])){
		$language_id = $_POST["Language_ID"];
	}
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];
	
	$questions = $database->get_questions_for_questionnaire_type($questionnaire_type_id,$language_code); 
	$answer_group = $database->get_answer_group_for_questionnaire_type($questionnaire_type_id,$language_code);
	
	$rows = array();
	
	while($r = $questions->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
	
	$json_q = json_encode($rows);
	
	$rows = array();
	
	while($r = $answer_group->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
	
	$json_ag = json_encode($rows);
	
	$json = "{\"Result_Code\":0,\"Questions\":".$json_q.",\"Answer_Group\":".$json_ag."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>