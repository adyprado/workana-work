<?php
header('Content-Type: application/json; charset=utf-8');

require_once('functions.php');
include('calc_questionnaire.php');


$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$is_admin = $_POST["Is_Admin"];
	
	$questionnaire = new Questionnaire();
	$questionnaire->initFromRecord($_POST); 
	
	$proceed = true;
	$process_limit_reached = false;
	$new_questionnaire = false;
	
	if($is_admin != 1 && $questionnaire->ID<1){
		$proceed = $database->check_user_limit($uid,$questionnaire->Questionnaire_Type_ID,"Questionnaires");	
	}
	
	if($is_admin != 1 && $questionnaire->ID>0){
		$process_limit_reached = $database->check_process_limit($questionnaire->ID);	
		if($process_limit_reached){$proceed = false;}
	}
	
	if($proceed){
		
		if($questionnaire->ID<1){$new_questionnaire=true;}
		
		$questionnaire->saveToDatabase();
	
		$answers = $_POST["Answers"];
		$questionnaire_id = $questionnaire->ID; 
		$questionnaire->initFromDatabase($questionnaire_id);
		
		$questions_failed = "";
		$result = $database->set_questionnaire_answers($questionnaire_id,$answers);
	
		if(!$result){
		//$questions_failed = substr($questions_failed,0,-1);
			$json = "{\"Result_Code\":-2,\"Result_Message\":\"Questionnaire save error.\",".Utils::toJson($questionnaire)."}";
		}
		else{
			$result = calc_questionnaire($questionnaire_id);
			if(!$result){
				$json = "{\"Result_Code\":-3,\"Result_Message\":\"Questionnaire process error.\",".Utils::toJson($questionnaire)."}";
			}
			else{
				if(!$new_questionnaire && $is_admin != 1){
					$database->adjust_questionnaire_process_times($questionnaire_id,1);
				}	
				
				$json = $questionnaire->toJSON();
			 
			}
		}
	}
	else{
		if($process_limit_reached){
			$json = "{\"Result_Code\":-5,\"Result_Message\":\"You have exceeded the maximum process times for the selected questionnaire.\"}";
		}
		else{
			$json = "{\"Result_Code\":-4,\"Result_Message\":\"You have exceeded the purchased number of the specific questionnaire type.\"}";
		}
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 



?>