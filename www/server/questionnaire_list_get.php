<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$cid = $_POST["Client_ID"];
$purpose = $_POST["Purpose"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$sql= "SELECT 
			q.ID,
			q.Questionnaire_Type,
			c.Surname,
			c.Name,
			IFNULL(q.Comments,'') as Comments,
			q.Questionnaire_File
		FROM v_Questionnaires q
		join Clients c on q.Client_ID = c.ID		
		where c.ID in (".$cid.") order by 2,3,4";
	
	$questionnaires = $database->get_sql_results($sql);
	
	if($questionnaires->num_rows==0){
		$json = "{\"Result_Code\":0,\"Result_Message\":\"No questionnaires found.\"}";		
	}
	else{
		$rows = array();
	
		while($q = $questionnaires->fetch_array(MYSQLI_ASSOC)) {
    		$rows[] = $q;
		}	
		$json_q = json_encode($rows);
		$json = "{\"Result_Code\":0,\"Purpose\":\"".$purpose."\",\"Questionnaires\":".$json_q."}";
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>