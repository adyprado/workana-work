<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$qid = $_POST["Questionnaire_ID"];
	$result = $database->delete_questionnaire($qid);
	
	if($result){
		$json = "{\"Result_Code\":0,\"Questionnaire_ID\":".$qid."}";
	}
	else{
		$json = "{\"Result_Code\":-2,\"Result_Message\":\"Error deleting questionnaire.\"}";	
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>