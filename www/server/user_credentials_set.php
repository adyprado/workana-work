<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$app_user_id = $_POST["App_User_ID"];
$new_password = $_POST["Password"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$user = new App_User();
	$user->initFromDatabase($app_user_id);
	$user->Password = $new_password;
	
	if(!$database->check_conficts_app_user_save($user)){
		$json = "{\"Result_Code\":-100,\"Result_Message\":\"User with the same credentials already exists.\"}";	
		
	}
	else{
		$result = $database->set_user_credentials($app_user_id,$new_password);
		if($result){
			$json = "{\"Result_Code\":0}";
		}
		else{
			$json = "{\"Result_Code\":-2,\"Result_Message\":\"Credentials save failed\"}";	
		}
	}

}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>