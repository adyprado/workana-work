<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$update_stmts = array();
	$insert_stmts = array();
	
	if(isset($_POST["Config"])){
		
		foreach($_POST["Config"] as $rule)
		{
			$ruleObj = new Rep_Config();
			$ruleObj->initFromRecord($rule);
			
			if($ruleObj->ID>0){
				$upd = $database->get_object_sql_update("PARAM_Rep_Full",$ruleObj);
				$upd = $database->replaceMathOperators($upd);				
				//error_log($upd);
				$update_stmts[] = $upd;
			}
			else{
				$ins = $database->get_object_sql_insert("PARAM_Rep_Full",$ruleObj);
				$ins = $database->replaceMathOperators($ins);
				$insert_stmts[] = $ins;	
			}
			 			
		}		
	}
	
	$result = $database->executeBatchInsertUpdates($insert_stmts,$update_stmts);
	//$result = true;	
	if($result){
		$json = "{\"Result_Code\":0}";
	}
	else{
		$json = "{\"Result_Code\":-2,\"Result_Message\":\"Error Saving Changes\"}";
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json;