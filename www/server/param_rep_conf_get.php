<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$questionnaire_type_id = 1 ;
	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	
	$config = $database->get_rep_config($questionnaire_type_id);
	$rows = array();
	
	while($r = $config->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
			
	$json_q = json_encode($rows);
		 
	
	$json = "{\"Result_Code\":0,\"Rep_Config\":".$json_q."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>