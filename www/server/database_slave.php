<?php 

   
class database_slave 
{
	  
	   /* 
	private $server_ip = 'localhost';

	*/     
	    
	private $server_ip = 'localhost';
	private $server_username = 'copserfe';
	private $server_password = 'fpEE58-43k#mdhErpVXcB4FwXX4';
	private $server_database = 'copserfe';
	private $server_port = 3306;
	 
	 
    /**
     *  Searches the into the string $str the symbols "& lt ; " and "& gt ; " 
     *  ,And replaces with the symbols less than and greater than
     *  ,respectively. 
     */ 
	public function replaceMathOperators($str){
	
		$new_str = str_replace("&lt;","<",$str);
		$new_str = str_replace("&gt;",">",$new_str);
		
		return $new_str;
	}
	
    /**
     * Depending on the date on which the script is executed 
     * and the $input to be passed to the function will return 
     * a different hash code for each call 
     */
	public static function generateApiKey($input)
	{
		$salt="pbi";
		return database_slave::toHash(date('l jS \of F Y h:i:s A').$input.$salt);
	}

    /**
     * Convert the string $pwd in a hash code
     * with a codification "sha256"
     */
	public static function toHash($pwd)
	{
		return hash('sha256',$pwd);
	}
	
    /**
     * Depending on the date on which the script is executed 
     * and the $input to be passed to the function will return 
     * a different hash code for each call 
     */
	public static function generateHashCode($input)
	{
		$salt="pbi";
		return database_slave::toHash(date('l jS \of F Y h:i:s A').$input.$salt);
	}
	
    /**
     * Inserted into the table values ​​App_log 
     * $proc_name , $message_type and$ message.
     * Returning true if it was achieved to store 
     * information in a database and false if it failed.
     */
	public function app_log($proc_name,$message_type,$message){
		$mysqli = $this->get_connection();
		$message_esc = str_replace("'", "\"", $message);
		$sql = "Insert into App_Log(Proc_Name,Message_Type,Message) values ('".$proc_name."','".$message_type."','".$message_esc."')";
	
		//error_log($sql);
		$result = $mysqli->query($sql);
		
		return $result;
	}
	
	/**
     * Returns a string,
     * which is a command to insert 
     * ready to be executed , 
     * with the extracted information of the object $obj and
     * into the table $tbl
     */
	public function get_object_sql_insert($tbl,$obj,$with_id=false)
	{
		$sql_template = "insert into %s(%s) values (%s)";
		$sql_cols = "";
		$sql_vals = "";
		
		foreach($obj as $key => $value) {
           		 
           		if($key=='ID' && $with_id == true)
           		{
           			$sql_cols = $sql_cols."".$key.",";
           			$sql_vals = $sql_vals."'".$value."',";  
           		}
           		else if($key != 'ID' && $key != 'Password' && $key != 'Api_Key' && !is_array($obj->$key) && !is_object($obj->$key) && isset($obj->$key)){
           			
           			$sql_cols = $sql_cols."".$key.",";
           			
           			if($value==""){
           				$sql_vals = $sql_vals."NULL,"; //set null istead of empty string value
           			}
           			else{
           				$sql_vals = $sql_vals."'".$value."',";
           			}
           			    			
           		}
           		if($key == 'Password' && isset($obj->$key))
           		{
           			$sql_cols = $sql_cols."".$key.",";
           			$sql_vals = $sql_vals."'".database_slave::toHash($value)."',"; 
           		}
           		if($key == 'Api_Key' && isset($obj->$key))
           		{
           			$sql_cols = $sql_cols."".$key.",";
           			$sql_vals = $sql_vals."'".database_slave::toHash(date('l jS \of F Y h:i:s A'))."',"; 
           		}
        	}        	
        $sql_cols=substr($sql_cols,0,-1);
        $sql_vals=substr($sql_vals,0,-1);
        	
        $sql_insert = sprintf($sql_template,$tbl,$sql_cols,$sql_vals);
        return $sql_insert;
	}
	
    /**
     * Returns a string,
     * which is a command to update 
     * ready to be executed , 
     * with the extracted information of the object $obj and
     * into the table $tbl
     */
	public function get_object_sql_update($tbl,$obj)
	{
		$sql_template = "update %s set %s where ID='%s'";
		$sql_set = "";
		
		foreach($obj as $key => $value) {
           		 
           		if($key != 'ID' && $key != 'Password' && $key != 'Api_Key' && !is_array($obj->$key) && !is_object($obj->$key) && isset($obj->$key)){    			
           			
           			if($value==""){
           				$sql_set = $sql_set.$key."=NULL,";	//set null istead of empty string value
           			}
           			else{
           				$sql_set = $sql_set.$key."='".$value."',";
           			}     			
           			        			
           		}
           		
           		if($key == 'Password' && isset($obj->$key))
           		{$sql_set = $sql_set.$key."='".database_slave::toHash($value)."',";}
           		
           		if($key == 'ID')
           		{$sql_id = $value;}
        	
       	}        	
 		$sql_set=substr($sql_set,0,-1);
 		$sql_update = sprintf($sql_template,$tbl,$sql_set,$sql_id);
        	
         
        return $sql_update;
	}
	
    /**
    * Run the SQL sent through the variable $sql 
    * and if they are executed correctly return the 
    * corresponding ID in the table , 
    * otherwise it will return false
    */	
	public function execute_insert($sql,$ask_id=true){
		
		$newid = -1;
		$result = false;
		
		$mysqli = $this->get_connection();
		if($mysqli){
			try
			{
				if($result = $mysqli->query($sql)){
					if($ask_id){
						$newid = $mysqli->insert_id;
					}
				}
				else{
					error_log($mysqli->error." ".$mysqli->errno);
				}
			}
			catch (mysqli_sql_exception $e) { 
      			//throw $e; 
   			} 		 
		}
		$mysqli->close();
		
		if($ask_id) {
			return $newid;
		}
		else{
			return $result;
		}
	
	}
	
    /**
     * Run the SQL sent through the variable $sql
     * and if they are executed correctly 
     * return TRUE else return FALSE
     */
	public function execute_update($sql)
	{
		$mysqli = $this->get_connection();
		
		if($mysqli){		
			$result = $mysqli->query($sql);
		}						
		$mysqli->close();
		return $result;
	}
	
    /**
     * Depending on the values ​​that have 
     * the $insert_stmts and  $update_stmts objects, 
     * update or insertion commands is executed on the database.
     * If there is an error in the information 
     * of the objects, it will return the error as 
     * an exception and also will return false ,
     * otherwise will return true
     */
	public function executeBatchInsertUpdates($insert_stmts,$update_stmts){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
			
			foreach ($update_stmts as $value) {
				 
				//$sql = addslashes ($value);
				//error_log($value);
				$up = $mysqli->query($value);
				if($up===false)
				{throw new Exception('Wrong SQL: ' . $value . ' Error: ' . $mysqli->error);}
			}
			
			foreach ($insert_stmts as $value) {
				//$sql = addslashes ($value);
				$up = $mysqli->query($value);
				if($up===false)
				{throw new Exception('Wrong SQL: ' . $value . ' Error: ' . $mysqli->error);}
			}			
		
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			return false;
		}	
		
		$mysqli->close();
		return true;
		
	}
	
    /**
     * Try a connection to the database 
     * and if satisfactory return 
     * an object with the connection .
     * In case of failed will return false.
     */
	public function get_connection()
	{
		
		try{
			$mysqli = new mysqli($this->server_ip, $this->server_username, $this->server_password, $this->server_database,$this->server_port);
			$mysqli->set_charset("utf8");
		}
		catch(Exception $e)
		{
			error_log("MySql connection failed!");
			return false;
			
		}
		return $mysqli;	
	}
	
    /**
     * Connecto to the DB,
     * if not achieved returns FALSE    . 
     * If achieved the connection look at 
     * the table ADMIN_App_Users user ( $username ) 
     * and password ( $ password), 
     * if found return an associative array with the user information 
     * otherwise return FALSE.
     */
	public function login($username,$password)
	{
		$mysqli = $this->get_connection();
		
		if(!$mysqli){
			return false;
		}
		else{
			$sql = "select * from ADMIN_App_Users where Username='".$username."' and Password='".database_slave::toHash($password)."'";
			//error_log($sql);
			$users = $mysqli->query($sql);
			if($users->num_rows ==0){
				return false;
			}			
			$user= $users->fetch_array(MYSQLI_ASSOC);
			
			return $user;
		}	
	}
	
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * If the connection is achieved in ADMIN_App_Users
     * table will update the user with 
     * ID ( $app_user_id ) with password ( $new_password ) ,
     * if he can update it will return TRUE , 
     * otherwise it returns FALSE.
     */
	public function set_user_credentials($app_user_id,$new_password){
		$mysqli = $this->get_connection();
		
		if(!$mysqli){
			return false;
		}
		
		$sql = "UPDATE ADMIN_App_Users set Password='".database_slave::toHash($new_password)."' where ID='".$app_user_id."'";
		//error_log($sql);
		$result = $mysqli->query($sql);
								
		$mysqli->close();
		return $result;
		
	}

    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * If the connection is achieved in ADMIN_App_Users
     * will look using the propoerties of $app_user for a
     * user with those properties, if doesn't find it will return TRUE,
     * otherwise if find coincidences values will return FALSE. 
     */
	public function check_conficts_app_user_save($app_user){
		
		$mysqli = $this->get_connection();
		if(!$mysqli){
			return false;
		}
		
		$sql = "select * from ADMIN_App_Users where Username='".$app_user->Username."' and Password='".database_slave::toHash($app_user->Password)."' and ID<>'".$app_user->ID."'";

		$users = $mysqli->query($sql);
		if($users->num_rows >0){
			return false;
		}			
		
		return true;
	}
	
	/**
	 * Connecto to the DB ,
	 * if not achieved returns FALSE.
	 * Depending on the category ( $category ) that is send will choose
	 * a one of the next for "Questionnaires" or for "Tests".
	 * 1. Counts Object_Count for App_User_ID($uid) and Questionnaire_Type_ID($type_id) from table ADMIN_Data_Access joined to Table Questionnaires.
	 * 2. Counts Object_Count for App_User_ID($uid) and Questionnaire_Type_ID($type_id) from table ADMIN_Data_Access joined to Table Tests.
	 * Later will save into the variable $record the Object_Count and will find
	 * into the table "Registrations" for the limit of "Pieces" that user can realize,
	 * If $limit is greater than $count then return TRUE, otherwise return FALSE
     */
	public function check_user_limit($uid,$type_id,$category){
		
		$mysqli = $this->get_connection();
		if(!$mysqli){
			return false;
		}
		
		if($category=="Questionnaires"){
			$sql = "SELECT IFNULL(count(1),0) as Object_Count	FROM `ADMIN_Data_Access` aa
					join Questionnaires qur on aa.Client_ID = qur.Client_ID
					where aa.App_User_ID = '".$uid."' and qur.Questionnaire_Type_ID = '".$type_id."'";
		}
		else{
			$sql = "SELECT IFNULL(count(1),0) as Object_Count	FROM `ADMIN_Data_Access` aa
					join Tests qur on aa.Client_ID = qur.Client_ID
					where aa.App_User_ID = '".$uid."' and qur.Test_Type_ID = '".$type_id."'";
		}
		$recordset = $mysqli->query($sql);
		
		$record = $recordset->fetch_array(MYSQLI_ASSOC);
		$count = $record["Object_Count"];
		//error_log($count);
		
		$sql = "SELECT IFNULL(sum(Pieces),0) as Pieces FROM  `Registrations` where App_User_ID = '".$uid."' and Type_ID='".$type_id."' and Category='".$category."'";
		$recordset = $mysqli->query($sql);
		$record = $recordset->fetch_array(MYSQLI_ASSOC);
		$limit = $record["Pieces"];
		//error_log($limit);
		if($count<$limit){return true;}
		
		return false;
	}
	
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Will check for a valid USER from "ADMIN_App_Users"
     * using the $user_id and $api_key. If find a match return TRUE,
     * otherwise will return FALSE. 
     */
	public function app_user_valid($user_id,$api_key){
		
		$mysqli = $this->get_connection();
		
		if(!$mysqli){
			return false;
		}
		
		$sql = "select * from ADMIN_App_Users where ID='".$user_id."' and Api_Key='".$api_key."'";
		$users = $mysqli->query($sql);
		if($users->num_rows ==0){
				return false;
		}
		
		return true;		
	}
	
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Will return the Query executed, assambled with $table_name,
     * $column and $value,
     */
	public function get_recordset($table_name,$column="",$value=""){
		
		$mysqli = $this->get_connection();
		
		if(!$mysqli){
			return false;
		}
		$sql = "select * from ".$table_name ;
		
		if($column!=""){
			$sql = $sql." where ".$column."='".$value."'";
		}

		$set = $mysqli->query($sql);
		
		return $set;
		
	}
	
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Will execute and return the result from the SQL command
     * passed by $sql variable. 
     */
	public function get_sql_results($sql,$first_only=false){
		
		$mysqli = $this->get_connection();
		
		if(!$mysqli){
			return false;
		}
		$set = $mysqli->query($sql);
		
		return $set;
	
	}
	
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Will execute and return the SQL command from PARAM_Rep_Full
     * where Rule_Type is different to 'GDIV','CDIV' or 'CRIT_ITEM' and
     * Active_Rule=1 and Questionnaire_Type_ID= $questionnaire_type_id
     * ordered by Report_Name,Order_Presentation,Order_Section,ID
     */
	public function get_rep_config($questionnaire_type_id){
	
		$mysqli = $this->get_connection();
		if(!$mysqli){return false;}
		$sql = "select * from PARAM_Rep_Full where Rule_Type not in ('GDIV','CDIV','CRIT_ITEM') and Active_Rule=1 and Questionnaire_Type_ID='".$questionnaire_type_id."' order by Report_Name,Order_Presentation,Order_Section,ID";
		$set = $mysqli->query($sql);		
		return $set;		
	
	}
	
    /**
     * Select and return from "Scale_Groups_Rep"
     * all the information if exists in "MAP_Scales_Groups_Rep"
     * one Questionnaire_Type_Id equal to $questionnaire_type_id
     * and one Group_Rep_ID is equal to a "Scale_Groups_Rep" ID.
     */	
	public function get_scale_groups_rep_for_questionnaire_type($questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		/*$sql = "SELECT * FROM `Scale_Groups_Rep` sc where exists
				(select 1 from PARAM_Scales p
				join MAP_Scales_Questionnaire_Types t 
  					on p.ID = t.Scale_ID
  					and t.Questionnaire_Type_Id = '".$questionnaire_type_id."'
				where p.Group_Rep_ID = sc.ID);";*/
		
		$sql =  "SELECT * FROM `Scale_Groups_Rep` sc where exists
				(select 1 from MAP_Scales_Groups_Rep p
				 where p.Questionnaire_Type_Id = '".$questionnaire_type_id."'
				and p.Group_Rep_ID = sc.ID)";
		
		$set = $mysqli->query($sql);
		
		return $set;	
	}
	
    /**
     * Returns all the questions for a $questionnaire_type_id
     * and for a $language_code from "PARAM_Questions" joined to "MAP_Questions_Questionnaire_Types"
     */
	public function get_questions_for_questionnaire_type($questionnaire_type_id,$language_code){
		
		$mysqli = $this->get_connection();
		
		$sql = "Select p.Question_".$language_code." as Question,p.* from PARAM_Questions p 
				join MAP_Questions_Questionnaire_Types t on p.ID = t.Question_ID
				where t.Questionnaire_Type_ID='".$questionnaire_type_id."' order by p.Code";
		 
		/*$sql = "Select p.* from PARAM_Questions p 
				join MAP_Questions_Questionnaire_Types t on p.ID = t.Question_ID
				where t.Questionnaire_Type_ID='".$questionnaire_type_id."' order by p.Code";*/
				
		$set = $mysqli->query($sql);
		
		return $set;
		
	}
	
    /**
     * Returns all the answers from "PARAM_Answer_Groups" joined with 
     * "PARAM_Questionnaire_Types" where the ID of 
     * "PARAM_Questionnaire_Types" is equal to $questionnaire_type_id.
     */
	public function get_answer_group_for_questionnaire_type($questionnaire_type_id,$language_code){
		
		$mysqli = $this->get_connection();
		
		$sql = "Select ag.Answer,ag.Order from PARAM_Answer_Groups ag 
				join PARAM_Questionnaire_Types qt on ag.ID = qt.Answer_Group_ID
				where qt.ID='".$questionnaire_type_id."' order by ag.Order";
		
		$set = $mysqli->query($sql);
		
		return $set;
	
	}
	
    /**
     * Select all the information from "PARAM_Scales"
     * joined with "MAP_Scales_Questionnaire_Types"
     * where Questionnaire_Type_ID is equal to $questionnaire_type_id
     * and the Type_ID is equal to $scale_type
     */
	public function get_scales_for_questionnaire_type($questionnaire_type_id,$scale_type){
		
		$mysqli = $this->get_connection();
		$sql = "SELECT sc.* FROM 
				PARAM_Scales sc join 
				MAP_Scales_Questionnaire_Types m
  					on sc.ID = m.Scale_ID
				where Questionnaire_Type_ID = '".$questionnaire_type_id."' and Type_ID='".$scale_type."'";
		
		$set = $mysqli->query($sql);
		
		return $set;
	}
	
	/**
     * Select and return the ID, Code, Question_el and Question_en from 
     * PARAM_Questions joined with MAP_Questions_Scales if Question_ID is equal to Param_Questions.ID
     * and joined also to 
     * PARAM_Scales where ID is equal to MAP_Questions_Scales.ID
     * where MAP_Questions_Scales.Questionnaire_Type_ID is equal to $questionnaire_type_id and
     * PARAM_Scales.ID is equal to $scale_id
     * all ordered by PARAM_Questions.Code.
     */
	public function get_questions_for_scale($scale_id,$questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		$sql = "SELECT 
					que.ID,
					que.Code,
					que.Question_el,
					que.Question_en
				FROM 
				PARAM_Questions que 
				join MAP_Questions_Scales map on map.Question_ID = que.ID
				join PARAM_Scales sca on sca.ID = map.Scale_ID
				where map.Questionnaire_Type_ID ='".$questionnaire_type_id."' and sca.ID ='".$scale_id."' 
				order by que.Code";
		
		$set = $mysqli->query($sql);
		
		return $set;
	
	}
	
	/**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Otherwise will execute SQL command selecting
     * Scale_Code, Scale_Code_Descr , Raw_Score and all the columns from the table CALC_A_Scores.
     * Joined to the table "PARAM_Scales", "Questionnaires", "CALC_Raw_Scores" and "MAP_Scales_Groups_Rep"
     * where Questionnaires.Hash_Code is equal to $questionnaire_hash.
     * If the command returns 1 or more rows will return that information, 
     * otherwise will return FALSE in response.
     */
	public function get_group_rep_scores($questionnaire_hash,$group_rep_id=""){
		
		$mysqli = $this->get_connection();
		if(!$mysqli){return false;}
		
		if($group_rep_id==""){$filter="";}
		else{$filter="and rep.Group_Rep_ID = '".$group_rep_id."'";}
		
		$sql = "SELECT sc.Code as Scale_Code,sc.Descr_en as Scale_Code_Descr,a.*,rr.Raw_Score
				FROM CALC_A_Scores a
				join PARAM_Scales sc on a.Scale_ID = sc.ID
				join Questionnaires q on q.ID = a.Questionnaire_ID
				join CALC_Raw_Scores rr on rr.Questionnaire_ID = a.Questionnaire_ID and rr.Scale_ID=a.Scale_ID
				join MAP_Scales_Groups_Rep rep on rep.Scale_ID=sc.ID and rep.Questionnaire_Type_ID=q.Questionnaire_Type_ID
				where q.Hash_Code = '".$questionnaire_hash."' ".$filter."
				order by rep.Group_Rep_ID,sc.Code desc";
		//error_log($sql);
		$set = $mysqli->query($sql);
		
		if($set->num_rows >0){
			return $set;
		}
		
		return false;
	}
	
        
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Otherwise will select FROM "PARAM_Scales"
     * joined to "CALC_A_Scores", "CALC_Raw_Scores", "Questionnaires",
     * "CALC_A_Scores", and "CALC_Raw_Scores" where 
     * Questionnaires.Hash_Code is equal to $q1_hash 
     * and Questionnaires.Hash_Code is equal also to $q2_hash.
     * If the query return more than 1 row, it will return the result
     * Otherwise will return FALSE.
     * 
     */
	public function get_group_rep_scores_cmp($q1_hash,$q2_hash,$group_rep_id=""){
		
		$mysqli = $this->get_connection();
		if(!$mysqli){return false;}
		
		if($group_rep_id==""){$filter="";}
		else{$filter="and exists (select 1 from MAP_Scales_Groups_Rep rep where rep.Questionnaire_Type_ID = q.Questionnaire_Type_ID
					  			  and rep.Scale_ID = sc.ID and rep.Group_Rep_ID = '".$group_rep_id."') ";}
		
		$sql = "SELECT   
  					sc.Code as Scale_Code
  					,sc.Descr_en as Scale_Code_Descr
  					,a.Questionnaire_ID as Questionnaire_ID_1  
  					,q.Comments as Comments_1
  					,a.A_Score as A_Score_1
  					,IFNULL(a.B_Score_1,0) as B_Score_1_1
  					,rr.Raw_Score as Raw_Score_1
  					,a2.Questionnaire_ID as Questionnaire_ID_2 
  					,q2.Comments as Comments_2
  					,a2.A_Score as A_Score_2
  					,IFNULL(a2.B_Score_1,0) as B_Score_1_2
  					,rr2.Raw_Score as Raw_Score_2
			FROM PARAM_Scales sc         
			join CALC_A_Scores a on a.Scale_ID = sc.ID				
			join CALC_Raw_Scores rr on rr.Questionnaire_ID = a.Questionnaire_ID and rr.Scale_ID=a.Scale_ID
			join Questionnaires q on q.ID = a.Questionnaire_ID
			join CALC_A_Scores a2 on a2.Scale_ID = sc.ID
			join CALC_Raw_Scores rr2 on rr2.Questionnaire_ID = a2.Questionnaire_ID and rr2.Scale_ID=a2.Scale_ID
			join Questionnaires q2 on q2.ID = a2.Questionnaire_ID
			where 
			q.Hash_Code = '".$q1_hash."' 
			and q2.Hash_Code = '".$q2_hash."' ".$filter."			 
			order by sc.Group_Rep_ID,sc.Code desc";
		
		$set = $mysqli->query($sql);
		
		if($set->num_rows >0){
			return $set;
		}
		
		return false;
	}
	
    
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Otherwise will select FROM "CALC_Questions_Points" 
     * joined to "PARAM_Questions", "MAP_Questions_Critical_Categories",
     * "Critical_Questions_Categories", "Critical_Questions_Groups" and
     * "Questionnaires" where Questionnaires.Hash_Code is equal to $questionnaire_hash.
     * If the query return more than 1 row, it will return the result
     * Otherwise will return FALSE. 
     */
	public function get_critical_questions_records($questionnaire_hash,$language_code,$type_id){
		$mysqli = $this->get_connection();
		if(!$mysqli){return false;}
		
		$sql ="select 
					cqg.ID as Group_ID,
					cqg.Descr_en as Group_Descr_en,
					cqg.Descr_el as Group_Descr_el,
					cqg.Descr_".$language_code." as Group_Descr,
					/*Case when cqg.Descr_en<>cqg.Descr_el then CONCAT(cqg.Descr_el,' / ',cqg.Descr_en) else cqg.Descr_en end as Group_Descr,*/
					cqc.ID as Category_ID,
					cqc.Descr_en as Category_Descr_en,
					cqc.Descr_el as Category_Descr_el,
					cqc.Descr_".$language_code." as Category_Descr,
					/*Case when cqc.Descr_en<>cqc.Descr_el then CONCAT(cqc.Descr_el,' / ',cqc.Descr_en) else cqc.Descr_en end as Category_Descr,*/
					pr.Code,
					pr.Question_".$language_code." as Question,
					clc.Points
			from CALC_Questions_Points clc
			join PARAM_Questions pr on clc.Question_ID = pr.ID
			join MAP_Questions_Critical_Categories cat on cat.Question_ID = pr.ID and cat.Questionnaire_Type_ID='".$type_id."'
			join Critical_Questions_Categories cqc on cqc.ID = cat.Critical_Category_ID
			join Critical_Questions_Groups cqg on cqg.ID = cqc.Group_ID
			join Questionnaires que on que.ID = clc.Questionnaire_ID
			where que.Hash_Code = '".$questionnaire_hash."'
			order by cqg.ID,cqc.ID,pr.Code";
		
		$set = $mysqli->query($sql);
		
		if($set->num_rows >0){return $set;}
		
		return false;	
	}
	
        
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Otherwise will select FROM "Scale_Groups_Mean"
     * joined to tables "CALC_Mean_Scores", "Questionnaires"
     * where Questionnaires.Hash_Code is equal to $q1_hash and
     * Questionnaires.Hash_Code is equal to $q2_hash.
     * If the query return more than 1 row, it will return the result
     * Otherwise will return FALSE. 
     */
	public function get_group_mean_scores_cmp($q1_hash,$q2_hash,$group_mean_code=""){
		$mysqli = $this->get_connection();
		if(!$mysqli){return false;}
		
		if($group_mean_code==""){$filter="";}
		else{$filter=" and sgm.Code = '".$group_mean_code."'";}
		
		$sql = "
				Select 
  					sgm.Code as Mean_Code,
  					sgm.Descr_en,
  					que.Comments as Comments_1,
  					clc.Mean_Score as Mean_Score_1,
  					que2.Comments as Comments_2,
  					clc2.Mean_Score as Mean_Score_2  
				from Scale_Groups_Mean sgm 
				join CALC_Mean_Scores clc on clc.Mean_ID = sgm.ID
				join Questionnaires que on que.ID = clc.Questionnaire_ID
				join CALC_Mean_Scores clc2 on clc2.Mean_ID = sgm.ID
				join Questionnaires que2 on que2.ID = clc2.Questionnaire_ID
				where 
				que.Hash_Code = '".$q1_hash."' 
				and que2.Hash_Code = '".$q2_hash."' ".$filter.""; 
				 
		
		
		$set = $mysqli->query($sql);
		
		if($set->num_rows >0){
			return $set;
		}
		
		return false;
	}
	
    
    /**
     * Connecto to the DB ,
     * if not achieved returns FALSE.
     * Otherwise will select from the table "CALC_Mean_Scores"
     * joined to the tables "Scale_Groups_Mean" and "Questionnaires"
     * where Questionnaires.Hash_Code is equal to $questionnaire_hash.
     * If the query return more than 1 row, it will return the result
     * Otherwise will return FALSE. 
     */
	public function get_group_mean_scores($questionnaire_hash,$group_mean_code=""){
		$mysqli = $this->get_connection();
		if(!$mysqli){return false;}
		
		if($group_mean_code==""){$filter="";}
		else{$filter=" and sgm.Code = '".$group_mean_code."'";}
		
		$sql = "Select sgm.Code as Mean_Code,sgm.Descr_en,clc.Mean_Score
				from CALC_Mean_Scores clc 
				join Scale_Groups_Mean sgm on clc.Mean_ID = sgm.ID
				join Questionnaires que on que.ID = clc.Questionnaire_ID
				where que.Hash_Code = '".$questionnaire_hash."' ".$filter ;
				
		
		/*$sql = "SELECT sgm.ID,sgm.Descr_en,SUM(a.A_Score)/COUNT(a.Scale_Code) as Mean_Score
				FROM CALC_A_Scores a
				join Questionnaires q on q.ID = a.Questionnaire_ID
				join MAP_Scales_Groups_Mean mm on mm.Scale_Code = a.Scale_Code
				join Scale_Groups_Mean sgm on sgm.ID = mm.Group_Mean_ID
				where q.Hash_Code = '".$questionnaire_hash."' ".$filter."
				group by sgm.ID,sgm.Descr_en";
		*/
		$set = $mysqli->query($sql);
		
		if($set->num_rows >0){
			return $set;
		}
		
		return false;
		
	}
	
    
    /**
     * Will execute a command SQL formed with the information contained into
     * the object $filter_cols from the table passed in $db_table ordered by 
     * the statement passed in $order_col, if none is passed the default is by ID.
     * If the query return more than 1 row, it will close the connection and return the result
     * Otherwise will return FALSE. 
     */
	public function get_db_recordset($db_table,$filter_cols,$order_col="ID"){
		
		$mysqli = $this->get_connection();
		
		$where_clause = "";
		
		foreach ($filter_cols as $key => $val) {
    		$where_clause = $where_clause." $key = '$val' and";    		 
		}
		$where_clause = substr($where_clause,1,-3);
		
		
		$sql = "select * from ".$db_table." where ".$where_clause." order by ".$order_col;
			 
		
		$recordset = $mysqli->query($sql);
		
		if($recordset->num_rows > 0){
			 $mysqli->close();
			 return $recordset;
		}
		 
		$mysqli->close();
		return false;
	}
	
        
    /**
     * Will select all the information from $db_table
     * where $filter_col is equal to $rec_id.
     * If the query return more than 1 row, it will close the connection and return the result
     * in form of associative array.
     * Otherwise will return FALSE. 
     * 
     * @param $db_table Table where is going to find
     * @param $rec_id  Value in the column used for filtering.
     * @param $filter_col Column where is going to be filtered data
     * 
     * 
     */
	public function get_db_record($db_table,$rec_id,$filter_col="ID")
	{
		$mysqli = $this->get_connection();
		
		$sql = "select * from ".$db_table." where ".$filter_col."='".$rec_id."'";
		 
		$recordset = $mysqli->query($sql);
		
		if($recordset->num_rows > 0){
			$record = $recordset->fetch_array(MYSQLI_ASSOC);
		}
		else
		{
			$record = false;
		}
		$mysqli->close();
		
		return $record;
	}
	
    /**
     * Will select from the $table, the values $id_col and $text_col
     * Where if $fcolumn is empty will only order the result by $id_col, otherwise
     * will add to the query the where statement for $fcolumn equal to $fvalue
     * If the query return more than 1 row, it will close the connection and return the result
     * Otherwise will return FALSE. 
     */
	public function get_recordset_combo($id_col,$text_col,$table,$fcolumn="",$fvalue="")
	{
		$mysqli = $this->get_connection();
		$sql = "select ".$id_col." as value, ".$text_col." as text from ".$table."";
	 	
	 	if($fcolumn!=""){
			$sql = $sql." where ".$fcolumn."='".$fvalue."' order by 1";
		}
		else{
			$sql = $sql." order by 1";
		}

	 	
		$records = $mysqli->query($sql);
		
		if($records->num_rows == 0){			
			$mysqli->close();
			return false;
		}
		else {		
			$mysqli->close();	
			return $records;
		}
	}
	
        
    /**
     * This function update the value of Process_Times in "Questionnaires" table
     * whith Process_Times plus $times where ID is equal to $questionnaire_id.
     * If the update is succesfull will return TRUE otherwise FALSE.
     */
	public function adjust_questionnaire_process_times($questionnaire_id,$times){
		
		$mysqli = $this->get_connection();
		$sql = "Update Questionnaires set Process_Times=Process_Times+".$times." where ID='".$questionnaire_id."'";
    	$res = $mysqli->query($sql);
    	
    	return $res;	
	}
        
    /**
     * Make the connection to DB.
     * And Update into the table Questionnaires the value Fill_Time with $fill_time
     * where ID is $questionnaire_id.
     * Returns True if is updated and FALSE otherwise
     */
	public function adjust_questionnaire_fill_time($questionnaire_id,$fill_time){
		
		$mysqli = $this->get_connection();
		$sql = "Update Questionnaires set Fill_Time='".$fill_time."' where ID='".$questionnaire_id."'";
    	$res = $mysqli->query($sql);
    	
    	return $res;	
	}
	
    
    /**
     * Make the connection to DB.
     * And Update into the table Questionnaires the value Online_Access with $access
     * where ID is $questionnaire_id.
     * Returns True if is updated and FALSE otherwise
     */
	public function adjust_questionnaire_online_access($questionnaire_id,$access){
		
		$mysqli = $this->get_connection();
		$sql = "Update Questionnaires set Online_Access='".$access."' where ID='".$questionnaire_id."'";
    	$res = $mysqli->query($sql);
    	
    	return $res;	
	}
	
    /**
     * Will store in variable as associative Array
     * $proc_rec from table ADMIN_Parameters where Code is equal to MAX_PROCESS_TIMES.
     * And will store to into $ques_rec where ID is equal to $questionnaire_id from table Questionnaires.
     * if the value in $proc_rec["Value"] is greater than the value in $ques_rec["Process_Times"] 
     * will return FALSE, otherwise will return TRUE.
     */
	public function check_process_limit($questionnaire_id){
		
		$proc_rec = $this->get_db_record("ADMIN_Parameters","MAX_PROCESS_TIMES","Code");
		$ques_rec = $this->get_db_record("Questionnaires",$questionnaire_id);
		
		//error_log($ques_rec["Process_Times"]);
		//error_log($proc_rec["Value"]);
		
		if($ques_rec["Process_Times"]<$proc_rec["Value"]){return false;}
		
		return true;
	}
	
        
        /**
         * Make the connection to DB.
         * Will select all information from  ADMIN_App_Users.
         * And will return that object.
         */
	public function get_user_list(){
		
		$mysqli = $this->get_connection();
		
		$sql = "Select *  from ADMIN_App_Users order by 1";		
		$recordset = $mysqli->query($sql);
				
		$mysqli->close();
		return $recordset;
	}
	
    
    /**
     * If $is_admin is 0, Then:
     *      1.Select ID, Surname, Name, Birthdate, Gender_ID, Marital_Status_ID, Children
     *        Profession, Comments from table Clients.
     *      2.Where ADMIN_Data_Access equal to $user_id.
     * else, if $is_admin is different to 0, Then:
     *      1.Select ID, Surname, Name, Birthdate, Gender_ID, Marital_Status_ID, Children
     *        Profession, Comments from table Clients.
     *      2.Where ADMIN_Data_Access is not NULL.
     * 
     * Then return that object.
     */
	public function get_client_list($is_admin,$user_id){
		
		$sec_policy = "";
		$privs = "";
		
		if($is_admin == 0){
			//$sec_policy = " where exists (select 1 from ADMIN_Data_Access ad where ad.App_User_ID='".$user_id."' and ad.Client_ID = c.ID) ";
			$privs = "acc.Priviledges";
			$sec_policy = " where acc.App_User_ID='".$user_id."'";
			
		}
		else{
			$privs = "'rw'";
			$sec_policy = " where ad.App_User_ID is not null ";		
		}
		
		$mysqli = $this->get_connection();
		$sql = "Select 
					c.ID,
					c.Surname,
					c.Name,
					c.Birthdate,
					c.Gender_ID,
					c.Marital_Status_ID,
					c.Children,
					c.Profession,
					c.Comments,
					u.Username as Owner_Username,
					u.Surname as Owner_Surname,
					u.Name as Owner_Name,
					".$privs." as Priviledges,
					count(distinct q.ID) as Questionnaires
				from Clients c 
				left join Questionnaires q on c.ID=q.Client_ID 
				left join ADMIN_Data_Access acc on acc.Client_ID = c.ID and acc.App_User_ID = '".$user_id."' 
				left join ADMIN_Data_Access ad on ad.Client_ID = c.ID and ad.Is_Owner = 1
				left join ADMIN_App_Users u on ad.App_User_ID = u.ID
				".$sec_policy."
				group by 
					c.ID,
					c.Surname,
					c.Name,
					c.Birthdate,
					c.Gender_ID,
					c.Marital_Status_ID,
					c.Children,
					c.Profession,
					c.Comments,
					u.Username,
					u.Surname,
					u.Name,
					".$privs."
				";
		
		//error_log($sql);
		
		$recordset = $mysqli->query($sql);
		$mysqli->close();
		return $recordset;		 
	
	}
	
        
    /**
     * Make the connection to DB.
     * 1. Select ID, Code, Question_$language_code and Answer from table PARAM_Questions
     * 2.where Questionnaires.$filter_col is equal to $filter_val 
     * 3. Ordered by PARAM_Questions.Code.
     * 
     * Return that object.
     */
	public function get_questionnaire_answers($filter_val,$language_code,$filter_col="ID")
	{
		$mysqli = $this->get_connection();
		$sql = "Select p.ID,p.Code,p.Question_".$language_code." as Question,IFNULL(det.Answer,1) as Answer from PARAM_Questions p 
				left outer join Questionnaires_Details det 
			    	on p.ID = det.Question_ID
			    left outer join Questionnaires que 
			    	on que.ID=det.Questionnaire_ID
			    where que.".$filter_col." = '".$filter_val."'	
			    order by p.Code
			    ";
		$recordset = $mysqli->query($sql);
		$mysqli->close();
		return $recordset;
		
	}
	
    /**
     * Make the connection to DB.
     * 1. Select Column_$language_code from table PARAM_Rep_Columns
     * 2. Where Rep_Name equal to $report_name
     * 3. Ordered by Column_Order.
     * 
     * Return that object.
     */
	public function get_report_columns($report_name,$language_code){
		
		$mysqli = $this->get_connection();
		$sql = "SELECT 
					Column_".$language_code." as Column_Name,
					Width
				FROM `PARAM_Rep_Columns` 
				where Rep_Name = '".$report_name."'
				order by Column_Order";
		
		$recordset = $mysqli->query($sql);
		$mysqli->close();
		return $recordset;
	}
	
    /**
     * Make the connection to DB.
     * 1. Select ID, Code, Descr_el, Descr_en, A_Score and Questionnaire_ID from Tables PARAM_Scales and CALC_A_Scores
     * 2. Where PARAM_Scales and CALC_A_Scores ID's are equals ,
     * Type_ID equal to $scale_type_id and Questionnaire_ID to Questionnaire_ID.
     * 
     * Return That Object.
     */
	public function get_questionnaire_scales_score($questionnaire_id,$scale_type_id){
		
		$mysqli = $this->get_connection();
		
		$sql = "select 
					p.ID
					,p.Code
					,p.Descr_el
					,p.Descr_en
					,ca.A_Score
					,ca.Questionnaire_ID 
				from PARAM_Scales p
				join CALC_A_Scores ca on p.ID = ca.Scale_ID
				where p.Type_ID = '".$scale_type_id."' and ca.Questionnaire_ID = '".Questionnaire_ID."'
				order by 1";
		//error_log($sql);
		
		$recordset = $mysqli->query($sql);
		$mysqli->close();
		return $recordset;
	}
	
        
    /**
     * Make the connection to DB.
     * 1. Select ID, Code, Question in lenguage ($language_code), Answer and Points from tables PARAM_Questions and CALC_Questions_Points
     * 2. Where PARAM_Questions, MAP_Questions_Scales and CALC_Questions_Points ID's are equals and  
     * Questionnaire_ID equal to $questionnaire_id 
     * 3. Order by Code.
     * 
     * Return that object.
     */
	public function get_questionnaire_scale_questions($questionnaire_id,$scale_id,$language_code){
	
		$mysqli = $this->get_connection();
		
		$sql = "select 
					p.ID,
					p.Code,
					p.Question_".$language_code." as Question,					
					calc.Answer,
					calc.Points
				from PARAM_Questions p
				join MAP_Questions_Scales m
  					on p.ID = m.Question_ID
				join PARAM_Scales pc
  					on pc.ID = m.Scale_ID
				join CALC_Questions_Points calc
  					on calc.Question_ID = p.ID
				where pc.ID = '".$scale_id."'
					and calc.Questionnaire_ID = '".$questionnaire_id."'
				order by 2";

		$recordset = $mysqli->query($sql);
		$mysqli->close();
		return $recordset;
	
	}
	
        
    /**
     * Make the connection to DB.
     * 1. Select Surname, Name, Birthdate, Client_Comments, Profession,
     * Marital_Status, Gender, Last_Save, Fill_Time, Questionnaire_Comments, Questionnaire_Type_ID,
     * Questionnaire_Type_Descr from Tables Questionnaires, Clients, Marital_Statuses and Genders.
     * 2. Where Questionnaires Hash_Code equal to $hash.
     * 
     * If the query return 1 or more rows, will return the information as associative array.
     * Otherwise will return FALSE.
     */
	public function get_questionnaire_info($hash){
		$mysqli = $this->get_connection();
		$sql = "select 
  					c.Surname,
  					c.Name,
  					c.Birthdate,
  					c.Comments as Client_Comments,
  					c.Profession,
  					ma.Descr_En as Marital_Status,
  					ge.Descr_En as Gender,
  					q.Last_Save,
  					q.Fill_Time,
  					q.Comments as Questionnaire_Comments,
  					q.Questionnaire_Type_ID,
  					param.Descr_en as Questionnaire_Type_Descr
				from Questionnaires q 
				join PARAM_Questionnaire_Types param on param.ID = q.Questionnaire_Type_ID
				join Clients c on c.ID = q.Client_ID 
				join Marital_Statuses ma on c.Marital_Status_ID = ma.ID
				join Genders ge on c.Gender_ID = ge.ID				
				where q.Hash_Code = '".$hash."'";
		//error_log($sql);
		$recordset = $mysqli->query($sql);
		
		if($recordset->num_rows > 0)
			{$record = $recordset->fetch_array(MYSQLI_ASSOC);}
		else
			{$record = false;}
		$mysqli->close();
		
		return $record;
	}
	
    /**
     * Make the connection to DB.
     * 1. Select ID, Object_Type,Rule_Type,Presentation_Section,Presentation_Order,
     *    Presentation_Duration, Expiration_Action,Section_Break,
     *    Content_ID, Content_Code, Content_TXT, Content_Data, HTML_el, HTML_en,
     *    Content_Attribute_1, Content_Attribute_2, Content_Attribute_3, Content_Attribute_4,
     *    Content_Width, Content_Height FROM tables PARAM_Test_Full, PARAM_Exercises and PARAM_Test_Material
     * 2.Where Test_Type_ID equal to $test_type_id
     * 
     * Then return that object.
     */
	public function get_test_structure($test_type_id){
		
		$mysqli = $this->get_connection();
		$sql = "
				SELECT 
					pf.ID,
					pf.Object_Type,
					pf.Rule_Type,
					pf.Presentation_Section,
					pf.Presentation_Order,
					pf.Presentation_Duration,
					coalesce(pf.Expiration_Action,'n/a') as Expiration_Action,
					pf.Section_Break,
					coalesce(ts.ID,pe.ID,ptm.ID,-1) as Content_ID,
					coalesce(pe.Code,-1) as Content_Code,
					coalesce(ts.Descr_en,pe.Descr_en,ptm.Descr_en) as Content_TXT,
					coalesce(pf.Data,'n/a') as Content_Data,
					pf.Html_el as HTML_el,
					pf.Html_en as HTML_en,
					coalesce(pf.Attribute_1,'n/a') as Content_Attribute_1,
					coalesce(pf.Attribute_2,'n/a') as Content_Attribute_2,
					coalesce(pf.Attribute_3,'n/a') as Content_Attribute_3,
					coalesce(pf.Attribute_4,'n/a') as Content_Attribute_4,
					coalesce(pf.Image_Width,-1) as Content_Width,
					coalesce(pf.Image_Height,-1) as Content_Height
				FROM PARAM_Test_Full pf
				left join PARAM_Test_Sections ts 
  					on pf.Object_Type = 'PARAM_Test_Sections'
  					and pf.Object_ID = ts.ID
				left join PARAM_Exercises pe
  					on pf.Object_Type = 'PARAM_Exercises'
  					and pf.Object_ID = pe.ID
				left join PARAM_Test_Material ptm 
  					on pf.Object_Type = 'PARAM_Test_Material'
  					and pf.Object_ID = ptm.ID
				where pf.Test_Type_ID = '".$test_type_id."'
				order by Presentation_Section,Presentation_Order
			";
			$recordset = $mysqli->query($sql);
			return $recordset;
	}	
	
        
    /**
     * Make the connection to DB.
     * 1. Select ID,Questionnaire_Type_ID from Questionnaires
     * 2. Where Hash_Code equal to $hash_code
     * 
     * If that query return 1 row or more then will do the next (otherwise will return FALSE):
     *      1. Put the last query into an Associative Array.
     *      2. Then Select ID, Rule_Type, Title_el, Title_en, Descr_el, Descr_en, Scale_Code_1,
     *         Scale_Code_2, Scale_Code_1_Descr_en, Scale_Code_2_Descr_en,Info_Title_el,
     *         Info_Title_en, Info_Descr_el, Info_Descr_en, Heading_el, Heading_en, Data,
     *         Min_Value, Max_Value, Image_File, Image_Width, Image_Height, Page_Break,
     *         Expression, Sc_1_Score, Sc_2_Score, Sc_3_Score, Sc_1_Score_b, Sc_2_Score_b,
     *         Sc_3_Score_b FROM Tables PARAM_Rep_Full, PARAM_Scales, CALC_A_Scores and Scale_Groups_Rep.
     *      3. Where Report_Name equal to $report_name and Questionnaire_Type_ID equal to $type_id
     *         and Rule_Type different to  'I' and Active_Rule equal to 1.
     *      4. Ordered by Order_Presentation and  Order_Section and ID
     * 
     * Then will return that object.
     */
	public function get_report($report_name,$hash_code){
				
		
		$mysqli = $this->get_connection();
		$sql = "Select ID,Questionnaire_Type_ID from Questionnaires where Hash_Code='".$hash_code."'";
		$recordset = $mysqli->query($sql);
		
		if($recordset->num_rows > 0){
			$record = $recordset->fetch_array(MYSQLI_ASSOC);
			$qid = $record["ID"];
			$type_id = $record["Questionnaire_Type_ID"];
						 
					
			$sql = "SELECT 
						re.ID,
						re.Rule_Type,
						re.Title_el,
						re.Title_en,
						re.Descr_el,
						re.Descr_en,
												
						psc1.Code as Scale_Code_1,
						psc2.Code as Scale_Code_2,
						psc1.Descr_en as Scale_Code_1_Descr_en,
						psc2.Descr_en as Scale_Code_2_Descr_en,						
						
						i.Title_el as Info_Title_el,
						i.Title_en as Info_Title_en,
						i.Descr_el as Info_Descr_el,
						i.Descr_en as Info_Descr_en,

						IFNULL(pp.Descr_el,pp.Descr_en) as Heading_el,
						pp.Descr_en as Heading_en,
						
						re.Data,
						re.Min_Value,
						re.Max_Value,
						
						IFNULL(re.Image_File,'') as Image_File,
						IFNULL(re.Image_Width,'') as Image_Width,
						IFNULL(re.Image_Height,'') as Image_Height,
						
						re.Page_Break,
						
						re.Expression,
						IFNULL(calc.A_Score,0) as Sc_1_Score,
						IFNULL(calc2.A_Score,0) as Sc_2_Score,
						IFNULL(calc3.A_Score,0) as Sc_3_Score,
						
						IFNULL(calc.B_Score_1,0) as Sc_1_Score_b,
						IFNULL(calc2.B_Score_1,0) as Sc_2_Score_b,
						IFNULL(calc3.B_Score_1,0) as Sc_3_Score_b
						
						FROM `PARAM_Rep_Full` re
						left join `PARAM_Rep_Full` i 
  							on i.Rule_Type = 'I' 
  							and re.Rule_Type = 'E'
  							and re.Info_Row_ID = i.ID
  							and re.Report_Name = i.Report_Name
  							and re.Questionnaire_Type_ID = i.Questionnaire_Type_ID
						left join CALC_A_Scores calc on calc.Scale_ID = re.Scale_Code_1_ID and calc.Questionnaire_ID = ".$qid."
						left join CALC_A_Scores calc2 on calc2.Scale_ID = re.Scale_Code_2_ID and calc2.Questionnaire_ID = ".$qid."
						left join CALC_A_Scores calc3 on calc3.Scale_ID = re.Scale_Code_3_ID and calc3.Questionnaire_ID = ".$qid."
						left join Scale_Groups_Rep pp on re.Rule_Type = 'GDIV' and re.Data = pp.ID
						left join PARAM_Scales psc1 on psc1.ID = re.Scale_Code_1_ID
						left join PARAM_Scales psc2 on psc2.ID = re.Scale_Code_2_ID
						where re.Report_Name = '".$report_name."' and re.Questionnaire_Type_ID =".$type_id." and re.Rule_Type <> 'I' and re.Active_Rule = 1
						order by re.Order_Presentation,re.Order_Section,re.ID;
					";							
		 		
			$recordset = $mysqli->query($sql);
		}
		else{
			$mysqli->close();
			return false;	
		}
		
		$mysqli->close();
		return $recordset;
	
	}
	
	
    /**
     * Make the connection to DB.
     * 1.Select ID and Questionnaire_Type_ID from Questionnaires Table 
     * 2. Where Hash_Code equal to $hash
     * 
     * If the query returns one row or more then do next ( Otherwise return FALSE ):
     *      1.Transform the query into an Associative Array.
     *      2.Select Label, Descr, Raw_Score_1, Raw_Score_2, Raw_Score, Max_Value_Descr and Percentage 
     *        From Tables PARAM_Rep_Risk_Assessment and CALC_Raw_Scores
     *      3.Where Questionnaire_Type_ID equal to $type_id.
     *      4.Ordered by ID.
     * 
     * Then return the result of the query.
     *
     */
	public function get_rep_risk_assessment($hash,$language_code){
	
		$mysqli = $this->get_connection();
		$sql = "Select ID,Questionnaire_Type_ID from Questionnaires where Hash_Code='".$hash."'";
		$recordset = $mysqli->query($sql);
		
		if($recordset->num_rows > 0){
			$record = $recordset->fetch_array(MYSQLI_ASSOC);
			$qid = $record["ID"];
			$type_id = $record["Questionnaire_Type_ID"];
			
			$sql = "Select
						Label_".$language_code." as Label,
						Descr_".$language_code." as Descr,
						cc.Raw_Score as Raw_Score_1,
						cc2.Raw_Score as Raw_Score_2,
						IFNULL(cc.Raw_Score,0)+IFNULL(cc2.Raw_Score,0) as Raw_Score,
						r.Max_Value_Descr_".$language_code." as Max_Value_Descr,
						ROUND((IFNULL(cc.Raw_Score,0)+IFNULL(cc2.Raw_Score,0))*100/r.Max_Value,2) as Percentage
					from PARAM_Rep_Risk_Assessment r
					left join CALC_Raw_Scores cc on r.Scale_ID_1 = cc.Scale_ID and cc.Questionnaire_ID = '".$qid."'
					left join CALC_Raw_Scores cc2 on r.Scale_ID_2 = cc2.Scale_ID and cc2.Questionnaire_ID = '".$qid."'
					where r.Questionnaire_Type_ID = '".$type_id."'
					order by r.ID
					";
			$recordset = $mysqli->query($sql);
		}
		else{
			$mysqli->close();
			return false;
		}
		$mysqli->close();
		return $recordset;
	}
	
    
    /**
     * Make the connection to DB.
     * 1.Select ID,Questionnaire_Type_ID from Questionnaires Table.
     * 2.Where Hash_Code equal to $hash
     * 
     * If the query returns one row or more then do next ( Otherwise return FALSE ):
     *      1.Transform the query into an Associative Array.
     *      2.Select p.ID, Expr, Expr_Descr, Score, Row_Type, Header_en, Label_en, Mean_Descr_en From Table
     *        PARAM_Rep_Diagnostic_Criteria, Joined to CALC_A_Scores, PARAM_Scales, CALC_Mean_Scores and CALC_Mean_Scores.
     *      3.where Questionnaire_Type_ID equal to $type_id
     *      4.Ordered by ID.
     * 
     * If the query is executed succesfully will return the Object, otherwise will return FALSE.
     * 
     */
	public function get_rep_diagnostic_criteria($hash){
		
		$mysqli = $this->get_connection();
		$sql = "Select ID,Questionnaire_Type_ID from Questionnaires where Hash_Code='".$hash."'";
		$recordset = $mysqli->query($sql);
		
		if($recordset->num_rows > 0){
			$record = $recordset->fetch_array(MYSQLI_ASSOC);
			$qid = $record["ID"];
			$type_id = $record["Questionnaire_Type_ID"];
			$sql = "
					SELECT 
						p.ID,
						CASE 
   					 		WHEN Row_Type = 'scale_code_thrd' THEN
    							CONCAT('(',IFNULL(cla.A_Score,0),IFNULL(Operand_1,'-'),IFNULL(cla2.A_Score,0),IFNULL(Operand_2,'-'),IFNULL(cla3.A_Score,0),')',IFNULL(Operand_CMP,'='),IFNULL(Threshold,0),'?',IFNULL(Result_True,0),':',IFNULL(Result_False,0))
    						WHEN Row_Type = 'group_mean_thrd' THEN
    							CONCAT(IFNULL(msc.Mean_Score,0),IFNULL(Operand_CMP,'='),IFNULL(Threshold,0),'?',IFNULL(Result_True,0),':',IFNULL(Result_False,0))
    						ELSE ''
						END as Expr,
						CASE 
    						WHEN Row_Type = 'scale_code_thrd' THEN
    							CONCAT(IFNULL(aa.Code,''),IFNULL(Operand_1,''),IFNULL(aa2.Code,''),IFNULL(Operand_2,''),IFNULL(aa3.Code,''),IFNULL(Operand_CMP,''),IFNULL(Threshold,'')) 
    						WHEN Row_Type = 'group_mean_thrd' THEN
    							CONCAT(IFNULL(sgm.Descr_en,''),IFNULL(Operand_CMP,'='),IFNULL(Threshold,''))
    						WHEN Row_Type = 'scale_code_score' THEN
    							aa.Code
    						ELSE Label_en
						END as Expr_Descr,
						CASE 
    						WHEN Row_Type = 'scale_code_thrd' THEN
    							CONCAT('(',IFNULL(cla.A_Score,0),IFNULL(Operand_1,'-'),IFNULL(cla2.A_Score,0),IFNULL(Operand_2,'-'),IFNULL(cla3.A_Score,0),')')
    						WHEN Row_Type = 'group_mean_thrd' THEN
    							msc.Mean_Score
    						WHEN Row_Type = 'scale_code_score' THEN
    							cla.A_Score
    						ELSE 0
						END as Score,
						p.Row_Type,
						p.Header_en,
						p.Label_en, 
						sgm.Descr_en as Mean_Descr_en
					FROM PARAM_Rep_Diagnostic_Criteria p
					left join CALC_A_Scores cla on p.Scale_ID_1=cla.Scale_ID and cla.Questionnaire_ID = '".$qid."'
					left join PARAM_Scales aa on aa.ID = cla.Scale_ID
					left join CALC_A_Scores cla2 on p.Scale_ID_2=cla2.Scale_ID and cla2.Questionnaire_ID = '".$qid."'
					left join PARAM_Scales aa2 on aa.ID = cla2.Scale_ID
					left join CALC_A_Scores cla3 on p.Scale_ID_3=cla3.Scale_ID and cla3.Questionnaire_ID = '".$qid."'
					left join PARAM_Scales aa3 on aa.ID = cla3.Scale_ID
					left join CALC_Mean_Scores msc on p.Mean_ID = msc.Mean_ID and msc.Questionnaire_ID = '".$qid."'
					left join Scale_Groups_Mean sgm on sgm.ID = msc.Mean_ID
					where p.Questionnaire_Type_ID = '".$type_id."'
					order by p.ID";
			
			//error_log($sql);
			$recordset = $mysqli->query($sql);
		}
		else{
			$mysqli->close();
			return false;
		}
		$mysqli->close();
		return $recordset;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try next:
     *  1. Delete from Table CALC_Test_Answers values where Test_ID equal to $qid.
     *  2. if succesfully deleted will also Delete from Table Test where id equal to $qid.
     *  3. If some of the last 2 querys fails will launch an exception and rollback the querys.
     *  4. Otherwise, will return TRUE.
     */
	public function delete_test($qid){
	
		$mysqli = $this->get_connection();
		try {
			$mysqli->autocommit(FALSE);
			$sql="Delete from CALC_Test_Answers where Test_ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql="Delete from Tests where ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();  			 
  			return false;
		}	
		
		$mysqli->close();
  		return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try next:
     * 1. Delete from  CALC_A_Scores Table where Questionnaire_ID equal to $qid.
     * 2. Delete from CALC_Questions_Points Table where Questionnaire_ID equal tp $qid.
     * 3. Delete from CALC_Raw_Scores Table where Questionnaire_ID equal to $qid.
     * 4. Delete from Questionnaires_Details Table where Questionnaire_ID equal $qid.
     * 5. Delete from Questionnaires Table where ID equal to $qid.
     * 
     * If any of the last querys can not be executed, then will launch an exception and roll back the querys.
     * And return FALSE,
     * Otherwise will return TRUE.
     * 
     */
	public function delete_questionnaire($qid){
		
		$mysqli = $this->get_connection();
		
		try {
			$mysqli->autocommit(FALSE);
			
			$sql="Delete from CALC_A_Scores where Questionnaire_ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql="Delete from CALC_Questions_Points where Questionnaire_ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql="Delete from CALC_Raw_Scores where Questionnaire_ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				
			$sql="Delete from Questionnaires_Details where Questionnaire_ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql="Delete from Questionnaires where ID='".$qid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			//error_log($e->getMessage());
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("delete_questionnaire(".$qid.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
  		return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try next: 
     * 1. Delete from Table Registrations where ID equal to $cid.
     * 2. If the query fails, then will launch an exception and save into log the error.
     *    and return FALSE.
     * 3. Otherwise will return TRUE.
     */
	public function delete_registration($cid){
		$mysqli = $this->get_connection();
		try {
			$mysqli->autocommit(FALSE);
			
			$sql = "Delete from Registrations where ID='".$cid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("delete_registration(".$cid.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
  		return true;
	}
	
	
    /**
     * Make the connection to DB.
     * Then will execute the next query:
     * 1. Select Section_ID, Descr_$language_code as Section_Descr, Exercise_ID,
     *    Exercise_Code, Descr_$language_code as Exercise_Descr, Exercise_Pass_Score, Option_ID,
     *    Option_Code, Option_Score, Descr_$language_code as Option_Descr, Test_Answer, Is_Answered 
     *    From Table PARAM_Test_Sections
     * 2.Joined to MAP_Test_Sections_Test_Types, MAP_Exercises_Test_Sections, PARAM_Exercises and CALC_Test_Answers.
     * 3.Ordered by Presentation_Order.
     * 
     * Then will return False if fail the query or the object if is successfully.
     */
	public function get_test_answers($test_id,$test_type_id,$language_code = "en"){
		
		$mysqli = $this->get_connection();
		$sql = "
				select 
  					ps.ID as Section_ID,
  					ps.Descr_".$language_code." as Section_Descr,
  					pe.ID as Exercise_ID,
  					pe.Code as Exercise_Code,
  					pe.Descr_".$language_code." as Exercise_Descr,
  					pe.Pass_Score as Exercise_Pass_Score,
  					peo.ID as Option_ID,
  					peo.Code as Option_Code,
  					peo.Score as Option_Score,
  					peo.Descr_".$language_code." as Option_Descr,
  					case when clc.ID is not null then 'T' else 'F' end as Test_Answer,
  					case when clc2.Answered = '1' then 'T' else 'F' end as Is_Answered 
				from PARAM_Test_Sections ps 
				join MAP_Test_Sections_Test_Types mtt
  					on ps.ID = mtt.Test_Section_ID
  					and mtt.Test_Type_ID = '".$test_type_id."'
				join MAP_Exercises_Test_Sections ma 
  					on ps.ID = ma.Test_Section_ID
  					and ma.Test_Type_ID = mtt.Test_Type_ID 
				join PARAM_Exercises pe 
  					on pe.ID = ma.Exercise_ID
				join PARAM_Exercise_Options peo 
  					on peo.Exercise_ID = pe.ID
				join CALC_Test_Answers clc2
  					on clc2.Test_ID = '".$test_id."' 
  					and clc2.Exercise_ID = pe.ID
				left join CALC_Test_Answers clc 
  					on clc.Test_ID = '".$test_id."' 
  					and clc.Exercise_ID = pe.ID
  					and clc.Exercise_Option_ID = peo.ID
  					and clc.Answered = 1
				order by 
  				mtt.Presentation_Order,ma.Presentation_Order,peo.Presentation_Order
			";
			//error_log($sql);
			$recordset = $mysqli->query($sql);
			return $recordset;
		
		
	}
	
        
    /**
     * Make the connection to DB.
     * 1. Will find inside Table Test where Hash_Code equal to $hash_code.
     * 2. Then will try to Delete from CALC_Test_Answers Table where Test_ID equal to $test_rec["ID"].
     * 3. If fail will launch an exception, else for each answer will 
     *    look for for "exercise_" string into $answers array.
     * 4. If find it will replace the string for ""
     * 5. Insert into Table  CALC_Test_Answers the values of the answers.
     * 6. If the insert fails will launch another exception.
     * 7. Otherwise, Select from table MAP_Exercises_Test_Sections the values of $test_rec["ID"],
     *    Exercise_ID and where Test_Type_ID equal to $test_rec["Test_Type_ID"].
     * 8. And no exist a last answered saved into CALC_Test_Answers with that ID.
     * 9. If the select is successfully, then that information will be inserted in CALC_Test_Answers Table.
     * 10. And Update Tests table with Status='FINISHED' where ID $test_rec["ID"].
     * 11. If a exception was launched will return FALSE the function,
     *      Otherwise will return TRUE.
     * 
     */
	public function save_test_answers($answers){
	
			$mysqli = $this->get_connection();
			$client_id = $answers["client_id"];
 		 	$hash_code = $answers["hash_code"];
 		 	
 		 	$test_rec = $this->get_db_record("Tests",$hash_code,"Hash_Code");
 		 	
 		 	try {
				$mysqli->autocommit(FALSE);
			
 		 		$sql = "DELETE from CALC_Test_Answers where Test_ID='".$test_rec["ID"]."'";
 		 		$res = $mysqli->query($sql);
 		 		if($res===false)
					{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);} 		 		 
 		 	
 		 		foreach($answers as $key => $value) {

					if(strpos($key,"exercise_")===0){
 
						$exercise_id = str_replace("exercise_", "", $key);
						 				
						$sql = "insert into CALC_Test_Answers(Test_ID,Exercise_ID,Exercise_Option_ID) values ('".$test_rec["ID"]."','".$exercise_id."','".$value."')";
						$res = $mysqli->query($sql);			
						if($res===false)
						{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
						 			
					}
				} 
			    
				$sql = "insert into CALC_Test_Answers(Test_ID,Exercise_ID,Answered)
						select '".$test_rec["ID"]."',ma.Exercise_ID,'0'
						from MAP_Exercises_Test_Sections ma
						where Test_Type_ID='".$test_rec["Test_Type_ID"]."' 
						and not exists
						(select 1 from CALC_Test_Answers clc where clc.Test_ID='".$test_rec["ID"]."' and clc.Exercise_ID=ma.Exercise_ID)";
		 		$res = $mysqli->query($sql);
		 		if($res===false)
					{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				
				$sql = "update Tests set Last_Save='".date("Ymd")."',Status='FINISHED' where ID='".$test_rec["ID"]."'";
				$res = $mysqli->query($sql);
		 		if($res===false)
					{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
								
				$mysqli->commit();
			}
			catch (Exception $e) { 
				$mysqli->rollback();
  				$mysqli->close();  
  				$this->app_log("save_test_answers()","E",$e->getMessage());
  				return false;
			}	
		
			$mysqli->close();
			return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try:
     * 1.Delete from Table ADMIN_Data_Access where App_User_ID equal to $cid.
     * 2.Delete from Table Registrations where App_User_ID equal to $cid.
     * 3.Delete from Table ADMIN_App_Users where ID equal $cid.
     * If any of the last query fails will launche an exception and save the error into the log
     * and return FALSE,
     * Otherwise will return TRUE.
     */
	public function delete_user($cid){
		
		$mysqli = $this->get_connection();
		
		try {
			$mysqli->autocommit(FALSE);
			
			$sql = "Delete from ADMIN_Data_Access where App_User_ID='".$cid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from Registrations where App_User_ID='".$cid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from ADMIN_App_Users where ID='".$cid."'";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("delete_user(".$cid.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
  		return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try:
     * 1. select 1 from Questionnaires Table where ID is equal to Questionnaire_ID and Client_ID in $cid
     * 2. Then delete the selected value from <b>Table CALC_A_Scores.</b>
     * 3. Select 1 from Questionnaires Table where ID is equal to Questionnaire_ID and Client_ID in $cid
     * 4. Then Delete the selected value from <b>Table CALC_Questions_Points.</b>
     * 5. select 1 from Questionnaires Table where ID is equal to Questionnaire_ID and Client_ID in $cid
     * 6. Then Delete the selected value from <b>table CALC_Raw_Scores</b>
     * 7. select 1 from Questionnaires table where ID is equal to Questionnaire_ID and Client_ID in $cid
     * 8. Then Delete the selected value from <b>table Questionnaires_Details</b>
     * 9. Delete from table Questionnaires where Client_ID in $cid.
     * 10.Delete from table ADMIN_Data_Access where Client_ID in $cid.
     * 11.Delete from table Clients where ID in $cid.
     * 
     * If any of the last querys fails, will trow an exception and save into the LOG and returning FALSE.
     * Otherwise will return TRUE.
     * 
     */    
	public function delete_client($cid){
		
		$mysqli = $this->get_connection();
		
		try {
			
			$mysqli->autocommit(FALSE);
			
			$sql = "Delete from CALC_A_Scores   where exists 
					(select 1 from Questionnaires q where q.ID=CALC_A_Scores.Questionnaire_ID and q.Client_ID in (".$cid."))";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from CALC_Questions_Points   where exists 
					(select 1 from Questionnaires q where q.ID=CALC_Questions_Points.Questionnaire_ID and q.Client_ID in (".$cid."))";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from CALC_Raw_Scores   where exists 
					(select 1 from Questionnaires q where q.ID=CALC_Raw_Scores.Questionnaire_ID and q.Client_ID in (".$cid."))";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from Questionnaires_Details   where exists 
					(select 1 from Questionnaires q where q.ID=Questionnaires_Details.Questionnaire_ID and q.Client_ID in (".$cid."))";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from Questionnaires where Client_ID in (".$cid.")";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from ADMIN_Data_Access where Client_ID in (".$cid.")";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Delete from Clients where ID in (".$cid.")";
			$res = $mysqli->query($sql);			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			//error_log($e->getMessage());
  			$this->app_log("delete_client(".$cid.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
  		return true;
	
	}
	
    /**
     * Make the connection to DB.
     * Will delete the old answers for the questionary and later will 
     * save the new ones passed trought the array $answers.
     * 
     * If some of the querys of Delete actual answers or insert the new ones, the script
     * will launch an exception and return FALSE.
     * Otherwise, will return TRUE.
     *  
     * @param int $questionnaire_id
     * @param array $answers
     * @return boolean TRUE or FALSE.
     * @throws Exception
     */
	public function set_questionnaire_answers($questionnaire_id,$answers){
		
		$mysqli = $this->get_connection();
		
		try {
			$mysqli->autocommit(FALSE);
			
			foreach ($answers as $asw) {
    	
    			//$result = $database->set_question_answer($questionnaire_id,$asw["QID"],$asw["Ans"]);
    			$sql = "Delete from Questionnaires_Details where Questionnaire_ID='".$questionnaire_id."' and Question_ID='".$asw["QID"]."'";
    			$res = $mysqli->query($sql);
				if($res===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				
				$sql = "insert into Questionnaires_Details(Questionnaire_ID,Question_ID,Answer) values ('".$questionnaire_id."','".$asw["QID"]."','".$asw["Ans"]."')";
    			$res = $mysqli->query($sql);
				if($res===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
    			 
			}			
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("set_questionnaire_answers(".$questionnaire_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Will Try:
     * 1.Delete from Questionnaires_Details Table
     * 2.where Questionnaire_ID equal to $questionnaire_id and Question_ID equal to $question_id.
     * 3. Then Insert into Questionnaires_Details Table the parameters passed to the function with the new information
     *    for the question.
     * 
     * If some of the last 2 querys fails, the script will launch an exception and later will return FALSE.
     * 
     * Otherwise will return the id on the table of the insert.
     *  
     * @param int $questionnaire_id
     * @param int $question_id
     * @param value $answer
     * @return boolean FALSE or $new_id of Questionnaires_Details
     * @throws Exception
     */    
	public function set_question_answer($questionnaire_id,$question_id,$answer){
		
		$mysqli = $this->get_connection();
		
		try {
			
			$mysqli->autocommit(FALSE);
			$sql = "Delete from Questionnaires_Details where Questionnaire_ID='".$questionnaire_id."' and Question_ID='".$question_id."'";
			$res = $mysqli->query($sql);
			
			if($res===false)
			{
				throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);
			}
			
			$sql = "insert into Questionnaires_Details(Questionnaire_ID,Question_ID,Answer) values ('".$questionnaire_id."','".$question_id."','".$answer."')";
			$res = $mysqli->query($sql);
			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$new_id = $mysqli->insert_id;
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("set_question_answer(".$questionnaire_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
  		return $new_id;
	}	
	
        
    /**
     * Make the connection to DB.
     * Then will try:
     * 1.Delete from CALC_Questions_Points table  where Questionnaire_ID $questionnaire_id.
     * 2.Select Questionnaire_ID, Question_ID, Answer, Points
     * 3. From Table Questionnaires_Details joined to MAP_Questions_Points 
     * 4. where Questionnaire_ID is queal to $questionnaire_id and Questionnaire_Type_ID is equal to $type_id.
     * 5.Then, the last select is Inserted into CALC_Questions_Points
     * 
     * If someone of the last querys fail, the script will launch an exception and insert the error into the log and returning FALSE.
     * Otherwise will return TRUE.
     *  
     * @param int $questionnaire_id
     * @param int $type_id
     * @return boolean FALSE or TRUE.
     * @throws Exception
     */
	public function calc_questions_points($questionnaire_id,$type_id){
		
		$mysqli = $this->get_connection();
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "Delete from CALC_Questions_Points where Questionnaire_ID='".$questionnaire_id."'";
			$res = $mysqli->query($sql);
			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Insert into CALC_Questions_Points(Questionnaire_ID,Question_ID,Answer,Points)
					Select d.Questionnaire_ID,d.Question_ID,d.Answer,p.Points
					from Questionnaires_Details d join MAP_Questions_Points p 
						on d.Question_ID=p.Question_ID and d.Answer = p.Answer 
					where d.Questionnaire_ID='".$questionnaire_id."' and p.Questionnaire_Type_ID='".$type_id."'";
			
			$res = $mysqli->query($sql);
			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_questions_points(".$questionnaire_id.",".$type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;
	
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try to:
     * 1.Delete from table CALC_Raw_Scores
     * 2. Where Questionnaire_ID equal to $questionnaire_id and Phase equal to SIMPLE_SCALES.
     * 3. If the delete executes successfully then will insert the Next
     * 4.Select Questionnaire_ID, ID,Raw_Score,SIMPLE_SCALES.
     * 5.From table CALC_Questions_Points joined to MAP_Questions_Scales and PARAM_Scales
     * 6.Where Questionnaire_ID equal to $questionnaire_id
     * 7.Then insert the selected items into CALC_Raw_Scores
     * 
     * If some of the last querys fails then the script will launch an exception,
     * otherwise will launch TRUE
     *  
     * @param int $questionnaire_id
     * @param int $type_id
     * @return boolean TRUE and FALSE.
     * @throws Exception
     */    
	public function calc_simple_scale_raw_score($questionnaire_id,$type_id){
		
		$mysqli = $this->get_connection();
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "Delete from CALC_Raw_Scores where Questionnaire_ID='".$questionnaire_id."' and Phase='SIMPLE_SCALES'";
			$res = $mysqli->query($sql);
			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Insert into CALC_Raw_Scores(Questionnaire_ID,Scale_ID,Raw_Score,Phase)
					SELECT q.Questionnaire_ID,c.ID,(sum(q.Points)+c.Factor_Add)*c.Factor_Multiplication as Raw_Score,'SIMPLE_SCALES' 
					FROM `CALC_Questions_Points` q
					join MAP_Questions_Scales s on q.Question_ID=s.Question_ID and s.Questionnaire_Type_ID='".$type_id."'
					join PARAM_Scales c on c.ID = s.Scale_ID
					where q.Questionnaire_ID ='".$questionnaire_id."'
					group by c.ID";
			
			$res = $mysqli->query($sql);
			
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_simple_scale_raw_score(".$questionnaire_id.",".$type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;
		
	
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try to:
     * 1.Select ID
     * 2.From table PARAM_Scales
     * 3.Where Type_ID equal to 5.
     * 
     * If the query returns TRUE then will convert the query to an associative array
     * and do while $scale equal to that array:
     *  1.Delete from CALC_Raw_Scores table
     *  2.where Scale_ID equal to $scale_id and Questionnaire_ID $questionnaire_id.
     *  3.If the delete is successfully
     *  4.SELECT Scale_ID, Factor_1, Operand_1, Points, Factor_2, Points, Factor_3,
     *    Operand_3, Factor_4, Operand_4, Expr 
     *  5.From table PARAM_ICN
     *  6.Joined to CALC_Questions_Points
     *  7.where Scale_ID equal to $scale_id and Questionnaire_Type_ID equal to $type_id.
     * 
     *  If the last select return rows then will calculate the $raw_score
     *  Eventually will insert into CALC_Raw_Scores Table the last select 
     *  for questionnaire in each while.
     * 
     * If some of the last querys fails then the script will launch an exception,
     * otherwise will launch TRUE 
     *  
     * @param int $questionnaire_id
     * @param int $type_id
     * @return boolean TRUE or FALSE
     * @throws Exception
     */    
	public function calc_icn_scale_raw_score($questionnaire_id,$type_id){
		
		$mysqli = $this->get_connection();
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "Select ID from PARAM_Scales ps where ps.Type_ID=5 
					and exists (select 1 from MAP_Scales_Questionnaire_Types m where m.Scale_ID=ps.ID and m.Questionnaire_Type_ID = '".$type_id."')";
			$res = $mysqli->query($sql);
			if($res===false)
			{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			while($scale = $res->fetch_array(MYSQLI_ASSOC))
			{
				$scale_id = $scale["ID"];
				
				$sql = "Delete from CALC_Raw_Scores where Scale_ID='".$scale_id."' and Questionnaire_ID='".$questionnaire_id."'";
				$del = $mysqli->query($sql);
				if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				
				$sql = "SELECT  
							i.Scale_ID,i.Factor_1,i.Operand_1,p1.Points
							,i.Factor_2,i.Operand_3,p2.Points
							,i.Factor_3,i.Operand_3,p3.Points
							,i.Factor_4,i.Operand_4,p4.Points
							,CONCAT('ABS(ABS(',i.Factor_1,i.Operand_1,IFNULL(p1.Points,0),')-ABS(',i.Factor_2,i.Operand_2,IFNULL(p2.Points,0),')-ABS(',i.Factor_3,i.Operand_3,IFNULL(p3.Points,0),')-ABS(',i.Factor_4,i.Operand_4,IFNULL(p4.Points,0),'))') as Expr
						FROM PARAM_ICN i
						left join CALC_Questions_Points p1 on i.Question_1 = p1.Question_ID and p1.Questionnaire_ID=".$questionnaire_id."
						left join CALC_Questions_Points p2 on i.Question_2 = p2.Question_ID and p2.Questionnaire_ID=".$questionnaire_id."
						left join CALC_Questions_Points p3 on i.Question_3 = p3.Question_ID and p3.Questionnaire_ID=".$questionnaire_id."
						left join CALC_Questions_Points p4 on i.Question_4 = p4.Question_ID and p4.Questionnaire_ID=".$questionnaire_id."
						where i.Scale_ID='".$scale_id."' and i.Questionnaire_Type_ID='".$type_id."'";
				 $statements = $mysqli->query($sql);
				 if($statements===false){throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				 
				 $raw = 0;
				 
				 while($statement  = $statements->fetch_array(MYSQLI_ASSOC))
				 {
				 	$math = $statement["Expr"];				 	
				 	$raw = $raw + eval("return ($math);"); 
				 	//eval("\$raw = \"$math\";");
				 	//error_log($statement["Expr"]);
				 	//error_log($raw);
				 }
				 
				 $sql = "Insert into CALC_Raw_Scores (Questionnaire_ID,Scale_ID,Raw_Score,Phase)
				 		values ('".$questionnaire_id."','".$scale_id."','".$raw."','ICN_SCALES')";
				 
				 $ins = $mysqli->query($sql);
				 
				 if($ins===false)
				 {throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}			
			}
			
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_icn_scale_raw_score(".$questionnaire_id.",".$type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;
	
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try to:
     * 1.Delete from  CALC_Raw_Scores 
     * 2.Where Questionnaire_ID is equal to $questionnaire_id and Phase equal to MASTER_SCALES.
     * 3.Then will select Process_Order 
     * 4.From MAP_Master_Scales
     * 5.where exists values in MAP_Scales_Questionnaire_Types table
     * 6.where Scale_ID equal to Scale_ID_Master and Questionnaire_Type_ID equal to $type_id 
     * 
     * If the last select return one or more rows then will put that into an associative array and to while:
     *      1.$proc_order be equal to that array.
     *      2.Select Questionnaire_ID, Scale_ID_Master,Raw_Score,'MASTER_SCALES'
     *      3.From CALC_Raw_Scores joined to MAP_Master_Scales
     *      4.where Questionnaire_ID equal to $questionnaire_id and Questionnaire_Type_ID equal to $type_id.
     *      5. Then insert that select into CALC_Raw_Scores table.
     * 
     * If some of the last querys fails then the script will launch an exception,
     * otherwise will launch TRUE 
     *  
     * @param int $questionnaire_id
     * @param int $type_id
     * @return boolean TRUE or FALSE
     * @throws Exception
     */    
	public function calc_master_scale_raw_score($questionnaire_id,$type_id){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
		
			$sql = "DELETE from CALC_Raw_Scores where Questionnaire_ID='".$questionnaire_id."' and Phase='MASTER_SCALES'";
			$del = $mysqli->query($sql);
			if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "Select distinct Process_Order from MAP_Master_Scales sc 
					where exists 
					(select 1 from MAP_Scales_Questionnaire_Types map where map.Scale_ID = sc.Scale_ID_Master and map.Questionnaire_Type_ID='".$type_id."')
					order by 1";
			$ord = $mysqli->query($sql);
			
			if($ord===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			if($ord->num_rows == 0){
				$mysqli->commit();
				$mysqli->close();
				return true;
			}
			
			while($proc_order = $ord->fetch_array(MYSQLI_ASSOC))
			{
				$sql = "Insert into CALC_Raw_Scores (Questionnaire_ID,Scale_ID,Raw_Score,Phase)
						Select r.Questionnaire_ID,m.Scale_ID_Master,sum(r.Raw_Score*Factor) as Raw_Score,'MASTER_SCALES'
						from CALC_Raw_Scores r join MAP_Master_Scales m 
							on m.Scale_ID_Child = r.Scale_ID and m.Process_Order='".$proc_order["Process_Order"]."'
						where r.Questionnaire_ID='".$questionnaire_id."' and m.Questionnaire_Type_ID='".$type_id."'
						group by r.Questionnaire_ID,m.Scale_ID_Master";
				
				$res = $mysqli->query($sql);
				if($res===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				
			}
				
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_master_scale_raw_score(".$questionnaire_id.",".$type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try to:
     *  1.Update  CALC_A_Scores table
     *  2. where Questionnaire_ID equal to $questionnaire_id and Phase equal to $phase
     *  3. with B_Score_1=NULL.
     *  
     *  If the update is successfull then:
     *      1.Select ID, Scale_ID, Expression, q1, q2, q3, q4, f1, f2
     *      2. From PARAM_A_Score_Questions joined to CALC_Questions_Points tables.
     *      3.where Questionnaire_Type_ID equal to $questionnaire_type_id and exists a value in
     *      4. Table PARAM_Scales joined to MAP_Scales_Questionnaire_Types
     *      5. where ID equal to Scale_ID  and Type_ID in $scale_types.
     * 
     *  if the select return one or more rows then will take that information to an associative array.
     *  and do while $rule have information:
     *          1. if $scale_id is different to $prev_scale_id and  $prev_scale_id greater than 0
     *          2. INSERT INTO CALC_A_Scores table the value of $questionnaire_id, $prev_scale_id, $a_score. and $phase.
     *          
     *          Then will fix the value into $rule_expr for calculate the $a_score.
     *          If the $prev_scale_id is greater than 0 will do the next:
     *              1. INSERT INTO CALC_A_Scores Table the value of $a_score and $questionnaire_id.
     * 
     *  
     * If some of the last querys fails then the script will launch an exception,
     * otherwise will launch TRUE 
     *  
     *  
     * @param int $questionnaire_id
     * @param type $scale_types
     * @param type $phase
     * @param int $questionnaire_type_id
     * @return boolean TRUE or FALSE.
     * @throws Exception
     */    
	public function calc_a_score_questions($questionnaire_id,$scale_types,$phase,$questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
		
			
			$sql = "Update CALC_A_Scores set B_Score_1=NULL where Questionnaire_ID='".$questionnaire_id."' and Phase='".$phase."'";
			$del = $mysqli->query($sql);
			if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			
			$sql = "select 
						pa.ID,
						pa.Scale_ID,
						pa.Expression,
						IFNULL(po1.Points,0) as q1,
						IFNULL(po2.Points,0) as q2,
						IFNULL(po3.Points,0) as q3,
						IFNULL(po4.Points,0) as q4,
						IFNULL(pa.Factor_1,0) as f1,
						IFNULL(pa.Factor_2,0) as f2
					from PARAM_A_Score_Questions pa
					left join CALC_Questions_Points po1 on pa.Question_ID_1 = po1.Question_ID and po1.Questionnaire_ID = '".$questionnaire_id."'
					left join CALC_Questions_Points po2 on pa.Question_ID_2 = po2.Question_ID and po2.Questionnaire_ID = '".$questionnaire_id."'
					left join CALC_Questions_Points po3 on pa.Question_ID_3 = po3.Question_ID and po3.Questionnaire_ID = '".$questionnaire_id."'
					left join CALC_Questions_Points po4 on pa.Question_ID_4 = po4.Question_ID and po4.Questionnaire_ID = '".$questionnaire_id."'
					where pa.Questionnaire_Type_ID = '".$questionnaire_type_id."' and exists
						( select 1 from PARAM_Scales cc 
							join MAP_Scales_Questionnaire_Types map 
    							on cc.ID = map.Scale_ID 
								and map.Questionnaire_Type_ID ='".$questionnaire_type_id."'
							where cc.ID = pa.Scale_ID  and cc.Type_ID in (".$scale_types.")
						)";
			
			//error_log($sql);
			
			$rule_set = $mysqli->query($sql);
			if($rule_set===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			if($rule_set->num_rows ==0){
				$mysqli->commit();
				$mysqli->close();
  				return true;
			}
			
			$prev_scale_id = -100; 
			$a_score = 0 ;
			
			while($rule = $rule_set->fetch_array(MYSQLI_ASSOC))
			{
				$scale_id = $rule["Scale_ID"];
				
				if($scale_id != $prev_scale_id && $prev_scale_id>0){
					
					$sql = "INSERT INTO CALC_A_Scores(Questionnaire_ID,Scale_ID,B_Score_1,Phase)
							VALUES ('".$questionnaire_id."','".$prev_scale_id."',ROUND(".$a_score."),'".$phase."')
							ON DUPLICATE KEY UPDATE B_Score_1=ROUND(".$a_score.")";	
					
					//error_log($sql);
					
					$res = $mysqli->query($sql);
					$a_score = 0;					
					if($res===false)
						{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				}
				
				$prev_scale_id = $scale_id;
				$rule_expr = $rule["Expression"];
				$rule_expr = str_replace("q1",$rule["q1"],$rule_expr);
				$rule_expr = str_replace("q2",$rule["q2"],$rule_expr);
				$rule_expr = str_replace("q3",$rule["q3"],$rule_expr);
				$rule_expr = str_replace("q4",$rule["q4"],$rule_expr);
				$rule_expr = str_replace("f1",$rule["f1"],$rule_expr);
				$rule_expr = str_replace("f2",$rule["f2"],$rule_expr);
				$rule_expr = str_replace("--","+",$rule_expr);
				
				$new_score = eval("return ($rule_expr);"); 
				$a_score = $a_score + $new_score;
			}
			
			if($prev_scale_id>0){
				$sql = "INSERT INTO CALC_A_Scores(Questionnaire_ID,Scale_ID,A_Score,Phase)
							values ('".$questionnaire_id."','".$prev_scale_id."',ROUND(".$a_score."),'".$phase."')
							ON DUPLICATE KEY UPDATE B_Score_1=ROUND(".$a_score.")";					
				$res = $mysqli->query($sql);
				if($res===false)
						{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_a_score_questions(".$questionnaire_id.",".$scale_types.",".$phase.",".$questionnaire_type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try to: 
     *  1.Delete from CALC_A_Scores Table 
     *  2.Where Questionnaire_ID equal to $questionnaire_id and Phase equal to $phase.
     * 
     *  if delete succesfully, then:
     *      1. Select ID, Scale_ID, Expression, s1, s2, s3, s4, s5, s6, f1, f2
     *      2. From Table PARAM_A_Score_Functions joined to CALC_Raw_Scores
     *      3. Where Questionnaire_Type_ID equal to $questionnaire_type_id 
     *      4. And exists a value into the table PARAM_Scales joined to MAP_Scales_Questionnaire_Types
     *      5. where ID = Scale_ID  and Type_ID in $scale_types.
     *  
     *  If the select return one or more rows then will take that information to an associative array.
     *  and do while $rule have information:
     *      1. Fix the value into $rule_expr for calculate the $a_score.
     *      2. INSERT INTO CALC_A_Scores table the values of Questionnaire_ID,Scale_ID,A_Score,Phase.
     * 
     * If some of the last querys fails then the script will launch an exception,
     * otherwise will launch TRUE 
     * 
     * @param int $questionnaire_id
     * @param type $scale_types
     * @param type $phase
     * @param int $questionnaire_type_id
     * @return boolean TRUE or FALSE
     * @throws Exception
     */    
	public function calc_a_score($questionnaire_id,$scale_types,$phase,$questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "Delete from CALC_A_Scores where Questionnaire_ID='".$questionnaire_id."' and Phase='".$phase."'";
			$del = $mysqli->query($sql);
			if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "SELECT 
						f.ID,
						f.Scale_ID,
						f.Expression,
						IFNULL(r1.Raw_Score,0) as s1,
						IFNULL(r2.Raw_Score,0) as s2,
						IFNULL(r3.Raw_Score,0) as s3,
						IFNULL(r4.Raw_Score,0) as s4,
						IFNULL(r5.Raw_Score,0) as s5,
						IFNULL(r6.Raw_Score,0) as s6,
						IFNULL(f.Factor_1,0) as f1,
						IFNULL(f.Factor_2,0) as f2
					FROM PARAM_A_Score_Functions f
					left join CALC_Raw_Scores r1 on r1.Scale_ID = f.Scale_ID_1  and r1.Questionnaire_ID=".$questionnaire_id."
					left join CALC_Raw_Scores r2 on r2.Scale_ID = f.Scale_ID_2  and r2.Questionnaire_ID=".$questionnaire_id."
					left join CALC_Raw_Scores r3 on r3.Scale_ID = f.Scale_ID_3  and r3.Questionnaire_ID=".$questionnaire_id."
					left join CALC_Raw_Scores r4 on r4.Scale_ID = f.Scale_ID_4  and r4.Questionnaire_ID=".$questionnaire_id."
					left join CALC_Raw_Scores r5 on r5.Scale_ID = f.Scale_ID_5  and r5.Questionnaire_ID=".$questionnaire_id."
					left join CALC_Raw_Scores r6 on r6.Scale_ID = f.Scale_ID_6  and r6.Questionnaire_ID=".$questionnaire_id."
					where f.Questionnaire_Type_ID='".$questionnaire_type_id."' and exists
					(select 1 from PARAM_Scales cc 
					join MAP_Scales_Questionnaire_Types map 
						on cc.ID = map.Scale_ID 
						and map.Questionnaire_Type_ID ='".$questionnaire_type_id."'
					where cc.ID = f.Scale_ID  and cc.Type_ID in (".$scale_types."))";
			
			$rule_set = $mysqli->query($sql);
			if($rule_set===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			if($rule_set->num_rows ==0){
				$mysqli->commit();
				$mysqli->close();
  				return true;
			}
			
			while($rule = $rule_set->fetch_array(MYSQLI_ASSOC))
			{
				$rule_expr = $rule["Expression"];
				$a_score = 0;
				$scale_id = $rule["Scale_ID"];
				
				$rule_expr = str_replace("s1",$rule["s1"],$rule_expr);
				$rule_expr = str_replace("s2",$rule["s2"],$rule_expr);
				$rule_expr = str_replace("s3",$rule["s3"],$rule_expr);
				$rule_expr = str_replace("s4",$rule["s4"],$rule_expr);
				$rule_expr = str_replace("s5",$rule["s5"],$rule_expr);
				$rule_expr = str_replace("s6",$rule["s6"],$rule_expr);
				$rule_expr = str_replace("f1",$rule["f1"],$rule_expr);
				$rule_expr = str_replace("f2",$rule["f2"],$rule_expr);
				$rule_expr = str_replace("--","+",$rule_expr); //double minus equals plus (eval function issue...)
				
				//error_log($scale_code." ".$rule_expr);
				
				$a_score = eval("return ($rule_expr);"); 
				
				$sql = "INSERT INTO CALC_A_Scores(Questionnaire_ID,Scale_ID,A_Score,Phase)
						values ('".$questionnaire_id."','".$scale_id."',GREATEST(ROUND(".$a_score."),0),'".$phase."')";
				
				$res = $mysqli->query($sql);
				if($res===false)
					{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
				
				//error_log($a_score);
			}
			
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_a_score(".$questionnaire_id.",".$scale_types.",".$phase.",".$questionnaire_type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;		
	}
	
        
    /**
     * Make the connection to DB.
     * Then will try to: 
     *  1. DELETE FROM CALC_Raw_Scores table
     *  2. where Questionnaire_ID equal $questionnaire_id and Phase equal SUPPL_IDX_FACTOR_SCALES.
     * 
     *  If delete succesfully then:
     *      1. SELECT Questionnaire_ID, Scale_ID_SUPL, Scale_ID,  Weighted_Score
     *      2. from PARAM_SUPL_IDX_Factors joined to CALC_A_Scores
     *      3. where Questionnaire_ID equal to $questionnaire_id and Questionnaire_Type_ID equal $questionnaire_type_id
     *      4. then will select from the last select Questionnaire_ID, Scale_ID_SUPL, Raw_Score and SUPPL_IDX_FACTOR_SCALES
     *      5. and insert that values into table CALC_Raw_Scores.
     * 
     * If some of the last querys fails then the script will launch an exception,
     * otherwise will launch TRUE 
     * 
     * @param int $questionnaire_id
     * @param int $questionnaire_type_id
     * @return boolean TRUE or FALSE
     * @throws Exception
     */    
	public function calc_suppl_idx_factor_scale_raw_score($questionnaire_id,$questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "DELETE FROM CALC_Raw_Scores where Questionnaire_ID='".$questionnaire_id."' and Phase='SUPPL_IDX_FACTOR_SCALES'";
			$del = $mysqli->query($sql);
			if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql = "INSERT INTO CALC_Raw_Scores (Questionnaire_ID,Scale_ID,Raw_Score,Phase)
					Select x.Questionnaire_ID,x.Scale_ID_SUPL,SUM(Weighted_Score)+p.Factor_Add as Raw_Score,'SUPPL_IDX_FACTOR_SCALES'
					from (  
 					 	SELECT a.Questionnaire_ID,si.Scale_ID_SUPL,si.Scale_ID,Factor*a.A_Score as Weighted_Score 
 					 	FROM PARAM_SUPL_IDX_Factors si 					 	
  						join CALC_A_Scores a 
  						on si.Scale_ID = a.Scale_ID
  						where a.Questionnaire_ID = '".$questionnaire_id."' and si.Questionnaire_Type_ID='".$questionnaire_type_id."'
  					)x
					join PARAM_Scales p on p.ID = x.Scale_ID_SUPL
					group by x.Scale_ID_SUPL";
			
			$ins = $mysqli->query($sql);
			if($ins===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
						
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_suppl_idx_factor_scale_raw_score(".$questionnaire_id.",".$questionnaire_type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;	
	}
	
        
    /**
     * Make the connection to DB.
     * Will try to do:
     * 1. Delete from the table CALC_Raw_Scores where Questionnaire_ID is equal to $questionnaire_id and Phase equal to SUPPL_IDX_THRESHOLD_SCALES.
     * 2. Select Scale_ID_SUPL, Function_Start, A_Score_1, Operand_1, A_Score_2, Operand_2, A_Score_3, Function_End, Operand_CMP, Threshold
     *    Result_True and Result_False
     * 3. From tables PARAM_SUPL_IDX_Thresholds
     * 4. Joined to CALC_A_Scores
     * 5. Where Questionnaire_Type_ID is equal $questionnaire_type_id and  exists a value in 
     *    MAP_Scales_Questionnaire_Types table where Scale_ID is equal to Scale_ID_SUPL.
     * 6. Then the last select is selected only Scale_ID_SUPL and Expr.
     * 
     * If the last query no return any rows, the function will return TRUE and and commit the delete query.
     * Otherwise,
     * will insert into CALC_Raw_Scores for each value into the associative array obtained from the last select.
     * 
     * If any exception was launched because a query fails, then the error will be saved into the LOG
     * and return FALSE.
     * Otherwise will return TRUE and commits the query.
     * 
     * 
     * @param int $questionnaire_id
     * @param int $questionnaire_type_id
     * @return boolean TRUE or FALSE
     * @throws Exception
     */    
	public function calc_suppl_idx_threshold_scale_raw_score($questionnaire_id,$questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "DELETE FROM CALC_Raw_Scores where Questionnaire_ID='".$questionnaire_id."' and Phase='SUPPL_IDX_THRESHOLD_SCALES'";
			$del = $mysqli->query($sql);
			if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$sql="Select 
					Scale_ID_SUPL
					,CONCAT(Function_Start,A_Score_1,Operand_1,A_Score_2,Operand_2,A_Score_3,Function_End,Operand_CMP,Threshold,'?',Result_True,':',Result_False) as Expr
				 from
				(
					SELECT 
						t.Scale_ID_SUPL
						,case when t.Function is null then '(' else CONCAT(t.Function,'(') end as Function_Start
						,IFNULL(r1.A_Score,0) as A_Score_1 
						,IFNULL(t.Operand_1,'-') as Operand_1 
						,IFNULL(r2.A_Score,0) as A_Score_2 
						,IFNULL(t.Operand_2,'-') as Operand_2 
						,IFNULL(r3.A_Score,0) as A_Score_3 
						,')' as Function_End
						,t.Operand_CMP
						,t.Threshold
						,Result_True
						,Result_False
					FROM PARAM_SUPL_IDX_Thresholds t
					left join CALC_A_Scores r1 on t.Scale_ID_1 = r1.Scale_ID and r1.Questionnaire_ID ='".$questionnaire_id."' 
					left join CALC_A_Scores r2 on t.Scale_ID_2 = r2.Scale_ID and r2.Questionnaire_ID ='".$questionnaire_id."' 
					left join CALC_A_Scores r3 on t.Scale_ID_3 = r3.Scale_ID and r3.Questionnaire_ID ='".$questionnaire_id."'
					where t.Questionnaire_Type_ID='".$questionnaire_type_id."' and exists 
					(	select 1 from MAP_Scales_Questionnaire_Types map 
						where map.Scale_ID = t.Scale_ID_SUPL
						and map.Questionnaire_Type_ID = '".$questionnaire_type_id."'
					)
				)x ORDER BY 1";
			
			$stmt = $mysqli->query($sql);
			if($stmt===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			if($stmt->num_rows == 0){
				$mysqli->commit();
				$mysqli->close();
  				return true;
			}
			
			$prev_scale = "-1";
			$raw_score = 0;
			
			while($expression = $stmt->fetch_array(MYSQLI_ASSOC))
			{
				//error_log($expression["Scale_Code_SUPL"]." ".$expression["Expr"]);	
				
				if($expression["Scale_ID_SUPL"]!=$prev_scale && $prev_scale!="-1")
				{
					$sql="INSERT INTO CALC_Raw_Scores(Questionnaire_ID,Scale_ID,Raw_Score,Phase)
						values ('".$questionnaire_id."','".$prev_scale."','".$raw_score."','SUPPL_IDX_THRESHOLD_SCALES')";
					
					//error_log($sql);
					$res = $mysqli->query($sql);
					if($res===false)
						{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
					
					$raw_score =0;					
				}
				
				$math = $expression["Expr"];
				$raw_score = $raw_score + eval("return ($math);"); 
				$prev_scale = $expression["Scale_ID_SUPL"];				 
			}
			
			$sql="INSERT INTO CALC_Raw_Scores(Questionnaire_ID,Scale_ID,Raw_Score,Phase)
						values ('".$questionnaire_id."','".$prev_scale."','".$raw_score."','SUPPL_IDX_THRESHOLD_SCALES')";
								 
			$res = $mysqli->query($sql);
			if($res===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_suppl_idx_threshold_scale_raw_score(".$questionnaire_id.",".$questionnaire_type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;	
	
	
	}
	
    /**
     * Make the connection to DB.
     * Then will try to execute the next:
     *      1.Delete from CALC_Mean_Scores table where Questionnaire_ID equal to $questionnaire_id.
     *      2. If the last query is successfull then will continue, otherwise will launch a exception.
     *      3.Select $questionnaire_id, ID , and the ROUND(SUM(clc.A_Score)/Count(clc.Scale_ID)) from Scale_Groups_Mean Table.
     *      4. Joined to the next 2 tables: MAP_Scales_Groups_Mean and CALC_A_Scores.
     *      5. where Questionnaire_ID equal to $questionnaire_id and Questionnaire_Type_ID equal to $questionnaire_type_id.
     *      6. Grouped by ID.
     *      7. Then will Insert into CALC_Mean_Scores Table the last selected Values.
     *      8. If the last select and insert fails will launch a exception and rollback all the querys.
     *      9. If the query is succesfully, will return TRUE.
     */
	public function calc_mean_scores($questionnaire_id,$questionnaire_type_id){
		
		$mysqli = $this->get_connection();
		
		try{
			$mysqli->autocommit(FALSE);
			
			$sql = "DELETE FROM CALC_Mean_Scores where Questionnaire_ID='".$questionnaire_id."'";
			$del = $mysqli->query($sql);
			if($del===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
			
			
			$sql = "INSERT INTO CALC_Mean_Scores (Questionnaire_ID,Mean_ID,Mean_Score)
					Select '".$questionnaire_id."',sgm.ID,ROUND(SUM(clc.A_Score)/Count(clc.Scale_ID))
					from Scale_Groups_Mean sgm 
					join MAP_Scales_Groups_Mean map on sgm.ID=map.Group_Mean_ID
					join CALC_A_Scores clc on clc.Scale_ID = map.Scale_ID
					where clc.Questionnaire_ID='".$questionnaire_id."' and map.Questionnaire_Type_ID='".$questionnaire_type_id."'
					group by sgm.ID";
			
			//error_log($sql);
			
			$stmt = $mysqli->query($sql);
			if($stmt===false)
				{throw new Exception('Wrong SQL: ' . $sql . ' Error: ' . $mysqli->error);}
		
			$mysqli->commit();
		}
		catch (Exception $e) { 
			$mysqli->rollback();
  			$mysqli->close();
  			$this->app_log("calc_mean_scores(".$questionnaire_id.",".$questionnaire_type_id.")","E",$e->getMessage());
  			return false;
		}	
		
		$mysqli->close();
		return true;	
		
		
	}
	 
	
	
	
}



?>