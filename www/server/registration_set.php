<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	$reg = new Registration();
	$reg->initFromRecord($_POST);
	$json = $reg->saveToDatabase();
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 

?>