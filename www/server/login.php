<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$username = $_POST["Username"];
$password = $_POST["Password"];

$database = new database_slave();

$user = $database->login($username,$password);

if(!$user){
	echo "{\"Result_Code\":-1,\"Result_Message\":\"Login Failed.\"}";
}
else{
	
	$genders = $database->get_recordset_combo("ID","Descr_en","Genders");
	$marital_statuses = $database->get_recordset_combo("ID","Descr_en","Marital_Statuses");
	$questionnaire_types = $database->get_recordset_combo("ID","Descr_en","PARAM_Questionnaire_Types");
	$test_types = $database->get_recordset_combo("ID","Descr_en","PARAM_Test_Types");
	$languages = $database->get_recordset_combo("ID","Descr_en","Languages");
	
	$sql = "select distinct Category from Registrations where App_User_ID='".$user["ID"]."'";
	$registrations = $database->get_sql_results($sql);
	
	if($registrations->num_rows==0){$json_regs = "[]";}
	else{
		$json_regs = "[";
		while($reg = $registrations->fetch_array(MYSQLI_ASSOC))
		{$json_regs=$json_regs.json_encode($reg).",";}	
		$json_regs = substr($json_regs,0,-1)."]";
	}
	
	$json_genders = "[";	
	while($gender = $genders->fetch_array(MYSQLI_ASSOC))
	{$json_genders=$json_genders.json_encode($gender).",";}	
	$json_genders = substr($json_genders,0,-1)."]";
	
	$json_languages = "[";	
	while($language = $languages->fetch_array(MYSQLI_ASSOC))
	{$json_languages=$json_languages.json_encode($language).",";}	
	$json_languages = substr($json_languages,0,-1)."]";
	
	$json_marital_statuses = "[";
	while($status = $marital_statuses->fetch_array(MYSQLI_ASSOC))
	{$json_marital_statuses=$json_marital_statuses.json_encode($status).",";}	
	$json_marital_statuses = substr($json_marital_statuses,0,-1)."]";
	
	$json_questionnaire_types = "[";
	while($type = $questionnaire_types->fetch_array(MYSQLI_ASSOC))
	{$json_questionnaire_types=$json_questionnaire_types.json_encode($type).",";}	
	$json_questionnaire_types = substr($json_questionnaire_types,0,-1)."]";
	
	$json_test_types = "[";
	while($type = $test_types->fetch_array(MYSQLI_ASSOC))
	{$json_test_types=$json_test_types.json_encode($type).",";}	
	$json_test_types = substr($json_test_types,0,-1)."]";
	
	
	
	echo "{\"Result_Code\":0,\"User_ID\":".$user["ID"].",\"Username\":\"".$user["Username"]."\",\"Api_Key\":\"".$user["Api_Key"]."\",\"Surname\":\"".$user["Surname"]."\",\"Name\":\"".$user["Name"]."\",\"Language_ID\":".$user["Language_ID"].",\"Is_Admin\":".$user["Is_Admin"].",\"Languages\":".$json_languages.",\"Genders\":".$json_genders.",\"Marital_Statuses\":".$json_marital_statuses.",\"Questionnaire_Types\":".$json_questionnaire_types.",\"Test_Types\":".$json_test_types.",\"Registration_Types\":".$json_regs."}";	
}


?>