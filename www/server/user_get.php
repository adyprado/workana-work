<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$cid = $_POST["App_User_ID"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	$user = new App_User();
	$user->initFromDatabase($cid);
	$json = $user->toJSON();
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>