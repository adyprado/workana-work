<?php
header("Content-Type: text/json");

include('model_classes.php');
require_once 'Excel/Classes/PHPExcel/IOFactory.php';

/*

HTML5/FLASH MODE

(MODE will detected on client side automaticaly. Working mode will passed to server as GET param "mode")

response format

if upload was good, you need to specify state=true and name - will passed in form.send() as serverName param
{state: 'true', name: 'filename'}

*/

 
if (@$_REQUEST["mode"] == "html5" || @$_REQUEST["mode"] == "flash") {
	
	
	$uid = $_GET["User_ID"];
	$akey = $_GET["Api_Key"];
	$database = new database_slave();

	if($database->app_user_valid($uid,$akey)){
	
		$filename = $_FILES["file"]["tmp_name"];
	
		if (!file_exists($filename)) {
			//error_log("no file");
			//exit("Please run 05featuredemo.php first." . EOL);
		}
	
		$inputFileType = PHPExcel_IOFactory::identify($filename);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($filename);
		
		try{
			$sheet_version = $objPHPExcel->getSheet(2);
			$version = $sheet_version->getCell('A1')->getValue();
		}
		catch (Exception $e) { 
  			$message = "{\"Result_Code\":-1,\"Error_Message\":\"Unregognized excel template.<br><br>Please download the appropriate template from the application.\"}";			
			print_r("{state: true, name:'".$message."'}");	
			exit;
		}					
		 
				 
		if($version!="v01"){
			$message = "{\"Result_Code\":-1,\"Error_Message\":\"Unregognized excel template.<br><br>Please download the appropriate template from the application.\"}";			
			print_r("{state: true, name:'".$message."'}");	
			exit;
		}
		
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = "M";
		
		$clients_array=array();
		$flag_error = false;
		
		$genders = $database->get_recordset("Genders");
		$marital_statuses = $database->get_recordset("Marital_Statuses");
		$languages = $database->get_recordset("Languages");
		
		for ($row = 1; $row <= $highestRow; $row++){ 
    		$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
   		   	 
    		$i=0;
    		while($i<count($rowData)){
    		 
    			if($row==1){    			 
    				$headers = $rowData[$i]; 
    			}
    			else if($row==2){}
    			else{
    				$innerData = $rowData[$i];
    				$client = new Client();
    				for ($j = 0; $j < count($innerData); $j++) {   				
    					
    					$value = $innerData[$j];
    					
    					if($headers[$j]=="Gender_ID"){
    						$genders->data_seek(0);	
    						while($res = $genders->fetch_array(MYSQLI_ASSOC)) {
    							if($res["Descr_en"]==$value)
    								$value = $res["ID"]; 
    						}
    					}
    					else if($headers[$j]=="Marital_Status_ID"){
    						$marital_statuses->data_seek(0);
    						while($res = $marital_statuses->fetch_array(MYSQLI_ASSOC)) {
    							if($res["Descr_en"]==$value)
    								$value = $res["ID"]; 
    						}
    						
    					}
    					else if($headers[$j]=="Language_ID"){
    						$languages->data_seek(0);
    						while($res = $languages->fetch_array(MYSQLI_ASSOC)) {
    							if($res["Descr_en"]==$value)
    								$value = $res["ID"]; 
    						}
    					}
    					
    					$client->$headers[$j] = $value; 				
    				}
    				 
    				if($client->Surname=="" || $client->Name==""){}    				
    				else{$clients_array[] = $client;};	

    			} 
    			$i=$i+1;	 
    		}
		}
		
		$mysqli = $database->get_connection();
		$message = "{\"Result_Code\":0}";
		$outcome = "true";
		$error_cl= "";
		try{
			$mysqli->autocommit(FALSE);
			
			foreach ($clients_array as $cl) {

				$sql = $database->get_object_sql_insert("Clients",$cl);
	
				$res = $mysqli->query($sql);			
				
				if($res===false)
				{
					$error_cl = $cl;
					throw new Exception('Wrong SQL: ' . $value . ' Error: ' . $mysqli->error);
				}
				
				$newid = $mysqli->insert_id;
				$sql_insert = "Insert into ADMIN_Data_Access(App_User_ID,Client_ID) values ('".$uid."','".$newid."')";
				 
				$res = $mysqli->query($sql_insert);
								 
				if($res===false)
				{
					$error_cl = $cl;
					throw new Exception('Wrong SQL: ' . $value . ' Error: ' . $mysqli->error);
				}				
			}
			$mysqli->commit();
		}
		catch (Exception $e) { 
			 
			$message = "{\"Result_Code\":-2,\"Error_Message\":\"Row for client: ".$error_cl->Surname." ".$error_cl->Name." contains error.<br><br>Please verify the data are correct and try again.\"}";
			 
			$mysqli->rollback();
  			$mysqli->close();
  			 
		}
		
		
		print_r("{state: ".$outcome.", name:'".$message."'}");
		 
	}
	//print_r("{state: true, name:'".str_replace("'","\\'",$filename)."'}");
	//error_log("{state: true, name:'".str_replace("'","\\'",$filename)."', size:".$_FILES["file"]["size"]/*filesize("uploaded/".$filename)*/."}");
	//error_log($_FILES["file"]["tmp_name"]);
	//error_log($new_filename);
}

/*

HTML4 MODE

response format:

to cancel uploading
{state: 'cancelled'}

if upload was good, you need to specify state=true, name - will passed in form.send() as serverName param, size - filesize to update in list
{state: 'true', name: 'filename', size: 1234}

*/

if (@$_REQUEST["mode"] == "html4") {
	header("Content-Type: text/html");
	if (@$_REQUEST["action"] == "cancel") {
		print_r("{state:'cancelled'}");
	} else {
		$filename = $_FILES["file"]["name"];
		// move_uploaded_file($_FILES["file"]["tmp_name"], "uploaded/".$filename);
		//error_log("in here!");
		print_r("{state: true, name:'".str_replace("'","\\'",$filename)."', size:".$_FILES["file"]["size"]/*filesize("uploaded/".$filename)*/."}");
		//error_log("{state: true, name:'".str_replace("'","\\'",$filename)."', size:".$_FILES["file"]["size"]/*filesize("uploaded/".$filename)*/."}");
	}
}

/* SILVERLIGHT MODE */
/*
{state: true, name: 'filename', size: 1234}
*/

if (@$_REQUEST["mode"] == "sl" && isset($_REQUEST["fileSize"]) && isset($_REQUEST["fileName"]) && isset($_REQUEST["fileKey"])) {
	
	// available params
	// $_REQUEST["fileName"], $_REQUEST["fileSize"], $_REQUEST["fileKey"] are available here
	
	// each file got temporary 12-chang length key due some inner silverlight limitations,
	// there will another request to check if file transferred and saved corrrectly
	// key matched to regex below
	
	preg_match("/^[a-z0-9]{12}$/", $_REQUEST["fileKey"], $p);
	
	if (@$p[0] === $_REQUEST["fileKey"]) {
		
		// generate temp name for saving
		$temp_name = "uploaded/".$p[0]."_data";
		
		// if action=="getUploadStatus" - that means file transfer was done and silverlight is wondering if php/orhet_server_side
		// got and saved file correctly or not, filekey same for both requests
		
		if (@$_REQUEST["action"] != "getUploadStatus") {
			// file is coming, save under temp name
			/*
			$postData = file_get_contents("php://input");
			if (strlen($postData) == $_REQUEST["fileSize"]) {
				file_put_contents($temp_name, $postData);
			}
			*/
			// no needs to output something
		} else {
			// second "check" request is coming
			/*
			$state = "false";
			if (file_exists($temp_name)) {
				rename($temp_name, "uploaded/".$_REQUEST["fileName"]);
				$state = "true";
			}
			*/
			$state = "true"; // just for tests
			// pring upload state
			// state: true/false (w/o any quotas)
			// name: server name/id
			header("Content-Type: text/json");
			print_r('{state: '.$state.', name: "'.str_replace('"','\\"',$_REQUEST["fileName"]).'"}');
		}
	}
}

?>
