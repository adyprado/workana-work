<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$is_admin = $_POST["Is_Admin"];
$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	$client = new Client();
	$client->initFromRecord($_POST);
	$json = $client->saveToDatabase($uid,$is_admin);
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 

?>