<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];


$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$scale_id = -1;
	$questionnaire_type_id = 1;
	
	if(isset($_POST["Scale_ID"])){
		$scale_id = $_POST["Scale_ID"];		
	}
	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	
	$scales = $database->get_questions_for_scale($scale_id,$questionnaire_type_id);
		
	$rows = array();
	
	while($r = $scales->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
	
	$json_q = json_encode($rows);
	$json = "{\"Result_Code\":0,\"Questions\":".$json_q."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>