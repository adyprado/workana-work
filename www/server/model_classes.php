<?php
include (__DIR__.'/database_slave.php');

class Utils
{
	public static function toJson($obj)
	{
		$main = "";
		foreach($obj as $key => $value) {       		 
           		if(!is_array($obj->$key) && isset($obj->$key) && $key != 'Password' && $key != 'Api_Key' && !is_object($obj->$key)){           			
           			
           			if(is_numeric($obj->$key) && strpos($key,"_Code")===false && $key!='Code')
           			{
           				$main = $main."\"".$key."\":".$value.",";
           			}
           			else
           			{
           				$main = $main."\"".$key."\":\"".$value."\",";	           			
           			}
           		}
        	}
        $main=substr($main,0,-1);
        return $main;
	}
	
	public static function errorJSON($code,$message)
	{
		$json = "{\"Result_Code\":".$code.",\"Result_Message\":\"".$message."\"}";
		return $json ;
	}
}

class Test
{
	public $ID;
	public $Client_ID;
	public $Last_Save;
	public $Comments;
	public $Hash_Code;
	public $Test_Type_ID;
	public $Online_Access;
	public $Process_Times;
	public $Fill_Time;
	public $Test_File;
	public $Result_File;
	public $Status;
	
	function initFromDatabase($qid)
	{
		$database = new database_slave();
		$data = $database->get_db_record("v_Tests",$qid);
		$this->initFromRecord($data);
	}
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	function saveToDatabase()
	{
		$database = new database_slave();
		
		if($this->ID<1){			
			$this->Hash_Code = database_slave::generateHashCode($this->ID);
			$this->Last_Save = date("Y-m-d");
			
			$sql_insert= $database->get_object_sql_insert("Tests",$this); 
        	$new_id = $database->execute_insert($sql_insert);
        	$this->ID = $new_id;
		}
		else{
			$sql_update = $database->get_object_sql_update("Tests",$this);
        	$result = $database->execute_update($sql_update);
		}		
		return $this->toJSON();		
	}
	function toJSON($autonomous=true)
	{
		$main = Utils::toJson($this);		 
						
		if($autonomous){
			$json_template = "{\"Result_Code\":0%s}"; 
		}
		else{
			$json_template = "{%s}"; 
		}
		
		return sprintf($json_template, isset($main) ? ",".$main : "");	
	}
}

class Questionnaire
{
	public $ID;
	public $Client_ID;
	public $Last_Save;
	//public $Date_Processed;
	public $Comments;
	public $Hash_Code;
	public $Questionnaire_Type_ID;
	public $Questionnaire_File;
	public $Result_File;
	public $Result_Cmp_File;
	public $Online_Access;
	public $Process_Times;
	public $Fill_Time;
	
	function initFromDatabase($qid)
	{
		$database = new database_slave();
		$data = $database->get_db_record("v_Questionnaires",$qid);
		$this->initFromRecord($data);
		
		//$data = $database->get_db_record("PARAM_Questionnaire_Types",$this->Questionnaire_Type_ID);
		//$this->Questionnaire_File = $data["Questionnaire_File"]."?q=".$this->Hash_Code;
		//$this->Result_File = $data["Result_File"]."?q=".$this->Hash_Code;
		//$db_table,$rec_id,$filter_col="ID"
	}
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	
	function saveToDatabase()
	{
		$database = new database_slave();
		
		if($this->ID<1){
			
			$this->Hash_Code = database_slave::generateHashCode($this->ID);
			
			$sql_insert= $database->get_object_sql_insert("Questionnaires",$this); 
        	$new_id = $database->execute_insert($sql_insert);
        	$this->ID = $new_id;
		}
		else{
			$sql_update = $database->get_object_sql_update("Questionnaires",$this);
        	$result = $database->execute_update($sql_update);
		}
		
		return $this->toJSON();	
	
	}
	
	function toJSON($autonomous=true)
	{
		$main = Utils::toJson($this);		 
						
		if($autonomous){
			$json_template = "{\"Result_Code\":0%s}"; 
		}
		else{
			$json_template = "{%s}"; 
		}
		
		return sprintf($json_template, isset($main) ? ",".$main : "");	
	}
	
	
}

class Supplementary_Index_Threshold
{
	public $ID;
	public $Scale_Code_SUPL;
	public $Function;
	public $Scale_Code_1;
	public $Operand_1;
	public $Scale_Code_2;
	public $Operand_2;
	public $Scale_Code_3;
	public $Operand_CMP;
	public $Threshold;
	public $Result_True;
	public $Result_False;
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	
	
}

class Supplementary_Index_Factor
{
	public $ID;
	public $Scale_Code_SUPL;
	public $Scale_Code;
	public $Factor;
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}	
}

class Question
{
	public $ID;
	public $Code;
	public $Question_el;
	public $Question_en;
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	function initFromDatabase($qid)
	{
		$database = new database_slave();
		$data = $database->get_db_record("PARAM_Questions",$qid);
		$this->initFromRecord($data);	
	}
	
	function toJSON($autonomous=true)
	{
		$main = Utils::toJson($this);		 
						
		if($autonomous){
			$json_template = "{\"Result_Code\":0%s}"; 
		}
		else{
			$json_template = "{%s}"; 
		}
		
		return sprintf($json_template, isset($main) ? ",".$main : "");	
	}
	
	
}

class ICN_Rule
{
	public $ID;
	public $Scale_Code;
	public $Factor_1;
	public $Operand_1;
	public $Question_1;	
	public $Factor_2;
	public $Operand_2;
	public $Question_2;
	public $Factor_3;
	public $Operand_3;
	public $Question_3;
	public $Factor_4;
	public $Operand_4;
	public $Question_4;
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
}

class Rep_Config
{
	public $ID;
	public $Title_el;
	public $Title_en;
	public $Descr_el;
	public $Descr_en;
	public $Questionnaire_Type_ID;
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = addslashes($data[$key]);
			}
		}
	}
	
}


class A_Score_Function
{
	public $ID;
	public $Scale_Code;
	public $Expression;
	public $Scale_Code_1;
	public $Scale_Code_2;
	public $Scale_Code_3;
	public $Scale_Code_4;
	public $Scale_Code_5;
	public $Scale_Code_6;
	public $Factor_1;
	public $Factor_2;
	public $Questionnaire_Type_ID;
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	
}

class Registration{
	
	public $ID;
	public $App_User_ID;
	public $Category;
	public $Type_ID;
	public $Pieces;
	public $Amount;
	public $Purchase_Date;
	
	function initFromRecord($data)	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	
	function saveToDatabase(){
		$database = new database_slave();
		
		if($this->ID<1){
			 	
			$sql_insert= $database->get_object_sql_insert("Registrations",$this); 
        	$new_id = $database->execute_insert($sql_insert);
        	$this->ID = $new_id;
		}
		else{
			$sql_update = $database->get_object_sql_update("Registrations",$this);
        	$result = $database->execute_update($sql_update);
		}
		
		return $this->toJSON();		
	}
	
	function toJSON($autonomous=true)
	{
		$main = Utils::toJson($this);		 
						
		if($autonomous){
			$json_template = "{\"Result_Code\":0%s}"; 
		}
		else{
			$json_template = "{%s}"; 
		}
		
		return sprintf($json_template, isset($main) ? ",".$main : "");	
	}

}

class App_User{
	
	public $ID;
	public $Surname;
	public $Name;
	public $Email;
	public $Tax_Code;
	public $Username;
	public $Is_Admin;
	public $Api_Key;
	public $Password;
	public $Is_Blocked;
	public $Language_ID;
	
	public $Registrations;
	
	function initFromRecord($data)	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	
	function initFromDatabase($user_id)	{
		$database = new database_slave();
		$data = $database->get_db_record("ADMIN_App_Users",$user_id);
		$this->initFromRecord($data);
		
		$q_list = $database->get_recordset("Registrations","App_User_ID",$user_id);
		
		if($q_list->num_rows > 0)
		{
			$this->Registrations = array();
			
			while($que = $q_list->fetch_array(MYSQLI_ASSOC))
			{
				$queObj = new Registration();
				$queObj->initFromRecord($que);				
				$this->Registrations[] = $queObj;				
			}			
		}
		
	}	
	
	function saveToDatabase(){
		$database = new database_slave();
		
		if($this->ID<1){			
			
			if(!$database->check_conficts_app_user_save($this)){
				return Utils::errorJSON(-100,"User with the same credentials already exists.");
			}
			else{
				$this->Is_Blocked = "0";			 
				$sql_insert= $database->get_object_sql_insert("ADMIN_App_Users",$this);         	 
        		$new_id = $database->execute_insert($sql_insert);
        		$this->ID = $new_id;
        	}
		}
		else{
			$sql_update = $database->get_object_sql_update("ADMIN_App_Users",$this);
        	$result = $database->execute_update($sql_update);
		}
		
		//Utils::toJson($this);
		return $this->toJSON();		
	}
	
	function toJSON($autonomous=true)
	{
		$main = Utils::toJson($this);		 
		
		if(isset($this->Registrations))
		{
			$ajson = "";
			foreach($this->Registrations as $que)
        	{
        		$ajson = $ajson."{".Utils::toJson($que)."},";	
        	}
        	$ajson=",\"Registrations\":[".substr($ajson,0,-1)."]";
		}
				
		if($autonomous){
			$json_template = "{\"Result_Code\":0%s%s}"; 
		}
		else{
			$json_template = "{%s%s}"; 
		}
		
		return sprintf($json_template, isset($main) ? ",".$main : "",isset($ajson) ? $ajson : "");
	
	}

}

class Client
{
	public $ID;
	public $Surname;
	public $Name;
	public $Gender_ID;
	public $Birthdate;
	public $Marital_Status_ID;
	public $Children;
	public $Profession;
	public $Comments;
	
	public $Work_Position;
	public $Work_Proposed;
	public $Work_Years;
	public $Studies;
	
	public $Language_ID;
	
	public $Questionnaires;
	
	function initFromDatabase($client_id)	{
		$database = new database_slave();
		$data = $database->get_db_record("Clients",$client_id);
		$this->initFromRecord($data);
		
		$q_list = $database->get_recordset("Questionnaires","Client_ID",$client_id);
		
		if($q_list->num_rows > 0)
		{
			$this->Questionnaires = array();
			
			while($que = $q_list->fetch_array(MYSQLI_ASSOC))
			{
				$queObj = new Questionnaire();
				$queObj->initFromRecord($que);				
				$this->Questionnaires[] = $queObj;				
			}			
		}
		
	}
	
	function initFromRecord($data)
	{
		foreach($this as $key => $value) {			
			if(isset($data[$key])){				 
				$this->$key = $data[$key];
			}
		}
	}
	
	function saveToDatabase($uid,$is_admin)
	{
		$database = new database_slave();
		
		if($this->ID<1){
			$sql_insert= $database->get_object_sql_insert("Clients",$this); 
        	$new_id = $database->execute_insert($sql_insert);
        	
        	//if($is_admin == 0){
        	$sql_insert = "Insert into ADMIN_Data_Access(App_User_ID,Client_ID) values ('".$uid."','".$new_id."')";
        	$database->execute_insert($sql_insert);
        	//}
        	
        	$this->ID = $new_id;
		}
		else{
			$sql_update = $database->get_object_sql_update("Clients",$this);
        	$result = $database->execute_update($sql_update);
		}
		
		return $this->toJSON();	
	
	}
	
	function isValidated(){
		
	}
	
	function toJSON($autonomous=true)
	{
		$main = Utils::toJson($this);		 
		
		if(isset($this->Questionnaires))
		{
			$ajson = "";
			foreach($this->Questionnaires as $que)
        	{
        		$ajson = $ajson."{".Utils::toJson($que)."},";	
        	}
        	$ajson=",\"Questionnaires\":[".substr($ajson,0,-1)."]";
		}
				
		if($autonomous){
			$json_template = "{\"Result_Code\":0%s%s}"; 
		}
		else{
			$json_template = "{%s%s}"; 
		}
		
		return sprintf($json_template, isset($main) ? ",".$main : "",isset($ajson) ? $ajson : "");
	
	}
}


?>