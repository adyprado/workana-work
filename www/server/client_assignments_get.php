<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$cid = $_POST["Client_ID"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$sql = "SELECT 
				u.ID,
				u.Surname,
				u.Name,
				u.Username,
				IFNULL(da.Priviledges,'na') as Priviledges
			FROM ADMIN_App_Users u 
			left join ADMIN_Data_Access da  on da.App_User_ID = u.ID and da.Client_ID = '".$cid."'";
	$results = $database->get_sql_results($sql);
	
	$json = "";
	//error_log(json_encode($results));
	while($row = $results->fetch_array(MYSQLI_ASSOC)) {
		$json = $json.json_encode($row).",";
	}
	
	$json = "{\"Result_Code\":0,\"Client_ID\":".$cid.",\"Assignments\":[".substr($json,0,-1)."]}";
	//error_log($json);
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>