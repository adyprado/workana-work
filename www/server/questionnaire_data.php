<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$q_hash = $_GET["q"];
$database = new database_slave();


$json = "{";

$groups = $database->get_recordset("Scale_Groups_Rep");



while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
	
	$scores = $database->get_group_rep_scores($q_hash,$g["ID"]); 
	
	if($scores){
		$json = $json."\"".$g["ID"]."\":[";		
		//error_log("1"); 
		while($sc = $scores->fetch_array(MYSQLI_ASSOC)) {
			//$scale_code = str_replace("-","<br>",$sc["Scale_Code"]);
			$scale_code =preg_replace('/-/', '<br>', $sc["Scale_Code"], 1);
			$scale_code_descr =preg_replace('/-/', '<br>', $sc["Scale_Code_Descr"], 1);
			
			$json = $json."{\"Scale_Code\":\"".$scale_code."\",\"Scale_Code_Descr\":\"".$scale_code_descr."\",\"A_Score\":\"".$sc["A_Score"]."\",\"B_Score_1\":\"".$sc["B_Score_1"]."\",\"Raw_Score\":\"".$sc["Raw_Score"]."\"},";	
		}
		$json=substr($json,0,-1)."],";
	}
}

$mean_groups = $database->get_group_mean_scores($q_hash);

if($mean_groups!=false){
	$json = $json."\"Means\":[";
 
	while($mean = $mean_groups->fetch_array(MYSQLI_ASSOC)) {  
		$json=$json."{\"Code\":\"".$mean["Mean_Code"]."\",\"Description\":\"".$mean["Descr_en"]."\",\"Score\":".$mean["Mean_Score"]."},";
	}

	$json = substr($json,0,-1)."]}";
}
else{
	$json = substr($json,0,-1)."}";
}
echo $json;


 

?>