<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$is_admin = $_POST["Is_Admin"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	$test = new Test();
	$test->initFromRecord($_POST);
	$licensed = true;
	
	if($is_admin!=1){
		$licensed = $database->check_user_limit($uid,$test->Test_Type_ID,"Tests");
	}
	
	if($licensed){
		$json = $test->saveToDatabase();	
		$test->initFromDatabase($test->ID);
		$json = $test->toJSON();
	}
	else{
		$json = "{\"Result_Code\":-2,\"Result_Message\":\"You have exceeded the purchased number of the specific test type.\"}";	
	}
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 

?>