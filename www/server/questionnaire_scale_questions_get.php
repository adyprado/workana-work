<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];


$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$scale_id = -1;
	$questionnaire_id = -1;
	$language_id = 1;
	
	if(isset($_POST["Scale_ID"])){
		$scale_id = $_POST["Scale_ID"];
	}
	if(isset($_POST["Questionnaire_ID"])){
		$questionnaire_id = $_POST["Questionnaire_ID"];
	}
	if(isset($_POST["Questionnaire_ID"])){
		$language_id = $_POST["Language_ID"];
	}
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];
	
	$scales = $database->get_questionnaire_scale_questions($questionnaire_id,$scale_id,$language_code); 
		
	$rows = array();
	
	while($r = $scales->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
	
	$json_q = json_encode($rows);
	$json = "{\"Result_Code\":0,\"Questions\":".$json_q."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>