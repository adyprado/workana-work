<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$questionnaire_type_id = 1 ;
	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	
	$factors = $database->get_recordset("PARAM_SUPL_IDX_Factors","Questionnaire_Type_ID",$questionnaire_type_id);
	$rows = array();
	
	while($r = $factors->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
	
	$json_q = json_encode($rows);
	
	$thresholds = $database->get_recordset("PARAM_SUPL_IDX_Thresholds","Questionnaire_Type_ID",$questionnaire_type_id);
	$rows_t = array();
	
	while($r = $thresholds->fetch_array(MYSQLI_ASSOC)) {
    	$rows_t[] = $r;
	}
	
	$json_t = json_encode($rows_t);
	
	$all_scales = $database->get_recordset_combo("ID","Code","PARAM_Scales");
	$scale_rows = array();
	while($r = $all_scales->fetch_array(MYSQLI_ASSOC)) {
    	$scale_rows[] = $r;
	}
	$json_sc = json_encode($scale_rows);
	
	$json = "{\"Result_Code\":0,\"Factors\":".$json_q.",\"Thresholds\":".$json_t.",\"Scales\":".$json_sc."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>