<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$is_admin = $_POST["Is_Admin"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$client_list = $database->get_client_list($is_admin,$uid);
	
 	if($client_list->num_rows >0){
 		$cjson = "[";
 		
 		while($client = $client_list->fetch_array(MYSQLI_ASSOC)) {
    	 	$cjson = $cjson.json_encode($client).",";
		}
 		
 		$cjson = substr($cjson,0,-1)."]";
 		$json = "{\"Result_Code\":0,\"Clients\":".$cjson."}";
 	}
 	else{
 		$json = "{\"Result_Code\":0}";		
 	}

}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>