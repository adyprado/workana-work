<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$language_id = 1; 
	if(isset($_POST["Language_ID"])){
		$language_id = $_POST["Language_ID"];
	}	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];
	
	$questionnaire_id = $_POST["Questionnaire_ID"];
	$questionnaire = new Questionnaire();
	$questionnaire->initFromDatabase($questionnaire_id);
	$json_info = Utils::toJson($questionnaire);
	
	$results = $database->get_questionnaire_answers($questionnaire_id,$language_code);
	
	if($results->num_rows==0){
		$json = "{\"Result_Code\":-2,\"Result_Message\":\"Questionnaire not found\"}";		
	}
	else{
		$rows = array();
	
		while($r = $results->fetch_array(MYSQLI_ASSOC)) {
    		$rows[] = $r;
		}
	
		$json_q = json_encode($rows);
		
		$answer_group = $database->get_answer_group_for_questionnaire_type($questionnaire->Questionnaire_Type_ID,$language_code);
		
		$rows = array();	
		while($r = $answer_group->fetch_array(MYSQLI_ASSOC)) {
    		$rows[] = $r;
		}
		$json_ag = json_encode($rows);
		
		
		$json = "{\"Result_Code\":0,".$json_info.",\"Questionnaire\":".$json_q.",\"Answer_Group\":".$json_ag."}";
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>