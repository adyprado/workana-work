<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$questionnaire_type_id = 1 ;
	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	
	$questions = $database->get_recordset("v_PARAM_A_Score_Functions","Questionnaire_Type_ID",$questionnaire_type_id);
	$rows = array();
	
	while($r = $questions->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
			
	$json_q = json_encode($rows);
	
	$all_scales = $database->get_recordset_combo("ID","Code","PARAM_Scales");
	$scale_rows = array();
	while($r = $all_scales->fetch_array(MYSQLI_ASSOC)) {
    	$scale_rows[] = $r;
	}
	$json_sc = json_encode($scale_rows);
	
	$json = "{\"Result_Code\":0,\"Functions\":".$json_q.",\"Scales\":".$json_sc."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>