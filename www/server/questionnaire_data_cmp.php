<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$q1_hash = $_GET["q1"];
$q2_hash = $_GET["q2"];
$database = new database_slave();

//error_log($q1_hash);
//error_log($q2_hash);

$json = "{";

$groups = $database->get_recordset("Scale_Groups_Rep");

while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
	$scores = $database->get_group_rep_scores_cmp($q1_hash,$q2_hash,$g["ID"]); 
	
	if($scores){
		$json = $json."\"".$g["ID"]."\":[";		
		while($sc = $scores->fetch_array(MYSQLI_ASSOC)) {
			$scale_code =preg_replace('/-/', '<br>', $sc["Scale_Code"], 1);
			$scale_code_descr =preg_replace('/-/', '<br>', $sc["Scale_Code_Descr"], 1);
			
			$json = $json."{\"Scale_Code\":\"".$scale_code."\",\"Scale_Code_Descr\":\"".$scale_code_descr."\",\"A_Score_1\":\"".$sc["A_Score_1"]."\",\"B_Score_1_1\":\"".$sc["B_Score_1_1"]."\",\"Raw_Score_1\":\"".$sc["Raw_Score_1"]."\",\"A_Score_2\":\"".$sc["A_Score_2"]."\",\"B_Score_1_2\":\"".$sc["B_Score_1_2"]."\",\"Raw_Score_2\":\"".$sc["Raw_Score_2"]."\",\"Comments_1\":\"".$sc["Comments_1"]."\",\"Comments_2\":\"".$sc["Comments_2"]."\"},";	
		}
		$json=substr($json,0,-1)."],";
	}
}

$mean_groups = $database->get_group_mean_scores_cmp($q1_hash,$q2_hash);

if($mean_groups!=false){
	$json = $json."\"Means\":["; 
	while($mean = $mean_groups->fetch_array(MYSQLI_ASSOC)) {  
		$json=$json."{\"Code\":\"".$mean["Mean_Code"]."\",\"Description\":\"".$mean["Descr_en"]."\",\"Score_1\":".$mean["Mean_Score_1"].",\"Score_2\":".$mean["Mean_Score_2"].",\"Comments_1\":\"".$mean["Comments_1"]."\",\"Comments_2\":\"".$mean["Comments_2"]."\"},";
	}
	$json = substr($json,0,-1)."]}";
}
else{
	$json = substr($json,0,-1)."}";
}
echo $json;


 

?>