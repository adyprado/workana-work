<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$update_stmts = array();
	$insert_stmts = array();
	
	if(isset($_POST["Thresholds"])){
		
		foreach($_POST["Thresholds"] as $thr)
		{
			$thrObj = new Supplementary_Index_Threshold();
			$thrObj->initFromRecord($thr);
			if($thrObj->ID>0){
				$upd = $database->get_object_sql_update("PARAM_SUPL_IDX_Thresholds",$thrObj);
				$upd = $database->replaceMathOperators($upd);				
				$update_stmts[] = $upd;
			}
			else{
				$ins = $database->get_object_sql_insert("PARAM_SUPL_IDX_Thresholds",$thrObj);
				$ins = $database->replaceMathOperators($ins);
				$insert_stmts[] = $ins;	
			}
			 			
		}
	}
	
	if(isset($_POST["Factors"])){
		
		foreach($_POST["Factors"] as $fac)
		{
			$facObj = new Supplementary_Index_Factor();
			$facObj->initFromRecord($fac);
			if($facObj->ID>0){
				$upd = $database->get_object_sql_update("PARAM_SUPL_IDX_Factors",$facObj);
				$upd = $database->replaceMathOperators($upd);
				$update_stmts[] = $upd;
			}
			else{
				$ins = $database->get_object_sql_insert("PARAM_SUPL_IDX_Factors",$facObj);
				$ins = $database->replaceMathOperators($ins);
				$insert_stmts[] = $ins; 	
			}
			 			
		}
		
	}
	
	$result = $database->executeBatchInsertUpdates($insert_stmts,$update_stmts);
		
	if($result){
		$json = "{\"Result_Code\":0,\"Result_Message\":\"Unauthorized Access.\"}";
	}
	else{
		$json = "{\"Result_Code\":-2,\"Result_Message\":\"Error Saving Changes\"}";
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


/*
{"Api_Key":"123","User_ID":"1",
"Thresholds":[{"ID":1,"Scale_Code_SUPL":"ESP","Function":"","Scale_Code_1":"ALCP","Operand_1":"","Scale_Code_2":"","Operand_2":"","Scale_Code_3":"","Operand_CMP":"&gt;","Threshold":"61","Result_True":"1","Result_False":"0"}],
"Factors":[{"ID":1,"Scale_Code_SUPL":"DGAF","Scale_Code":"ANSC-SLFF","Factor":"0.0215256"}]}
*/

?>
