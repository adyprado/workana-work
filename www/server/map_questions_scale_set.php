<?php
header('Content-Type: application/json; charset=utf-8');

include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];


$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
		
	$scale_id = $_POST["Scale_ID"];	
	$question_id = $_POST["Question_ID"];
	$action = $_POST["Action"];
	$questionnaire_type_id = 1;

	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	
	if($action=="delete"){
		$sql = "Delete from MAP_Questions_Scales where Scale_ID ='".$scale_id."' and Question_ID='".$question_id."' and Questionnaire_Type_ID='".$questionnaire_type_id."'";
		$result = $database->execute_update($sql);
		//error_log($sql);
		$result = true;
		if($result){
			$json = "{\"Result_Code\":0,\"Question_ID\":".$question_id.",\"Action\":\"".$action."\"}";	
		}
		else{
			$json = "{\"Result_Code\":-2,\"Result_Message\":\"Error deleting question.\"}";	
		}
		
	}
	else if($action="add"){
		$sql = "Insert into MAP_Questions_Scales (Scale_ID,Question_ID,Questionnaire_Type_ID) values ('".$scale_id."','".$question_id."','".$questionnaire_type_id."')";	
		$result = $database->execute_insert($sql,false);
		 
		if($result){
			$question = new Question();
			$question->initFromDatabase($question_id);
			$json = $question->toJSON();
		}
		else{
			$json = "{\"Result_Code\":-4,\"Result_Message\":\"Error saving question.\"}";
		}
	}
	else{
		$json = "{\"Result_Code\":-3,\"Result_Message\":\"Insufficient arguments\"}";
	}

}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>