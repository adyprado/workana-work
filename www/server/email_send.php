<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');
include('PHPMailer/class.phpmailer.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$email = $_POST["Mail"];
	$subject = $_POST["Subject"];
	$body = $_POST["Body"];
	
	$mail = new PHPMailer(true);
	$mail->CharSet = "UTF-8";
	
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_SERVER","Code");
	$mail_server = $dbrec["Value"] ;		 
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_UID","Code");
	$mail_uid= $dbrec["Value"];			 
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_PWD","Code");	
	$mail_pwd=	$dbrec["Value"];		 
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_PORT","Code");
	$mail_port=	$dbrec["Value"];
	
	$mail->IsSMTP();                       // telling the class to use SMTP

	$mail->SMTPDebug = 0;          
 	$mail->SMTPAuth = true;                // enable SMTP authentication 
	
	$mail->Host = $mail_server;        // sets Gmail as the SMTP server
	$mail->Port = $mail_port;                     // set the SMTP port for the GMAIL 

	$mail->Username = $mail_uid;  // Gmail username
	$mail->Password = $mail_pwd;      // Gmail password

	$mail->CharSet = 'utf8';
	$mail->SetFrom ($mail_uid,'Eurika Platform'); 
	$mail->Subject = $subject;
	$mail->ContentType = 'text/plain'; 
	$mail->IsHTML(true);
	$mail->Body = $body;
	
	$mail->AddAddress($email);
	
	try{
		$mail->Send();
		$json = "{\"Result_Code\":0}";
	}
	catch (phpmailerException $e) {
  		error_log($e->errorMessage());
  		$json = "{\"Result_Code\":-1}";
  		//echo $e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
  		error_log($e->getMessage());
  		$json = "{\"Result_Code\":-1}";
  		//echo $e->getMessage(); //Boring error messages from anything else!
	}
 
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>