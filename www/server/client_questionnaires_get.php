<?php
header('Content-Type: application/json; charset=utf-8');

//include('database_slave.php');
include('model_classes.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];
$cid = $_POST["Client_ID"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	$questionnaires = $database->get_recordset("v_Questionnaires","Client_ID",$cid);
	
	if($questionnaires->num_rows==0){
		$json = "{\"Result_Code\":0,\"Client_ID\":".$cid.",\"Result_Message\":\"No questionnaires found for the selected client.\"}";		
	}
	else{
		$rows = array();
	
		while($q = $questionnaires->fetch_array(MYSQLI_ASSOC)) {
    		$rows[] = $q;
		}
	
		$json_q = json_encode($rows);
		$json = "{\"Result_Code\":0,\"Client_ID\":".$cid.",\"Questionnaires\":".$json_q."}";
	}
	
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";		
}

echo $json ;


?>