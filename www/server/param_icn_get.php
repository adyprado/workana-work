<?php
header('Content-Type: application/json; charset=utf-8');

include('database_slave.php');

$uid = $_POST["User_ID"];
$akey = $_POST["Api_Key"];

$database = new database_slave();

if($database->app_user_valid($uid,$akey)){
	
	$questionnaire_type_id = 1 ;
	if(isset($_POST["Questionnaire_Type_ID"])){
		$questionnaire_type_id = $_POST["Questionnaire_Type_ID"];
	}
	
	$questions = $database->get_recordset("PARAM_ICN","Questionnaire_Type_ID",$questionnaire_type_id);
	$rows = array();
	
	while($r = $questions->fetch_array(MYSQLI_ASSOC)) {
    	$rows[] = $r;
	}
		
	$json_q = json_encode($rows);
	
	$icn_scales = $database->get_recordset_combo("ID","Code","PARAM_Scales","Type_ID","5");
	$icn_rows = array();
	while($r = $icn_scales->fetch_array(MYSQLI_ASSOC)) {
    	$icn_rows[] = $r;
	}
	$json_r = json_encode($icn_rows);
		
	$json = "{\"Result_Code\":0,\"Rules\":".$json_q.",\"ICN_Scales\":".$json_r."}";
}
else{
	$json = "{\"Result_Code\":-1,\"Result_Message\":\"Unauthorized Access.\"}";	
}

echo $json; 


?>