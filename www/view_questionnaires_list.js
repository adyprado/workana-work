function initQuestionnairesListWin(client_id,data,privs){
	
	var surnameIndex = gdata.clients_grid.getColIndexById("Surname");
	var nameIndex = gdata.clients_grid.getColIndexById("Name");
	
	var surname = gdata.clients_grid.cells(client_id,surnameIndex).getValue();
	var name	= gdata.clients_grid.cells(client_id,nameIndex).getValue();
	
	var win = gdata.main_layout.dhxWins.createWindow("win_questionnaires_list",0,0,650,400);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText(surname+" "+name+" Questionnaires");
	win.setIcon("../../../images/chart-icon-16.png", "../../../images/chart-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	//toolbar.addButton('add', 0, 'New','./images/add-icon-32.png','./images/add-icon-32.png');
	
	toolbar.addButtonSelect('add', 0, 'New', [], './images/add-icon-32.png','./images/add-icon-32.png');
	toolbar.addSeparator('sep_0', 1);
	toolbar.addButton('results', 2, 'Results','./images/chart-search-icon-32.png','./images/chart-search-icon-32.png');
	toolbar.addSeparator('sep_1', 3);
	toolbar.addButton('edit', 4, 'Edit','./images/pencil-icon-32.png','./images/pencil-icon-32.png');
	toolbar.addSeparator('sep_2', 5);
	toolbar.addButton('delete', 6, 'Delete','./images/chart-delete-icon-32.png','./images/chart-delete-icon-32.png');
	toolbar.addSeparator('sep_3', 7);
	toolbar.addButton('compare', 8, 'Compare','./images/balance-icon-32.png','./images/balance-icon-32.png');
	toolbar.addSeparator('sep_4', 9);
	toolbar.addButton('url', 10, 'URL','./images/url-icon-32.png','./images/url-icon-32.png');
	toolbar.addSeparator('sep_5', 11);
	toolbar.addButton('email', 12, 'E-Mail','./images/email-icon-32.png','./images/email-icon-32.png');
	
	if(gdata.is_admin==1){
		toolbar.addSeparator('sep_6', 13);
		toolbar.addButton('scales', 14, 'Scales','./images/measure-icon-32.png','./images/measure-icon-32.png');
		toolbar.disableItem('scales');
	}
	if(privs != "rw"){
		toolbar.disableItem('add');
	}
	
	toolbar.disableItem('results');
	toolbar.disableItem('edit');
	toolbar.disableItem('delete');
	toolbar.disableItem('compare');
	toolbar.disableItem('url');
	toolbar.disableItem('email');
	
	toolbar.addInput('cid', 9, client_id,1);
	toolbar.hideItem('cid');
 	
	for(var i=0;i<gdata.questionnaire_types.length;i++){
		//alert(gdata.questionnaire_types[i].value)
		toolbar.addListOption('add', 'type-'+gdata.questionnaire_types[i].value, i, 'button', '<b>'+gdata.questionnaire_types[i].text+'</b>' ,'./images/'+gdata.questionnaire_types[i].text+'-32.png' )/*'./images/test-icon-32.png'*/
	}
 	
 	//var request_layout = win.attachLayout("2E");
	//var questionnaires_cell = request_layout.cells('a');
 	//questionnaires_cell.setText("Questionnaires");
 	
 	//var requests_cell = request_layout.cells('b');
 	//requests_cell.setText("Request Codes");
 	
	var questionnaire_grid = win.attachGrid();	
	questionnaire_grid.setHeader(",Last Save,Comments,,,,,,Type");
	questionnaire_grid.setColumnIds("Compare,Last_Save,Comments,Questionnaire_File,Hash_Code,Questionnaire_Type_ID,Result_File,Result_Cmp_File,Questionnaire_Type");
	 
	questionnaire_grid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro");
	questionnaire_grid.setColSorting("str,str,str,str,str,str,str,str,str");
	questionnaire_grid.setInitWidthsP("10,25,42,0,0,0,0,0,23");	
	questionnaire_grid.setColumnsVisibility("false,false,false,true,true,true,true,true,false");
	questionnaire_grid.setSkin("dhx_skyblue");
	questionnaire_grid.setImagePath(gdata.img_path);		
	questionnaire_grid.init();
	
	if(typeof data !='undefined'){
		for(var i=0;i<data.length;i++){
			
			var type;
			for(var j=0;j<gdata.questionnaire_types.length;j++)
			{if(gdata.questionnaire_types[j].value==data[i].Questionnaire_Type_ID){type = gdata.questionnaire_types[j].text}}
			questionnaire_grid.addRow(data[i].ID,[0,data[i].Last_Save,data[i].Comments,data[i].Questionnaire_File,data[i].Hash_Code,data[i].Questionnaire_Type_ID,data[i].Result_File,data[i].Result_Cmp_File,type]);
		}
	}
	win.attachEvent("onClose", cleanQuestionnairesListWin);
	
	questionnaire_grid.attachEvent("onRowSelect", function(id,ind){
		
		if(privs=="rw"){
			toolbar.enableItem('edit');
			toolbar.enableItem('delete');
		}
		
		toolbar.enableItem('results');			
		toolbar.enableItem('url');
		if(gdata.is_admin==1){toolbar.enableItem('scales');}
	});
	
	questionnaire_grid.attachEvent("onRowDblClicked", function(rId,cInd){
		var qid = questionnaire_grid.getSelectedRowId();
		var hash_code = questionnaire_grid.cells(qid,4).getValue();
		var url_code = hash_code + gdata.language_id;
		var result_file = questionnaire_grid.cells(qid,6).getValue();
		window.open(result_file+"?q="+url_code,"_blank")
	});  
	
	questionnaire_grid.attachEvent("onCheck", function(rId,cInd,state){
		var checked=questionnaire_grid.getCheckedRows(0);
		
		if(checked.split(",").length>0 && checked!=""){toolbar.enableItem('email');}
		else{toolbar.disableItem('email');}
		
		if(checked=="" || checked.split(",").length!=2){toolbar.disableItem('compare');}
		else{
			toolbar.enableItem('compare');
		}
	});
	
	toolbar.attachEvent('onClick', questionnairesListToolbarClicked);
	
	gdata.questionnaires_list_grid = questionnaire_grid;
	gdata.questionnaires_list_toolbar = toolbar;
	
}

function cleanQuestionnairesListWin(){
	gdata.questionnaires_list_grid = null;
	gdata.questionnaires_list_toolbar = null;
	return true;
}
 
function initMailWin(html_body,type){
	
	var win = gdata.main_layout.dhxWins.createWindow("win_mail",0,0,850,350);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText("Mail");
	win.setIcon("../../../images/email-icon-16.png", "../../../images/email-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.addText('email_label', 0,"E-Mail");
	toolbar.addInput('email', 1,"",300);
	toolbar.addText('subject_label', 2,"Subject");
	toolbar.addInput('subject', 3,"",250);
	toolbar.setIconSize(32);
	toolbar.addButton('send', 4, 'Send','./images/send-icon-32.png','./images/send-icon-32.png');
	
	toolbar.setValue('subject', type);
	
	var editor = win.attachEditor();
	editor.setContent(html_body);
 
	toolbar.attachEvent('onClick',function() {
    	if(toolbar.getValue('email')==""){
    		dhtmlx.alert({title:"No e-mail address",type:"alert",text:"Please enter an e-mail address"});
    	}
    	else{
    		//alert(toolbar.getValue('email')+' '+toolbar.getValue('subject')+' '+editor.getContent())
    		sendMail(toolbar.getValue('email'),toolbar.getValue('subject'),editor.getContent())
    	}
    });
}

function sendMail(email,subject,body){
	var win = gdata.main_layout.dhxWins.window("win_mail");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: mail_send,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Mail':email,'Subject':subject,'Body':body},
    		success:CallbackMailSend , 
    		error: CallbackMailSendFailed,
    		cache: false
	});	
}
function CallbackMailSend(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_mail");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Mail sent successfully."});		
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error sending mail."});
	}
}
function CallbackMailSendFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_mail");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error"});
}

function questionnairesListToolbarClicked(id){
	 
	if(id=="add"){
		//var cid = gdata.questionnaires_list_toolbar.getValue("cid");
		//initQuestionnaireWin(cid,-1000)
	}
	else if(id.indexOf("type")!=-1){
		var qtype_id = id.split("-");
		var cid = gdata.questionnaires_list_toolbar.getValue("cid");
		initQuestionnaireWin(cid,-1000,qtype_id[1]);
	}
	else if(id=="results"){
		var qid = gdata.questionnaires_list_grid.getSelectedRowId();
		var hash_code = gdata.questionnaires_list_grid.cells(qid,4).getValue();
		var url_code = hash_code + gdata.language_id;
		var result_file = gdata.questionnaires_list_grid.cells(qid,6).getValue();
		window.open(result_file+"?q="+url_code,"_blank")
		//window.print();
	}
	else if(id=="scales"){
		var qid = gdata.questionnaires_list_grid.getSelectedRowId();
		var comments = gdata.questionnaires_list_grid.cells(qid,2).getValue();
		initQuestionnaireScalesWin(qid,comments)
	}
	else if(id=="edit"){
		var qid = gdata.questionnaires_list_grid.getSelectedRowId();
		var cid = gdata.questionnaires_list_toolbar.getValue("cid");
		var type_id = gdata.questionnaires_list_grid.cells(qid,5).getValue();
		initQuestionnaireWin(cid,qid,type_id)
	}
	else if(id=="delete"){
		var qid = gdata.questionnaires_list_grid.getSelectedRowId();
		dhtmlx.confirm({type:"confirm",text: "Are you sure you want to delete the selected questionnaire?",callback: function(result){if(result){deleteQuestionnaire(qid)}}});
	}
	else if(id=="url"){
		var qid = gdata.questionnaires_list_grid.getSelectedRowId();
		var url = server+gdata.questionnaires_list_grid.cells(qid,3).getValue();
		window.open(url,"_blank")
	}
	else if(id=="email"){
		var checked=gdata.questionnaires_list_grid.getCheckedRows(0);
		var checked_rows = checked.split(",")
		if(checked!="" && checked_rows.length>0){
						
			var html_body = ""; //getURLTable(checked_rows);
			for(var i=0;i<checked_rows.length;i++){
				html_body = html_body+server+gdata.questionnaires_list_grid.cells(checked_rows[i],3).getValue()+" <br>";
			}
			initMailWin(html_body,"Questionnaires")
			
		}
	}
	else if(id=="compare"){
		var checked=gdata.questionnaires_list_grid.getCheckedRows(0);
		var checked_rows = checked.split(",")
		if(checked=="" || checked_rows.length!=2){
		}
		else{
			var type_1 = gdata.questionnaires_list_grid.cells(checked_rows[0],5).getValue();
			var type_2 = gdata.questionnaires_list_grid.cells(checked_rows[1],5).getValue();
			
			if(type_1!=type_2){
				dhtmlx.alert({title:"Different Questionnaire Types",type:"alert",text:"Cannot compare questionnaires of different types."});
			}
			else{			
				var cmp_file = gdata.questionnaires_list_grid.cells(checked_rows[0],7).getValue();
				var q1_hash = gdata.questionnaires_list_grid.cells(checked_rows[0],4).getValue();
				var q2_hash = gdata.questionnaires_list_grid.cells(checked_rows[1],4).getValue();
				var url = cmp_file+"?q1="+q1_hash+"&q2="+q2_hash 
			 
				window.open(url,"_blank")
			}
		}
	}
}

function deleteQuestionnaire(qid){
	
	var win = gdata.main_layout.dhxWins.window("win_questionnaires_list");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: questionnaire_delete,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Questionnaire_ID':qid},
    		success:CallbackDeleteQuestionnaire , 
    		error: CallbackDeleteQuestionnaireFailed,
    		cache: false
	});
}

function CallbackDeleteQuestionnaire(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_questionnaires_list");
	win.progressOff();
	
	if(data.Result_Code==0){
		gdata.questionnaires_list_grid.deleteRow(data.Questionnaire_ID);
		var cid = gdata.questionnaires_list_toolbar.getValue("cid");
		adjustQuestionnaireCount(cid,-1);
		dhtmlx.alert({title:"Success",type:"alert",text:"Questionnaire deleted successfully."});		
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error deleting questionnaire."});
	}
	
}

function CallbackDeleteQuestionnaireFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_questionnaires_list");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error"});
}