<?php 
require_once('PHPMailer/class.phpmailer.php');

function notifyForCompletion($database,$entity_id,$entity_type,$status_code,$language_code) {
	
	$mail = new PHPMailer(true);
	
	$sql = "select				
				u.Email
			from ".$entity_type." x 
			join ADMIN_Data_Access a 	on x.Client_ID = a.Client_ID 
			join ADMIN_App_Users u 		on u.ID = a.App_User_ID
			where x.ID='".$entity_id."' and a.Is_Owner='1'";
		
	$results = $database->get_sql_results($sql);
	
	
	
	$sql = "select cl.Surname,cl.Name,x.Comments from Clients cl join ".$entity_type." x on cl.ID = x.Client_ID where x.ID='".$entity_id."'";
	$info_rec = $database->get_sql_results($sql);
	$info = $info_rec->fetch_array(MYSQLI_ASSOC);
		
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_SERVER","Code");
	$mail_server = $dbrec["Value"] ;		 
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_UID","Code");
	$mail_uid= $dbrec["Value"];			 
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_PWD","Code");	
	$mail_pwd=	$dbrec["Value"];		 
	$dbrec = $database->get_db_record("ADMIN_Parameters","MAIL_PORT","Code");
	$mail_port=	$dbrec["Value"];			 
	
	//error_log($mail_server."-".$mail_uid."-".$mail_pwd."-".$mail_port);
	
	$dbrec = $database->get_db_record("META_UI_Labels","MAIL_BODY","Code");
	$mail_param_text = $dbrec["Descr_en"]; 		 
	$mail_body = str_replace("%pname", $info["Surname"]." ".$info["Name"],$mail_param_text);
	$mail_body = str_replace("%qcomments", $info["Comments"],$mail_body);
	
	$mail_body = iconv("UTF-8", "ISO-8859-7", $mail_body); 
	
	$dbrec = $database->get_db_record("META_UI_Labels","MAIL_SUBJECT","Code");
	$mail_subject = $dbrec["Descr_en"]; 
	$mail_subject = iconv("UTF-8", "ISO-8859-7", $mail_subject);
	
	$mail->IsSMTP();                       // telling the class to use SMTP

	$mail->SMTPDebug = 0;          
 	$mail->SMTPAuth = true;                // enable SMTP authentication 
	//$mail->SMTPSecure = "ssl";              // sets the prefix to the servier
	
	$mail->Host = $mail_server;        // sets Gmail as the SMTP server
	$mail->Port = $mail_port;                     // set the SMTP port for the GMAIL 

	$mail->Username = $mail_uid;  // Gmail username
	$mail->Password = $mail_pwd;      // Gmail password

	$mail->CharSet = 'utf8';
	$mail->SetFrom ($mail_uid,'Eurika Platform'); 
	$mail->Subject = $mail_subject;
	$mail->ContentType = 'text/plain'; 
	$mail->IsHTML(false);
	$mail->Body = $mail_body;
	    
 	
	while($res = $results->fetch_array(MYSQLI_ASSOC)) { 		
		$mail->AddAddress ($res["Email"]); 			
 	}
 	/*if(!$mail->Send()) 
	{
    	error_log("error sending mail");
	}*/
	try{
		$mail->Send();
	}
	catch (phpmailerException $e) {
  		error_log($e->errorMessage());
  		//echo $e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
  		error_log($e->getMessage());
  		//echo $e->getMessage(); //Boring error messages from anything else!
	}
	 
}


?>