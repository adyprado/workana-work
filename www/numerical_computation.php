<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	include('server/database_slave.php');
    $database = new database_slave();
    			
    $hash_code = "-1";
    $client_id = "-1";
    
    if(isset($_GET['q']) && strlen($_GET['q'])>1){
    	$parts = explode("_",$_GET['q']);
    	$hash_code = $parts[0];
    	$client_id = $parts[1];
    }
    if(isset($_GET['q']) && strlen($_GET['q'])>1){
    	$parts = explode("_",$_GET['q']);
    	$hash_code = $parts[0];
    	$client_id = $parts[1];
    }
    $record = $database->get_db_record("Tests",$hash_code,"Hash_Code");      
    $crecord = $database->get_db_record("Clients",$client_id);
    $language = $database->get_db_record("Languages",$crecord["Language_ID"]);
    $language_code = $language["Code"];
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title><?php if($crecord==false){echo "Error";}else{echo $crecord["Surname"]." ".$crecord["Name"];} ?></title>

    	<link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
  		<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">
		<link href="results.css" rel="stylesheet">
		
		<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
  		
  		<script type="text/javascript">
  			
  			//var time_in;
  			var timer;
  			var timer_updater;
  			var visible_div;
  			var interval ;
  			var sum_time;
  			
  			function nextFormGroup(current_div){			
				
				//alert('start function:'+current_div)
  				var divs = document.getElementsByTagName("div");
				
				var next_id = 1000000
				current_div = Number(current_div)
				
				for(var i = 0; i < divs.length; i++){   			 

   					var div_id = Number(divs[i].id)
   					if(hasClass(divs[i], "form-group") && div_id<next_id && divs[i].id>current_div){
   						next_id = divs[i].id
   					}
				}
				var current_div_obj = document.getElementById(current_div);
				current_div_obj.style.display='none';
				
				var next_div_obj = document.getElementById(next_id);
				
				
				//alert('duration:'+next_div_obj.getAttribute("data-duration")+' value min:'+next_progress_bar_obj.getAttribute("aria-valuemin"))
				next_div_obj.style.display='';
				
				if(typeof timer!='undefined'){clearTimeout(timer);}
				if(typeof timer_updater!='undefined'){clearInterval(timer_updater);}

				visible_div = next_id
				sum_time = 0;
				
				var duration = Number(next_div_obj.getAttribute("data-duration"))

				var action = next_div_obj.getAttribute("data-action")
				var action_time = Number(next_div_obj.getAttribute("data-action-time"))
				
				
				if(action=='play_section_video'){
					
					//var video_obj = document.getElementById('video_'+next_id);
					//video_obj.controls = false;
					
					var sub_timer = setTimeout(function(){
						 				//var video_div_obj = document.getElementById('video_div_'+next_id);
						 				//video_div_obj.style.display='';
						 				
						 				var video_obj = document.getElementById('video_'+next_id);	
						 				video_obj.play();
									},action_time*1000);
				}
				
				if(duration>0){
					timer = setTimeout(function(){
						nextFormGroup(visible_div)
					},duration*1000);
					
					timer_updater=setInterval(function(){
						sum_time = sum_time+1;
						var next_progress_bar_obj = document.getElementById('progress_bar_'+visible_div);
						var next_progress_span_obj = document.getElementById('progress_span_'+visible_div);
						max_value = next_progress_bar_obj.getAttribute("aria-valuemax")
						var new_width = (max_value-sum_time)/max_value*100						
						next_progress_bar_obj.style.width = new_width+'%';
						next_progress_span_obj.innerHTML = max_value-sum_time
					}, 1000);
				}
  			}  
  			
  			function hasClass(element, cls) {
    			return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
			}
  			
  		</script>
  		
  	</head>
	<body>
    	<div class="container">
    		
    		<div class="page-header">
    			<h3 class="text-muted"><? echo $crecord["Surname"]." ".$crecord["Name"].":&nbspNumerical Computation Test"; ?></h3>
    		</div>
    		
    		
    		<?php 
    			if($record["Status"]=="FINISHED"){    				 
	 		 		$lb_error_msg_rec = $database->get_db_record("META_UI_Labels","TEST_DISABLED","Code");	
	 		 		echo "  		
	 		 			<div class=\"alert alert-danger\">
    						".$lb_error_msg_rec["Descr_en"]."	 
    					</div>";	 		 		
    			}
    			else if($record["Status"]=="IN_PROGRESS"){
    				$lb_error_msg_rec = $database->get_db_record("META_UI_Labels","TEST_IN_PROGRESS","Code");
    				echo "  		
	 		 			<div class=\"alert alert-danger\">
    						".$lb_error_msg_rec["Descr_en"]."	 
    					</div>";
    			}
    			else{
    			
    			$test_type_id = 4;
    			$test_structure = $database->get_test_structure($test_type_id);
    			$submit_section = false; 
    			$table_open = false;
    			
    			if($test_structure->num_rows > 0){
    				
    				$prev_section = -1;
    				
    				echo "<form action=\"numerical_computation_submit.php\" name=\"frm_numerical_computation\" method=\"post\">";
    				echo 	"<input type='hidden' name='hash_code' value='".$hash_code."'>";
					echo 	"<input type='hidden' name='client_id' value='".$client_id."'>";
    						
    				while($row = $test_structure->fetch_array(MYSQLI_ASSOC)) {    					
    					    					
    					if($row["Presentation_Section"]!=$prev_section){    
    						$set_hidden = "style=\"display: none;\"" ;    			
     						
    						if($prev_section!=-1){echo "</div>";}  
    						else{$set_hidden = "";}
    						
    						echo "<div class=\"form-group\" id=\"".$row["Presentation_Section"]."\" ".$set_hidden." data-duration=\"".$row["Presentation_Duration"]."\" data-action=\"".$row["Expiration_Action"]."\" data-action-time=\"".$row["Content_Attribute_4"]."\">"; /*open form group*/
    						$submit_section = false;
    					}
    					
    					
    					if($row["Object_Type"]=="PARAM_Test_Sections"){
    						echo "<h3>".$row["Content_TXT"]."</h3>";
    					}
    					else if($row["Object_Type"]=="HTML_TXT"){
    						echo "<div class='well' align=\"justify\">".$row["HTML_".$language_code]."</div>";
    						
    						
    					}
    					else if($row["Object_Type"]=="Info"){
    						echo "<div align=\"justify\">".$row["Content_TXT"]."</div>";
    					}
    					else if($row["Object_Type"]=="HTML_TABLE_START"){
    						 $table_open = true;
    						 echo "<div class=\"panel panel-default\">";
    						 echo "<table class=\"table\">";
    					}
    					else if($row["Object_Type"]=="HTML_TABLE_END"){
    						
    						echo "</table>";
    						echo "</div>";
    						$table_open = false;	 
    					}
    					else if($row["Object_Type"]=="PARAM_Exercises"){
    						
    						if($table_open){
    							echo "<tr><td bgcolor=\"#eeeeee\">".$row["Content_TXT"]."</td>";
    						}
    						
    						$sql_options = "select * from PARAM_Exercise_Options where Exercise_ID='".$row["Content_ID"]."' order by Code";
    						$options = $database->get_sql_results($sql_options);    						
    						
    						while($option = $options->fetch_array(MYSQLI_ASSOC)) { 
    							echo "<td>";
    							echo "<div class=\"radio-inline\">";
    							echo 	"<label><input type=\"radio\" name=\"exercise_".$row["Content_ID"]."\" value=\"".$option["ID"]."\">".$option["Descr_en"]."</label>";
    							echo "</div>";
    							echo "</td>";
    						}
    						echo "</tr>";
    						
    					}
    					else if($row["Object_Type"]=="PARAM_Test_Material"){
    						echo "<div align=\"justify\">".$row["Content_TXT"]."</div><br>";
    					}
    					else if($row["Object_Type"]=="HTML_INPUT" && $row["Rule_Type"]=="Submit"){
      						echo "<div align='right'><input type=\"".$row["Rule_Type"]."\" value=\"".$row["HTML_".$language_code]."\"></div>";
    						$submit_section = true;
    					}
    					else if($row["Object_Type"]=="HTML_VIDEO"){
    						echo "<div id=\"video_div_".$row["Presentation_Section"]."\" align='center'>";
    						echo "<video align=\"center\" id=\"video_".$row["Presentation_Section"]."\" width=\"".$row["Content_Width"]."\" height=\"".$row["Content_Height"]."\" controls>";
    						echo "<source src=\"".$row["Content_Attribute_1"]."\" type=\"".$row["Content_Attribute_2"]."\">";
    						//echo "<source src=\"".$row["Content_Attribute_1"]."\">";
    						echo "</video>";
    						echo "</div>";
    					}
    					else{
    						echo $row["Content_TXT"];
    					}    					 
    					
    					if($row["Section_Break"]=="T" && $row["Object_Type"]!="HTML_INPUT"){
    						
    						if($row["Presentation_Duration"]>0){
    							echo "<div class=\"progress\">
  										<div class=\"progress-bar\" id=\"progress_bar_".$row["Presentation_Section"]."\" role=\"progressbar\" aria-valuenow=\"".$row["Presentation_Duration"]."\" aria-valuemin=\"0\" aria-valuemax=\"".$row["Presentation_Duration"]."\" style=\"width: 100%;\">
    										<span id=\"progress_span_".$row["Presentation_Section"]."\">".$row["Presentation_Duration"]."</span>
  										</div>
									</div>";    						
    						}
    						if(!$submit_section){
    							echo "<div align='right'><button type=\"button\" onClick=\"nextFormGroup(".$row["Presentation_Section"].")\" style=\"width:100px;\">Next</button></div>";
    						}
    					}
    					$prev_section = $row["Presentation_Section"];
    				}
    				
    				//echo "<input type=\"submit\" value=\"Finish\" >";
    				echo "</form>";
    				
    				$sql = "Update Tests set Status='IN_PROGRESS' where ID='".$record["ID"]."'";
    				$result = $database->execute_update($sql);
    				
    				}
    			}
    		?>
    		 
    	</div>
	</body>
</html>








