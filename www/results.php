<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>.</title>

    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">

	
	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
  	<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
  	<script type="text/javascript" src="graphs.js"></script>
  	<script type="text/javascript" src="html2canvas.js"></script>
  	<script src="canvas2image.js"></script>
	<script src="base64.js"></script>
	
    <script type="text/javascript">
      //google.load("visualization", "1", {packages:["corechart"]});
      //google.setOnLoadCallback(drawChart);
      
      var chart_list=new Array;
      
      function drawChart() {
        
        var data_url =  "server/questionnaire_data.php?q="+<? echo "'".$_GET['q']."'"; ?>+"" 
        
        $.ajax({
    		type: 'GET',
    		url: data_url,
    		dataType: 'json',
    		success:CallbackQuestionnaireData , 
    		error: CallbackQuestionnaireDataFailed,
    		cache: false
		});        
        /*
        var jsonData = $.ajax({
          url: data_url,
          dataType:"json",
          async: false
          }).responseText;
               
        var data = new google.visualization.DataTable(jsonData);
      
        var options = {
          title: 'Results',
          hAxis: {title: 'Scales', titleTextStyle: {color: 'red'}},
          fontSize:'10',
          height: 71 * 25
        };
		
        //var chart = new google.visualization.ColumnChart(document.getElementById('bar_chart_div'));
        //var chart = new google.visualization.BarChart(document.getElementById('bar_chart_div'));
        //chart.draw(data, options);  
        */
      
      	drawCircleScatterChart("scatter_1","High Psychopathology","Low Psychopathology","High Addiction","Low Addiction")
        drawCircleScatterChart("scatter_2","High Psychopathology","Low Psychopathology","High Risk","Low Risk")
        drawCircleScatterChart("scatter_3","High Empathy","Low Empathy","High Assertiveness","Low Assertiveness")
      	 
      }
      function CallbackQuestionnaireData(data, textStatus, jqXHR){
      	
      	var scale_asrt;
      	var scale_empt;
      	var scale_ansc;
      	var scale_mean_clinical;
      	var scale_addiction;
      	
      	for (item in data) {       		
      		if(item!="Means"){
      			var barChart1 = new dhtmlXChart({
    							view: "bar",
   								container: document.getElementById(item),//"chartDiv",
    							value: "#A_Score#",
    							label: "#A_Score#",
    							//color: "#color#",
    							width: 10,
    							gradient: "rising",
    							radius: 0,
   								tooltip: {
        							template: "#Scale_Code#<br>#A_Score#"
    							},
   								xAxis: {
        							title: "Scale Code",
        							template: "#Scale_Code#"
        							
   								 },
    							yAxis: {
        							title: "AScore"
    							},   								
    							origin: 0
		 				});
				barChart1.parse(data[item], "json");
			
				for(var i=0;i<data[item].length;i++){
					//alert(data[item][i].Scale_Code);
					if(data[item][i].Scale_Code=="ASRT"){scale_asrt=data[item][i].A_Score}
					if(data[item][i].Scale_Code=="EMPT"){scale_empt=data[item][i].A_Score}
					if(data[item][i].Scale_Code=="ANSC"){scale_ansc=data[item][i].A_Score}
				}
			   
			   
			   chart_list.push(barChart1);
			   
			}
			else{
				for(var i=0;i<data[item].length;i++){
					if(data[item][i].Code=="MCS"){scale_mean_clinical=data[item][i].Score}
					if(data[item][i].Code=="ASME"){scale_addiction=data[item][i].Score}
				}
			}
      	}

      	drawPointOnScatterChart("scatter_1",scale_mean_clinical,scale_addiction,"./images/red-icon-8.png")
      	drawPointOnScatterChart("scatter_2",scale_mean_clinical,scale_ansc,"./images/red-icon-8.png")
      	drawPointOnScatterChart("scatter_3",scale_empt,scale_asrt,"./images/red-icon-8.png")
      }
      function CallbackQuestionnaireDataFailed(jqXHR, textStatus, errorThrown){
      	alert("error loading graph data")
      }
      
      function printDiv(divId){
      	//var chart = chart_list[0];
      	//var canvas = document.getElementById("1")//chart.getCanvas();//canvas;
      	//var img = canvas.toDataURL("image/png");
      		html2canvas(document.getElementById(divId), {
      			onrendered: function(canvas) {
      				Canvas2Image.saveAsPNG(canvas);
      				//canvas.toDataURL("image/png");
      				//document.body.appendChild(canvas);
      				 //link.href = canvas.toDataURL();
    				 //link.download = "test.png";
      			}
      		});
      }
      
    </script>
  
  </head>
  <body onLoad="drawChart()">
    <div class="container">
    
    <?php 
    	include('server/database_slave.php');
    	$database = new database_slave();
    	
    	$info = $database->get_questionnaire_info($_GET["q"]);
    	if(!$info){echo "error";}
    ?>
        
    <div class="header">
        <h3 class="text-muted"><? echo $info["Surname"]." ".$info["Name"]?></h3>
    </div>
    
    <!--<div class="jumbotron" id="bar_chart_div">aa</div>-->
    
    <ul class="nav nav-tabs">
  		<li class="active"><a href="#graphs" data-toggle="tab">Graphs</a></li>
  		<li><a href="#circle_graphs" data-toggle="tab">Scatter Charts</a></li>
  		<li><a href="#critical_items" data-toggle="tab">Critical Items</a></li>
  		<li><a href="#diagnostic_criteria" data-toggle="tab">Diagnostic Criteria</a></li>
  		<li><a href="#analytics" data-toggle="tab">Analytic Results</a></li>
  		<li><a href="#questionnaire" data-toggle="tab">Questionnaire</a></li>
	</ul>
    
    <div class="tab-content">
    	<div class="tab-pane active" id="graphs">
    		<div class="row marketing">
    	<? 
    		$groups = $database->get_recordset("Scale_Groups_Rep");//,"MOD(ID,2)",$i);
    		while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
    			$scores = $database->get_group_rep_scores($_GET["q"],$g["ID"]);    			
    			if($scores){
    				echo "<div class=\"panel panel-default\">
    						<div class=\"panel-heading\">".$g["Descr_en"]." <!--<button onClick=\"printDiv('".$g["ID"]."')\">test</button>--></div>
    						<div class=\"panel-body\" id=\"".$g["ID"]."\"></div>
    					  </div>"; 				
    			}
    			
    		}
    	?>		
    		</div>
		</div>		
		
		<div class="tab-pane" id="circle_graphs">
			<div class="row marketing">
				<div class="panel panel-default">
					<div class="panel-heading">Addiction / Psychopathology</div>
					<div class="panel-body">
						<canvas id="scatter_1" width="600" height="400" style="background-color:#ffffff"></canvas>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Risk / Psychopathology</div>
					<div class="panel-body">
						<canvas id="scatter_2" width="600" height="400" style="background-color:#ffffff"></canvas>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Assertiveness / Empathy</div>
					<div class="panel-body">
						<canvas id="scatter_3" width="600" height="400" style="background-color:#ffffff"></canvas>
					</div>
				</div>
			
			
			</div>		
		</div>		
		
		<div class="tab-pane" id="critical_items">
			<div class="row marketing">
				<?
					$records = $database->get_critical_questions_records($_GET["q"]);
					$grp_id = -1;
					$cat_id = -1;
					//error_log($records->num_rows);
					while($rec = $records->fetch_array(MYSQLI_ASSOC)) {						
						
						if($grp_id != $rec["Group_ID"]){
							
							if($grp_id != -1){echo "</tbody></table></div>";}
													
							echo "<div class=\"panel panel-default\">
								     <div class=\"panel-heading\">".$rec["Group_Descr"]."</div>
								     <table class=\"table table-condensed\">
								     	<!--<thead>
                							<tr>
                  								<th width='20'>Question Code</th>
                  								<th width='180'>Question</th>
                  								<th width='100'>Points</th>
                							</tr>
              							</thead>-->
              							<tbody>";						
							
							$grp_id = $rec["Group_ID"];
						}
						if($cat_id != $rec["Category_ID"]){
							
							echo "<tr><td colspan='3'><b>".$rec["Category_Descr"]."</b></td></tr>";
							
							$cat_id = $rec["Category_ID"];	
						}
						
						echo "
								<tr>
									<td>".$rec["Code"]."</td>
									<td>".$rec["Question_el"]."</td>
									<td>".$rec["Points"]."</td>
								</tr>
							";
						
					}
					echo "</tbody></table></div>";
					
				?>
			</div>
		</div>
		
		<div class="tab-pane" id="diagnostic_criteria">
			<div class="row marketing">
				<?
					$records = $database->get_rep_diagnostic_criteria($_GET["q"]);
					$in_header = -1;
					while($rec = $records->fetch_array(MYSQLI_ASSOC)) {
						
						if($rec["Row_Type"]=="header"){
							if($in_header!=-1){
								echo "</tbody></table></div>";
							}
							echo "<div class=\"panel panel-default\">
								     <div class=\"panel-heading\">".$rec["Header_en"]."</div>
								     <table class=\"table table-condensed\">
								     	<thead>
                							<tr>
                  								<th width='160'></th>
                  								<th width='100'>Information</th>
                  								<th width='20'>Result</th>
                  								<th width='20'>Points</th>
                							</tr>
              							</thead>
              							<tbody>";
              				$in_header = 1;
						}
						else if($rec["Row_Type"]=="scale_code_thrd" || $rec["Row_Type"]=="group_mean_thrd"){
							$math = $rec["Expr"];
							$math_score = $rec["Score"];
							$descr = $rec["Expr_Descr"];
							$label = $rec["Label_en"];
							$pts =  eval("return ($math);");
							$score = eval("return ($math_score);");
							echo "
								<tr>
									<td>".$label."</td>
									<td>".$descr."</td>
									<td>".$score."</td>
									<td>".$pts."</td>
								</tr>
							";
						}
						else if($rec["Row_Type"]=="scale_code_score"){
							$label = $rec["Label_en"];
							$descr = $rec["Expr_Descr"];
							$score = $rec["Score"];
							echo "
								<tr>
									<td>".$label."</td>
									<td>".$descr."</td>
									<td>".$score."</td>
									<td></td>
								</tr>
							";							
						}
						else if($rec["Row_Type"]=="group_end"){
							echo "<tr style='height:1px; min-line-height: 1px;'><td colspan='4' bgcolor='#dfdfdf' style='height:1px'></td></tr>";
						}
						
						/*if($rec["Row_Type"]=="scale_code_thrd"){
							$math = $rec["Expr"];
							$pts =  eval("return ($math);");
						}
						else{
							$pts = "0";
						}
						error_log($rec["Expr"]);
						error_log($rec["Expr_Descr"]." ".$pts);
						*/
					}
					echo "</tbody></table></div>";
				?>
			</div>
		</div>
		
		
		<div class="tab-pane" id="analytics">
			<div class="row marketing">
    	<? 
    		$groups = $database->get_recordset("Scale_Groups_Rep");
    		while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
    			$scores = $database->get_group_rep_scores($_GET["q"],$g["ID"]);    			
    			if($scores){
    				echo "<div class=\"panel panel-default\">
    						<div class=\"panel-heading\">".$g["Descr_en"]."</div>
    						<table class=\"table table-condensed\">
    							<thead>
                					<tr>
                  						<th width='100'>Scale Code</th>
                  						<th width='100'>A-Score</th>
                  						<th width='100'>Raw-Score</th>
                					</tr>
              					</thead>
              					<tbody>
    						";
    				while($sc = $scores->fetch_array(MYSQLI_ASSOC)) { 		
    					echo "<tr>
    							<td>".$sc["Scale_Code"]."</td>
    							<td>".$sc["A_Score"]."</td>
    							<td>".$sc["Raw_Score"]."</td>
    						  </tr>";	
    				}
    				echo"</tbody></table></div>";    				
    			}
    			
    		}
    	?>
		
    	</div>
		</div>
		
		<div class="tab-pane" id="questionnaire">
			<div class="row marketing">
				<? 
					$questionnaire = $database->get_questionnaire_answers($_GET["q"],"Hash_Code");
					echo "<div class=\"panel panel-default\">
    						<div class=\"panel-heading\"></div>
    						<table class=\"table table-condensed\">
    							<thead>
                					<tr>
                  						<th width='50'>Code</th>
                  						<th width='150'>Question</th>
                  						<th width='50'>Answer</th>
                					</tr>
              					</thead>
              					<tbody>
    						";
    				while($question = $questionnaire->fetch_array(MYSQLI_ASSOC)) { 		
    					echo "<tr>
    							<td>".$question["Code"]."</td>
    							<td>".$question["Question_el"]."</td>
    							<td>".$question["Answer"]."</td>
    						  </tr>";	
    				}
    				echo"</tbody></table></div>";
					
				?>
			</div>			
		</div>
		
	</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
  </body>
</html>