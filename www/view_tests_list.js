function initTestsListWin(client_id,data,privs){
	
	var surnameIndex = gdata.clients_grid.getColIndexById("Surname");
	var nameIndex = gdata.clients_grid.getColIndexById("Name");
	
	var surname = gdata.clients_grid.cells(client_id,surnameIndex).getValue();
	var name	= gdata.clients_grid.cells(client_id,nameIndex).getValue();
	
	var win = gdata.main_layout.dhxWins.createWindow("win_tests_list",0,0,600,400);
	
	win.centerOnScreen();
	win.setModal(true);
	win.setText(surname+" "+name+" Tests");
	win.setIcon("../../../images/test-icon-16.png", "../../../images/test-icon-16.png");
	win.show();
	
	var toolbar = win.attachToolbar();
	toolbar.setIconSize(32);
	//toolbar.addButton('add', 0, 'New','./images/add-icon-32.png','./images/add-icon-32.png');
	
	toolbar.addButton('add', 0, 'New','./images/add-icon-32.png','./images/add-icon-32.png');
	toolbar.addSeparator('sep_0', 1);
	toolbar.addButton('results', 2, 'Results','./images/test-result-icon-32.png','./images/test-result-icon-32.png');
	toolbar.addSeparator('sep_1', 3);
	toolbar.addButton('delete', 4, 'Delete','./images/recycle-bin-icon-32.png','./images/recycle-bin-icon-32.png');
	toolbar.addSeparator('sep_2', 5);
	toolbar.addButton('url', 6, 'Fill Online','./images/test-icon-32.png','./images/test-icon-32.png');
	toolbar.addSeparator('sep_3', 7);
	toolbar.addButton('email', 8, 'E-Mail','./images/email-icon-32.png','./images/email-icon-32.png');
	toolbar.addSeparator('sep_4', 9);
	toolbar.addButton('unlock', 10, 'Unlock','./images/unlock-icon-32.png','./images/unlock-icon-32.png');
	
	if(privs != "rw"){
		toolbar.disableItem('add');
	}
	
	toolbar.disableItem('results');	 
	toolbar.disableItem('delete');	 
	toolbar.disableItem('url');
	toolbar.disableItem('email');
	//toolbar.disableItem('unlock');
	
	toolbar.addInput('cid', 7, client_id,1);
	toolbar.hideItem('cid');
 	
	 
 	
	var test_grid = win.attachGrid();	
	test_grid.setHeader(",Last Save,Comments,,,,,Type,");
	test_grid.setColumnIds("Compare,Last_Save,Comments,Test_File,Hash_Code,Test_Type_ID,Result_File,Test_Type,Status");
	 
	test_grid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro");
	test_grid.setColSorting("str,str,str,str,str,str,str,str,str");
	test_grid.setInitWidthsP("10,25,42,0,0,0,0,23,0");	
	test_grid.setColumnsVisibility("false,false,false,true,true,true,true,false,true");
	test_grid.setSkin("dhx_skyblue");
	test_grid.setImagePath(gdata.img_path);		
	test_grid.init();
	
	if(typeof data !='undefined'){
		for(var i=0;i<data.length;i++){			
			var type;
			for(var j=0;j<gdata.test_types.length;j++)
			{if(gdata.test_types[j].value==data[i].Test_Type_ID){type = gdata.test_types[j].text}}
			test_grid.addRow(data[i].ID,[0,data[i].Last_Save,data[i].Comments,data[i].Test_File,data[i].Hash_Code,data[i].Test_Type_ID,data[i].Result_File,type,data[i].Status]);
		}
	}
	win.attachEvent("onClose", cleanTestsListWin);
	
	test_grid.attachEvent("onRowSelect", function(id,ind){
		
		if(privs=="rw"){			
			toolbar.enableItem('delete');
		}		
		toolbar.enableItem('results');			
		toolbar.enableItem('url');
		
		var status_idx = test_grid.getColIndexById("Status");
		var status = test_grid.cells(id,status_idx).getValue();
		/*if(status!="PENDING"){
			toolbar.enableItem('unlock');
		}
		else{
			toolbar.disableItem('unlock');
		}*/
		
	});
	
	test_grid.attachEvent("onRowDblClicked", function(rId,cInd){
		var qid = test_grid.getSelectedRowId();
		var hash_code_idx = test_grid.getColIndexById("Hash_Code");
		var hash_code = test_grid.cells(qid,hash_code_idx).getValue();
		var url_code = hash_code + gdata.language_id;
		
		var result_file_idx = test_grid.getColIndexById("Result_File");
		var result_file = test_grid.cells(qid,result_file_idx).getValue();
		window.open(result_file+"?q="+url_code,"_blank")
	}); 	
		
	test_grid.attachEvent("onCheck", function(rId,cInd,state){
		var checked=test_grid.getCheckedRows(0);
		
		if(checked.split(",").length>0 && checked!=""){toolbar.enableItem('email');}
		else{toolbar.disableItem('email');}
	});	
		
		
	toolbar.attachEvent('onClick', testsListToolbarClicked);
	
	var myPop = new dhtmlXPopup({
            toolbar: toolbar,
            id: "add"
        });
	
		
	var formStructure = [
		{type:"settings",position:"label-top" },
		    
		    {type:"fieldset", label: "Information",name: "new_test_info", inputWidth:200, offsetLeft:"7",offsetTop:"15", list:[
		    	{type:"input", name:"Comments", label:"Comments",inputWidth:180, validate: "NotEmpty"},		    	
				{type:"combo", name:"Test_Type_ID", label:"Test Type",inputWidth:180, validate: "NotEmpty"}	    	
		    ]},
		    {type: "button", value:"Save"},
		    {type: "hidden", name:"ID",value:-1000},	
		    {type: "hidden", name:"Client_ID",value:-1000},
		    {type: "hidden", name:"Is_Admin",value:-1000},
		    {type: "hidden", name:"json",value:""}
 	] 
			
	var new_test_form = myPop.attachForm(formStructure);
	var test_type_combo = new_test_form.getCombo("Test_Type_ID");
	test_type_combo.addOption(gdata.test_types);
	test_type_combo.readonly(true);
	 	
	myPop.attachEvent("onShow", function() {
            new_test_form.clear();
            new_test_form.setItemValue('ID',-1000);
            new_test_form.setItemValue('Client_ID',client_id);
            new_test_form.setItemValue('Is_Admin',gdata.is_admin);
            new_test_form.setFocusOnFirstActive();            
        });
	
	new_test_form.attachEvent("onButtonClick", function() {            
			if(new_test_form.validate()){
				var json = formToJSON(new_test_form) 
				saveNewTest(json)	
				//alert(json)
				myPop.hide();						
			}
        });
	
	gdata.tests_list_grid = test_grid;
	gdata.tests_list_toolbar = toolbar;
	
}

function cleanTestsListWin(){
	gdata.tests_list_grid = null;
	gdata.tests_list_toolbar = null;
	return true;
}

function saveNewTest(json){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: test_set,
    		dataType: 'json',
    		data: json,
    		success:CallbackSaveTest , 
    		error: CallbackSaveTestFailed,
    		cache: false
	});
}

function CallbackSaveTest(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOff();
	
	if(data.Result_Code==0){
		dhtmlx.alert({title:"Success",type:"alert",text:"Test saved successfully."});
		addTestToGrid(data)
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:data.Result_Message});
	}	
}

function addTestToGrid(test){
	
	var type;
	for(var j=0;j<gdata.test_types.length;j++)
		{if(gdata.test_types[j].value==test.Test_Type_ID){type = gdata.test_types[j].text}}
	
	gdata.tests_list_grid.addRow(test.ID,[0,test.Last_Save,test.Comments,test.Test_File,test.Hash_Code,test.Test_Type_ID,test.Result_File,type,test.Status]);
		
}


function CallbackSaveTestFailed(jqXHR, textStatus, errorThrown){	
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOff();	
}

function testsListToolbarClicked(id){
	 
	if(id=="add"){
		//var cid = gdata.questionnaires_list_toolbar.getValue("cid");
		//initQuestionnaireWin(cid,-1000)
		
	}	
	else if(id=="results"){
		var qid = gdata.tests_list_grid.getSelectedRowId();
		var hash_code_idx = gdata.tests_list_grid.getColIndexById("Hash_Code");
		var hash_code = gdata.tests_list_grid.cells(qid,hash_code_idx).getValue();
		var url_code = hash_code + gdata.language_id;
		
		var result_file_idx = gdata.tests_list_grid.getColIndexById("Result_File");
		var result_file = gdata.tests_list_grid.cells(qid,result_file_idx).getValue();
		window.open(result_file+"?q="+url_code,"_blank")
	}	 
	else if(id=="delete"){
		var qid = gdata.tests_list_grid.getSelectedRowId();
		dhtmlx.confirm({type:"confirm",text: "Are you sure you want to delete the selected test?",callback: function(result){if(result){deleteTest(qid)}}});
	}
	else if(id=="email"){
		var checked=gdata.tests_list_grid.getCheckedRows(0);
		var checked_rows = checked.split(",")
		var test_file_idx = gdata.tests_list_grid.getColIndexById("Test_File");
		
		if(checked!="" && checked_rows.length>0){
						
			var html_body = "";  
			for(var i=0;i<checked_rows.length;i++){
				//html_body = html_body+server+gdata.tests_list_grid.cells(checked_rows[i],3).getValue()+" <br>";
				var test_file = gdata.tests_list_grid.cells(checked_rows[i],test_file_idx).getValue();		
				var url = server+test_file;
				html_body = html_body+url+" <br>";
				
			}
			initMailWin(html_body,"Tests")
			
		}
	}
	else if(id=="url"){
		var qid = gdata.tests_list_grid.getSelectedRowId();
		var test_file_idx = gdata.tests_list_grid.getColIndexById("Test_File");
		var test_file = gdata.tests_list_grid.cells(qid,test_file_idx).getValue();
		
		var url = server+test_file;
		window.open(url,"_blank")
	}
	else if(id=="unlock"){
		var qid = gdata.tests_list_grid.getSelectedRowId();
		dhtmlx.confirm({type:"confirm",text: "Are you sure you want to unlock the selected test?",callback: function(result){if(result){unlockTest(qid)}}});
	}
	 
}
function unlockTest(qid){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: test_unlock,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Test_ID':qid},
    		success:CallbackUnlockTest , 
    		error: CallbackUnlockTestFailed,
    		cache: false
	});
}

function CallbackUnlockTest(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOff();
	
	if(data.Result_Code==0){
		var qid = gdata.tests_list_grid.getSelectedRowId();
		var status_idx = gdata.tests_list_grid.getColIndexById("Status");
		gdata.tests_list_grid.cells(qid,status_idx).setValue("PENDING");
		
		dhtmlx.alert({title:"Success",type:"alert",text:"Test unlocked."});		
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error unlocking test."});
	}
}

function CallbackUnlockTestFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error"});
}

function deleteTest(qid){
	
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOn();
	
	$.ajax({
    		type: 'POST',
    		url: test_delete,
    		dataType: 'json',
    		data: {'User_ID':gdata.uid,'Api_Key':gdata.api_key,'Is_Admin':gdata.is_admin,'Channel':"WebAdmin",'Test_ID':qid},
    		success:CallbackDeleteTest , 
    		error: CallbackDeleteTestFailed,
    		cache: false
	});
}

function CallbackDeleteTest(data, textStatus, jqXHR){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOff();
	
	if(data.Result_Code==0){
		gdata.tests_list_grid.deleteRow(data.Test_ID);		 
		dhtmlx.alert({title:"Success",type:"alert",text:"Test deleted successfully."});		
	}
	else{
		dhtmlx.alert({title:"Error",type:"alert",text:"Error deleting test."});
	}
	
}

function CallbackDeleteTestFailed(jqXHR, textStatus, errorThrown){
	var win = gdata.main_layout.dhxWins.window("win_tests_list");
	win.progressOff();
	
	dhtmlx.alert({title:"Error",type:"alert",text:"Unknown error"});
}