<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	
	
	include('server/database_slave.php');
    $database = new database_slave();
    
    $params = explode(',', $_GET['q']);
	
	$is_admin = $params[sizeof($params)-1];
	$language_id = $params[sizeof($params)-2];
	$api_key = $params[sizeof($params)-3];
	$uid = $params[sizeof($params)-4];
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">

	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
	<script src="base64.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	 
  	<script type="text/javascript">
  		function drawCanvas(total_items,max_value){
  			
 			for(var i=1;i<total_items;i++){
  				//var i = 1;
  				var canvas_id = "canvas"+i;
  				var cell_id = "td"+i;
  			  			
  				var c = document.getElementById(canvas_id);
				var t = document.getElementById(cell_id);
			
				var total_width = c.width;
				var value = parseInt(t.innerHTML);
				var bar_length = ((value/max_value).toPrecision(2)) * total_width
				
				var ctx = c.getContext("2d");
				
				var grd=ctx.createLinearGradient(0,0,total_width,0);
				grd.addColorStop(0,"#d3d3d3");
				grd.addColorStop(0.3,"orange");
				grd.addColorStop(1,"red");
			
				ctx.fillStyle = grd //"#FF0000";
				ctx.fillRect(0,0,bar_length,10);
			}
			
  		}
  	</script>
  </head>
	
  <body>
  	<?php
  	
  	if($database->app_user_valid($uid,$api_key)){
		$qids = ""; 
		for($i=0;$i<sizeof($params)-4;$i++){		
			$qids = $qids.$params[$i].",";	 	
		}	
		$qids = substr($qids, 0, -1);	
		
		if($is_admin!="1"){
			$sql = "select count(1) as AccessTo from Questionnaires q 
					join ADMIN_Data_Access a on q.Client_ID = a.Client_ID
					where q.ID in (".$qids.") and App_User_ID = '".$uid."'";
			
			error_log($sql);
			$recordset = $database->get_sql_results($sql);
			$record = $recordset->fetch_array(MYSQLI_ASSOC);
			
			if($record["AccessTo"]!=(sizeof($params)-4)){
				error_log("unauthorized");
				exit;
			}		
		}
		
		echo "<img src='./images/LOGO_FILISTOS_".$language_code.".png' height='66' width='150' class=\"logo_image\" style=\"margin-left:50px;\">";
	?>
		
	<div class="container">	
		
		<?php 
 
			$prev_qtype = "";
			$prev_scale = "";
			$row_idx = -1;
			$row_span_scale = -1;
		 	$canvas_id = 1;
		 	$max_val = -1;
		 	
			$sql = "select 
						qt.Descr_en as Q_Type,
						repg.Descr_en as Rep_Sec,
						pa.Code as Code,
						q.ID as Q_ID,
						cl.Surname,
						cl.Name,
						q.Comments,
						a.A_Score
					from CALC_A_Scores a
					join PARAM_Scales pa on a.Scale_ID = pa.ID
					join Questionnaires q on a.Questionnaire_ID = q.ID
					join Clients cl on cl.ID = q.Client_ID
					join PARAM_Questionnaire_Types qt on qt.ID = q.Questionnaire_Type_ID 
					join MAP_Scales_Groups_Rep rep on rep.Scale_ID = a.Scale_ID and rep.Questionnaire_Type_ID = q.Questionnaire_Type_ID
					join Scale_Groups_Rep repg on rep.Group_Rep_ID = repg.ID 
					where a.Questionnaire_ID in (".$qids.")
					order by 1,2,3,4 
				" ;
			//error_log($sql);	 
			 
			$recordset = $database->get_sql_results($sql);
			
			if($recordset->num_rows>0){				
											
				while($record = $recordset->fetch_array(MYSQLI_ASSOC)){
					$row_idx = $row_idx+1;
					
					if($record["Q_Type"]!=$prev_qtype){
						$row_span_scale = -1;
						if($prev_qtype!=""){
							echo "</table></div>";	
						}
					
						echo "<div class=\"panel panel-default\">
							<div class=\"panel-heading\">".$record["Q_Type"]."</div>
							<table class=\"table table-condensed\">
								<thead>
                					<tr>
                						<th class=\"col-md-2\">Scale Code</th>  
                						<th class=\"col-md-4\">Questionnaire</th>
                  						<th class=\"col-md-2\">A-Score</th>
                  						<th class=\"col-md-8\">Graph</th>
                					</tr>
              					</thead>";						
					}
					if($record["Code"]!=$prev_scale){
						 $curr_scale = $record["Code"];
						 $row_span = 1;
						 
						 if($row_span_scale == -1){
						 	while($rec = $recordset->fetch_array(MYSQLI_ASSOC)){
						 		if($rec["Code"]==$curr_scale){
						 			$row_span = $row_span+1;
						 		}						 	
						 	}
						 	$recordset->data_seek($row_idx+1);
						 	$row_span_scale = $row_span;
						 	//error_log($row_idx);
						 }
						 echo"<tr>
  								<td class=\"active\" rowspan=\"".$row_span_scale."\"><b>".$record["Code"]."</b></td>  								 
  								<td>".$record["Surname"]." ".$record["Name"].":".$record["Comments"]."</td>
  								<td id=\"td".$canvas_id."\">".round($record["A_Score"])."</td>
  								<td class=\"active\"><canvas id=\"canvas".$canvas_id."\" height='10' width='450'></canvas></td>
  							</tr>";					 
						 
					}
					else{
						echo"<tr>  																 
  								<td>".$record["Surname"]." ".$record["Name"].":".$record["Comments"]."</td>
  								<td id=\"td".$canvas_id."\">".round($record["A_Score"])."</td>
  								<td class=\"active\"><canvas id=\"canvas".$canvas_id."\" height='10' width='450'></canvas></td>
  							</tr>";	
					}
					
					$canvas_id = $canvas_id+1;
					$prev_scale = $record["Code"];
					$prev_qtype = $record["Q_Type"];
					
					if($max_val<round($record["A_Score"])){
				 		$max_val=round($record["A_Score"]);
				 	}
					

				}
				echo "</table></div>";
			}				
			else{			
				
			}
			
		?>		
		 
	</div>
		
		
	<?php			 
	}
	else{
		echo "out";
	}
  	
  	echo "<script type=\"text/javascript\">drawCanvas(".$canvas_id.",".$max_val.");</script>";
  	?>
  
  
  </body>	
	
</html>