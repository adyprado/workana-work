<?php 
	header('X-Robots-Tag: noindex');
	header('Content-Type: text/html; charset=utf-8'); 
	
	include('server/database_slave.php');
    $database = new database_slave();
	
	$hash_code = substr($_GET['q'],0,-1);
	$language_id = substr($_GET['q'], -1);
	
	$record = $database->get_db_record("Languages",$language_id);
	$language_code = $record["Code"];    	
    	
    $info = $database->get_questionnaire_info($hash_code);
    
    $unit = " secs";
    $ftime = $info["Fill_Time"];
    if($ftime>60){
    	$ftime = round($ftime/60,1);
    	$unit = "mins";
    }
    if($ftime==0){$ftime="N/A";$unit ="";}
    
    if(!$info){
    	echo "Invalid questionnaire.";
    }
    else{
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><? echo $info["Surname"]." ".$info["Name"]?></title>

    <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="results.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.css">

	
	<script src="dhtmlxSuite_v36_pro_131108_eval/dhtmlx_pro_full/dhtmlx.js"></script>
  	<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
  	<script type="text/javascript" src="graphs.js"></script>
  	<script type="text/javascript" src="html2canvas.js"></script>
  	<script src="canvas2image.js"></script>
	<script src="base64.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	
    <script type="text/javascript">

      var chart_list=new Array;
      
      function drawChart() {
        
        var data_url =  "server/questionnaire_data.php?q="+<? echo "'".$hash_code."'"; ?>+"" 
        $.ajax({
    		type: 'GET',
    		url: data_url,
    		dataType: 'json',
    		success:CallbackQuestionnaireData , 
    		error: CallbackQuestionnaireDataFailed,
    		cache: false
		});        
      	 
      }
      function CallbackQuestionnaireData(data, textStatus, jqXHR){
      	
      	
      	for (item in data) {     		
      		 	
      		 	var copy_data = data[item].slice(0);
      		 	
      			var barChart1 = new dhtmlXChart({
    							view: "radar",
   								container: document.getElementById(item), 
    							value: "#Raw_Score#",
    							tooltip: {
        							template: "#Scale_Code_Descr#<br>Raw_Score=#Raw_Score#"
    							},
    							alpha:0.1,
    							line:{
                     				color:"#58dccd",
                     				width:2
                				},
   								xAxis: {
        							template: "#Scale_Code_Descr#"
   								 },
    							yAxis: {
        							lineShape:'arc'
    							},
    							fill:true,
                				color: "#58dccd",
                				disableItems:true,
                				
    							legend:{
									values:[{text:"Raw Score",color:"#58dccd"}],
									valign:"top",
									//align:"center",
									width:100,
									layout:"y"
								}
								
    							//origin: 0
		 				});
				
				var element = "cmp_"+item
				var cmp_barChart1 = new dhtmlXChart({
    							view: "radar",
   								container: document.getElementById(element), 
    							value: "#B_Score_1#",
    							tooltip: {
        							template: "#Scale_Code_Descr#<br>Comparison=#B_Score_1#"
    							},
    							alpha:0.1,
    							line:{
                     				color:"#58dccd",
                     				width:2
                				},
   								xAxis: {
        							template: "#Scale_Code_Descr#"
   								 },
    							yAxis: {
        							lineShape:'arc'
    							},
    							fill:true,
                				color: "#58dccd",
                				disableItems:true,
                				
    							legend:{
									values:[{text:"Comparison",color:"#58dccd"}],
									valign:"top",
									//align:"center",
									width:100,
									layout:"y"
								}
								
    							//origin: 0
		 				});
				
				
				/*barChart1.addSeries({
	    			value:"#Raw_Score#",
					tooltip: {
        					template: "#Scale_Code_Descr#<br>Raw_Score=#Raw_Score#"
    				},
					color:"#a7ee70",
					label:"#Raw_Score#",
						
    				line:{
                		color:"#a7ee70",
                		width:2
                	}
				});*/
				
				element = "s_"+item
				var score_barChart1 = new dhtmlXChart({
    							view: "radar",
   								container: document.getElementById(element), 
    							value: "#A_Score#",
    							tooltip: {
        							template: "#Scale_Code_Descr#<br>A-Score=#A_Score#"
    							},
    							alpha:0.1,
    							line:{
                     				color:"#58dccd",
                     				width:2
                				},
   								xAxis: {
        							template: "#Scale_Code_Descr#"
   								 },
    							yAxis: {
        							lineShape:'arc'
    							},
    							fill:true,
                				color: "#58dccd",
                				disableItems:true,
                				
    							legend:{
									values:[{text:"A-Score",color:"#58dccd"}],
									valign:"top",
									//align:"center",
									width:100,
									layout:"y"
								}
								
    							//origin: 0
		 				});
				
				
				for(var i=copy_data.length-1;i>=0;i--){
					if(copy_data[i].Scale_Code_Descr == "Organizational Support"){copy_data.splice(i,1);}
					if(copy_data[i].Scale_Code_Descr == "Supervisor & colleague Support"){copy_data.splice(i,1);}
					if(copy_data[i].Scale_Code_Descr == "Non<br>Devaluation"){copy_data.splice(i,1);}
					if(copy_data[i].Scale_Code_Descr == "Non<br>Avoidance"){copy_data.splice(i,1);}
					
				}
				
				
				barChart1.parse(data[item], "json");
		        cmp_barChart1.parse(copy_data, "json");
		        score_barChart1.parse(data[item], "json");
		        
		         
		        
		        //chart_list.push(barChart1);
		        //document.getElementById("score_graphs").style.display = "none";
		        //document.getElementById('lnk_graph').click();
      	}
      	//document.getElementById('lnk_graph').click();

      }
      function CallbackQuestionnaireDataFailed(jqXHR, textStatus, errorThrown){
      	alert("error loading graph data")
      }
      
      function printDiv(divId){
      	//var chart = chart_list[0];
      	//var canvas = document.getElementById("1")//chart.getCanvas();//canvas;
      	//var img = canvas.toDataURL("image/png");
      		html2canvas(document.getElementById(divId), {
      			onrendered: function(canvas) {
      				Canvas2Image.saveAsPNG(canvas);
      				//canvas.toDataURL("image/png");
      				//document.body.appendChild(canvas);
      				 //link.href = canvas.toDataURL();
    				 //link.download = "test.png";
      			}
      		});
      }
      
    </script>
  
  </head>
  <body onLoad="drawChart()">
    <?php echo "<img src='./images/LOGO_FILISTOS_".$language_code.".png' height='66' width='150' class=\"logo_image\" style=\"margin-left:50px;\">";?>
    <div class="container">
        
    <div class="header">
        <h3 class="text-muted"><? echo $info["Surname"]." ".$info["Name"]?></h3>
        
        <small class="print_only">
        <table>
    	 	<tr><td>Birthdate</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Birthdate"]?></td></tr>
    	 	<tr><td>Gender</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Gender"]?></td></tr>
    	 	<tr><td>Marital Status</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Marital_Status"]?></td></tr> 
    	 	<tr><td>Profession</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Profession"]?></td></tr> 
    	 	<tr><td>Fill out date</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$info["Last_Save"]?></td></tr>
    	 	<tr><td>Fill time</td><td><? echo ":&nbsp;&nbsp;&nbsp;&nbsp;".$ftime." ".$unit?></td></tr> 
  		</table>
  		</small>
        <br>
        
    </div>
    
    <!--<div class="jumbotron" id="bar_chart_div">aa</div>-->
    
    <ul class="nav nav-tabs">
  		<li class="active"><a href="#report" data-toggle="tab" id="lnk_graph"><b>Report</b><? echo "&nbsp&nbsp(fill time: ".$ftime." ".$unit.")"; ?></a></li>  		
  		<li><a href="#analytics" data-toggle="tab"><b>Analytic Results</b></a></li>
  		<li><a href="#questionnaire" data-toggle="tab"><b>Questionnaire</b></a></li>
	</ul>
    
    <div class="tab-content">
    	<div class="tab-pane active" id="report">
    		<div class="row marketing">
    	<? 
    		$report_rows = $database->get_report("WBEI",$hash_code);
    		
    		$title_col = "Title_".$language_code;
    		$descr_col = "Descr_".$language_code;
    		$heading_col = "Heading_".$language_code;
    		$info_title_col = "Info_Title_".$language_code;
    		$info_descr_col = "Info_Descr_".$language_code;
    		
    		while($row = $report_rows->fetch_array(MYSQLI_ASSOC)) {
    		
    			if($row["Rule_Type"]=="H1"){
    				
    				if($row["Page_Break"]=="B"){$extra_style=" style=\"page-break-before: always;\"";}
    				else{$extra_style="";}
    				
    				if($row[$title_col]!=""){
    					
    					if($row["Image_File"]!=""){
    						echo "<table><tr><td style=\"padding:0px 10px 0px 10px;\"><img src='".$row["Image_File"]."' width='".$row["Image_Width"]."' height='".$row["Image_Height"]."'/></td><td><h4>".$row[$title_col]."</h4></td></tr></table><br>";
    					}
    					else{
    						echo "<h4 ".$extra_style.">".$row[$title_col]."</h4><br>";	
    					}
    					
    					//echo "<div><img src='images/PBI.png' width='64' height='64'/></div><div><h4>".$row[$title_col]."</h4></div><br>";
    					//echo "<table><tr><td><img src='images/PBI.png' width='64' height='64'/></td><td><h4>".$row[$title_col]."</h4></td></tr></table><br>";
    				}
    			}
    			if($row["Rule_Type"]=="TXT"){
    				if($row[$descr_col]!=""){echo "<div align=\"justify\">".$row[$descr_col]."</div><br><br>";}
    			
    			}
    			if($row["Rule_Type"]=="E"){
    				
    				$rule_expr = $row["Expression"];
    				
    				$rule_expr = str_replace("sc1",$row["Sc_1_Score"],$rule_expr);
					$rule_expr = str_replace("sc2",$row["Sc_2_Score"],$rule_expr);
					$rule_expr = str_replace("sc3",$row["Sc_3_Score"],$rule_expr);
    				
    				$rule_expr = str_replace("scb1",$row["Sc_1_Score_b"],$rule_expr);
					$rule_expr = str_replace("scb2",$row["Sc_2_Score_b"],$rule_expr);
					$rule_expr = str_replace("scb3",$row["Sc_3_Score_b"],$rule_expr);
    				
    				$rule_expr = str_replace("min_val",$row["Min_Value"],$rule_expr);
    				$rule_expr = str_replace("max_val",$row["Max_Value"],$rule_expr);
    				//error_log($rule_expr);
    				$condition = eval("if($rule_expr){return 1;}else {return 0;}"); 
    				
    				if($condition==1){    					
    					echo "<div align=\"justify\">";
    					if($row[$info_descr_col]!=""){echo $row[$info_descr_col]."<br>";}
    					
    					if($row[$descr_col]!=""){
    						echo $row[$descr_col];
    						$pos = strrpos($row["Expression"],"scb");
    						if ($pos === false) {
    							echo "<br><br>";
    						}else echo " ";
    						
    					}
    					echo "</div>";
    				}   				 
    			}
    			if($row["Rule_Type"]=="GDIV"){
    				$scores = $database->get_group_rep_scores($hash_code,$row["Data"]);
    				if($scores){
    				$print_style = "no-break";
    				
    				echo "<br><div class=\"panel panel-default ".$print_style." \">
    						<div class=\"panel-heading no_print\">".$row[$heading_col]."<button class='no_print' style='float: right;' onClick=\"printDiv('".$row["Data"]."')\">Download</button></div>
    						<div class=\"panel-body\" id=\"".$row["Data"]."\">
    							 
    						</div>
    					  </div></br>";
    				echo "<br><div class=\"panel panel-default ".$print_style." \">
    						<div class=\"panel-heading no_print\">".$row[$heading_col]."<button class='no_print' style='float: right;' onClick=\"printDiv('cmp_".$row["Data"]."')\">Download</button></div>
    						<div class=\"panel-body\" id=\"cmp_".$row["Data"]."\">
    							 
    						</div>
    					  </div></br>";
    				echo "<br><div class=\"panel panel-default ".$print_style." \">
    						<div class=\"panel-heading no_print\">".$row[$heading_col]."<button class='no_print' style='float: right;' onClick=\"printDiv('s_".$row["Data"]."')\">Download</button></div>
    						<div class=\"panel-body\" id=\"s_".$row["Data"]."\">
    							 
    						</div>
    					  </div></br>";
    				
    				}
    			}
    		
    		}
    		
    		 
    	?>		
    		</div>
		</div>				
	 
		<div class="tab-pane" id="analytics">
			<div class="row marketing">
    	<? 
    		$groups = $database->get_recordset("Scale_Groups_Rep");
    		$group_no = 1;
    		while($g = $groups->fetch_array(MYSQLI_ASSOC)) {    			
    			$scores = $database->get_group_rep_scores($hash_code,$g["ID"]);    			
    			if($scores){
    			
    				if($group_no==1){$extra_style=" style=\"page-break-before: always;\"";}
    				else{$extra_style="";}    				
    				$group_no =$group_no+1;
    				
    				echo "<div class=\"panel panel-default no-break\" ".$extra_style.">
    						<div class=\"panel-heading\">".$g["Descr_en"]."</div>
    						<table class=\"table table-condensed\">
    							<thead>
                					<tr>
                  						<th width='100'>Scale Code</th>
                  						<th width='66'>A-Score</th>
                  						<th width='66'>Comparison</th>
                  						<th width='66'>Raw-Score</th>
                					</tr>
              					</thead>
              					<tbody>
    						";
    				while($sc = $scores->fetch_array(MYSQLI_ASSOC)) { 		
    					echo "<tr>
    							<td>".$sc["Scale_Code_Descr"]."</td>
    							<td>".$sc["A_Score"]."</td>
    							<td>".$sc["B_Score_1"]."</td>
    							<td>".$sc["Raw_Score"]."</td>
    						  </tr>";	
    				}
    				echo"</tbody></table></div>";    				
    			}
    			
    		}
    	?>
		
    	</div>
		</div>
		
		<div class="tab-pane" id="questionnaire">
			<div class="row marketing no_print">
				<? 
					$questionnaire = $database->get_questionnaire_answers($hash_code,$language_code,"Hash_Code");
					echo "<div class=\"panel panel-default\">
    						<div class=\"panel-heading\"></div>
    						<table class=\"table table-condensed\">
    							<thead>
                					<tr>
                  						<th width='50'>Code</th>
                  						<th width='150'>Question</th>
                  						<th width='50'>Answer</th>
                					</tr>
              					</thead>
              					<tbody>
    						";
    				while($question = $questionnaire->fetch_array(MYSQLI_ASSOC)) { 		
    					echo "<tr>
    							<td>".$question["Code"]."</td>
    							<td>".$question["Question"]."</td>
    							<td>".$question["Answer"]."</td>
    						  </tr>";	
    				}
    				echo"</tbody></table></div>";
					
				?>
			</div>			
		</div>
		
	</div>
	</div>
    
  </body>
</html>
<?php }?>