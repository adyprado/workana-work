-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2016 at 12:11 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `copserfe`
--

-- --------------------------------------------------------

--
-- Table structure for table `ADMIN_App_Users`
--

CREATE TABLE IF NOT EXISTS `ADMIN_App_Users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Surname` varchar(45) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Tax_Code` varchar(45) DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Api_Key` varchar(45) DEFAULT NULL,
  `Is_Admin` tinyint(4) DEFAULT '0',
  `Is_Blocked` tinyint(4) DEFAULT '0',
  `Language_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Table structure for table `ADMIN_Data_Access`
--

CREATE TABLE IF NOT EXISTS `ADMIN_Data_Access` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `App_User_ID` int(11) DEFAULT NULL,
  `Client_ID` int(11) DEFAULT NULL,
  `Priviledges` varchar(4) DEFAULT 'rw',
  `Is_Owner` int(11) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=957 ;

-- --------------------------------------------------------

--
-- Table structure for table `ADMIN_Import_Generic`
--

CREATE TABLE IF NOT EXISTS `ADMIN_Import_Generic` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Generic_Field_1` varchar(200) DEFAULT NULL,
  `Generic_Field_2` varchar(200) DEFAULT NULL,
  `Generic_Field_3` varchar(200) DEFAULT NULL,
  `Generic_Field_4` varchar(200) DEFAULT NULL,
  `Generic_Field_5` varchar(200) DEFAULT NULL,
  `Generic_Field_6` varchar(200) DEFAULT NULL,
  `Generic_Field_7` varchar(200) DEFAULT NULL,
  `Generic_Field_8` varchar(200) DEFAULT NULL,
  `Generic_Number_1` decimal(18,2) DEFAULT NULL,
  `Generic_Number_2` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=687 ;

-- --------------------------------------------------------

--
-- Table structure for table `ADMIN_Parameters`
--

CREATE TABLE IF NOT EXISTS `ADMIN_Parameters` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(100) DEFAULT NULL,
  `Value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `App_Log`
--

CREATE TABLE IF NOT EXISTS `App_Log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Proc_Name` varchar(100) DEFAULT NULL,
  `Message_Type` varchar(45) DEFAULT NULL,
  `Message` mediumtext,
  `Log_Date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `CALC_A_Scores`
--

CREATE TABLE IF NOT EXISTS `CALC_A_Scores` (
  `Questionnaire_ID` int(11) NOT NULL,
  `Scale_ID` int(11) NOT NULL,
  `A_Score` float DEFAULT NULL,
  `Phase` varchar(30) DEFAULT NULL,
  `B_Score_1` float DEFAULT NULL COMMENT 'Additional Score per Scale',
  `B_Score_2` float DEFAULT NULL,
  `B_Score_3` float DEFAULT NULL,
  PRIMARY KEY (`Questionnaire_ID`,`Scale_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `CALC_Mean_Scores`
--

CREATE TABLE IF NOT EXISTS `CALC_Mean_Scores` (
  `Questionnaire_ID` int(11) NOT NULL,
  `Mean_ID` int(11) NOT NULL,
  `Mean_Score` float DEFAULT NULL,
  PRIMARY KEY (`Mean_ID`,`Questionnaire_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `CALC_Questions_Points`
--

CREATE TABLE IF NOT EXISTS `CALC_Questions_Points` (
  `Questionnaire_ID` int(11) NOT NULL,
  `Question_ID` int(11) NOT NULL,
  `Answer` int(11) DEFAULT NULL,
  `Points` int(11) DEFAULT NULL,
  PRIMARY KEY (`Questionnaire_ID`,`Question_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `CALC_Raw_Scores`
--

CREATE TABLE IF NOT EXISTS `CALC_Raw_Scores` (
  `Questionnaire_ID` int(11) NOT NULL,
  `Scale_ID` int(11) NOT NULL,
  `Raw_Score` float DEFAULT NULL,
  `Phase` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Scale_ID`,`Questionnaire_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `CALC_Test_Answers`
--

CREATE TABLE IF NOT EXISTS `CALC_Test_Answers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Test_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Exercise_Option_ID` int(11) DEFAULT NULL,
  `Answered` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1027 ;

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE IF NOT EXISTS `Clients` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Surname` varchar(45) DEFAULT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Birthdate` varchar(10) DEFAULT NULL,
  `Gender_ID` int(11) DEFAULT '1',
  `Marital_Status_ID` int(11) DEFAULT '1',
  `Children` int(11) DEFAULT NULL,
  `Profession` varchar(60) DEFAULT NULL,
  `Comments` varchar(400) DEFAULT NULL,
  `Language_ID` int(11) DEFAULT '1',
  `Work_Position` varchar(100) DEFAULT NULL,
  `Work_Proposed` varchar(100) DEFAULT NULL,
  `Work_Years` int(11) DEFAULT NULL,
  `Studies` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=908 ;

-- --------------------------------------------------------

--
-- Table structure for table `Critical_Questions_Categories`
--

CREATE TABLE IF NOT EXISTS `Critical_Questions_Categories` (
  `ID` int(11) NOT NULL,
  `Group_ID` int(11) DEFAULT NULL,
  `Descr_el` varchar(100) DEFAULT NULL,
  `Descr_en` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Critical_Questions_Groups`
--

CREATE TABLE IF NOT EXISTS `Critical_Questions_Groups` (
  `ID` int(11) NOT NULL,
  `Descr_el` varchar(100) DEFAULT NULL,
  `Descr_en` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Genders`
--

CREATE TABLE IF NOT EXISTS `Genders` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `Languages`
--

CREATE TABLE IF NOT EXISTS `Languages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_en` varchar(45) DEFAULT NULL,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Exercises_Material`
--

CREATE TABLE IF NOT EXISTS `MAP_Exercises_Material` (
  `Material_ID` int(11) NOT NULL,
  `Exercise_ID` int(11) NOT NULL,
  `Test_Type_ID` int(11) NOT NULL,
  PRIMARY KEY (`Material_ID`,`Exercise_ID`,`Test_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Exercises_Test_Sections`
--

CREATE TABLE IF NOT EXISTS `MAP_Exercises_Test_Sections` (
  `Exercise_ID` int(11) NOT NULL,
  `Test_Section_ID` int(11) NOT NULL,
  `Test_Type_ID` int(11) NOT NULL,
  `Presentation_Order` int(11) DEFAULT NULL,
  PRIMARY KEY (`Exercise_ID`,`Test_Section_ID`,`Test_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Master_Scales`
--

CREATE TABLE IF NOT EXISTS `MAP_Master_Scales` (
  `Scale_ID_Master` int(11) NOT NULL DEFAULT '0',
  `Scale_ID_Child` int(11) NOT NULL DEFAULT '0',
  `Factor` float DEFAULT '1',
  `Process_Order` int(11) DEFAULT '1',
  `Questionnaire_Type_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Scale_ID_Master`,`Scale_ID_Child`,`Questionnaire_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Questions_Critical_Categories`
--

CREATE TABLE IF NOT EXISTS `MAP_Questions_Critical_Categories` (
  `Question_ID` int(11) NOT NULL,
  `Critical_Category_ID` int(11) NOT NULL,
  `Questionnaire_Type_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Question_ID`,`Critical_Category_ID`,`Questionnaire_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Questions_Points`
--

CREATE TABLE IF NOT EXISTS `MAP_Questions_Points` (
  `Question_ID` int(11) NOT NULL,
  `Answer` int(11) NOT NULL,
  `Points` int(11) DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Question_ID`,`Answer`,`Questionnaire_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Questions_Questionnaire_Types`
--

CREATE TABLE IF NOT EXISTS `MAP_Questions_Questionnaire_Types` (
  `Question_ID` int(11) NOT NULL,
  `Questionnaire_Type_ID` int(11) NOT NULL,
  PRIMARY KEY (`Question_ID`,`Questionnaire_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Questions_Scales`
--

CREATE TABLE IF NOT EXISTS `MAP_Questions_Scales` (
  `Scale_ID` int(11) NOT NULL,
  `Question_ID` int(11) NOT NULL,
  `Questionnaire_Type_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Scale_ID`,`Question_ID`,`Questionnaire_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Scales_Groups_Mean`
--

CREATE TABLE IF NOT EXISTS `MAP_Scales_Groups_Mean` (
  `Scale_ID` int(11) NOT NULL,
  `Group_Mean_ID` int(11) NOT NULL,
  `Questionnaire_Type_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Scale_ID`,`Group_Mean_ID`,`Questionnaire_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Scales_Groups_Rep`
--

CREATE TABLE IF NOT EXISTS `MAP_Scales_Groups_Rep` (
  `Scale_ID` int(11) DEFAULT NULL,
  `Group_Rep_ID` int(11) DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Scales_Questionnaire_Types`
--

CREATE TABLE IF NOT EXISTS `MAP_Scales_Questionnaire_Types` (
  `Scale_ID` int(11) NOT NULL,
  `Questionnaire_Type_ID` int(11) NOT NULL,
  PRIMARY KEY (`Questionnaire_Type_ID`,`Scale_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MAP_Test_Sections_Test_Types`
--

CREATE TABLE IF NOT EXISTS `MAP_Test_Sections_Test_Types` (
  `Test_Type_ID` int(11) NOT NULL,
  `Test_Section_ID` int(11) NOT NULL,
  `Presentation_Order` int(11) DEFAULT NULL,
  PRIMARY KEY (`Test_Type_ID`,`Test_Section_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Marital_Statuses`
--

CREATE TABLE IF NOT EXISTS `Marital_Statuses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `META_UI_Labels`
--

CREATE TABLE IF NOT EXISTS `META_UI_Labels` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(400) DEFAULT NULL,
  `Descr_el` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Answer_Groups`
--

CREATE TABLE IF NOT EXISTS `PARAM_Answer_Groups` (
  `ID` int(11) NOT NULL,
  `Answer` varchar(45) NOT NULL,
  `Order` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`,`Answer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_A_Score_Functions`
--

CREATE TABLE IF NOT EXISTS `PARAM_A_Score_Functions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Scale_ID` int(11) DEFAULT NULL,
  `Expression` varchar(100) DEFAULT NULL,
  `Scale_ID_1` int(11) DEFAULT NULL,
  `Scale_ID_2` int(11) DEFAULT NULL,
  `Scale_ID_3` int(11) DEFAULT NULL,
  `Scale_ID_4` int(11) DEFAULT NULL,
  `Scale_ID_5` int(11) DEFAULT NULL,
  `Scale_ID_6` int(11) DEFAULT NULL,
  `Factor_1` float DEFAULT NULL,
  `Factor_2` float DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=321 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_A_Score_Questions`
--

CREATE TABLE IF NOT EXISTS `PARAM_A_Score_Questions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Scale_ID` int(11) DEFAULT NULL,
  `Expression` varchar(100) DEFAULT NULL,
  `Question_ID_1` int(11) DEFAULT NULL,
  `Question_ID_2` int(11) DEFAULT NULL,
  `Question_ID_3` int(11) DEFAULT NULL,
  `Question_ID_4` int(11) DEFAULT NULL,
  `Factor_1` float DEFAULT NULL,
  `Factor_2` float DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Exercises`
--

CREATE TABLE IF NOT EXISTS `PARAM_Exercises` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(45) DEFAULT NULL,
  `Descr_el` varchar(300) DEFAULT NULL,
  `Descr_en` varchar(300) DEFAULT NULL,
  `Pass_Score` int(11) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=202 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Exercise_Options`
--

CREATE TABLE IF NOT EXISTS `PARAM_Exercise_Options` (
  `ID` int(11) NOT NULL,
  `Code` varchar(45) DEFAULT NULL,
  `Exercise_ID` int(11) DEFAULT NULL,
  `Descr_el` varchar(100) DEFAULT NULL,
  `Descr_en` varchar(100) DEFAULT NULL,
  `Score` int(11) DEFAULT NULL,
  `Presentation_Order` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_ICN`
--

CREATE TABLE IF NOT EXISTS `PARAM_ICN` (
  `Scale_ID` int(11) DEFAULT NULL,
  `Factor_1` float DEFAULT '0',
  `Operand_1` varchar(3) DEFAULT '-',
  `Question_1` int(11) DEFAULT '0',
  `Factor_2` float DEFAULT '0' COMMENT '	',
  `Operand_2` varchar(3) DEFAULT '-',
  `Question_2` int(11) DEFAULT '0',
  `Factor_3` float DEFAULT '0',
  `Operand_3` varchar(3) DEFAULT '-',
  `Question_3` int(11) DEFAULT '0',
  `Factor_4` float DEFAULT '0',
  `Operand_4` varchar(3) DEFAULT '-',
  `Question_4` int(11) DEFAULT '0',
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Questionnaire_Types`
--

CREATE TABLE IF NOT EXISTS `PARAM_Questionnaire_Types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(100) DEFAULT NULL,
  `Descr_en` varchar(100) DEFAULT NULL,
  `Questionnaire_File` varchar(100) DEFAULT NULL,
  `Result_File` varchar(100) DEFAULT NULL,
  `Result_Cmp_File` varchar(100) DEFAULT NULL,
  `Answer_Group_ID` int(11) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Questions`
--

CREATE TABLE IF NOT EXISTS `PARAM_Questions` (
  `ID` int(11) NOT NULL,
  `Code` varchar(4) NOT NULL,
  `Question_el` varchar(300) NOT NULL,
  `Question_en` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Rep_Columns`
--

CREATE TABLE IF NOT EXISTS `PARAM_Rep_Columns` (
  `ID` int(11) NOT NULL,
  `Rep_Name` varchar(45) DEFAULT NULL,
  `Column_el` varchar(45) DEFAULT NULL,
  `Column_en` varchar(45) DEFAULT NULL,
  `Column_Order` int(11) DEFAULT NULL,
  `Width` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Rep_Diagnostic_Criteria`
--

CREATE TABLE IF NOT EXISTS `PARAM_Rep_Diagnostic_Criteria` (
  `ID` int(11) NOT NULL,
  `Row_Group` int(11) DEFAULT NULL,
  `Row_Type` varchar(20) DEFAULT NULL,
  `Header_en` varchar(100) DEFAULT NULL,
  `Header_el` varchar(100) DEFAULT NULL,
  `Label_en` varchar(100) DEFAULT NULL COMMENT '	',
  `Label_el` varchar(100) DEFAULT NULL,
  `Mean_ID` int(11) DEFAULT NULL,
  `Scale_ID_1` int(11) DEFAULT NULL,
  `Operand_1` varchar(3) DEFAULT NULL,
  `Scale_ID_2` int(11) DEFAULT NULL,
  `Operand_2` varchar(3) DEFAULT NULL,
  `Scale_ID_3` int(11) DEFAULT NULL,
  `Operand_CMP` varchar(3) DEFAULT NULL,
  `Threshold` float DEFAULT NULL,
  `Result_True` int(11) DEFAULT NULL,
  `Result_False` int(11) DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Rep_Full`
--

CREATE TABLE IF NOT EXISTS `PARAM_Rep_Full` (
  `ID` int(11) NOT NULL,
  `Scale_Code_1` varchar(45) DEFAULT NULL,
  `Scale_Code_1_ID` int(11) DEFAULT '0',
  `Scale_Code_2` varchar(45) DEFAULT NULL,
  `Scale_Code_2_ID` int(11) DEFAULT '0',
  `Scale_Code_3` varchar(45) DEFAULT NULL,
  `Scale_Code_3_ID` int(11) DEFAULT '0',
  `Min_Value` int(11) DEFAULT NULL,
  `Max_Value` int(11) DEFAULT NULL,
  `Data` varchar(45) DEFAULT NULL,
  `Rule_Type` varchar(45) DEFAULT NULL,
  `Order_Section` int(11) DEFAULT NULL,
  `Order_Presentation` int(11) DEFAULT NULL,
  `Title_el` varchar(200) DEFAULT NULL,
  `Title_en` varchar(200) DEFAULT NULL,
  `Descr_el` text,
  `Descr_en` text,
  `Report_Name` varchar(45) DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  `Info_Row_ID` int(11) DEFAULT NULL,
  `Active_Rule` int(11) DEFAULT '1',
  `Expression` varchar(100) DEFAULT NULL,
  `Image_File` varchar(45) DEFAULT NULL,
  `Image_Width` int(11) DEFAULT NULL,
  `Image_Height` int(11) DEFAULT NULL,
  `Page_Break` varchar(2) DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Rep_Risk_Assessment`
--

CREATE TABLE IF NOT EXISTS `PARAM_Rep_Risk_Assessment` (
  `ID` int(11) NOT NULL,
  `Label_en` varchar(45) DEFAULT NULL,
  `Label_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Scale_ID_1` int(11) DEFAULT NULL,
  `Scale_ID_2` int(11) DEFAULT NULL,
  `Max_Value_Descr_en` varchar(45) DEFAULT NULL,
  `Max_Value_Descr_el` varchar(45) DEFAULT NULL,
  `Max_Value` float DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Scales`
--

CREATE TABLE IF NOT EXISTS `PARAM_Scales` (
  `Code` varchar(30) NOT NULL,
  `Descr_el` varchar(200) DEFAULT NULL,
  `Descr_en` varchar(200) DEFAULT NULL,
  `Factor_Multiplication` float DEFAULT '1',
  `Factor_Add` float DEFAULT NULL,
  `Type_ID` int(11) DEFAULT NULL,
  `Group_Rep_ID` int(11) DEFAULT NULL,
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_SUPL_IDX_Factors`
--

CREATE TABLE IF NOT EXISTS `PARAM_SUPL_IDX_Factors` (
  `Scale_ID_SUPL` int(11) NOT NULL,
  `Scale_ID` int(11) NOT NULL,
  `Factor` float DEFAULT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_SUPL_IDX_Thresholds`
--

CREATE TABLE IF NOT EXISTS `PARAM_SUPL_IDX_Thresholds` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Scale_ID_SUPL` int(11) DEFAULT NULL,
  `Function` varchar(20) DEFAULT NULL,
  `Scale_ID_1` int(11) DEFAULT NULL,
  `Operand_1` varchar(3) DEFAULT NULL,
  `Scale_ID_2` int(11) DEFAULT NULL,
  `Operand_2` varchar(3) DEFAULT NULL,
  `Scale_ID_3` int(11) DEFAULT NULL,
  `Operand_CMP` varchar(3) DEFAULT NULL,
  `Threshold` float DEFAULT NULL,
  `Result_True` float DEFAULT NULL,
  `Result_False` float DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=223 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Test_Full`
--

CREATE TABLE IF NOT EXISTS `PARAM_Test_Full` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Object_Type` varchar(45) DEFAULT NULL,
  `Object_ID` int(11) DEFAULT NULL,
  `Rule_Type` varchar(45) DEFAULT NULL,
  `Data` text,
  `Attribute_1` varchar(100) DEFAULT NULL,
  `Attribute_2` varchar(100) DEFAULT NULL,
  `Attribute_3` varchar(100) DEFAULT NULL,
  `Attribute_4` varchar(100) DEFAULT NULL,
  `Presentation_Section` int(11) DEFAULT NULL,
  `Presentation_Order` int(11) DEFAULT NULL,
  `Presentation_Duration` int(11) DEFAULT NULL,
  `Expiration_Action` varchar(100) DEFAULT NULL,
  `Image_File` varchar(45) DEFAULT NULL,
  `Image_Width` int(11) DEFAULT NULL,
  `Image_Height` int(11) DEFAULT NULL,
  `Page_Break` varchar(1) DEFAULT NULL,
  `Section_Break` varchar(1) DEFAULT NULL,
  `Test_Type_ID` int(11) DEFAULT NULL,
  `Html_en` text,
  `Html_el` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=309 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Test_Material`
--

CREATE TABLE IF NOT EXISTS `PARAM_Test_Material` (
  `ID` int(11) NOT NULL,
  `Descr_el` text,
  `Descr_en` text,
  `Type` varchar(45) DEFAULT NULL,
  `Media_File` varchar(100) DEFAULT NULL,
  `Media_File_Type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Test_Sections`
--

CREATE TABLE IF NOT EXISTS `PARAM_Test_Sections` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `PARAM_Test_Types`
--

CREATE TABLE IF NOT EXISTS `PARAM_Test_Types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  `Test_File` varchar(45) DEFAULT NULL,
  `Result_File` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `Questionnaires`
--

CREATE TABLE IF NOT EXISTS `Questionnaires` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) NOT NULL,
  `Last_Save` varchar(10) DEFAULT NULL,
  `Comments` varchar(300) DEFAULT NULL,
  `Hash_Code` varchar(100) DEFAULT NULL,
  `Questionnaire_Type_ID` int(11) DEFAULT NULL,
  `Online_Access` tinyint(4) DEFAULT '1',
  `Process_Times` int(11) DEFAULT '0',
  `Fill_Time` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IDX_Questionnaires_01` (`Hash_Code`),
  KEY `IDX_Questionnaires_02` (`Client_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2506 ;

-- --------------------------------------------------------

--
-- Table structure for table `Questionnaires_Details`
--

CREATE TABLE IF NOT EXISTS `Questionnaires_Details` (
  `Questionnaire_ID` int(11) NOT NULL,
  `Question_ID` int(11) NOT NULL,
  `Answer` int(11) NOT NULL,
  PRIMARY KEY (`Questionnaire_ID`,`Question_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Registrations`
--

CREATE TABLE IF NOT EXISTS `Registrations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `App_User_ID` int(11) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `Type_ID` int(11) DEFAULT NULL,
  `Pieces` int(11) DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  `Purchase_Date` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=115 ;

-- --------------------------------------------------------

--
-- Table structure for table `Scale_Groups_Mean`
--

CREATE TABLE IF NOT EXISTS `Scale_Groups_Mean` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(45) DEFAULT NULL,
  `Descr_el` varchar(100) DEFAULT NULL,
  `Descr_en` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `Scale_Groups_Rep`
--

CREATE TABLE IF NOT EXISTS `Scale_Groups_Rep` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `Scale_Types`
--

CREATE TABLE IF NOT EXISTS `Scale_Types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descr_el` varchar(45) DEFAULT NULL,
  `Descr_en` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `Tests`
--

CREATE TABLE IF NOT EXISTS `Tests` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) DEFAULT NULL,
  `Last_Save` varchar(10) DEFAULT NULL,
  `Comments` varchar(300) DEFAULT NULL,
  `Hash_Code` varchar(100) DEFAULT NULL,
  `Test_Type_ID` int(11) DEFAULT NULL,
  `Online_Access` tinyint(4) DEFAULT '1',
  `Process_Times` int(11) DEFAULT '0',
  `Fill_Time` int(11) DEFAULT '0',
  `Status` varchar(45) NOT NULL DEFAULT 'PENDING',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_PARAM_A_Score_Functions`
--
CREATE TABLE IF NOT EXISTS `v_PARAM_A_Score_Functions` (
`ID` int(11)
,`Scale_ID` int(11)
,`Expression` varchar(100)
,`Scale_ID_1` bigint(11)
,`Scale_ID_2` bigint(11)
,`Scale_ID_3` bigint(11)
,`Scale_ID_4` bigint(11)
,`Scale_ID_5` bigint(11)
,`Scale_ID_6` bigint(11)
,`Factor_1` float
,`Factor_2` float
,`Questionnaire_Type_ID` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_Questionnaires`
--
CREATE TABLE IF NOT EXISTS `v_Questionnaires` (
`ID` int(11)
,`Client_ID` int(11)
,`Last_Save` varchar(10)
,`Process_Times` int(11)
,`Fill_Time` int(11)
,`Comments` varchar(300)
,`Hash_Code` varchar(100)
,`Online_Access` tinyint(4)
,`Questionnaire_Type_ID` int(11)
,`Questionnaire_File` varbinary(215)
,`Result_File` varchar(100)
,`Result_Cmp_File` varchar(100)
,`Questionnaire_Type` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_Tests`
--
CREATE TABLE IF NOT EXISTS `v_Tests` (
`ID` int(11)
,`Client_ID` int(11)
,`Last_Save` varchar(10)
,`Fill_Time` int(11)
,`Comments` varchar(300)
,`Hash_Code` varchar(100)
,`Online_Access` tinyint(4)
,`Test_Type_ID` int(11)
,`Test_File` varbinary(160)
,`Result_File` varchar(45)
,`Status` varchar(45)
);
-- --------------------------------------------------------

--
-- Structure for view `v_PARAM_A_Score_Functions`
--
DROP TABLE IF EXISTS `v_PARAM_A_Score_Functions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`copserfe`@`localhost` SQL SECURITY DEFINER VIEW `v_PARAM_A_Score_Functions` AS select `PARAM_A_Score_Functions`.`ID` AS `ID`,`PARAM_A_Score_Functions`.`Scale_ID` AS `Scale_ID`,`PARAM_A_Score_Functions`.`Expression` AS `Expression`,ifnull(`PARAM_A_Score_Functions`.`Scale_ID_1`,-(1)) AS `Scale_ID_1`,ifnull(`PARAM_A_Score_Functions`.`Scale_ID_2`,-(1)) AS `Scale_ID_2`,ifnull(`PARAM_A_Score_Functions`.`Scale_ID_3`,-(1)) AS `Scale_ID_3`,ifnull(`PARAM_A_Score_Functions`.`Scale_ID_4`,-(1)) AS `Scale_ID_4`,ifnull(`PARAM_A_Score_Functions`.`Scale_ID_5`,-(1)) AS `Scale_ID_5`,ifnull(`PARAM_A_Score_Functions`.`Scale_ID_6`,-(1)) AS `Scale_ID_6`,`PARAM_A_Score_Functions`.`Factor_1` AS `Factor_1`,`PARAM_A_Score_Functions`.`Factor_2` AS `Factor_2`,`PARAM_A_Score_Functions`.`Questionnaire_Type_ID` AS `Questionnaire_Type_ID` from `PARAM_A_Score_Functions`;

-- --------------------------------------------------------

--
-- Structure for view `v_Questionnaires`
--
DROP TABLE IF EXISTS `v_Questionnaires`;

CREATE ALGORITHM=UNDEFINED DEFINER=`copserfe`@`localhost` SQL SECURITY DEFINER VIEW `v_Questionnaires` AS select `q`.`ID` AS `ID`,`q`.`Client_ID` AS `Client_ID`,`q`.`Last_Save` AS `Last_Save`,`q`.`Process_Times` AS `Process_Times`,`q`.`Fill_Time` AS `Fill_Time`,`q`.`Comments` AS `Comments`,`q`.`Hash_Code` AS `Hash_Code`,`q`.`Online_Access` AS `Online_Access`,`q`.`Questionnaire_Type_ID` AS `Questionnaire_Type_ID`,concat(`t`.`Questionnaire_File`,'?q=',`q`.`Hash_Code`,'_',`q`.`Client_ID`) AS `Questionnaire_File`,`t`.`Result_File` AS `Result_File`,`t`.`Result_Cmp_File` AS `Result_Cmp_File`,`t`.`Descr_en` AS `Questionnaire_Type` from (`Questionnaires` `q` join `PARAM_Questionnaire_Types` `t` on((`q`.`Questionnaire_Type_ID` = `t`.`ID`)));

-- --------------------------------------------------------

--
-- Structure for view `v_Tests`
--
DROP TABLE IF EXISTS `v_Tests`;

CREATE ALGORITHM=UNDEFINED DEFINER=`copserfe`@`localhost` SQL SECURITY DEFINER VIEW `v_Tests` AS select `q`.`ID` AS `ID`,`q`.`Client_ID` AS `Client_ID`,`q`.`Last_Save` AS `Last_Save`,`q`.`Fill_Time` AS `Fill_Time`,`q`.`Comments` AS `Comments`,`q`.`Hash_Code` AS `Hash_Code`,`q`.`Online_Access` AS `Online_Access`,`q`.`Test_Type_ID` AS `Test_Type_ID`,concat(`t`.`Test_File`,'?q=',`q`.`Hash_Code`,'_',`q`.`Client_ID`) AS `Test_File`,`t`.`Result_File` AS `Result_File`,`q`.`Status` AS `Status` from (`Tests` `q` join `PARAM_Test_Types` `t` on((`q`.`Test_Type_ID` = `t`.`ID`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
